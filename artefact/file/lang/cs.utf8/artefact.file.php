<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['3gp'] = 'Médium 3GPP';
$string['Contents'] = 'Obsah';
$string['Continue'] = 'Pokračovat';
$string['Created'] = 'Vytvořeno';
$string['Date'] = 'Datum';
$string['Decompress'] = 'Rozbalit';
$string['Default'] = 'Výchozí';
$string['Description'] = 'Popis';
$string['Descriptionandtags'] = 'Popis / Značky';
$string['Details'] = 'Detaily';
$string['Download'] = 'Stáhnout';
$string['File'] = 'Soubor';
$string['Files'] = 'Soubory';
$string['Folder'] = 'Složka';
$string['Folders'] = 'Složky';
$string['Image'] = 'Obrázek';
$string['Images'] = 'Obrázky';
$string['License'] = 'Licence';
$string['Name'] = 'Název';
$string['Owner'] = 'Vlastník';
$string['Preview'] = 'Náhled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Název';
$string['Type'] = 'Typ';
$string['Unzip'] = 'Rozbalit';
$string['View'] = 'Náhled';
$string['addafile'] = 'Přidat soubor';
$string['adm_group_notificationmessage'] = 'Skupina %s dosáhla %s%% ze své kvóty %s.';
$string['adm_group_notificationsubject'] = 'Skupina, kterou spravujete, téměř dosáhla limitu kvóty souborů';
$string['adm_notificationmessage'] = 'Uživatel %s dosáhl %s%% své kvóty %s.';
$string['adm_notificationsubject'] = 'Uživatel téměř dosáhl limitu své kvóty souborů';
$string['ai'] = 'Dokument Postscript';
$string['aiff'] = 'Audio soubor AIFF';
$string['anytypeoffile'] = 'Soubor (jakýkoliv typ)';
$string['application'] = 'Neznámá aplikace';
$string['archive'] = 'Archiv';
$string['au'] = 'Audio soubor AU';
$string['audio'] = 'Soubor s audiem';
$string['avi'] = 'Video soubor AVI';
$string['bmp'] = 'Rastrová grafika';
$string['bytes'] = 'bajtů';
$string['bz2'] = 'Archiv Bzip2';
$string['cannotcopytemparchive'] = 'Nelze zkopírovat archiv %s to %s.';
$string['cannotdeletetemparchive'] = 'Nelze smazat dočasný archiv %s.';
$string['cannoteditfolder'] = 'Nemáte oprávnění přidat obsah do této složky';
$string['cannoteditfoldersubmitted'] = 'Nemůžete přidat obsah do složky s odeslanou stránkou.';
$string['cannotextractarchive'] = 'Nelze extrahovat archiv do %s.';
$string['cannotextractfileinfoldersubmitted'] = 'Soubor nelze rozbalit do složky ve stránce';
$string['cannotextractfilesubmitted'] = 'Soubor nelze rozbalit do stránky';
$string['cannotopenarchive'] = 'Nelze otevřít archiv %s.';
$string['cannotreadarchivecontent'] = 'Nelze přečíst obsah archivu.';
$string['cannotremovefromsubmittedfolder'] = 'Nemůžete obdebrat obsah ze složky s odeslanou stránkou.';
$string['cannotuploadtofolder'] = 'Nemáte povolení nahrávat obsah do této složky.';
$string['cannotviewfolder'] = 'Nemáte povolení zobrazit obsah této složky';
$string['cantcreatetempprofileiconfile'] = 'Nelze uložit dočasný profilový obrázek v %s';
$string['changessaved'] = 'Změny uloženy';
$string['clickanddragtomovefile'] = 'Klepnutím a tažením přesunout %s';
$string['commentsanddetails'] = 'Komentáře (%d) a podrobnosti';
$string['confirmdeletefile'] = 'Opravdu chcete odstranit tento soubor?';
$string['confirmdeletefolder'] = 'Opravdu chcete odstranit tuto složku?';
$string['confirmdeletefolderandcontents'] = 'Opravdu chcete odstranit tuto složku s celým jejím obsahem?';
$string['contents'] = 'Obsah';
$string['copyrightnotice'] = 'Prohlášení';
$string['create'] = 'Vytvořit';
$string['createdtwothings'] = 'Vytvořit %s a %s.';
$string['createfolder'] = 'Vytvořit složku';
$string['createfoldersuccess'] = 'Složka úspěšně vytvořena';
$string['customagreement'] = 'Vlasní dohoda';
$string['decompressspecific'] = 'Rozbalit "%s"';
$string['defaultagreement'] = 'Výchozí dohoda';
$string['defaultgroupquota'] = 'Výchozí kvóta skupiny';
$string['defaultgroupquotadescription'] = 'Můžete nastavit množství diskového prostoru, které mohou nové skupiny využít pro své soubory.';
$string['defaultprofileicon'] = 'Toto je v současnosti nastaveno jako výchozí profilový obrázek.';
$string['defaultquota'] = 'Výchozí kvóta';
$string['defaultquotadescription'] = 'Výchozí hodnota velikosti prostoru na disku, který budou mít noví uživatelé k dispozici. Již nastavené kvóty nebudou změněny.';
$string['defaultuserquota'] = 'Výchozí uživatelská kvóta';
$string['deletefile?'] = 'Opravdu chcete odstranit tento soubor?';
$string['deletefolder?'] = 'Opravdu chcete odstranit tuto složku?';
$string['deletefolderspecific'] = 'Odstranit složku "%s"';
$string['deleteselectedicons'] = 'Odstranit vybrané';
$string['deletingfailed'] = 'Odstranění se nezdařilo: soubor nebo složka již dále neexistuje';
$string['destination'] = 'Cíl';
$string['doc'] = 'Dokument MS Word';
$string['downloadfile'] = 'Stáhnout %s';
$string['downloadfilesize'] = 'Stáhnout %s [%s]';
$string['downloadfolderzip'] = 'Stáhnout složky jako soubor zip';
$string['downloadfolderzipblock'] = 'Zobrazit odkaz pro stažení';
$string['downloadfolderzipdescription1'] = 'Pokud je zaškrtnuto, uživatelé mohou stáhnout složku zobrazenou pomocí bloku "Složka" jako zip soubor.';
$string['downloadfolderzipdescription2'] = 'Pokud je povoleno, uživatelé mohou stahovat složky zobrazené pomocí bloku "Složka" jako zip archiv.';
$string['downloadfolderzipdescription3'] = 'Povolit uživatelům stáhnout složku zobrazenou prostřednictvím bloku "Složka" jako zip archiv.';
$string['downloadfolderzipdescriptionblock'] = 'Pokud je zaškrtnuto, uživatelé mohou stáhnout složku jako zip soubor.';
$string['downloadfolderziplink'] = 'Stáhnout obsah složky jako soubor zip';
$string['downloadoriginalversion'] = 'Stáhnout původní verzi';
$string['dragdrophere'] = 'Sem přesuňte soubory, které chcete nahrát';
$string['dss'] = 'Audio soubor DSS';
$string['editfile'] = 'Upravit soubor';
$string['editfilespecific'] = 'Upravit soubor "%s"';
$string['editfolder'] = 'Upravit složku';
$string['editfolderspecific'] = 'Upravit složku "%s"';
$string['editingfailed'] = 'Úprava se nezdařila: soubor nebo složka již neexistuje';
$string['editprofileicon'] = 'Upravit profilový obrázek';
$string['emptyfolder'] = 'Prázdná složka';
$string['extractfilessuccess'] = 'Vytvořeno %s složek a %s souborů.';
$string['file'] = 'Soubor';
$string['fileadded'] = 'Soubor vybrán';
$string['filealreadyindestination'] = 'Přesouvaný soubor v této složce již existuje';
$string['fileappearsinposts'] = 'Tento soubor se vyskytuje minimálně v jednom vašem příspěvku na fóru';
$string['fileappearsinskins'] = 'Tento soubor je použitý jako obrázek pozadí pro jeden nebo více vzhledů stránek.';
$string['fileappearsinviews'] = 'Tento soubor se zobrazí na jedné nebo více z vašich stránek.';
$string['fileattached'] = 'Tento soubor je připojen k tomuto počtu dalších položek ve vašem portfoliu: %s';
$string['fileattachedtoportfolioitems'] = array(

		0 => 'Tento soubor je svázán s %s dalšími prvkem vašeho portfolia.', 
		1 => 'Tento soubor je svázán s %s dalšími prvky vašeho portfolia.', 
		2 => 'Tento soubor je svázán s %s dalšími prvky vašeho portfolia.'
);
$string['fileexists'] = 'Soubor existuje';
$string['fileexistsoverwritecancel'] = 'Soubor s tímto názvem již existuje. Můžete buď zvolit jiný název nebo přepsat stávající soubor.';
$string['fileinstructions'] = 'Sem můžete nahrávat obrázky, dokumenty a další soubory, které budete chtít zveřejňovat na stránkách vašeho portfolia. K přesunutí souboru nebo složky na ně klikněte myší a přetáhněte na požadované místo.';
$string['fileisfolder'] = '"{{filename}}" je složka. Pro nahrání složky prosím použijte ZIP archiv, nahrajte jej, a následně použijte volbu pro rozbalení níže.';
$string['filelistloaded'] = 'Nahrán seznam souborů';
$string['filemoved'] = 'Soubor úspěšně přesunut';
$string['filenamefieldisrequired'] = 'Pole se souborem je povinné';
$string['filenamefieldisrequired1'] = 'Je vyžadováno jméno souboru / složky';
$string['filepermission.edit'] = 'Upravit';
$string['filepermission.republish'] = 'Zveřejnit';
$string['filepermission.view'] = 'Pohled';
$string['fileremoved'] = 'Soubor odstraněn';
$string['files'] = 'soubory';
$string['filesextractedfromarchive'] = 'Extrakce souborů z archivu';
$string['filesextractedfromziparchive'] = 'Extrakce souborů z archivu ZIP';
$string['filespagedescription1'] = 'Zde jsou vaše obrázky, dokumenty a ostatní soubory, které můžete vložit do stránek. Přetáhněte ikonu souboru nebo složky pro jejich přesunutí mezi složkami.';
$string['fileswillbeextractedintofolder'] = 'Soubory byly extrahovány do %s';
$string['fileswillbeextractedintofolder1'] = 'Soubory budou extrahovány do složky "%s"';
$string['filethingdeleted'] = '%s odstraněn';
$string['filetypenotallowed'] = 'Soubory s příponou "%s" nejsou povoleny.';
$string['filetypenotmatchingmimetype'] = 'Přípona souboru se neshoduje s typem souboru "%s".';
$string['fileuploadedas'] = '%s nahrán jako "%s"';
$string['fileuploadedtofolderas'] = '%s nahrán do %s jako "%s"';
$string['fileuploadinstructions1'] = 'Můžete označit více souborů a nahrát je najednou.';
$string['filewithnameexists'] = 'Soubor nebo složka jména "%s" již existuje.';
$string['flv'] = 'Video FLV flash';
$string['folder'] = 'Složka';
$string['folderappearsinviews'] = 'Tato složka se zobrazí na jedné nebo více z vašich stránek.';
$string['foldercontainsprofileicons'] = array(

		0 => 'Složka obsahuje %s profilový obrázek.', 
		1 => 'Složka obsahuje %s profilové obrázky.', 
		2 => 'Složka obsahuje %s profilových obrázků.'
);
$string['foldercontents'] = 'Obsah složky';
$string['foldercreated'] = 'Složka vytvořena';
$string['folderdownloadnofolderfound'] = 'Složka s ID %d nelze najít';
$string['foldername'] = 'Název složky';
$string['foldernamerequired'] = 'Prosím zadejte jméno nové složky.';
$string['foldernotempty'] = 'Tato složka není prázdná.';
$string['gif'] = 'Obrázek GIF';
$string['gotofolder'] = 'Přejít do %s';
$string['groupfiles'] = 'Seskupit soubory';
$string['groupfilespagedescription1'] = 'Zde jsou skupiny obrázků, dokumentů a dalších souborů, které můžete přidat do svých stránek.';
$string['gz'] = 'Archiv Gzip';
$string['home'] = 'Domů';
$string['html'] = 'HTML soubor';
$string['htmlremovedmessage'] = 'Prohlížíte si <strong>%s</strong> od uživatele <a href="%s">%s</a>. Obsah níže zobrazeného souboru byl profiltrován, aby neobsahoval případný škodlivý obsah, a nemusí zcela odpovídat původnímu originálu.';
$string['htmlremovedmessagenoowner'] = 'Prohlížíte si <strong>%s</strong>. Obsah níže zobrazeného souboru byl profiltrován, aby neobsahoval případný škodlivý obsah, a nemusí zcela odpovídat původnímu originálu.';
$string['image'] = 'Obrázek';
$string['imagesdir'] = 'Obrázky';
$string['imagesdirdesc'] = 'Soubory s obrázky';
$string['imagetitle'] = 'Popisek obrázku';
$string['institutionfilespagedescription1'] = 'Zde jsou obrázky, dokumenty a další soubory instituce, které můžete přidat do stránek.';
$string['institutionoverride'] = 'Kvóta instituce';
$string['institutionoverride1'] = 'Kvóta instituce';
$string['institutionoverridedescription'] = 'Můžete povolit správcům institucí, aby mohli nastavit kvóty pro soubory uživatelů a mít výchozí kvóty pro každou z institucí.';
$string['institutionoverridedescription1'] = 'Pokud je zapnuto, umožníte správcům institucí nastavit kvóty jednotlivým uživatelům a pro instituce ponechat výchozí kvóty.';
$string['institutionoverridedescription2'] = 'Povolit správcům institucí nastavovat uživatelské kvóty a mít výchozí kvótu pro každou instituci,';
$string['insufficientmemoryforresize'] = '(Nedostatek paměti pro změnu velikosti obrázku. Velikost obrázku prosím upravte před jeho nahráním)';
$string['insufficientquotaforunzip'] = 'Vaše zbývající disková kvóta je příliš malá k rozbalení tohoto souboru.';
$string['invalidarchive'] = 'Chyba při čtení archívu.';
$string['invalidarchive1'] = 'Neplatný archiv.';
$string['invalidarchivehandle'] = 'Neplatný popisovač archivu.';
$string['jpeg'] = 'Obrázek JPEG';
$string['jpg'] = 'Obrázek JPEG';
$string['js'] = 'Soubor Javascript';
$string['keepzipfor'] = 'Doba, po kterou mají být ponechány zip soubory na serveru';
$string['keepzipfordescription'] = 'Zip soubory vytvořené během stahování složek mají být ponechány na serveru po tuto dobu (v sekundách).';
$string['lastmodified'] = 'Naposledy upraveno';
$string['latex'] = 'Dokument LaTeX';
$string['logoxsnotsquare'] = 'Obrázek, který jste nahráli jako logo pro mobil, nemá správné rozměry. Prosím, nahrajte čtvercové logo.';
$string['m3u'] = 'Audio M3U';
$string['m4a'] = 'Audio M4A';
$string['markfordeletion'] = 'Označit k vymazání';
$string['markfordeletionspecific'] = 'Označí "%s" pro smazání';
$string['maxfileuploads'] = 'maximum souborů k nahrání: %s';
$string['maxquota'] = 'Maximální disková kvóta';
$string['maxquotadescription'] = 'Můžete nastavit maximální diskovou kvótu, kterou správce může přidělit uživateli. Stávající diskové kvóty tím nebudou nijak ovlivněny.';
$string['maxquotaenabled'] = 'Vynutit maximální diskovou kvótu na webu';
$string['maxquotaexceeded'] = 'Zadali jste diskovou kvótu vyšší, než je momentální nastavení pro tyto stránky (% s). Zkuste zadat nižší hodnotu, nebo kontaktujte správce webu, aby maximální diskovou kvótu zvýšil.';
$string['maxquotaexceededform'] = 'Prosím nastavte kvótu pro velikost nahrávaných souborů menší než %s.';
$string['maxquotatoolow'] = 'Maximální disková kvóta nemůže být menší než výchozí kvóta.';
$string['maxuploadsize'] = 'Maximální velikost nahrávaných souborů';
$string['maxuploadsizeis'] = 'Maximální velikost souboru: %s';
$string['mov'] = 'MOV Quicktime video.';
$string['movefailed'] = 'Přesun selhal';
$string['movefaileddestinationinartefact'] = 'Nelze přesunout složku do sebe sama';
$string['movefaileddestinationnotfolder'] = 'Cíl přesunu musí být složka';
$string['movefailednotfileartefact'] = 'Přesouvat lze pouze soubory, složky a obrázky';
$string['movefailednotowner'] = 'Nemáte oprávnění přesouvat tento soubor do zadané složky';
$string['moveto'] = 'Přesunout do %s';
$string['movingfailed'] = 'Přesun selhal: soubor nebo složky již dále neexistuje';
$string['mp3'] = 'Audio MP3';
$string['mp4'] = 'Médium MP4';
$string['mp4_audio'] = 'MP4 audio';
$string['mp4_video'] = 'MP4 video';
$string['mpeg'] = 'Video MPEG';
$string['mpg'] = 'Video MPG';
$string['myfiles'] = 'Moje soubory';
$string['namefieldisrequired'] = 'Musíte vyplnit pole s názvem';
$string['nametoolong'] = 'Název je příliš dlouhý. Vyberte nějaký kratší.';
$string['nfiles'] = array(

		0 => '%s soubor', 
		1 => '%s soubory', 
		2 => '%s souborů'
);
$string['nfolders'] = array(

		0 => '%s složka', 
		1 => '%s složky', 
		2 => '%s složek'
);
$string['nofilesfound'] = 'Nenalezeny žádné soubory';
$string['nofolderformove'] = 'Nenalezena složka k přesunutí';
$string['noimagesfound'] = 'Nenalezeny žádné obrázky';
$string['notpublishable'] = 'Nemáte oprávnění ke zveřejnění tohoto souboru';
$string['notrendertopdf'] = 'Tento blok není možné správně vyrenderovat do exportu PDF.';
$string['notrendertopdffiles'] = array(

		0 => 'Soubor byl přidán do:', 
		1 => 'Soubory byly přidány do:', 
		2 => 'Soubory byly přidány do:'
);
$string['notrendertopdflink'] = 'K souboru se dá dostat přes:';
$string['notrendertopdfview'] = 'Portfolio %s bylo přidáno jako:';
$string['nprofilepictures'] = array(

		0 => 'Profilový obrázek', 
		1 => 'Profilové obrázky', 
		2 => 'Profilové obrázky'
);
$string['odb'] = 'Databáze OpenOffice';
$string['odc'] = 'Tabulka OpenOffice';
$string['odf'] = 'Vzorec OpenOffice';
$string['odg'] = 'Grafika OpenOffice';
$string['odi'] = 'Obrázek OpenOffice';
$string['odm'] = 'Hlavní dokument OpenOffice';
$string['odp'] = 'Prezentace OpenOffice';
$string['ods'] = 'Sešit OpenOffice';
$string['odt'] = 'Textový dokument OpenOffice';
$string['oga'] = 'Audio soubor OGA';
$string['ogg'] = 'Audio soubor OGG Vorbis';
$string['ogv'] = 'Video soubor OGV';
$string['onlyfiveprofileicons'] = 'Můžete nahrát maximálně pět profilových obrázků.';
$string['or'] = 'nebo';
$string['oth'] = 'Webový dokument OpenOffice';
$string['ott'] = 'Šablona OpenOffice';
$string['overwrite'] = 'Přepsat';
$string['parentfolder'] = 'Nadřazená složka';
$string['pdf'] = 'Dokument PDF';
$string['phpzipneeded'] = 'Pro použití této funkcionality je potřebné PHP rozšíření zip';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Počkejte prosím, než se vaše soubory rozbalí.';
$string['pluginname'] = 'Soubory';
$string['png'] = 'Obrázek PNG';
$string['ppt'] = 'Prezentace Powerpoint';
$string['profileicon'] = 'Profilový obrázek';
$string['profileiconaddedtoimagesfolder'] = 'Váš profilový obrázek byl nahrán do složky %s.';
$string['profileiconappearsinposts'] = 'Tento profilový obrázek se vyskytuje minimálně v jednom z vašich příspěvků na fóru';
$string['profileiconappearsinskins'] = 'Tento profilový obrázek je použitý jako obrázek pozadí pro jeden nebo více vzhledů stránek.';
$string['profileiconappearsinviews'] = 'Tento profilový obrázek se zobrazuje na jedné nebo více vašich stránkách';
$string['profileiconattachedtoportfolioitems'] = 'Tento profilový obrázek je svázán s dalšími prvky vašeho portfolia.';
$string['profileiconimagetoobig'] = 'Vámi nahraný obrázek je příliš velký (%sx%s pixelů). Nesmí být větší než %sx%s pixelů';
$string['profileicons'] = 'Profilové obrázky';
$string['profileiconsdefaultsetsuccessfully'] = 'Výchozí profilový obrázek úspěšně nastaven';
$string['profileiconsdeletedsuccessfully'] = 'Obrázek zdárně odstraněn';
$string['profileiconsetdefaultnotvalid'] = 'Nebylo možno nastavit výchozí profilový obrázek, volba nebyla platná';
$string['profileiconsiconsizenotice'] = 'Sem můžete nahrát až <strong>pět</strong> profilových obrázků s vaší fotografií, avatarem či ikonou. Jeden z nich pak kdykoliv můžete vybrat jako výchozí, který bude zobrazován. Obrázky musí mít velikost mezi 16x16 až %sx%s pixelů.';
$string['profileiconsize'] = 'Velikost profilového obrázku';
$string['profileiconsnoneselected'] = 'Nebyly vybrány žádné obrázky k odstranění';
$string['profileiconuploadexceedsquota'] = 'Nahráním těchto profilových obrázků by byla překročena vám přidělená kvóta diskového prostoru. Zkuste zmenšit rozměry obrázku nebo odstranit nějaké nepotřebné soubory.';
$string['profileiconxsnotsquare'] = 'Obrázek, který jste nahráli jako malé logo, nemá správné rozměry. Prosím, nahrajte čtvercové logo.';
$string['progress_archive'] = array(

		0 => 'Přidat jeden archiv', 
		1 => 'Přidat %s archivy', 
		2 => 'Přidat %s archivů'
);
$string['progress_audio'] = array(

		0 => 'Přidat jeden zvukový soubor', 
		1 => 'Přidat %s zvukové soubory', 
		2 => 'Přidat %s zvukových souborů'
);
$string['progress_file'] = array(

		0 => 'Přidat jeden soubor', 
		1 => 'Přidat %s soubory', 
		2 => 'Přidat %s souborů'
);
$string['progress_folder'] = array(

		0 => 'Přidat jednu složku', 
		1 => 'Přidat %s složky', 
		2 => 'Přidat %s složek'
);
$string['progress_image'] = array(

		0 => 'Přidat jeden obrázek', 
		1 => 'Přidat %s obrázky', 
		2 => 'Přidat %s obrázků'
);
$string['progress_profileicon'] = array(

		0 => 'Přidat jeden profilový obrázek', 
		1 => 'Přidat %s profilové obrázky', 
		2 => 'Přidat %s profilových obrázků'
);
$string['progress_video'] = array(

		0 => 'Přidat jedno video', 
		1 => 'Přidat %s videa', 
		2 => 'Přidat %s videí'
);
$string['quicktime'] = 'Quicktime video';
$string['quotanotifyadmin1'] = 'Nastavit upozornění správce';
$string['quotanotifyadmindescr2'] = 'Pokud je zapnuto, správci stránek dostanou upozornění, jakmile uživatelé dosáhnou nastavené hranice.';
$string['quotanotifyadmindescr3'] = 'Upozornit správce instituce pokud uživatelé dosáhnou hranice kvóty.';
$string['quotanotifylimitdescr1'] = 'Pokud je uživatelova kvóta nahraných souborů na tomto procentu, bude mu (a správcům) zasláno upozornění. Může tak smazat některé soubory a uvolnit místo nebo kontaktovat správce pro navýšení kvóty.';
$string['quotanotifylimitoutofbounds'] = 'Limit upozornění je udán v procentech a musí to být číslo mezi 0 a 100.';
$string['quotanotifylimittitle1'] = 'Hranice upozornění dosáhnutí kvóty';
$string['ra'] = 'Audio soubor Real';
$string['ram'] = 'Video RAM RealPlayer';
$string['removingfailed'] = 'Odstranění selhalo: soubor či složka již neexistuje';
$string['requireagreement'] = 'Potvrzení vyžadováno';
$string['resizeonupload'] = 'Zmenšit velikost obrázku při nahrávání';
$string['resizeonuploaddescription'] = 'Automaticky zmenšit velké obrázky při nahrávání';
$string['resizeonuploadenable1'] = 'Automaticky zmenšit velké obrázky';
$string['resizeonuploadenabledescription1'] = 'Pokud zaškrtnete, při nahrávání budou obrázky zmenšeny, pokud přesáhnou maximální výšku a šířku';
$string['resizeonuploadenabledescription2'] = 'Pokud je zapnuto, obrázky přesahující maximální nastavené rozměry budou při nahrání zmenšeny.';
$string['resizeonuploadenabledescription3'] = 'Zmenšit velké obrázky při nahrávání, pokud překročí nastavenou výšku a šířku.';
$string['resizeonuploadenablefilebrowser1'] = 'Automatické zmenšení obrázků větších než %sx%s (doporučeno)';
$string['resizeonuploadmaxheight'] = 'Maximální výška';
$string['resizeonuploadmaxwidth'] = 'Maximální šířka';
$string['resizeonuploaduseroption1'] = 'Uživatelské nastavení';
$string['resizeonuploaduseroptiondescription1'] = 'Zobrazit uživatelům možnost zapnout nebo vypnout při nahrávání automatické zmenšování velkých obrázků';
$string['resizeonuploaduseroptiondescription2'] = 'Pokud je zapnuto, uživatelům bude zobrazena volna pro automatické zmenšení obrázků.';
$string['resizeonuploaduseroptiondescription3'] = 'Zobrazit uživatelům nastavení pro zapnutí nebo vypnutí automatického zmenšování velkých obrázků při nahrávání.';
$string['rm'] = 'Video soubor RM RealPlayer';
$string['rotate0img'] = 'Otočit obrázek do původní pozice';
$string['rotate180img'] = 'Otočit obrázek o 180 stupňů ve směru hodinových ručiček od původní pozice';
$string['rotate270img'] = 'Otočit obrázek o 270 stupňů ve směru hodinových ručiček od původní pozice';
$string['rotate90img'] = 'Otočit obrázek o 90 stupňů ve směru hodinových ručiček od původní pozice';
$string['rpm'] = 'Video soubor RPM RealPlayer';
$string['rtf'] = 'Dokument RTF';
$string['savechanges'] = 'Uložit změny';
$string['selectafile'] = 'Vyberte soubor';
$string['selectingfailed'] = 'Výběr selhal: soubor či složka již dále neexistuje';
$string['selectspecific'] = 'Vybrat "%s"';
$string['setdefault'] = 'Nastavit výchozí';
$string['setdefaultfor'] = 'Nastavit výchozí pro "%s"';
$string['sgi_movie'] = 'Video SGI';
$string['sh'] = 'Shellový skript';
$string['sitefilesloaded'] = 'Soubory stránek nahrány';
$string['spacerequired'] = 'Požadovaný prostor';
$string['spaceused'] = 'Využitý prostor';
$string['standardavatarnote'] = 'Standardní nebo externí profilový obrázek';
$string['standardavatartitle'] = 'Standardní nebo externí avatar';
$string['swf'] = 'Video SWF flash';
$string['tar'] = 'Archiv TAR';
$string['textlinktouser'] = 'Upravit profil %s';
$string['timeouterror'] = 'Nahrávání souboru selhalo - pokuste se nahrát soubor znovu';
$string['title'] = 'Název';
$string['titlefieldisrequired'] = 'Pole s názvem je povinné';
$string['txt'] = 'Čistý text';
$string['typefile'] = 'Soubor';
$string['typefolder'] = 'Složka';
$string['unzipprogress'] = '%s souborů/složek vytvořeno.';
$string['updategroupquotas'] = 'Aktualizovat kvótu skupiny';
$string['updategroupquotasdesc'] = 'Je-li zaškrtnuto, bude výše zvolená výchozí kvóta použita pro všechny existující skupiny.';
$string['updategroupquotasdesc1'] = 'Pokud je zapnuto, výchozí kvóta nastavená výše bude aplikování na všechny existující skupiny.';
$string['updategroupquotasdesc2'] = 'Použít výchozí kvótu dle výběru výše pro všechny existující skupiny.';
$string['updateuserquotas'] = 'Aktualizovat kvótu uživatele';
$string['updateuserquotasdesc'] = 'Je-li zaškrtnuto, bude výše zvolená výchozí kvóta použita pro všechny existující uživatele.';
$string['updateuserquotasdesc1'] = 'Pokud je zapnuto, výchozí kvóta nastavená výše bude aplikována na všechny existující uživatele.';
$string['updateuserquotasdesc2'] = 'Použít výchozí kvótu dle vašeho výběru výše pro všechny existující uživatele.';
$string['upload'] = 'Nahrát';
$string['uploadagreement'] = 'Souhlas při nahrávání souborů';
$string['uploadagreementdescription'] = 'Povolte tuto volbu, pokud chcete přinutit uživatele, aby souhlasili s níže uvedenou dohodou, než budou moci nahrát soubor na web.';
$string['uploadedby'] = 'Náhrál';
$string['uploadedprofileicon'] = 'Nahraný profilový obrázek';
$string['uploadedprofileiconsuccessfully'] = 'Nové obrázky profilu úspěšně nahrány';
$string['uploadexceedsquota'] = 'Nahráním těchto souborů by byla překročena vám přidělená kvóta diskového prostoru. Zkuste odstranit nějaké nepotřebné soubory.';
$string['uploadexceedsquotagroup'] = 'Nahráním tohoto souboru byste překročili diskový prostor přidělený skupině. Zkuste smazat některé soubory a akci zopakujte.';
$string['uploadfile'] = 'Nahrát soubor';
$string['uploadfileexistsoverwritecancel'] = 'Soubor s tímto názvem již existuje. Můžete buď zvolit jiný název nebo přepsat stávající soubor.';
$string['uploadingfile'] = 'nahrávám soubor...';
$string['uploadingfiletofolder'] = 'Nahrávám %s do %s';
$string['uploadoffilecomplete'] = 'Nahrávání souboru %s dokončeno';
$string['uploadoffilefailed'] = 'Nahrávání souboru %s selhalo';
$string['uploadoffiletofoldercomplete'] = 'Nahrávání souboru %s do složky %s dokončeno';
$string['uploadoffiletofolderfailed'] = 'Nahrávání souboru %s do složky %s selhalo';
$string['uploadprofileicon'] = 'Nahrát profilový obrázek';
$string['usecustomagreement'] = 'Použít vlastní dohodu';
$string['usenodefault'] = 'Nezobrazovat žádný';
$string['usernotificationmessage'] = 'Právě využíváte %s %% ze své kvóty %s. Měli byste kontaktovat správce ohledně jejího navýšení.';
$string['usernotificationsubject'] = 'Vaše úložiště souborů je téměř plné';
$string['useroverquotathreshold'] = 'Uživatel %s dosáhl %s %% své kvóty %s.';
$string['usingnodefaultprofileicon'] = 'Žádný obrázek nebude zobrazován';
$string['validfiletypes'] = 'Můžete nahrávat následující typy souborů:';
$string['video'] = 'video';
$string['videofile'] = 'Video soubor';
$string['viruszipfile'] = 'Clam AV našel soubor napadený virem. Tento komprimovaný soubor byl umístěn do karantény a odstraněn z Vašeho účtu.';
$string['wav'] = 'Audio soubor WAV';
$string['webm'] = 'Video soubor WEBM';
$string['wmv'] = 'Video soubor WMV';
$string['wrongfiletypeforblock'] = 'Vámi nahraný soubor nekoresponduje s vybraným typem bloku.';
$string['xml'] = 'XML soubor';
$string['youmustagreetothecopyrightnotice'] = 'Musíte vyjádřit svůj souhlas s prohlášením o autorských právech.';
$string['zip'] = 'ZIP archiv';
$string['zipdownloadheading'] = 'Složka se staženými soubory';
$string['zipfilenameprefix'] = 'složka';
