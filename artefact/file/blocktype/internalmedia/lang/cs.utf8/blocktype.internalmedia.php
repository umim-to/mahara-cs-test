<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['browsercannotplay'] = 'Váš prohlížeč nedokáže přehrát toto médium.';
$string['browsercannotplay1'] = 'Váš webový prohlížeč nemůže přehrát toto video.';
$string['configdesc'] = 'Nastavte, jaké typy souborů budou moci uživatelé vkládat do tohoto bloku. Jednotlivé typy musejí být rovněž povoleny v konfiguraci položky portfolia Soubor. Pokud zakážete typ, který byl už někam vložen, přestane se zobrazovat.';
$string['configdesc1'] = 'Nastavte, které typy souborů mohou uživatelé přidávat do tohoto bloku. Pokud deaktivujete typ, který už byl někde použit, nebude se nadále zobrazovat.';
$string['description'] = 'Zobrazuje nástroj pro přehrávání videa';
$string['flashanimation'] = 'Animace Flash';
$string['media'] = 'Média';
$string['title'] = 'Přehrávání videa';
$string['typeremoved'] = 'V tomto bloku se má zobrazovat video, ale použitý formát byl správcem serveru zakázán.';
