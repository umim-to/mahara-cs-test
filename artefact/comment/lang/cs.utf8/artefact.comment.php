<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Allow'] = 'Povolit';
$string['Attachments'] = 'Přílohy';
$string['Comment'] = 'Přidat komentář';
$string['Comments'] = 'Komentáře';
$string['Details'] = 'Podrobnosti';
$string['Moderate'] = 'Moderovat';
$string['addcomment'] = 'Přidat komentář';
$string['addyourcomment'] = 'Přidejte svůj komentář';
$string['allowcomments'] = 'Povolit komentáře';
$string['anddetails'] = 'a podrobnosti';
$string['approvalrequired'] = 'Komentáře jsou moderovány, takže pokud se rozhodnete tento komentář zveřejnit, nebude viditelný pro ostatní až do schválení majitelem.';
$string['artefactcomments'] = 'Komentáře položek pro blok';
$string['artefactcomments1'] = 'Komentáře pro';
$string['artefactdefaultpermissions'] = 'Výchozí oprávnění k udělení komentáře';
$string['artefactdefaultpermissionsdescription'] = 'Vybrané položky portfolia budou mít při vytvoření povolené komentáře. Uživatelé mohou přepsat toto nastavení pro jednotlivé položky portfolia.';
$string['attachfile'] = 'Přiložit soubor';
$string['cantedithasreplies'] = 'Můžete upravovat pouze nejnovější komentář';
$string['canteditnotauthor'] = 'Nejste autor tohoto komentáře';
$string['cantedittooold'] = 'Můžete upravit komentáře, které nejsou starší více jak tento počet minut: %d';
$string['comment'] = 'komentář';
$string['commentdeletedauthornotification'] = 'Váš komentář na %s byl odstraněn: %s';
$string['commentdeletednotificationsubject'] = 'Komentář na %s odstraněn';
$string['commentmadepublic'] = 'Zveřejnit komentář';
$string['commentmoderatenotificationsubject'] = 'Moderovat nový komentář';
$string['commentnotinview'] = 'Komentář %d není ve stránce %d';
$string['commentonviewbyuser'] = 'Komentář na %s od %s';
$string['commentratings'] = 'Povolit komentář hodnocení';
$string['commentremoved'] = 'Komentář odstraněn';
$string['commentremovedbyadmin'] = 'Komentář odstraněn správcem';
$string['commentremovedbyauthor'] = 'Komentář odstraněn autorem';
$string['commentremovedbyowner'] = 'Komentář odstraněn majitelem';
$string['comments'] = 'komentáře';
$string['commentsanddetails'] = 'Komentáře (%d) a podrobnosti';
$string['commentsnotincluded'] = 'Do tohoto exportu nebyly zahrnuty komentáře.';
$string['commentsubmitted'] = 'Komentář odeslán';
$string['commentsubmittedmoderatedanon'] = 'Komentář odeslán, čeká na moderátora';
$string['commentsubmittedprivateanon'] = 'Soukromý komentář odeslán';
$string['commentupdated'] = 'Komentář aktualizován';
$string['editcomment'] = 'Upravit komentář';
$string['editcommentdescription'] = 'Můžete upravit své komentáře, pokud nejsou starší více jak %d minut a nebylo na ně nijak reagováno. Po uplynutí této doby můžte své komentáře odstranit a přidat nové.';
$string['entriesimportedfromleapexport'] = 'Položky importované z LEAP exportu, které nebylo možné importovat jinam.';
$string['feedback'] = 'Zpětná vazba';
$string['feedbackattachdirdesc'] = 'Soubory připojené ke komentáři ve vašem portfoliu';
$string['feedbackattachdirname'] = 'commentfiles';
$string['feedbackattachmessage'] = 'Připojené soubory (soubory) byly přidány do vaší složky %s';
$string['feedbackdeletedhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>Komentář na %s byl odstraněn.</strong><br />%s</div><div style="margin: 1em 0;">%s</div><div style="font-size: smaller; border-top: 1px solid #999;"><p><a href="%s">%s</a></p></div>';
$string['feedbackdeletedtext'] = 'Komentář k %s byl odstraněn uživatelem %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete vidět %s online, otevřete tento odkaz: %s';
$string['feedbackmoderationnotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"> <strong>Máte zapnutu moderaci komentářů na stránce "%s\'"</strong><br> %s vás žádá o nastavení svého komentáře jako veřejného. Napsal/a: </div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Můžete tedy<a href="%s">nastavit tento komentář jako veřejný</a>.</p> </div>';
$string['feedbackmoderationnotificationtext'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"> <strong>Máte zapnutu moderaci komentářů na stránce %s</strong><br> %s vás žádá o nastavení svého komentáře jako veřejného. Napsal/a: </div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Můžete tedy nastavit tento komentář jako veřejný na <a href="%s">. </a></p> </div>';
$string['feedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s byl komentován na %s</strong><br />%s</div><div style="margin: 1em 0;">%s</div><div style="font-size: smaller; border-top: 1px solid #999;"><p><a href="%s">Odpovědět na tento komentář online</a></p></div>';
$string['feedbacknotificationtext'] = '%s komentován na %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete vidět a odpovědět na tento komentář online, otevřete tento odkaz: %s';
$string['feedbackonviewbyuser'] = 'Zpětná vazba na %s od %s';
$string['feedbacksubmitted'] = 'Zpětná vazba odeslána';
$string['feedbacksubmittedmoderatedanon'] = 'Zpětná vazba odeslána, očekává se moderování';
$string['feedbacksubmittedprivateanon'] = 'Soukromá zpětná vazba odeslána';
$string['forceprivatecomment'] = 'Soukromě: Tato odpověď bude viditelná jen pro vás a autora předchozího komentáře.';
$string['forcepubliccomment'] = 'Veřejně';
$string['groupadmins'] = 'Správci skupiny';
$string['heart'] = 'Srdíčko';
$string['importedfeedback'] = 'Importovaná zpětná vazba';
$string['lastcomment'] = 'Poslední komentář';
$string['makecommentpublic'] = 'Udělat komentář veřejným';
$string['makepublic'] = 'Zveřejnit';
$string['makepublicnotallowed'] = 'Nemáte oprávnění zveřejnit tento komentář';
$string['makepublicrequestbyauthormessage'] = '%s požádal o zveřejnění tohoto komentáře.';
$string['makepublicrequestbyownermessage'] = '%s požádal o zveřejnění tohoto komentáře.';
$string['makepublicrequestsent'] = 'Zpráva s žádostí o zveřejnění příspěvku byla odeslána uživateli %s.';
$string['makepublicrequestsubject'] = 'Žádost o změnu soukromého příspěvku na veřejný';
$string['messageempty'] = 'Zpráva je prázdná. Prázdné zprávy jsou povoleny pouze pokud k nim přiložíte soubor.';
$string['moderatecomment'] = 'Autor komentáře požádal, abyste nastavil(a) jeho komentář jako veřejný.';
$string['moderatecomments'] = 'Moderovat komentáře';
$string['moderatecommentsdescription'] = 'Komentář zůstane soukromý, dokud ho neschválíte.';
$string['moderatecommentsdescription1'] = 'Komentáře na stránkách budou soukromé, dokud je neschválíte. Komentáře ke specifickému obsahu, např. záznamům do deníku, jsou z možnosti moderování vyloučeny.';
$string['moderatecommentsdescription2'] = 'Komentáře u stránek a položek zůstávají soukromé dokud je neschválíte. Komentáře lidí, kteří nejsou přihlášení ke svému účtu, jdou vždy do fronty pro moderování komentářů a potřebují být schváleny.';
$string['ncomments'] = array(

		0 => '%s komentář', 
		1 => '%s komentáře', 
		2 => '%s komentářů'
);
$string['newcommentnotificationsubject'] = 'Nový komentář na %s';
$string['newfeedbacknotificationsubject'] = 'Nová zpětná vazba na %s';
$string['ok'] = 'Zaškrtnout';
$string['placefeedback'] = 'Zanechat zpětnou vazbu';
$string['pluginname'] = 'Komentář';
$string['privatetopubliccomment'] = 'Vlastník portfolia požádal, abyste nastavil(a) svůj komentář jako veřejný.';
$string['progress_feedback'] = array(

		0 => 'Komentář na stránce jiného uživatele', 
		1 => 'Komentář na stránkách %s jiných uživatelů', 
		2 => 'Komentář na stránkách %s jiných uživatelů'
);
$string['rating'] = 'Hodnocení';
$string['ratingcolour'] = 'Barva';
$string['ratingcolourdesc'] = 'Barva zobrazení jednotlivých možností hodnocení. Ikona zvoleného hodnocení bude plně barevná, neoznačené možnosti budou mít barevný rámeček.';
$string['ratingexample'] = 'Automaticky generovaný příklad';
$string['ratingicons'] = 'Ikona pro zobrazení hodnocení';
$string['ratinglength'] = 'Počet možností hodnocení';
$string['ratingoption'] = 'Nastavit hodnocení na %s z %s';
$string['reallydeletethiscomment'] = 'Jste si jisti, že chcete odstranit tento komentář?';
$string['removerating'] = 'Obnovit hodnocení';
$string['reply'] = 'Odpověď';
$string['replyto'] = 'Odpovědět:';
$string['replytonoaccess'] = 'Nejste oprávněni přidávat odpovědi k tomuto komentáři.';
$string['replytonoprivatereplyallowed'] = 'Nejste oprávněni přidávat soukromé odpovědi k tomuto komentáři.';
$string['replytonopublicreplyallowed'] = 'Nejste oprávněni přidávat veřejné odpovědi k tomuto komentáři.';
$string['smile'] = 'Úsměv';
$string['star'] = 'Hvězdička';
$string['thiscommentisprivate'] = 'Tento komentář je soukromý';
$string['thiscommentisprivate1'] = 'Tento komentář je soukromý.';
$string['thumbsup'] = 'Palec nahoru';
$string['typefeedback'] = 'Zpětná vazba';
$string['viewcomment'] = 'Zobrazit komentář';
$string['viewcomments'] = 'Komentáře stránky';
$string['youhaverequestedpublic'] = 'Požádal jste o zveřejnění tohoto komentáře.';
