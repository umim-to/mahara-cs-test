<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Assessment'] = 'Hodnocení';
$string['Assessments'] = 'Hodnocení';
$string['assessment'] = 'hodnocení';
$string['assessmentremoved'] = 'Vyžádané hodnocení smazáno';
$string['assessments'] = 'hodnocení';
$string['assessmentsubmitted'] = 'Vyžádané hodnocení uloženo';
$string['assessmentsubmitteddraft'] = 'Vyžádané hodnocení uloženo jako rozpracované';
$string['attachfile'] = 'Přiložit soubor';
$string['deletedauthornotification1'] = 'Vámi vyžádaný hodnotitel, %s, smazal své hodnocení na stránce "%s". Znění smazaného hodnocení: %s';
$string['deletednotificationsubject'] = 'Vyžádané hodnocení na stránce "%s" smazáno';
$string['feedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s přidal vyžádané hodnocení na %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Zobrazit toto hodnocení online</a></p> </div>';
$string['feedbacknotificationtext1'] = '%s vložil vyžádané hodnocení na %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pro zobrazení tohoto hodnocení online, pokračujte prosím na odkaz: %s';
$string['importedassessment'] = 'Importované hodnocení na vyžádání';
$string['makeassessmentpublic'] = 'Veřejné hodnocení';
$string['messageempty'] = 'Vyžádané hodnocení je prázdné';
$string['nassessments'] = array(

		0 => '%s hodnocení', 
		1 => '%s hodnocení', 
		2 => '%s hodnocení'
);
$string['newassessmentnotificationsubject'] = 'Nové vyžádané hodnocení na stránce "%s"';
$string['nopeerassessmentrequired'] = 'Obsah této stránky se vám nezobrazuje, protože nevyžaduje vaše hodnocení';
$string['placeassessment'] = 'Umístit hodnocení';
$string['pluginname'] = 'Vyžádané hodnocení';
$string['progress_peerassessment'] = array(

		0 => 'Vložit 1 vyžádané hodnocení ke stránce', 
		1 => 'Vložit %s vyžádaná hodnocení ke stránkám', 
		2 => 'Vložit %s vyžádaných hodnocení ke stránkám'
);
$string['progress_verify'] = array(

		0 => 'Udělit 1 potvrzení ke stránce s vyžádaným hodnocením jiného uživatele', 
		1 => 'Udělit %s potvrzení ke stránkám s vyžádaným hodnocením jiných uživatelů', 
		2 => 'Udělit %s potvrzení ke stránkám s vyžádaným hodnocením jiných uživatelů'
);
$string['reallydeletethisassessment'] = 'Smazat toto vyžádané hodnocení';
$string['signedoffalertpeermsg'] = 'Tato stránka byla schválena uživatelem %s (vlastníkem). Již tedy není možné přidávat, měnit nebo mazat vaše "hodnocení na vyžádání". Pokud jej i přesto potřebujete dokončit, kontaktujte prosím %s.';
$string['thisassessmentisprivate'] = 'Uloženo jako koncept';
$string['typeassessmentfeedback'] = 'Hodnocení na vyžádání';
$string['verifyassessment'] = 'Potvrdit hodnocení';
