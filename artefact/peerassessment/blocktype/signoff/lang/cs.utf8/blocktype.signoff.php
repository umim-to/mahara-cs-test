<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2020
 *
 */

defined('INTERNAL') || die();

$string['description'] = 'Blok pro zobrazení možností schvalování a potvrzování stránky';
$string['placeholder'] = 'Obsah tohoto bloku se zobrazí pod nadpisem stránky, nebude tedy vypadat jako klasický samostatný blok na stránce.';
$string['placeholder1'] = 'Obsah tohoto bloku se zobrazuje zarovnaný vpravo. Nejlepší je tento blok umístit do pravého horního rohu stránky.';
$string['readyforverification'] = 'Tato stránka je připravena ke schválení.';
$string['removedverifynotification'] = 'Vlastník stránky %s odvolal své schválení stránky. Z toho důvodu bylo také Vaše potvrzení odstraněno. Prosím, přejděte na danou stránku a zkontrolujte, zda je v připravena na to, abyste ji potvrdil znovu.';
$string['removedverifynotificationsubject'] = 'Potvrzení stránky %s odstraněno';
$string['signedoff'] = 'Schváleno vlastníkem';
$string['signedoffbyondate'] = '%s (vlastník) schválil tuto stránku %s.';
$string['signoff'] = 'Schvalování vlastníkem';
$string['signoffdesc'] = 'Vlastník portfolia může stránku schválit v případě, že byly splněny všechny požadavky, a tak dát najevo, že je připravena k hodnocení.';
$string['signoffdetails'] = 'Podrobnosti o schválení vlastníkem';
$string['signoffpageconfirm'] = 'Potvrdit tuto akci?';
$string['signoffpagedesc'] = 'Zvolte "Ano", pokud chcete tuto stránku schválit a tím dát najevo, že byly splněny všechny požadavky na ni kladené. Zvolte "Ne" pro zrušení.';
$string['signoffpagetitle'] = 'Schválit stránku';
$string['signoffpageundodesc'] = 'Pokud zvolíte "Ano", zrušíte stav stránky "Schváleno vlastníkem". Tím bude krom Vašeho schválení zneplatněno i potvrzení stránky vedoucím, pokud bylo součásti kontrolního procesu. Pokud zvolíte "Ne", tato akce nebude provedena.';
$string['signoffviewupdated'] = 'Byl změněn stav schválení stránky vlastníkem';
$string['title'] = 'Schvalování stránky';
$string['updatesignoff'] = 'Aktualizovat schválení stránky vlastníkem';
$string['updateverify'] = 'Aktualizovat potvrzení stránky vedoucím';
$string['verified'] = 'Potvrzeno vedoucím';
$string['verifiedbyondate'] = '%s (vedoucí) potvrdil tuto stránku %s.';
$string['verify'] = 'Potvrdit';
$string['verifydesc'] = 'Rozhodněte, zda bude součásti schvalovacího procesu této stránky také potvrzování vedoucím.';
$string['verifypagedesc'] = 'Zvolte "Ano", pokud chcete potvrdit, že vlastník této stránky splnil všechny požadavky na ni kladené. Zvolte "Ne" pro návrat na stránku bez jejího potvrzení.';
$string['verifypagetitle'] = 'Potvrdit stránku (jako vedoucí)';
$string['verifyviewupdated'] = 'Vedoucí změnil stav potvrzení stránky';
$string['viewsignoffdetails'] = 'Zobrazit podrobné informace o schválení vlastíkem';
$string['wrongsignoffviewrequest'] = 'Nemáte oprávnění provést požadovanou akci';
