<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2020
 *
 */

defined('INTERNAL') || die();

$string['addpeerassessment'] = 'Přidat hodnocení na vyžádání';
$string['blockcontent'] = 'Instrukce';
$string['description'] = 'Blok pro zobrazení vyžádaného hodnocení';
$string['draft'] = 'Uložit jako koncept';
$string['instructions'] = 'Instrukce';
$string['nopeerassessment'] = 'Žádná zpětná vazba k hodnocení';
$string['publish'] = 'Publikovat';
$string['savedraft'] = 'Uložit koncept';
$string['savepublishhelp'] = '<p><strong>Uložit jako koncept:</strong> Můžete jej zobrazit pouze vy. Dokud je vaše hodnocení uloženo jako koncept, můžete provádět změny.</p> <p><strong>Publikovat:</strong> Hodnocení bude zpřístupněno hodnocené osobě.  Zobrazí se také všem ostatním, kdo mají přístup k portfoliu, pokud ovšem toto portfolio zároveň neobshuje schvalovací blok a není již schváleno vlastníkem portfolia. Publikované hodnocení již není možné vrátit do stavu konceptu.</p>';
$string['title'] = 'Hodnocení na vyžádání';
