<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Grouptasks'] = 'Úkoly skupiny';
$string['Plan'] = 'Plán';
$string['Plans'] = 'Plány';
$string['Task'] = 'Úkol';
$string['Tasks'] = 'Úkoly';
$string['URL'] = 'Odkaz na úkol';
$string['addtask'] = 'Přidat úkol';
$string['addtaskspecific'] = 'Přidat úkol k "%s"';
$string['alltasks'] = 'Všechny úkoly';
$string['canteditdontownplan'] = 'Nemůžete upravit tento plán, protože nejste jeho vlastníkem.';
$string['canteditdontowntask'] = 'Nemůžete upravit tento úkol, protože nejste jeho vlastníkem.';
$string['choosetemplate'] = 'Vybrat šablonu';
$string['close'] = 'Zavřít';
$string['completed'] = 'Dokončeno';
$string['completeddesc'] = 'Označte úkol jako dokončený.';
$string['completedstatechangedpagereload'] = 'Stupeň dokončení se v mezičase změnil. Pro zobrazení aktuálního stavu nechte stránku načíst znovu.';
$string['completiondate'] = 'Termín dokončení';
$string['completiondatedescription'] = 'Použijte formát %s. Pro úkoly v šabloně bude výchozí termín dokončení nastaven na hodnotu data konce upravitelnosti skupiny pro její členy.';
$string['completiondatemustbeinfuture'] = 'Termín dokončení musí být v budoucnosti.';
$string['completiondatemustbesetforreminder'] = 'Pro nastavení připomínky je nutné zadat termín dokončení.';
$string['deleteplan'] = 'Odstranit plán';
$string['deleteplanconfirm'] = 'Opravdu jste si jisti, že chcete odstranit tento plán? Odstraněním tohoto plánu smažete všechny události, které obsahuje.';
$string['deletetask'] = 'Smazat úkol';
$string['deletetaskconfirm'] = 'Opravdu jste si jisti, že chcete smazat tento úkol?';
$string['deletethisplan'] = 'Smazat plán: \'%s\'';
$string['deletethistask'] = 'Smazat úkol: \'%s\'';
$string['description'] = 'Popis';
$string['duplicatedplan'] = 'Duplikovaný plán';
$string['duplicatedtask'] = 'Duplikovaný úkol';
$string['editassignedoutcome'] = 'Otevřít portfolio';
$string['editingplan'] = 'Upravuji plán';
$string['editingtask'] = 'Upravuji úkol';
$string['editplan'] = 'Upravit plán';
$string['edittask'] = 'Upravit úkol';
$string['editthistask'] = 'Upravit úkol: \'%s\'';
$string['emailfooter'] = 'Toto je automaticky vygenerované upozornění od %s.';
$string['existingplans'] = 'Existující plány';
$string['existingtasks'] = 'Existující úkoly';
$string['fromtemplate'] = 'Z šablony';
$string['groupplans'] = 'Plány';
$string['grouptaskselected'] = 'Plán a zvolené úkoly byly přeneseny do prostoru vašich osobních portfolií. Pokud zde byla i portfolia s úkoly, byla překopírována také.';
$string['grouptaskunselected'] = 'Plán a / nebo vybrané úkoly byly odstraněny z prostoru vašich osobních portfolií. Pokud zde byla i portfolia s úkoly, byla také smazána.';
$string['incomplete'] = 'Nedokončené';
$string['managetasks'] = 'Spravovat úkoly';
$string['managetasksspecific'] = 'Spravovat úkoly v "%s"';
$string['mytasks'] = 'Moje úkoly';
$string['newplan'] = 'Nový plán';
$string['newtask'] = 'Nový úkol';
$string['none'] = 'Nic';
$string['noplans'] = 'Žádné plány ke zobrazení';
$string['noplansaddone'] = 'Dosud nejsou vytvořeny žádné plány. %sPřidejte nějaký%s!';
$string['noselectiontask'] = 'Toto není zadaný úkol.';
$string['notasks'] = 'Žádné úkoly ke zobrazení';
$string['notasksaddone'] = 'Dosud žádné úkoly. %sPřidejte nějaké%s!';
$string['notemplate'] = 'Žádná šablona';
$string['nplans'] = array(

		0 => '%s plán', 
		1 => '%s plány', 
		2 => '%s plánů'
);
$string['ntasks'] = array(

		0 => '1 úkol', 
		1 => '%s úkoly', 
		2 => '%s úkolů'
);
$string['outcome'] = 'Portfolio s úkolem';
$string['outcomedescription'] = 'Připojte portfolio pro dokončení tohoto úkolu. Poznámka: Toto portfolio může být použito pouze pro jeden úkol.';
$string['outcomeiscurrentlysubmitted'] = 'Toto portfolio je aktuálně odevzdáno k hodnocení.';
$string['overdue'] = 'Opožděné';
$string['ownerfieldsnotset'] = 'Pole ohledně vlastníka nejsou vyplněná.';
$string['plan'] = 'Plán';
$string['plandeletedsuccessfully'] = 'Plán byl úspěšně odstraněn.';
$string['plannotdeletedsuccessfully'] = 'Došlo k chybě při odstraňování plánu.';
$string['plannotsavedsuccessfully'] = 'Došlo k chybě při odesílání formuláře. Prosím zkontrolujte označená pole a zkuste to znovu.';
$string['plans'] = 'Plány';
$string['plansavedsuccessfully'] = 'Plán byl úspěšně uložen.';
$string['planstasks'] = 'Úkoly pro plán \'%s\'.';
$string['planstasks1'] = 'Naplánovat úkoly v %s  \'%s\'';
$string['planstasksdesc'] = 'Přidat úkoly níže, nebo pomocí tlačítka vpravo začít připravovat svůj plán.';
$string['planstasksdescription'] = 'Přidejte úkoly níže nebo použijte tlačítko "%s" a začněte vytvářet svůj plán.';
$string['pluginname'] = 'Plány';
$string['progress_plan'] = array(

		0 => 'Přidat plán', 
		1 => 'Přidat %s plány', 
		2 => 'Přidat %s plánů'
);
$string['progress_task'] = array(

		0 => 'Přidat úkol do plánu', 
		1 => 'Přidat %s úkoly do plánu', 
		2 => 'Přidat %s úkolů do plánu'
);
$string['reminder'] = 'Upomínka';
$string['reminderdescription'] = 'Zaslat upozornění pro připomenutí termínu dokončení.';
$string['reminderinfo'] = 'Info';
$string['reminderinfodue'] = array(

		0 => 'Zpoždění 1 den', 
		1 => 'Zpoždění %s dny', 
		2 => 'Zpoždění %s dnů'
);
$string['reminderinfonearlydue'] = array(

		0 => 'Zbývá jeden den', 
		1 => 'Zbývají %s dny', 
		2 => 'Zbývá %s dnů'
);
$string['remindersubject'] = 'Připomínka: Dokončete úkoly v plánu';
$string['saveplan'] = 'Uložit plán';
$string['savetask'] = 'Uložit úkol';
$string['selectionplan'] = 'Přidělování úkolů';
$string['selectionplandescription'] = 'Po té, co bude ve skupině vytvořen plán na základě této šablony plánu, její členové si z něj budou moci vybírat úkoly k plnění. Tato možnost je dostupná pouze tehdy, když vytváříte šablonu.';
$string['selectionstatechangedpagereload'] = 'Stav vybraného se v mezičase změnil. Pro zobrazení nynějšího stavu aktualizujte stránku.';
$string['showassignedoutcome'] = 'Náhled portfolia';
$string['showassignedview'] = 'Náhled úkolu';
$string['startdate'] = 'Datum začátku';
$string['startdatedescription'] = 'Použijte formát %s. Pro úkoly v šabloně bude výchozí datum začátku nastaveno na hodnotu počátečního data upravitelnosti skupiny pro její členy.';
$string['startdatemustbebeforecompletiondate'] = 'Datum začátku musí být před datem dokončení.';
$string['submitassignedoutcome'] = 'Odeslat k hodnocení';
$string['targetgroupplancollectiontitleprefix'] = 'Úkoly v plánu:';
$string['task'] = 'úkol';
$string['taskdeletedsuccessfully'] = 'Úkol byl úspěšně odstraněn.';
$string['tasks'] = 'úkoly';
$string['tasksavedsuccessfully'] = 'Úkol byl úspěšně uložen.';
$string['taskview'] = 'Stránka k úkolu';
$string['taskviewdescription'] = 'Připojte k tomuto úkolu stránku s jeho podrobným popisem.';
$string['taskviewsfortemplateplan'] = 'Stránky obsahující podrobnosti o úkolech z plánu "%s"';
$string['template'] = 'Šablona';
$string['templatedescription'] = 'Použijte tento plán jako šablonu pro vytvoření plánů skupiny.';
$string['templatedialogdescription'] = 'Vyberte šablonu plánu z vašich osobních plánů jako základ pro plány této skupiny. Všechny přidružené úkoly, úkolové stránky a portfolia s úkoly budou do této skupiny automaticky zkopírovány.';
$string['templateplan'] = 'šablona';
$string['title'] = 'Název';
$string['titledesc'] = 'Název bude použit při zobrazení každého úkolu sekci Mé plány.';
$string['unselecttaskconfirm'] = 'Skutečně chcete odstranit tento úkol z vašeho plánu?';
$string['unsupportedportfoliotype'] = 'Tento typ portfolia není podporován.';
$string['viewandoutputcontainsameelement'] = 'Úkol a portfolio s úkolem nemůže obsahovat stejnou stránku nebo stránku ze stejné sbírky.';
$string['visitplans'] = 'Připomínka dokončit úkoly';
$string['wrongfunctionrole'] = 'Tuto akci nemůžete dokončit, protože to vaše role nedovoluje.';
$string['youhavenplan'] = array(

		0 => 'Máte jeden plán.', 
		1 => 'Máte %d plány.',
		1 => 'Máte %d plánů.'
);
$string['youhaveremindertasksmessagetext'] = 'Vážený %s, rádi bychom vám připomněli, že je potřeb dokončit tyto naplánované úlohy: %s S pozdravem tým %s';
