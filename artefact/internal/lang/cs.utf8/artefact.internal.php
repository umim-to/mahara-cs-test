<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Created'] = 'Vytvořeno';
$string['Description'] = 'Popis';
$string['Download'] = 'Stáhnout';
$string['Note'] = 'Poznámka';
$string['Notes'] = 'Poznámky';
$string['Owner'] = 'Vlastník';
$string['Preview'] = 'Náhled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Název';
$string['Type'] = 'Typ';
$string['aboutdescription'] = 'Zadejte vaše skutečné křestní jméno a příjmení. Pokud chcete být znám pod jiným jménem, vložte jej do pole Upřednostňované jméno.';
$string['aboutme'] = 'O mně';
$string['aboutprofilelinkdescription'] = '<p>K nastavení viditelnosti informací ostatním uživatelům přejděte prosím na stránky vašeho <a href="%s">Profilu</a>.</p>';
$string['addbutton'] = 'Přidat';
$string['address'] = 'Poštovní adresa';
$string['adminusersearchfields'] = 'Správa hledání uživatelů';
$string['adminusersearchfieldsdescription'] = 'Pole profilu, která se zobrazují jako sloupce ve Správě hledání uživatelů';
$string['aim'] = 'AIM';
$string['aim.input'] = 'AIM jméno';
$string['aimscreenname'] = 'AIM';
$string['autogroupadmin'] = 'Automaticky přiřazený správce skupiny';
$string['blockTitle'] = 'Název bloku';
$string['blogaddress'] = 'Adresa deníku';
$string['businessnumber'] = 'Pracovní telefon';
$string['cancelbutton'] = 'Storno';
$string['city'] = 'Město';
$string['confirmdeletenote'] = 'Tato poznámka je použita v %s blocích a %s stránkách. Pokud ji odstraníte, všechny stávající bloky, které ji obsahují, budou prázdné.';
$string['contact'] = 'Kontaktní údaje';
$string['containedin'] = 'Obsažená v:';
$string['country'] = 'Země';
$string['currenttitle'] = 'Pojmenováno';
$string['deleteprofile'] = 'Odebrat účet sociální sítě';
$string['deleteprofileconfirm'] = 'Opravdu si přejete odebrat tento účet sociální sítě?';
$string['deletethisprofile'] = 'Odebrat účet sociální sítě: \'%s\'';
$string['duplicatedprofilefieldvalue'] = 'Zkopírovaná hodnota';
$string['duplicateurl'] = 'Nemůžete přidat tento účet ze sociálních médií, protože jeho uživatelské jméno nebo URL je duplikátem jednoho z těch, které již máte.';
$string['editnote'] = 'Upravit poznámku';
$string['editthisprofile'] = 'Upravit účet sociální sítě \'%s\'';
$string['email'] = 'E-mailová adresa';
$string['emailactivation'] = 'Aktivace e-mailu';
$string['emailactivationdeclined'] = 'Aktitace e-mailu byla úspěšně odmítnuta';
$string['emailactivationfailed'] = 'Aktivace e-mailu selhala';
$string['emailactivationsucceeded'] = 'Aktivace e-mailu úspěšně provedena';
$string['emailaddress'] = 'Další e-mail';
$string['emailalreadyactivated'] = 'E-mail byl již aktivován';
$string['emailingfailed'] = 'Profil uložen, ale e-maily nebyly odeslány na: %s';
$string['emailvalidation_body'] = 'Toto je automaticky generovaná zpráva pro: %s

K vašemu uživatelskému účtu byla přiřazena další emailová adresa %s. Tuto novou adresu je ještě třeba aktivovat na adrese:
%s';
$string['emailvalidation_body1'] = 'Dobrý den %s, do vašeho uživatelského účtu jste přidali e-mailovou adresu %s. Prosím, klikněte na odkaz níže pro její aktivaci.%s Pokud tento e-mail patří vám, ale nepožádali jste o jeho přidání do svého účtu %s, klikněte na následující odkaz, kterým tuto aktivaci odmítnete. %s';
$string['emailvalidation_subject'] = 'Ověření funkčnosti e-mailové adresy';
$string['existingprofilefieldvalues'] = 'Existující hodnoty';
$string['facebook'] = 'Facebook';
$string['facebook.input'] = 'Facebook URL';
$string['faxnumber'] = 'Fax';
$string['firstname'] = 'Křestní jméno';
$string['fullname'] = 'Celé jméno';
$string['general'] = 'Obecné';
$string['homenumber'] = 'Telefon domů';
$string['html'] = 'Poznámka';
$string['icq'] = 'ICQ';
$string['icq.input'] = 'ICQ number';
$string['icqnumber'] = 'ICQ';
$string['industry'] = 'Odvětví';
$string['infoisprivate'] = 'Tato informace zůstane soukromá, dokud ji nepřidáte na stránku, kterou sdílíte spolu s ostatními.';
$string['instagram'] = 'Instagram';
$string['instagram.input'] = 'Instagram - jméno uživatele';
$string['institution'] = 'Instituce';
$string['introduction'] = 'Úvod';
$string['invalidemailaddress'] = 'Neplatná e-mailová adresa';
$string['jabber'] = 'Jabber';
$string['jabber.input'] = 'Jabber - uživatelské jméno';
$string['jabberusername'] = 'Jabber';
$string['lastmodified'] = 'Naposledy upraveno';
$string['lastname'] = 'Příjmení';
$string['loseyourchanges'] = 'Zahodit změny?';
$string['maildisabled'] = 'E-mail zakázán';
$string['mandatory'] = 'Povinné';
$string['mandatoryfields'] = 'Povinná pole';
$string['mandatoryfieldsdescription'] = 'Pole v profilu, která musí být vyplněna';
$string['messaging'] = 'Zasílání zpráv';
$string['mobilenumber'] = 'Mobil';
$string['msnnumber'] = 'MSN';
$string['mynotes'] = 'Moje poznámky';
$string['name'] = 'Jméno';
$string['newemailalert_body_html'] = '<p>Dobrý den %s,<p> <p>k účtu %s jste přidali e-mailové adresy:</p> <p>%s</p>. <p>Pokud jste o tuto změnu v rámci účtu %s nepožádali, obraťte se na <a href="%scontact.php">správce webu</a></p>';
$string['newemailalert_body_html1'] = '<p>Vážený %s,</p> <p>právě byly k Vašemu účtu v %s přidány následující e-mailové adresy:</p><p> %s</p> <p>Pokud jste o tuto změnu ve vašem %s účtu nežádal, prosím <a href="%scontact.php">kontaktujte správce stránek</a>.</p>';
$string['newemailalert_body_text'] = 'Dobrý den %s, k účtu %s jste přidali e-mailové adresy: %s. Pokud jste o tuto změnu v rámci účtu %s nepožádali, obraťte se na správce webu %scontact.php';
$string['newemailalert_body_text1'] = 'Vážený %s, právě byly k Vašemu účtu v %s přidány následující e-mailové adresy: %s Pokud jste o tuto změnu ve vašem %s účtu nežádal, prosím kontaktujte správce stránek. %scontact.php';
$string['newemailalert_subject'] = 'K vašemu účtu %s byla přidána nová e-mailová adresa';
$string['newsocialprofile'] = 'Nový účet sociální sítě';
$string['nospecialroles'] = '<span class="text-midtone">Žádné speciální role</span>';
$string['noteTitle'] = 'Název poznámky';
$string['notedeleted'] = 'Poznámka odstraněna';
$string['notes'] = 'Poznámky';
$string['notesdescription'] = 'Toto jsou HTML poznámky, které jste vytvořili uvnitř bloků textového pole na vašich stránkách.';
$string['notesdescription1'] = 'Toto jsou HTML poznámky, které jste vytvořili v bloku "Poznámka" na vašich stránkách.';
$string['notesfor'] = 'Poznámky pro %s';
$string['noteupdated'] = 'Poznámka aktualizována';
$string['notvalidprofileurl'] = 'Toto není platné URL účtu sociální sítě. Vložte, prosím, platné URL, nebo zvolte komunikační službu z výše uvedeného seznamu.';
$string['occupation'] = 'Pracovní pozice';
$string['officialwebsite'] = 'Oficiální webová stránka';
$string['personalwebsite'] = 'Osobní webová stránka';
$string['pinterest'] = 'Pinterest';
$string['pinterest.input'] = 'Pinterest - uživatelské jméno';
$string['pluginname'] = 'Profil';
$string['preferredname'] = 'Upřednostňované jméno';
$string['principalemailaddress'] = 'Hlavní e-mail';
$string['profile'] = 'Profil';
$string['profiledeletedsuccessfully'] = 'Účet sociální sítě úspěšně odebrán';
$string['profilefailedsaved'] = 'Ukládání profilu selhalo';
$string['profileimagefor'] = 'Profilový obrázek pro %s';
$string['profileinformation'] = 'Profilové informace';
$string['profilepage'] = 'Profilová stránka';
$string['profilesaved'] = 'profil úspešně uložen';
$string['profilesavedsuccessfully'] = 'Účet sociální sítě úspěšně uložen';
$string['profiletype'] = 'Sociální síť';
$string['profileurl'] = 'Vaše URL nebo uživatelské jméno';
$string['profileurldesc'] = 'URL vaší profilové stránky nebo uživatelské jméno.';
$string['profileurlexists'] = 'Tento účet sociální sítě nemohl být přidán, protože jeho URL nebo uživatelské jméno již používáte.';
$string['progress_address'] = 'Přidat vaši poštovní adresu';
$string['progress_blogaddress'] = 'Přidat adresu vašeho blogu';
$string['progress_businessnumber'] = 'Přidat váš pracovní telefon';
$string['progress_city'] = 'Přidat město';
$string['progress_country'] = 'Přidat zemi';
$string['progress_email'] = 'Přidat váš e-mail';
$string['progress_faxnumber'] = 'Přidat váš fax';
$string['progress_firstname'] = 'Přidat vaše jméno';
$string['progress_homenumber'] = 'Přidat číslo vašeho telefonu domů';
$string['progress_industry'] = 'Přidat vaše odvětví';
$string['progress_introduction'] = 'Přidat pár slov o sobě jako úvod';
$string['progress_joingroup'] = array(

		0 => 'Přidat se do skupiny', 
		1 => 'Přidat se do %s skupin', 
		2 => 'Přidat se do %s skupin'
);
$string['progress_lastname'] = 'Přidat vaše příjmení';
$string['progress_makefriend'] = array(

		0 => 'Spřátelit se s jedním člověkem', 
		1 => 'Spřátelit se s %s přáteli', 
		2 => 'Spřátelit se s %s přáteli'
);
$string['progress_messaging'] = 'Přidat informace, které se týkají zasílání zpráv';
$string['progress_mobilenumber'] = 'Přidat číslo vašeho mobilního telefonu';
$string['progress_occupation'] = 'Přidat vaši pracovní pozici';
$string['progress_officialwebsite'] = 'Přidat vaši oficiální webovou stránku';
$string['progress_personalwebsite'] = 'Přidat vaši osobní webovou stránku';
$string['progress_preferredname'] = 'Přidat upřednostňované jméno';
$string['progress_studentid'] = 'Přidat vaše studentské ID';
$string['progress_town'] = 'Přidat město';
$string['progressbaritem_joingroup'] = 'Přidat se do skupiny';
$string['progressbaritem_makefriend'] = 'Spřátelit se';
$string['progressbaritem_messaging'] = 'Zasílání zpráv';
$string['public'] = 'Veřejné';
$string['saveprofile'] = 'Uložit profil';
$string['searchablefields'] = 'Prohledávatelná pole';
$string['searchablefieldsdescription'] = 'Pole v profilu, které ostatní uživatelé mohou využít k vyhledávání';
$string['service'] = 'Sociální síť';
$string['servicedesc'] = 'Vložte název sociální sítě, např. Facebook, LinkedIn, Twitter apod.';
$string['skype'] = 'Skype';
$string['skype.input'] = 'Skype - uživatelské jméno';
$string['skypeusername'] = 'Skype';
$string['social'] = 'Sociální sítě';
$string['socialprofile'] = 'Sociální síť';
$string['socialprofilerequired'] = 'Je vyžadován alespoň jeden účet sociální sítě.';
$string['socialprofiles'] = 'Účty sociálních sítí';
$string['studentid'] = 'Studentské ID';
$string['supportadmin'] = 'Pomocný správce';
$string['town'] = 'Městská část';
$string['tumblr'] = 'Tumblr';
$string['tumblr.input'] = 'Tumblr URL';
$string['twitter'] = 'Twitter';
$string['twitter.input'] = 'Twitter - uživatelské jméno';
$string['unvalidatedemailalreadytaken'] = 'Tato e-mailová adresa je již zabraná.';
$string['userroles'] = 'Role účtu';
$string['validationemailsent'] = 'Ověření e-mailové adresy bylo odesláno.';
$string['validationemailwillbesent'] = 'Po uložení profilu vám bude zaslán kontrolní e-mail pro ověření funkčnosti adresy.';
$string['verificationlinkexpired'] = 'Odkaz pro ověření platnosti e-mailové adresy již není platný';
$string['viewallprofileinformation'] = 'Zobrazit všechny profilové informace';
$string['viewmyprofile'] = 'Zobrazit můj profil';
$string['viewprofilepage'] = 'Zobrazit profilovou stránku';
$string['yahoo'] = 'Yahoo Messenger';
$string['yahoo.input'] = 'Yahoo Messenger';
$string['yahoochat'] = 'Yahoo';
