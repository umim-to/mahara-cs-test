<?php

defined('INTERNAL') || die();  

$string['description'] = 'Zobrazuje vybrané kontaktní údaje';
$string['dontshowemail'] = 'Nezobrazovat e-mailovou adresu';
$string['fieldstoshow'] = 'Zobrazit pole';
$string['title'] = 'Kontaktní údaje';

