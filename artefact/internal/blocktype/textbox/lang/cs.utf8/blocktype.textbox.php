<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['blockcontent'] = 'Obsah bloku';
$string['description'] = 'Přidat poznámky na stránku';
$string['editcontent'] = 'Upravit všechny kopie této poznámky';
$string['makeacopy'] = 'Vytvořit kopii';
$string['managealltextboxcontent'] = 'Spravovat obsah všech textových polí';
$string['managealltextboxcontent1'] = 'Spravovat veškerý obsah poznámek';
$string['readonlymessage'] = 'Vybraný text na této stránce nelze upravovat.';
$string['textusedinotherblocks'] = 'Pokud upravíte text tohoto bloku, dojde ke změněně obsahu %s dalších bloků.';
$string['textusedinothernotes'] = array(

		0 => 'Pokud upravíte text této poznámky, dojde ke změně také všude tam, kde se objeví %s blok.', 
		1 => 'Pokud upravíte text této poznámky, dojde ke změně také všude tam, kde se objeví %s bloky.', 
		2 => 'Pokud upravíte text této poznámky, dojde ke změně také všude tam, kde se objeví %s bloků.'
);
$string['title'] = 'Poznámka';
$string['usecontentfromanothertextbox'] = 'Použít obsah z jiného textového pole';
$string['usecontentfromanothertextbox1'] = 'Použít obsah z jiné poznámky';
