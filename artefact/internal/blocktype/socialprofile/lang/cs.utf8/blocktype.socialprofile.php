<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, David Mudrák, Adam Pátek, Matouš Trča, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2018
 *
 */

defined('INTERNAL') || die();

$string['description'] = 'Zvolte zobrazovaný účet sociální sítě';
$string['displayaddressesas'] = 'Zobrazit účty sociálních sítí jako:';
$string['displaydefaultemail'] = 'Zobrazit e-mail jako tlačítko?';
$string['displaymsgservices'] = 'Zobrazit komunikační služby jako tlačítka?';
$string['displaysettings'] = 'Zobrazit nastavení';
$string['noitemsselectone'] = 'Nebyla vybrána žádná sociální síť';
$string['optionicononly'] = 'tlačítka jen s ikonami';
$string['optiontexticon'] = 'tlačítka s ikonami a popisky';
$string['optiontextonly'] = 'tlačítka jen s popisky';
$string['profilestoshow'] = 'Zobrazované účty sociálních sítí';
$string['title'] = 'Sociální sítě';
