<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Attachments'] = 'Přílohy';
$string['academicgoal'] = 'Vzdělávací cíle';
$string['academicskill'] = 'Vzdělávací dovednosti';
$string['achievements'] = 'Úspěchy';
$string['addbook'] = 'Přidat knihy a publikace';
$string['addcertification'] = 'Přidat certifikáty, akreditace a ocenění';
$string['addeducationhistory'] = 'Přidat dosavadní vzdělání';
$string['addemploymenthistory'] = 'Přidat dosavadní zaměstnání';
$string['addmembership'] = 'Přidat členství v profesních organizacích';
$string['attachfile'] = 'Přiložit soubor';
$string['attachmentdesc'] = 'Přílohy k %s';
$string['book'] = 'Knihy a publikace';
$string['bookurl'] = 'URL';
$string['cannotfindcreateartefact'] = 'Obsahový blok nelze najít nebo vytvořit';
$string['careergoal'] = 'Profesní cíle';
$string['certification'] = 'Certifikáty, diplomy, ceny';
$string['citizenship'] = 'Občanství';
$string['compositedeleteconfirm'] = 'Opravdu si to přejete odstranit?';
$string['compositedeleted'] = 'Úspěšně odstraněno';
$string['compositesaved'] = 'Úspěšně uloženo';
$string['compositesavefailed'] = 'Ukládání selhalo';
$string['contactinformation'] = 'Kontaktní údaje';
$string['contribution'] = 'Autoři';
$string['coverletter'] = 'Průvodní dopis';
$string['current'] = 'Stávající';
$string['date'] = 'Datum';
$string['dateofbirth'] = 'Datum narození';
$string['dateofbirthinvalid1'] = 'Toto není platné datum narození. Prosím, opravte jej.';
$string['defaultacademicgoal'] = 'Akademický cíl';
$string['defaultcareergoal'] = 'Profesní cíl';
$string['defaultpersonalgoal'] = 'Osobní cíl';
$string['description'] = 'Popis';
$string['detailsofyourcontribution'] = 'Podrobnosti o vašem příspěvku';
$string['duplicateattachment'] = 'Soubor \'%s\' již byl použit v jiném vstupu do této sekce. Není možné použít stejný soubor vícekrát.';
$string['duplicateattachments'] = 'Soubory \'%s\' již byly použity v jiném vstupu do této sekce. Není možné použít stejné soubory vícekrát.';
$string['duplicatedresumefieldvalue'] = 'Duplikované hodnoty';
$string['editacademicgoal'] = 'Upravit vzdělávací cíle';
$string['editacademicskill'] = 'Upravit vzdělávací dovednosti';
$string['editcareergoal'] = 'Upravit profesní cíle';
$string['editpersonalgoal'] = 'Upravit osobní cíle';
$string['editpersonalskill'] = 'Upravit osobní dovednosti';
$string['editworkskill'] = 'Upravit profesní dovednosti';
$string['education'] = 'Vzdělání';
$string['educationandemployment'] = 'Vzdělání a zaměstnání';
$string['educationhistory'] = 'Dosažené vzdělání';
$string['employer'] = 'Zaměstnavatel';
$string['employeraddress'] = 'Adresa zaměstnavatele';
$string['employment'] = 'Zaměstnání';
$string['employmenthistory'] = 'Dosavadní praxe';
$string['enddate'] = 'Datum ukončení';
$string['existingresumefieldvalues'] = 'Existující hodnoty';
$string['female'] = 'Žena';
$string['gender'] = 'Pohlaví';
$string['gender1'] = 'Pohlaví';
$string['gendernotspecified'] = '(nespecifikováno)';
$string['goalandskillsaved'] = 'Úspěšně uloženo';
$string['goals'] = 'Cíle';
$string['goalsandskills'] = 'Cíle a dovednosti';
$string['institution'] = 'Instituce';
$string['institutionaddress'] = 'Adresa instituce';
$string['interest'] = 'Zájmy';
$string['interests'] = 'Zájmy';
$string['introduction'] = 'Představení';
$string['jobdescription'] = 'Popis pracovní pozice';
$string['jobtitle'] = 'Název pracovní pozice';
$string['license'] = 'Licence';
$string['male'] = 'Muž';
$string['man'] = 'Muž';
$string['maritalstatus'] = 'Stav';
$string['membership'] = 'Členství v profesních organizacích';
$string['movedown'] = 'Přesunout dolů';
$string['movedownspecific'] = 'Přesunout dolů "%s"';
$string['moveup'] = 'Přesunout nahoru';
$string['moveupspecific'] = 'Přesunout nahoru "%s"';
$string['mygoals'] = 'Mé cíle';
$string['myskills'] = 'Mé dovednosti';
$string['nodescription'] = '(bez popisu)';
$string['notvalidurl'] = 'Neplatná URL adresa';
$string['personalgoal'] = 'Osobní cíle';
$string['personalinformation'] = 'Osobní údaje';
$string['personalskill'] = 'Osobní dovednosti';
$string['placeofbirth'] = 'Místo narození';
$string['pluginname'] = 'Životopis';
$string['position'] = 'Pracovní pozice';
$string['positiondescription'] = 'Popis pozice';
$string['progress_academicgoal'] = 'Přidat vaše vzdělávací cíle';
$string['progress_academicskill'] = 'Přidat vaše vzdělávací dovednosti';
$string['progress_book'] = 'Přidat vaše knihy a publikace';
$string['progress_careergoal'] = 'Přidat vaše profesní cíle';
$string['progress_certification'] = 'Přidat vaše certifikáty, diplomy a ceny';
$string['progress_coverletter'] = 'Přidat průvodní dopis';
$string['progress_educationhistory'] = 'Přidat vaše dosažené vzdělání';
$string['progress_employmenthistory'] = 'Přidat vaši dosavadní praxi';
$string['progress_interest'] = 'Přidejte vaše zájmy';
$string['progress_membership'] = 'Přidejte vaše členství v profesních organizacích';
$string['progress_personalgoal'] = 'Přidat vaše osobní cíle';
$string['progress_personalinformation'] = 'Přidat vaše osobní údaje';
$string['progress_personalskill'] = 'Přidat vaše osobní dovednosti';
$string['progress_workskill'] = 'Přidat vaše profesní dovednosti';
$string['qualdescription'] = 'Popis kvalifikace';
$string['qualification'] = 'Odborná kvalifikace';
$string['qualname'] = 'Název kvalifikace';
$string['qualtype'] = 'Druh kvalifikace';
$string['resume'] = 'Životopis';
$string['resumeofuser'] = 'Životopis %s';
$string['resumesaved'] = 'Životopis uložen';
$string['resumesavefailed'] = 'Ukládání životopisu selhalo';
$string['skills'] = 'Dovednosti';
$string['startdate'] = 'Začátek';
$string['title'] = 'Název';
$string['viewyourresume'] = 'Zobrazit váš životopis';
$string['visastatus'] = 'Pracovní vízum';
$string['woman'] = 'Žena';
$string['workskill'] = 'Profesní dovednosti';
