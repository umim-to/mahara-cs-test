<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Blog'] = 'Deník';
$string['Blogpost'] = 'Příspěvek v deníku';
$string['Blogs'] = 'Deníky';
$string['addblog'] = 'Přidat deník';
$string['addone'] = 'Přidat příspěvek!';
$string['addpost'] = 'Přidat příspěvek';
$string['addpostspecific'] = 'Nový příspěvek "%s"';
$string['alignment'] = 'Zarovnání';
$string['alldraftposts'] = 'Příspěvky v deníku ještě nebyly zveřejněny.';
$string['allowcommentsonpost'] = 'Povolit komentáře k tomuto příspěvku.';
$string['allposts'] = 'Všechny přístpěvky';
$string['alt'] = 'Popis';
$string['attach'] = 'Přiložit soubor';
$string['attachedfilelistloaded'] = 'Seznam přiložených souborů nahrán';
$string['attachedfiles'] = 'Přiložené soubory';
$string['attachment'] = 'Příloha';
$string['attachments'] = 'Přílohy';
$string['baseline'] = 'Spodek obrázku je zarovnán na účaří (baseline)';
$string['blog'] = 'deník';
$string['blogcopiedfromanotherview'] = 'Poznámka: Tento deník byl zkopírován z jiné stránky. Můžete jej přesouvat, ale nemůžete měnit jeho obsah, tj. zobrazený %s.';
$string['blogdeleted'] = 'Deník odstraněn';
$string['blogdesc'] = 'Popis';
$string['blogdescdesc'] = 'např. \'Deník Davidových zkušeností a postřehů\'.';
$string['blogdoesnotexist'] = 'Deník, který se snažíte zobrazit, neexistuje';
$string['blogpost'] = 'Příspěvek v deníku';
$string['blogpostdeleted'] = 'Příspěvek odstraněn';
$string['blogpostdoesnotexist'] = 'Deník, který se snažíte zobrazit, neexistuje';
$string['blogpostpublished'] = 'Příspěvek zveřejněn';
$string['blogpostsaved'] = 'Příspěvek uložen';
$string['blogpostunpublished'] = 'Zveřejnění příspěvku zrušeno';
$string['blogs'] = 'deníky';
$string['blogsettings'] = 'Nastavení deníku';
$string['blogtitle'] = 'Název';
$string['blogtitledesc'] = 'např. \'Eliščin deník ke Cvičení z ošetřovatelských postupů\'.';
$string['border'] = 'Ohraničení';
$string['bottom'] = 'Spodek obrázku je zarovnán na spodek písma (bottom)';
$string['by'] = 'dle';
$string['bygroup'] = 'dle skupiny <a href="%s">%s</a>';
$string['byinstitution'] = 'dle instituce <a href="%s">%s</a>';
$string['cancel'] = 'Zrušit';
$string['cannotdeleteblogpost'] = 'Vyskytla se chyba při odstraňování tohoto příspěvku.';
$string['copyfull'] = 'Při kopírování stránky se vytvoří nová kopie tohoto bloku %s';
$string['copynocopy'] = 'Při kopírování stránky tento blok přeskočit';
$string['copyreference'] = 'Ostatní mohou zobrazit váš %s na svých stránkách';
$string['copytagsonly'] = 'Ostatní mohou získat kopii nastavení tohoto bloku';
$string['createandpublishdesc'] = 'Tímto se vytvoří nový příspěvek v deníku a zároveň se zpřístupní ostatním.';
$string['createasdraftdesc'] = 'Tímto se vytvoří nový příspěvek v deníku, ale prozatím nebude dostupný ostatním, dokud jej nezveřejníte.';
$string['createblog'] = 'Vytvořit deník';
$string['createdin'] = 'Vytvořeno v(e)';
$string['dataimportedfrom'] = 'Data importována z %s';
$string['defaultblogtitle'] = 'Deník uživatele %s';
$string['delete'] = 'Odstranit';
$string['deleteblog?'] = 'Opravdu si přejete odstranit celý tento deník?';
$string['deletebloghaspost?'] = array(

		0 => 'Tento deník obsahuje jeden příspěvek. Opravdu si přejete odstranit tento deník?', 
		1 => 'Tento deník obsahuje %d příspěvky. Opravdu si přejete odstranit tento deník?', 
		2 => 'Tento deník obsahuje %d příspěvků. Opravdu si přejete odstranit tento deník?'
);
$string['deletebloghasview?'] = array(

		0 => 'Tento deník obsahuje příspěvky využívané na jedné stránce. Opravdu si přejete odstranit tento deník?', 
		1 => 'Tento deník obsahuje příspěvky využívané na %d stránkách. Opravdu si přejete odstranit tento deník?', 
		2 => 'Tento deník obsahuje příspěvky využívané na %d stránkách. Opravdu si přejete odstranit tento deník?'
);
$string['deleteblogpost?'] = 'Opravdu si přejete odstranit celý tento příspěvek?';
$string['description'] = 'Popis';
$string['dimensions'] = 'Rozměry';
$string['draft'] = 'Pracovní verze';
$string['duplicatedblog'] = 'Zkopírovaný deník';
$string['duplicatedpost'] = 'Zkopírovaný přispěvek v deníku';
$string['edit'] = 'Upravit';
$string['editblogpost'] = 'Upravit příspěvek v deníku';
$string['enablemultipleblogstext'] = 'Jeden deník již máte. Pokud  si chcete založit další, povolte volbu "Více deníků" na stránce s <a href="%saccount/index.php">nastavením účtu</a>.';
$string['entriesimportedfromleapexport'] = 'Položky přidané z exportu LEAP není možné importovat jinam';
$string['errorsavingattachments'] = 'Při ukládání příloh se vyskytla chyba';
$string['existingblogs'] = 'Existující deníky';
$string['existingposts'] = 'Existující záznamy v deníku';
$string['feedrights'] = 'Copyright %s.';
$string['feedsnotavailable'] = 'Kanály nejsou k dispozici pro tento typ položky portfolia.';
$string['groupblogs'] = 'Skupinové deníky';
$string['hiddenblogsnotification'] = 'Byl vám vytvořen další deník, ale váš účet nepovoluje existenci více deníků. Můžete to povolit na stránce <a href="%saccount/index.php">nastavení účtu</a>.';
$string['horizontalspace'] = 'Okraj ve vodorovném směru (hspace)';
$string['image_list'] = 'Přiložený soubor';
$string['insert'] = 'Vložit';
$string['insertimage'] = 'Vložit obrázek';
$string['institutionblogs'] = 'Deníky instituce';
$string['institutionblogsdesc'] = 'Tvořit a spravovat deníky instituce';
$string['left'] = 'Obrázek je umístěn k levému okraji a obtékán zprava';
$string['middle'] = 'Střed obrázku je zarovnán s účařím písma řádku (middle)';
$string['moreoptions'] = 'Více možností';
$string['mustspecifycontent'] = 'Musíte vložit obsah vašeho příspěvku';
$string['mustspecifytitle'] = 'Musíte zadat název vašeho příspěvku';
$string['name'] = 'Název';
$string['nblogs'] = array(

		0 => '%s deník', 
		1 => '%s deníky', 
		2 => '%s deníků'
);
$string['newattachmentsexceedquota'] = 'Souhrnná velikost souborů připojených k tomuto příspěvku by přesáhla vám přidělenou kvótu. Před uložením příspěvku musíte odebrat některé z přiložených souborů.';
$string['newblog'] = 'Nový deník';
$string['newbloggroup'] = '%s: Nový deník';
$string['newbloginstitution'] = 'Nový deník "%s"';
$string['newblogpost'] = 'Nový příspěvek v deníku "%s"';
$string['newblogsite'] = 'Nový deník webových stránek';
$string['newerposts'] = 'Novější příspěvky';
$string['nodefaultblogfound'] = 'Nenalezen výchozí deník. Jedná se o chybu v systému. Pro opravu je třeba povolit volbu více deníků na stránce s <a href="%saccount/">nastavením účtu</a>.';
$string['nofilesattachedtothispost'] = 'Bez příloh';
$string['noimageshavebeenattachedtothispost'] = 'K příspěvku nebyly připojeny žádné obrázky. Před vložením obrázku jej musíte buď nahrát nebo připojit.';
$string['nopostsyet'] = 'Dosud žádné příspěvky.';
$string['noresults'] = 'Nebyly nalezeny žádné příspěvky';
$string['notpublishedblogpost'] = 'Tento příspěvek do deníku ještě nebyl zveřejněn.';
$string['nposts'] = array(

		0 => 'Jeden příspěvek', 
		1 => '%s příspěvky', 
		2 => '%s příspěvků'
);
$string['olderposts'] = 'Starší příspěvky';
$string['pluginname'] = 'Deníky';
$string['post'] = 'příspěvek';
$string['postbody'] = 'Tělo příspěvku';
$string['postbodydesc'] = 'Popis příspěvku';
$string['postedby'] = 'Publikováno';
$string['postedbyon'] = '%s, %s';
$string['postedon'] = 'Publikováno';
$string['posts'] = 'příspěvků';
$string['postscopiedfromview'] = 'Příspěvky zkopírovány z %s';
$string['posttitle'] = 'Název';
$string['progress_blog'] = 'Přidat deník';
$string['progress_blogpost'] = array(

		0 => 'Přidat jeden příspěvek do deníku', 
		1 => 'Přidat %s příspěvky do deníku', 
		2 => 'Přidat %s příspěvků do deníku'
);
$string['publish'] = 'Zveřejnit';
$string['publishblogpost?'] = 'Opravdu chcete zveřejnit tento příspěvek?';
$string['published'] = 'Zveřejněno';
$string['publishfailed'] = 'Vyskytla se chyba. Příspěvek nebyl zveřejněn';
$string['publishit'] = 'Zveřejnit.';
$string['publishspecific'] = 'Zveřejnit "%s"';
$string['remove'] = 'Odebrat';
$string['right'] = 'Obrázek je umístěn k pravému okraji a obtékán zleva (right)';
$string['save'] = 'Uložit';
$string['saveandpublish'] = 'Uložit a zveřejnit';
$string['saveasdraft'] = 'Uložit jako pracovní verzi';
$string['savepost'] = 'Uložit příspěvek';
$string['savesettings'] = 'Uložit nastavení';
$string['settings'] = 'Nastavení';
$string['shortcutadd'] = 'Přidat';
$string['shortcutaddpost'] = 'Přidat nový příspěvek do';
$string['shortcutgo'] = 'Přejít';
$string['shortcutnewentry'] = 'Nový příspěvek';
$string['siteblogs'] = 'Deníky weboých stránek';
$string['siteblogsdesc'] = 'Tvořit a spravovat deníky stránek';
$string['src'] = 'URL obrázku';
$string['taggedposts'] = 'Označit příspěvky';
$string['textbottom'] = 'Text dole';
$string['texttop'] = 'Vršek obrázku je zarovnán s nejvyšším bodem textu (texttop)';
$string['thisisdraft'] = 'Toto je pracovní verze příspěvku';
$string['thisisdraftdesc'] = 'Dokud je příspěvek v pracovní verzi, vidíte jej pouze vy.';
$string['title'] = 'Název';
$string['top'] = 'Vršek obrázku je zarovnán s nejvyšším bodem libovolného objektu řádku (top)';
$string['unpublish'] = 'Zrušit zveřejnění';
$string['unpublishspecific'] = 'Zrušit zveřejnění "%s"';
$string['update'] = 'Aktualizovat';
$string['updatedon'] = 'Naposledy aktualizované';
$string['verticalspace'] = 'Okraj ve svislém směru (vspace)';
$string['viewblog'] = 'Zobrazit deník';
$string['viewbloggroup'] = 'Zobrazit deník "%s"';
$string['viewposts'] = 'Zkopírované příspěvky (%s)';
$string['youarenotamemberof'] = 'Nejste členem skupiny "%s".';
$string['youarenotanadminof'] = 'Nejste správcem instituce "%s".';
$string['youarenotaneditingmemberof'] = 'Nemáte oprávnění upravovat deníky ve skupině "%s".';
$string['youarenotasiteadmin'] = 'Nejste správce webu.';
$string['youarenottheownerofthisblog'] = 'Nejste vlastníkem tohoto deníku.';
$string['youarenottheownerofthisblogpost'] = 'Nejste vlastníkem tohoto příspěvku.';
$string['youhaveblogs'] = 'Počet vašich deníků je %s.';
$string['youhavenblog'] = array(

		0 => 'Máte jeden deník.', 
		1 => 'Máte %d deníky.', 
		2 => 'Máte %d deníků.'
);
$string['youhavenoblogs'] = 'Nemáte žádné deníky.';
$string['youhavenoblogsaddone'] = 'Nemáte žádné deníky. <a href="%s">Založte si nějaký</a>.';
$string['youhavenogroupblogs'] = 'V této skupině nejsou žádné deníky.';
$string['youhavenogroupblogs1'] = 'V této skupině nemáte žádné deníky. <a href="%s">Založte si nějaký</a>.';
$string['youhavenogroupblogsaddone'] = 'V této skupině nejsou žádné deníky. <a href="%s">Vytvořte jej.</a>.';
$string['youhavenoinstitutionblogs'] = 'V  této instituci nejsou žádné deníky.';
$string['youhavenoinstitutionblogs1'] = 'V této instituci nemáte žádné deníky. <a href="%s">Založte si nějaký</a>.';
$string['youhavenositeblogs'] = 'V této síti nejsou žádné deníky.';
$string['youhavenositeblogs1'] = 'Na těchto stránkách nemáte žádné deníky. <a href="%s">Založte si nějaký</a>.';
$string['youhaveoneblog'] = 'Máte jeden deník.';
