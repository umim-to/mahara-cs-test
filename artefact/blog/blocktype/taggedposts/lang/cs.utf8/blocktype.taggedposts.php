<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['blockheading'] = 'Zápisy deníku označené';
$string['blockheadingtags'] = array(

		0 => 'Příspěvky v deníku se štítkem %2$s', 
		1 => 'Příspěvky v deníku se štítky %2$s', 
		2 => 'Příspěvky v deníku se štítky %2$s'
);
$string['blockheadingtagsomitboth'] = array(

		0 => 'ale ne štítek %2$s', 
		1 => 'ale ne štítky %2$s', 
		2 => 'ale ne štítky %2$s'
);
$string['blockheadingtagsomitonly'] = array(

		0 => 'Příspěvky v deníku bez štítku %2$s', 
		1 => 'Příspěvky v deníku bez štítků %2$s', 
		2 => 'Příspěvky v deníku bez štítků %2$s'
);
$string['configerror'] = 'Chyba při konfiguraci deníku';
$string['defaulttitledescription'] = 'Ponecháte-li toto pole prázdné, bude použit název deníku';
$string['description'] = 'Zobrazit příspěvky v deníku s konkrétním štítkem';
$string['excludetag'] = 'vyloučit štítek:';
$string['itemstoshow'] = 'Položky ke zobrazení';
$string['notags'] = 'Žádné příspěvky v deníku označené štítkem "%s"';
$string['notagsavailable'] = 'Nevytvořili jste žádné štítky';
$string['notagsavailableerror'] = 'Nebyl zvolen žádný štítek. Předtím, než zde budete moci vybírat štítky, musíte nějaké přiřadit svým záznamům v deníku.';
$string['notagsboth'] = 'Neexistují žádné příspěvky v deníku se štítkem "%s" a bez štítku "%s"';
$string['notagsomit'] = 'Neexistují žádné příspěvky v deníku se štítkem "%s"';
$string['postedin'] = 'z';
$string['postedon'] = 'na';
$string['postsperpage'] = 'Příspěvky na stránku';
$string['showjournalitemsinfull'] = 'Zobrazit celé příspěvky';
$string['showjournalitemsinfulldesc'] = 'Pokud zaškrtnete, budou zobrazovány celé příspěvky. V opačném případě se budou zobrazovat pouze názvy.';
$string['showjournalitemsinfulldesc1'] = 'Aktivujte tuto volbu pro zobrazení příspěvků v deníku. Jinak se zobrazují pouze jejich nadpisy.';
$string['tag'] = 'Štítek';
$string['taglist'] = 'Moje šítky';
$string['taglistdesc1'] = 'Napište znaménko minus před každý štítek, který chcete vyloučit. Tyto štítky jsou zobrazeny na červeném pozadí.';
$string['title'] = 'Příspěvky v deníku označené štítkem';
$string['updatedon'] = 'Naposledy aktualizováno';
