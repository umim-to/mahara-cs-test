<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2020
 *
 */

defined('INTERNAL') || die();

$string['Annotation'] = 'Anotace';
$string['Annotationfeedback'] = 'Zpětná vazba';
$string['Annotations'] = 'Anotace';
$string['allowannotationfeedback'] = 'Povolit zpětnou vazbu';
$string['annotation'] = 'anotace';
$string['annotationempty'] = 'Toto pole je povinné.';
$string['annotationfeedback'] = 'zpětná vazba';
$string['annotationfeedbackdeletedauthornotification'] = 'Vaše zpětná vazba k %s byla smazána: %s';
$string['annotationfeedbackdeletedhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>Zpětná vazba k anotaci %s byla odstraněna.</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">%s</a></p> </div>';
$string['annotationfeedbackdeletednotificationsubject'] = 'Zpětná vazba k %s smazána.';
$string['annotationfeedbackdeletedtext'] = 'Zpětnou vazbu k anotaci %s odstranil %s
------------------------------------------------------------------------ %s ------------------------------------------------------------------------ K zobrazení %s online klikněte na tento link: %s';
$string['annotationfeedbackempty'] = 'Vaše zpětná vazba je prázdná, prosím vložte zprávu.';
$string['annotationfeedbackisprivate'] = 'Tato zpětná vazba je soukromá.';
$string['annotationfeedbackmadepublic'] = 'Zpětná vazba zveřejněna.';
$string['annotationfeedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s přidal zpětnou vazbu k anotaci %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Reply to this feedback online</a></p> </div>';
$string['annotationfeedbacknotificationtext'] = '%s přidal zpětnou vazbu k anotaci  %s %s------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Zpětnou vazbu můžete zobrazit a reagovat na ni zde: %s';
$string['annotationfeedbacknotinview'] = 'Zpětná vazba %d nenalezena ve stránce %d';
$string['annotationfeedbackremoved'] = 'Zpětná vazba odebrána.';
$string['annotationfeedbacksubmitted'] = 'Zpětná vazba odeslána.';
$string['annotationfeedbacksubmittedmoderatedanon'] = 'Zpětná vazba odeslána, čeká na schválení.';
$string['annotationfeedbacksubmittedprivateanon'] = 'Soukromá zpětná vazba odeslána.';
$string['annotationfeedbackupdated'] = 'Zpětná vazba upravena.';
$string['annotationinformationerror'] = 'Nemáme odpovídající informace k zobrazení anotace.';
$string['annotations'] = 'anotace';
$string['approvalrequired'] = 'Zpětná vazba je moderována. Pokud se rozhodnete ke zveřejnění tohoto příspěvku, nebude viditelný ostatním, dokud jej neschválí vlastník.';
$string['artefactdefaultpermissions'] = 'Výchozí oprávnění anotací';
$string['artefactdefaultpermissionsdescription'] = 'Zvolený druh položky portfolia bude mít po vytvoření povolenou zpětnou vazbu. Uživatelé mohou toto nastavení změnit pro jednotlivé položky portfolia.';
$string['assessmentchangedto'] = 'Hodnocení: %s';
$string['cantedithasreplies'] = 'Můžete reagovat jen na nejnovější zpětnou vazbu.';
$string['canteditnotauthor'] = 'Nejste autorem této zpětné vazby.';
$string['cantedittooold'] = 'Můžete upravovat pouze takovou zpětnou vazbu, která není starší než %d minut.';
$string['commentremovedbyadmin'] = 'Zpětná vazba odebrána správcem.';
$string['commentremovedbyauthor'] = 'Zpětná vazba odebrána autorem.';
$string['commentremovedbyowner'] = 'Zpětná vazba odebrána vlastníkem.';
$string['duplicatedannotation'] = 'Duplicitní anotace';
$string['duplicatedannotationfeedback'] = 'Duplicitní anotace';
$string['editannotationfeedback'] = 'Upravit zpětnou vazbu';
$string['editannotationfeedbackdescription'] = 'Odeslanou zpětnou vazbu můžete pravovat do %d minut od odeslání, pokud na ni ještě nikdo nereagoval. Po této době můžete zpětnou vazbu pouze smazat a odeslat novou.';
$string['enteredon'] = 'vloženo';
$string['entriesimportedfromleapexport'] = 'Záznamy importované z Leap2A exportu, které nemohly být importovány jinam.';
$string['existingannotation'] = 'Existující zpětná vazba';
$string['existingannotationfeedback'] = 'Existující zpětná vazba';
$string['groupadmins'] = 'Správci skupiny';
$string['invalidannotationfeedbacklinkerror'] = 'Zpětná vazba musí být propojená buď s položkou portfolia, nebo stránkou.';
$string['invalidcreateannotationfeedback'] = 'Nelze vytvořit samostatnou zpětnou vazbu.';
$string['makepublic'] = 'Zveřejnit.';
$string['makepublicnotallowed'] = 'Nejste oprávněni zveřejnit tuto zpětnou vazbu.';
$string['makepublicrequestbyauthormessage'] = '%s požádal o zveřejnění své zpětné vazby.';
$string['makepublicrequestbyownermessage'] = '%s požádal o zveřejnění své zpětné vazby.';
$string['makepublicrequestsent'] = '%s byla odeslána zpráva s požadavkem na zveřejnění zpětné vazby.';
$string['makepublicrequestsubject'] = 'Požadavek na změnu soukromé zpětné vazby na veřejnou.';
$string['nannotationfeedback'] = array(

		0 => 'Jedna zpětná vazba',
                1 => '%s zpětné vazby', 
		2 => '%s zpětných vazeb'
);
$string['newannotationfeedbacknotificationsubject'] = 'Nová zpětná vazba k %s';
$string['nofeedback'] = 'Pro tuto anotaci zatím není žádná zpětná vazba.';
$string['noreflectionentryfound'] = 'Nelze najít položku zpětné vazby pro anotaci.';
$string['placeannotation'] = 'Přidat anotaci';
$string['placeannotationfeedback'] = 'Umístit zpětnou vazbu';
$string['pluginname'] = 'Anotace';
$string['private'] = 'Sourkomý';
$string['progress_annotation'] = array(

		0 => 'Přidat jednu anotaci na stránku',
		1 => 'Přidat %s anotace na stránky', 
		2 => 'Přidat %s anotací na stránky'
);
$string['progress_annotationfeedback'] = array(

		0 => 'Dát jednu zpětnou vazbu k anotaci jiného uživatele', 
		1 => 'Dát %s zpětné vazby k anotacím ostatních uživatelů',
                2 => 'Dát %s zpětných vazeb k anotacím ostatních uživatelů'
);
$string['public'] = 'Veřejný';
$string['reallydeletethisannotationfeedback'] = 'Jste si jisti, že chcete odstranit tuto zpětnou vazbu?';
$string['typeannotationfeedback'] = 'Zpětná vazba k anotacím';
$string['unknownstrategyforimport'] = 'Neznámý postup pro import záznamu.';
$string['youhaverequestedpublic'] = 'Zažádali jste o zveřejnění zpětné vazby.';
