<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['cantsendemptysubject'] = 'Prázdný předmět. Prosím, vyplňte předmět.';
$string['cantsendemptytext'] = 'Prázdná zpráva. Prosím, vyplňte obsah zprávy.';
$string['cantsendnorecipients'] = 'Prosím, zvolte alespoň jednoho adresáta.';
$string['clickformore'] = '(Stiskněte \'Enter\' pro více informací)';
$string['composemessage'] = 'Napsat';
$string['composemessagedesc'] = 'Vytvořit novou zprávu';
$string['deletednotifications1'] = 'Odstraněno %s upozornění. Pozor, interní upozornění nemohou být odstraněna z odeslaných.';
$string['deleteduser'] = 'odstranění uživatelé';
$string['fromuser'] = 'Od';
$string['inboxdesc1'] = 'Zprávy přijaté ze systému a od ostatních uživatelů';
$string['labelall'] = 'Vše';
$string['labelinbox'] = 'Doručené';
$string['labelmessage'] = 'Zpráva';
$string['labeloutbox1'] = 'Odeslané';
$string['labelrecipients'] = 'Pro:';
$string['labelsearch'] = 'Hledat';
$string['labelsubject'] = 'Předmět:';
$string['linkindicator'] = '»';
$string['multirecipientnotificationnotenabled'] = '\'Modul "multirecipientnotifications" musí být nainstalovaný a aktivní. Pokud přecházíte na novou verzi Mahary z verze starší než 16.10.0, proveďte prosím jako první přechod na ni a ujistěte se, že tento modul je nainstalovaný a aktivní. Tuto informaci najdete v "Administrátorské menu" → "Rozšíření" → "Správa modulů".';
$string['notification'] = 'Upozornění';
$string['outboxdesc'] = 'Zprávy odeslané ostatním uživatelům';
$string['pluginname'] = 'Oznámení pro více příjemců';
$string['removeduserfromlist'] = 'Uživatel, který od vás nemůže přijímat zprávy, byl odebrán se seznamu adresátů.';
$string['reply'] = 'Odpovědět';
$string['replyall'] = 'Odpovědět všem';
$string['replybuttonplaceholder'] = '...';
$string['replysubjectprefix'] = 'Re:';
$string['selectalldelete'] = 'Všechna upozornění ke smazání';
$string['selectallread'] = 'Všechna nepřečtená upozornění';
$string['sendmessageto'] = 'Odeslat zprávu';
$string['subject'] = 'Předmět';
$string['titlerecipient'] = 'Adresáti';
$string['titlesubject'] = 'Předmět';
$string['touser'] = 'Pro';
