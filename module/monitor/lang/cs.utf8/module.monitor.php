<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['accessdeniednotvalidip'] = 'Vaše IP "%s" není na seznamu povolených IP adres a bude vám zablokována možnost sledovat "monitor".';
$string['allowedips'] = 'Povolené IP adresy';
$string['allowedipsdescription'] = 'Vložte IP adresy, vždy jednu na řádek, na které má monitoring považovat za bezpečné. Pokud pole zůstane prázdné, monitoring bude přístupný s parametrem $cfg->urlsecret, je-li nastaven.';
$string['checkingcronprocesses'] = 'Kontrola procesů cronu';
$string['checkingcronprocessesfail'] = array(

		0 => 'Kritická chyba: %s dlouho běžící proces cronu pro %s: %s', 
		1 => 'Kritická chyba: %s dlouho běžící procesy cronu pro %s: %s', 
		2 => 'Kritická chyba: %s dlouho běžících procesů cronu pro %s: %s'
);
$string['checkingcronprocessessucceed'] = 'OK: Žádné dlouho běžící procesy cronu pro %s.';
$string['checkingelasticsearchfailedrecords'] = 'Kritická chyba: Ve frontě Elasticsearch jsou více jak hodinu nezpracované záznamy';
$string['checkingelasticsearcholdunprocesessedfail'] = array(

		0 => 'Kritická chyba: Ve frontě Elasticsearch jsou nezpracované záznamy starší než %s hodinu.', 
		1 => 'Kritická chyba: Ve frontě Elasticsearch jsou nezpracované záznamy starší než %s hodiny.', 
		2 => 'Kritická chyba: Ve frontě Elasticsearch jsou nezpracované záznamy starší než %s hodin.'
);
$string['checkingelasticsearchsucceed'] = 'OK: Ve fronte Elasticsearch nejsou žádné nezpracované záznamy nebo záznamy o selhání';
$string['checkingldapinstancesfail'] = array(

		0 => 'Kritická chyba: %s neplatné LDAP spojení pro %s: %s', 
		1 => 'Kritická chyba: %s neplatné LDAP spojení pro %s: %s', 
		2 => 'Kritická chyba: %s neplatných LDAP spojení pro %s: %s'
);
$string['checkingldapinstancessucceed'] = 'OK: Žádné neplatné spojení LDAP pro %s.';
$string['checkingldapsuspendedusersfail'] = array(

		0 => 'Kritická chyba: %s LDAP instance pro %s překročila limit pro varování pozastavených uživatelů: %s', 
		1 => 'Kritická chyba: %s LDAP instance pro %s překročily limit pro varování pozastavených uživatelů: %s', 
		2 => 'Kritická chyba: %s LDAP instancí pro %s překročilo limit pro varování pozastavených uživatelů: %s'
);
$string['checkingldapsuspendedusersssucceed'] = 'OK: Nejsou žádné instance LDAP pro %s, které by překročily limit pro varování ohledně pozastavených uživatelů.';
$string['clistatuscritical'] = 'Kritická chyba';
$string['clistatusok'] = 'OK';
$string['configmonitortype_searchhoursuntilolddescription'] = 'Počet hodin, jak dlouho může zůstat vyhledávací záznam nezpracovaný aniž by na tento fakt bylo upozorněno.';
$string['configmonitortype_searchhoursuntiloldtitle'] = 'Hodiny, kdy začne být fronta stará';
$string['configmonitortype_searchlegend'] = 'Nastavení vyhledávání';
$string['croncheckhelp'] = 'Zjistěte, jaké procesy cronu běží dlouho: croncheck.php [možnosti] mahara_path Možnosti: -h, --help Zobrazit tuto nápovědu Příklad: sudo -u www-data /usr/bin/php croncheck.php /var/www/mymaharaproject';
$string['cronlockhours'] = 'Počet hodin běhu cronu';
$string['cronlockhoursdescription'] = 'Maximální počet hodin, během kterých by měl probíhat cronový proces.';
$string['datestarted'] = 'Čas začátku';
$string['details'] = 'Podrobnosti';
$string['displaydatetimeformat'] = 'd/m/Y H:i:s';
$string['elasticsearch'] = 'Elasticsearch';
$string['elasticsearchcheckhelp'] = 'Zkontrolujte frontu procesů elasticsearch: elasticsearchcheck.php [možnosti] mahara_path Možnosti: -h, --help Zobrazit tuto nápovědu Příklad: sudo -u www-data /usr/bin/php elasticsearchcheck.php /var/www/mymaharaproject';
$string['exportresultscsv'] = 'Exportovat výsledky ve formátu CSV';
$string['failedqueuesize'] = 'Počet neúspěšných záznamů za více než 1 hodinu.';
$string['hourstoconsiderelasticsearchrecordold'] = 'Počet hodin pro stáří fronty elasticsearch';
$string['hourstoconsiderelasticsearchrecordolddescription'] = 'Počet hodin, po které může zůstat záznam v elasticsearch nezpracovaný bez upozornění';
$string['institution'] = 'Instituce';
$string['item'] = 'Položka';
$string['ldapauthority'] = 'Jméno LDAP autority';
$string['ldapcheckhelp'] = 'Zjistěte, které instance LDAP nefungují: ldaplookupcheck.php [možnosti] mahara_path Možnosti: -h, --help Zobrazte si tuto nápovědu Příklad: sudo -u www-data /usr/bin/php ldaplookupcheck.php /var/www/mymaharaproject';
$string['ldaplookup'] = 'Vyhledávání LDAP';
$string['ldapstatus'] = 'Stav';
$string['ldapstatusfail'] = 'Kontrola LDAP selhala s následující chybou: %d';
$string['ldapstatusmessage'] = 'Informace o stavu';
$string['ldapstatussuccess'] = 'Kontrola LDAP proběhla úspěšně';
$string['ldapstatustabletitle'] = 'Stav LDAP';
$string['ldapsuspendedusers'] = 'Pozastavení uživatelé LDAP';
$string['ldapsuspendeduserscheckhelp'] = 'Zkontrolujte, zda se v procesu synchronizace uživatelů LDAP nepřekročí velké objemy ldapsuspendeduserscheck.php [možnosti] mahara_path Možnosti: -h, --help Zobrazí tuto nápovědu Příklad: sudo -u www-data /usr/bin/php ldapsuspendeduserscheck.php /var/www/mymaharaproject';
$string['ldapsuspendeduserspercentage'] = 'Procento pozastavených uživatelů LDAP';
$string['ldapsuspendeduserspercentagedescription'] = 'Maximální procento uživatelů pozastavených v rámci synchronizace uživatelů LDAP od půlnoci před oznámením problému.';
$string['ldapsuspendeduserstabletitle'] = 'Procento uživatelů LDAP pozastavených v rámci synchronizace od půlnoci';
$string['longrunningprocesses'] = 'Dlouho běžící procesy';
$string['monitor'] = 'Monitoring';
$string['monitoringchecks'] = 'Monitoring kontrol';
$string['monitormodulenotactive'] = 'Monitorovací plugin není aktivní. Pro jeho instalaci či aktivaci přejděte prosím do “Administrátorské menu” → “Rozšíření” → “Správa modulů”.';
$string['no'] = 'Ne';
$string['okmessagedisabled'] = 'Pokud je nastaveno, nebude zobrazena žádná potvrzující zpráva.';
$string['processes'] = 'Procesy cronu';
$string['processname'] = 'Proces';
$string['queuehasolditems'] = array(

		0 => 'Nezpracované položky ve frontě Elasticsearch starší než %s hodinu.', 
		1 => 'Nezpracované položky ve frontě Elasticsearch starší než %s hodiny.', 
		2 => 'Nezpracované položky ve frontě Elasticsearch starší než %s hodin.'
);
$string['queuestatus'] = 'Stav fronty';
$string['searchcheckhelp'] = 'Zkontrolujte frontu zpracování vyhledávání: searchcheck.php [options] mahara_path Možnosti: -h, --help Zobrazit tuto nápovědu Příklad: sudo -u www-data /usr/bin/php searchcheck.php /var/www/mymaharaproject';
$string['status'] = 'Stav';
$string['statusfail'] = 'Selhalo';
$string['statussuccess'] = 'Ok';
$string['unprocessedqueuesize'] = 'Celkový počet nezpracovaných záznamů';
$string['yes'] = 'Ano';
