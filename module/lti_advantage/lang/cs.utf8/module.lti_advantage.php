<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['client_id'] = 'ID klienta';
$string['confirmareyousure'] = 'Jste si jisti?';
$string['confirmbtntxtcancel'] = 'Zrušit';
$string['confirmbtntxtconfirm'] = 'Potvrdit';
$string['confirmwarning'] = 'Pokud odešlete "%s" k hodnocení, nebudete mít již možnost upravovat jeho obsah, dokud nebude dokončeno známkování. Jste si jisti, že chcete toto portfolio nyní odevzdat?';
$string['deeplinknoportfolios'] = 'Nemáte žádné portfolio.';
$string['deeplinkportfoliostitle'] = 'Portfolia';
$string['deployment1_description'] = 'Pokud má vaše platforma pouze jedno ID nasazení, použijte toto pole.';
$string['deployment1_idalreadyinuse'] = 'Toto ID nasazení je již používáno.';
$string['deployment1_title'] = 'Hlavní ID nasazení';
$string['deployment2_description'] = 'Vložte další ID nasazení, pokud je potřeba, např. pokud chcete rozdělit nasazení pro NRPS nebo zanořené odkazy.';
$string['deployment2_idalreadyinuse'] = 'Toto ID nasazení je již používáno.';
$string['deployment2_title'] = 'ID dalšího nasazení 1';
$string['deployment3_description'] = 'Vložte další ID nasazení, pokud je potřeba, např. pokud chcete rozdělit nasazení pro NRPS nebo zanořené odkazy.';
$string['deployment3_idalreadyinuse'] = 'Toto ID nasazení je již používáno.';
$string['deployment3_title'] = 'ID dalšího nasazení 2';
$string['deployment_id'] = 'ID nasazení';
$string['deploymentidcannotbesame'] = 'Hodnoty polí ID nasazení nemohou být totožné.';
$string['deployments'] = 'Nasazení';
$string['deploymentsdesc'] = 'ID nasazení z LTI platformy';
$string['domain'] = 'Doména';
$string['groupname'] = '%s';
$string['issuer'] = 'Vydavatel';
$string['issueralreadyinuse'] = 'Vydavatel je již nastaven na spojení jiného klienta';
$string['keyset'] = 'URL sady klíčů';
$string['ltiserviceexists'] = 'Servisní skupina LTI Advantage je registrovaná.';
$string['maharaltiadvantage'] = 'LTI Advantage';
$string['nullprivatecert'] = 'Nepodařilo se vygenerovat nebo uložit privátní klíč';
$string['nullpubliccert'] = 'Nepodařilo se vygenerovat nebo uložit veřejný certifikát';
$string['oauth2'] = 'OAuth2';
$string['openidconnectlogin'] = 'URL pro OpenID Connect přihlášení';
$string['platform_auth_provider'] = 'Publikum platformy OAuth2';
$string['platform_jwks_endpoint'] = 'URL sady klíčů platformy';
$string['platform_login_auth_endpoint'] = 'OpenID Connect autentizační koncový bod';
$string['platform_service_auth_endpoint'] = 'URL přístupového tokenu platformy OAuth2';
$string['platformvendorissnotfound'] = 'Platforma nezaslala \'iss\' ve vrácených datech. Klíč chybí.';
$string['platformvendorkeycannotbeempty'] = 'Prosím zvolte nebo vložte poskytovatele platformy';
$string['platformvendorkeyinvalid'] = 'Klíč poskytovatele platformy nebyl v požadavku nalezen. Klíče ve vráceném požadavku zahrnují: %s';
$string['platformvendorkeynotfound'] = '\'iss\' (%s) z platformy nemá příslušného \'Vydavatele\' v databázové tabulce  \'lti_advantage_registration\'.';
$string['platformvendorkeyoptionbrightspace'] = 'Brightspace';
$string['platformvendorkeyoptionnone'] = 'Vyberte platformu';
$string['platformvendorkeytitle'] = 'Poskytovatel platformy';
$string['redirecturl'] = 'Přesměrované URL';
$string['short_name'] = 'Stručné jméno';
$string['short_namecannotbeempty'] = 'Stručné jméno nesmí být prázdné.';
$string['short_namedescription'] = 'Maximáně 30 znaků. Toto stručné jméno je jméno poskytovatele LTI, který bude zobrazován ve sloupci "Komu odevzdáno" v tabulce Aktuální příspěvky.';
$string['usercreationupdate'] = 'Členové skupiny budou brzy aktualizováni. Prosím, vraťte se zpátky za několik minut.';
