<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Vše';
$string['Commented'] = 'Okomentováno';
$string['Completed'] = 'Dokončeno';
$string['Date'] = 'Datum';
$string['Evaluator'] = 'Hodnotitel';
$string['Fail'] = 'Neprospěl(a)';
$string['Feedback'] = 'Zpětná vazba';
$string['Group'] = 'Skupina';
$string['MissingTask'] = 'Chybějící úkol';
$string['Name'] = 'Jméno';
$string['Off'] = 'Jakýkoli';
$string['Open'] = 'Otevřený';
$string['Pending'] = 'Chybí';
$string['Portfolio'] = 'Portfolio';
$string['PreferredName'] = 'Zobrazované jméno';
$string['ProceedWithBackRestrictionsToExistingTab'] = 'Odevzdané portfolio je již otevřeno na jiné kartě. Můžete do tohoto portfolia přejít, ale nebude vyznačeno v tabulce “Odevzdané příspěvky” až se vrátíte. To samé platí pro ono portfolio na jiné kartě. Přejete si pokračovat?';
$string['ProceedWithoutFlashbackFunctionality'] = 'Odevzdané portfolio je již otevřeno na jiné kartě. Můžete do tohoto portfolia přejít, ale nebude vyznačeno v tabulce “Odevzdané příspěvky”, když se pak vrátíte pomocí tlačítka “Zpět” v prohlížeči. Přejete si pokračovat?';
$string['Rating'] = 'Hodnocení';
$string['Reset'] = 'Obnovit';
$string['Result'] = 'Výsledek';
$string['Revision'] = 'K opravě';
$string['Role'] = 'Role';
$string['Search'] = 'Vyhledat';
$string['Select'] = 'Vybrat';
$string['State'] = 'Stav';
$string['Submissions'] = 'Odevzdané příspěvky';
$string['Success'] = 'Prospěl(a)';
$string['Task'] = 'Úkol';
$string['Unassigned'] = 'Nezadáno';
$string['Uncommented'] = 'Neokomentováno';
$string['actionfixed'] = 'opraveno';
$string['actionreleased'] = 'uvolněno';
$string['assessor'] = 'Hodnotitel';
$string['chooseresult'] = 'Zvolte výsledek';
$string['colvislabel'] = 'Nastavit sloupce';
$string['completed'] = 'Uvolněno';
$string['dependingonstatusmayeditupdatedfields'] = 'V závislosti na novém stavu nyní můžete upravovat aktualizovaná pole.';
$string['displayportfolio'] = 'Zobrazit portfolio';
$string['emptytable'] = 'V tabulce nejsou dostupná žádná data';
$string['evaluated'] = 'Ohodnoceno';
$string['eventgroupidnotfound'] = 'Skupina s ID "%s" mezi daty nenalezena.';
$string['eventgroupnotfound'] = 'Skupina "%s“ mezi daty nenalezena.';
$string['fail'] = 'Neprospěl(a)';
$string['first'] = 'První stránka';
$string['fix'] = 'Opravit';
$string['fixsubmission'] = 'Opravit příspěvek?';
$string['info'] = '_START_až_KONEC_VŠECH_ příspěvků';
$string['infoempty'] = '0 záznamů';
$string['last'] = 'Poslední stránka';
$string['lastsubmissionnoevaluationresultcontactinstructor'] = 'Vaše poslední odevzdání tohoto portfolia nebo skupinového úkolu nemá ještě výsledek hodnocení. Pro vyřešení tohoto problému kontaktujte svého instruktora. Váš odevzdaný příspěvek byl uvolněn automaticky.';
$string['lengthmenu'] = 'Ukázat záznamy _MENU_';
$string['loading'] = 'Načítání';
$string['missingcontrollerhandler'] = 'Chybějící sezení pro příkaz ovladače.';
$string['next'] = 'Další stránka';
$string['noresult'] = 'K opravě';
$string['notallowedtoassesssubmission'] = 'Nemáte povolení hodnotit tento odevzdaný příspěvek.';
$string['notevaluated'] = 'Nehodnoceno';
$string['pluginname'] = 'Odevzdané příspěvky';
$string['portfolioalreadysubmitted'] = 'Portfolio k tomuto úkolu již bylo odevzdáno. Před jeho opětovným odevzdáním musíte počkat na jeho uvolnění.';
$string['portfoliocurrentlybeingreleased'] = 'Portfolio k tomuto úkolu je právě uvolňováno. Před jeho opětovným odevzdáním musíte počkat, než bude kompletně uvolněno.';
$string['portfoliofieldsmustbesetforsettingtaskfields'] = 'Pole na položky portfolia musí být nastavena, aby bylo možné nastavit pole pro úkoly pro odevzdávané příspěvky.';
$string['portfolioortaskalreadyevaluated'] = 'Portfolio nebo skupinový úkol byl již ohodnocen. Váš odevzdaný příspěvek byl automaticky uvolněn.';
$string['previous'] = 'Předchozí stránka';
$string['processing'] = 'Zpracovávání';
$string['quickfilter'] = 'Rychlý filtr';
$string['quickfiltertooltip'] = 'Přenastavit filtry';
$string['release'] = 'Bude uvolněno';
$string['releasesubmission'] = 'Dokončit hodnocení a uvolnit odevzdané portfolio?';
$string['releasing'] = 'Uvolňování....';
$string['retentionperiod'] = 'Doba uchovávání pro příspěvky a archivovaná portfolia';
$string['retentionperioddescription'] = 'Časový úsek v letech počítaných od roku následujícího po uvolnění portfolia, po jehož uplynutí bude portfolio a s ním spojená archivovaná portfolia smazána. Pokud je tato hodnota nastavena na nulu, všechny příspěvky a s nimi spojená portfolia budou smazána po smazání autorova účtu.';
$string['revision'] = 'Kontrola';
$string['search'] = 'Vyhledat';
$string['searchplaceholder'] = 'Zadejte hledaný pojem…';
$string['shownameaslastnamefirstname'] = 'Zobrazovat jméno jako "příjmení, křestní jméno"';
$string['shownameaslastnamefirstnamedescription'] = 'Zobrazovat všechny výskyty jmen v tabulce příspěvků ve formátu "příjmení, křestní jméno"';
$string['showportfoliobuttons'] = 'Zobrazovat odkazy portfolia jako tlačítka';
$string['showportfoliobuttonsdescription'] = 'Změnit ze standardního nastavení Mahary, aby se odkazy v portfoliu zobrazovaly jako tlačítka, kvůli lepší optické diferenciaci funkce "stáhnout jako archiv" v tabulce "příspěvků".';
$string['sortascending'] = ': aktivujte pro seřazení sloupce vzestupně';
$string['sortdescending'] = ': aktivujte pro seřazení sloupce sestupně';
$string['submissionexceptionmessage'] = 'Portfolio nemůže být odevzdáno k hodnocení z následujícího důvodu:';
$string['submissionexceptiontitle'] = 'Nebylo možné portfolio odevzdat k hodnocení';
$string['submissionnotcreated'] = 'Příspěvek nevytvořen:';
$string['submissionnotcreatedorupdated'] = 'Příspěvek nevytvořen nebo neaktualizován:';
$string['submissionnotfixedmissingevaluationresult'] = 'Příspěvek neopraven kvůli chybějícímu výsledku hodnocení.';
$string['submissionnotreleasedorfixedwrongsubmissionstatus'] = 'Příspěvek neuvolněn či neopraven kvůli špatnému stavu ohledně odevzdání.';
$string['submissionreadonlynotupdated'] = 'Příspěvek neaktualizován kvůli stavu odevzdání pouze ke čtení.';
$string['submissionreleased'] = 'Příspěvek "%s" od autora %s %s.';
$string['submissionstatuschangedexternally'] = 'Stav odevzdání byl změněn externě.';
$string['submissionstitlegroup'] = '%s - Příspěvky';
$string['submitted'] = 'Odevzdáno';
$string['submitter'] = 'Autor';
$string['success'] = 'Prospěl(a)';
$string['thousands'] = ',';
$string['tooltip_question'] = 'Neoznámkováno';
$string['tooltip_refresh'] = 'K opravě';
$string['tooltip_remove'] = 'Neprospěl(a)';
$string['tooltip_success'] = 'Prospěl(a)';
$string['unassignedselectboxitem'] = '- Nezadáno -';
$string['unsupportedportfoliotype'] = 'Nepodporovaný typ položky v odevzdávaném portfoliu.';
$string['unsupportedsubmissionownertype'] = 'Nepodporovaný typ vlastníka odevzdávaného portfolia.';
$string['zerorecords'] = 'Žádné záznamy';
