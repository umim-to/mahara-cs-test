<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Begun'] = 'Započato';
$string['Completed'] = 'Dokončeno';
$string['Framework'] = 'Rámec SmartEvidence';
$string['Incomplete'] = 'Nedokončeno';
$string['Management'] = 'Správa';
$string['Partialcomplete'] = 'Částečně dokončeno';
$string['accessdeniednoframework'] = 'Tato sbírka nemůže zobrazit stránku SmartEvidence. To může být způsobeno některým z následujících důvodů: <ul><li>Zásuvný modul SmartEvidence není nainstalován nebo aktivní.</li> <li>Vaše instituce SmartEvidence nepovoluje.</li> <li>Tato sbírka nemá nastavený konkrétní rámec.</li> <li>V této sbírce nejsou žádné stránky.</li> </ul>';
$string['activeframework'] = 'Aktivní rámec';
$string['addannotation'] = 'Přidat anotaci pro normu "%s" ke stránce "%s"';
$string['addframework'] = 'Přidat rámec';
$string['addpages'] = 'Přidejte více stránek do této sbírky, pokud chcete, aby se objevily zde v přehledu SmartEvidence.';
$string['addspecific'] = 'Přidat %s';
$string['addstandard'] = 'Přidat standardní';
$string['addstandardelement'] = 'Přidat standardní prvek';
$string['all'] = 'vše';
$string['annotationclash'] = 'Na stránce se již blok anotace pro toto kritérium nachází.';
$string['assessment'] = 'Hodnocení';
$string['assessmenttypecount'] = 'Počet stránek obsahujících daný prvek standardu';
$string['begun'] = 'Připraveno k hodnocení';
$string['changeframeworkproblems'] = 'Nemůžete změnit rámec. Následující stránky obsahují výsledky spojené s tímto rámcem:';
$string['collapse'] = 'Sbalit';
$string['collapsedsection'] = 'Tabulková část pro tuto normu je schovaná.';
$string['collapseform'] = 'Sbalit formulář %s';
$string['collapseframework'] = 'Sbalit rozhraní SmartEvidence';
$string['collapsesection'] = 'Klikněte pro schování části pro standard "%s".';
$string['collapsespecific'] = 'Sbalit %s';
$string['completed'] = 'Splňuje kritérium';
$string['completedcount'] = 'Počet stránek, které obsahují kompletní standardní element:';
$string['copyexistingframework'] = 'Zkopírovat existující rámec';
$string['copyframework'] = 'Vybrat rámec ke zkopírování';
$string['copyframeworkdescription'] = 'Můžete zkopírovat kterýkoli nainstalovaný rámec a použít jej jako základ pro nový soubor rámce.';
$string['defaultdescription'] = 'Popis vašeho rámce';
$string['delete'] = 'Odstranit %s';
$string['deleteall'] = 'Odstranit vše';
$string['deleteallspecific'] = 'Smazat všechny %s';
$string['deleteallstandardelements'] = 'Smazat všechny standardní prvky';
$string['deletelast'] = 'Odstranit poslední';
$string['descriptioninfo'] = 'Napište více informací popisujících daný rámec. Můžete použít jednoduché HTML.';
$string['descstandard'] = 'Tento popis se objeví při přejetí myší přes tento standard na stránce SmartEvidence. Můžete použít jednoduché HTML.';
$string['descstandarddefault'] = 'Popis této skupiny standardů';
$string['displaystatusestitle'] = 'Zobrazit hodnotící statusy';
$string['displaystatusestitledetail'] = 'Rozhodněte, který z hodnotících statusů chcete zobrazit';
$string['editdescription1'] = 'Rámec musí být neaktivní a nesmí být aktuálně používaný v žádné sbírce, aby mohl být upravován.';
$string['editdescription2'] = 'Pokud upravíte rámec, změníte tím jeho uložená data.';
$string['editframework'] = 'Vyberte rámec k úpravě';
$string['editor'] = 'Editor';
$string['editsavedframework'] = 'Upravit uložený rámec';
$string['elementid'] = 'ID prvku';
$string['elementiddesc'] = 'Toto je ID pro tento prvek standardu. Pořadí čísel reprezentuje jejich hierarchii.';
$string['evidencedesc'] = 'Pojmenujte různé stavy, které vyjadřují, jak moc je hotová která část rámce. Jsou zde 4 možnosti s tím, že "Započato" vyjadřuje, že důkazy byly odeslány. Ostatní 3 stavy jsou hodnotící stavy.';
$string['evidencestatuses'] = 'Stavy hodnocení';
$string['expand'] = 'Rozbalit';
$string['expandform'] = 'Rozbalit formulář %s';
$string['firstviewlink'] = 'Přejděte prosím na <a href="%s">první stránku</a> sbírky.';
$string['frameworkdesc'] = 'Vyberte rámec kompetencí, který chcete přiřadit k vašemu portfoliu.';
$string['frameworkmissing'] = 'Rámec nebyl nazelen';
$string['frameworknav'] = 'SmartEvidence';
$string['frameworks'] = 'Nainstalované rámce';
$string['frameworksdesc'] = 'Seznam rámců nainstalovaných v systému. Neaktivní rámce nejsou zobrazeny, pokud je dosud z minulosti nevyužívá nějaká sbírka.';
$string['frameworktitle'] = 'Název vašeho rámce';
$string['frameworkupdated'] = 'Rámec aktualizován';
$string['gonextpages'] = 'Klikněte na tlačítko "Další", aby se ve SmartEvidence zobrazilo více stránek.';
$string['goprevpages'] = 'Klikněte na tlačítko "Předchozí", aby se ve SmartEvidence zobrazily stránky, které byly do sbírky zařazeny dříve.';
$string['headercompletedcount'] = 'Hlavička sloupce: Počet stránek, které obsahují dokončený prvek standardu';
$string['headerelements'] = 'Hlavička sloupce: Prvky standardu';
$string['headernotmatchcount'] = 'Hlavička sloupce: Počet stránek, které obsahují nedokončený prvek standardu';
$string['headerpage'] = 'Hlavička sloupce: Název stránky';
$string['headerpartiallycompletecount'] = 'Hlavička sloupce: Počet stránek, které obsahují částečně dokončený prvek standardu';
$string['headerreadyforassessmentcount'] = 'Hlavička sloupce: Počet stránek, které obsahují  prvek standardu připravený pro ohodnocení';
$string['headerrow'] = 'Hlavička řádku: Prvek standardu';
$string['incomplete'] = 'Nesplňuje kritérium';
$string['instdescription'] = 'Vyberte instituci, která má mít povoleno používání tohoto rámce SmartEvidence. Použití můžete vyhradit jedné instituci nebo dát přístup k rámci všem..';
$string['invalidjson'] = 'Toto není platný soubor .matrix: nelze zpracovat obsah JSON.';
$string['invalidjsonineditor'] = 'Aktuální formulář má neplatný json. Prosím, sjeďte do spodní části stránky pro zobrazení konkrétního popisu chyby.';
$string['jsondatanotsubmitted'] = 'Vaše data z tohoto formuláře nebyly uloženy do databáze. Prosím, uložte si vyplněné informace do vedlejšího souboru, aktualizujte stránku s formulářem a zkuste to znovu. Případně nahrajte json přímo za použití záložky "Nahrát".';
$string['jsonmissingvars'] = 'Toto není platný soubor .matrix: Chybí "framework" a / nebo "framework name".';
$string['manuallyremovematrices'] = 'Nepodařilo se odstranit instalační adresář "% s". Odeberte jej ručně.';
$string['matrixfile'] = 'Soubor s maticí';
$string['matrixfiledesc'] = 'Soubor .matrix obsahující rámec zakódovaný ve formátu JSON';
$string['matrixfilenotfound'] = 'Nebyl vybrán žádný platný soubor .matrix.';
$string['matrixpointinserted'] = 'SmartEvidence přidán';
$string['matrixpointupdated'] = 'SmartEvidence upraven';
$string['missingparamframeworkid'] = 'ID parametru rámce chybí';
$string['missingrecordsdb'] = 'Následující záznamy nebyly nalezeny v databázi: %s';
$string['moveleft'] = 'Přesunout doleva';
$string['moveleftspecific'] = 'Přesunout %s doleva';
$string['moveright'] = 'Přesunout doprava';
$string['moverightspecific'] = 'Přesunout %s doprava';
$string['needtoactivate'] = 'Zásuvný modul "annotation" musí být aktivní. Obraťte se prosím na správce webu, aby tak učinil.';
$string['noannotation'] = 'Na stránce "%s" nejsou žádné anotace pro prvek standardu "%s".';
$string['noframeworkfoundondb'] = 'Rámec nebyl v databázi nalezen';
$string['noframeworkselected'] = 'Žádný';
$string['notvalidmatrixfile'] = 'Toto není platný soubor .matrix.';
$string['parentelementdesc'] = 'Pro vytvoření prvku, který patří v hierarchii pod aktuální  prvek standardu, jej zvolte jako rodiče.';
$string['parentelementid'] = 'ID rodičovského prvku';
$string['partialcomplete'] = 'Částečně splňuje kritérium';
$string['removestandardorelementconfirm'] = 'Jste si jisti, že chcete tuto položku odstranit? Pokud ji odstraníte, pak standardní položky s ní spojené budou odstraněny také.';
$string['savematrix'] = 'Nahrát matici';
$string['selfassess'] = 'Ohodnotit se';
$string['selfassessed'] = 'Sebehodnocení';
$string['selfassesseddescription'] = 'Rozhodněte, zda mohou hodnotit zaměstnanci (výchozí) nebo zda mohou autoři portfolií používat tento standard k sebehodnocení a sami si volit stavy hodnocení.';
$string['shortnamestandard'] = 'Krátký název pro tuto skupinu standardů. Limit je 100 znaků.';
$string['showelementdetails'] = 'Klikněte pro zobrazení podrobností o prvku standardu.';
$string['standard'] = 'Kritérium';
$string['standardbegin'] = 'Začátek standardní části';
$string['standarddesc'] = 'Vyberte kritérium pro tento výsledek. Text můžete zadávat přímo do pole pro vyhledávání kritérií.';
$string['standardelement'] = 'Prvek standardu';
$string['standardelementdefault'] = 'Popis prvku standardu';
$string['standardelementdesc'] = 'Tento popis se objeví při přejetí myší přes tento prvek standardu na stránce SmartEvidence. Bude také zobrazen při výběru tohoto standardu v bloku anotací. Můžete použít jednoduché HTML.';
$string['standardelements'] = 'Prvky standardu';
$string['standardelementsdescription'] = 'Vytvořte jednotlivé prvky standardů, například popisy, ke kterým bude možno přiřadit obsah. Prvky standardů mohou být v případě potřeby hierarchicky uspořádány pomocí možnosti "Rodičovské ID".';
$string['standardid'] = 'ID standardu';
$string['standardiddesc'] = 'Mělo by jít o celé číslo.';
$string['standardiddesc1'] = 'Vyberte standard, jehož je tento prvek standardu součástí.';
$string['standards'] = 'Standardy';
$string['standardsdescription'] = 'Vytvořte skupiny standardů, na které bude váš rámec rozdělen. Později můžete těmto skupinám přiřadit jednotlivé prvky standardu a jejich pod-prvky. Musíte vytvořit minimálně jednu skupinu standardů.';
$string['statusdetail'] = 'Stránka "%s": %s';
$string['studentannotation'] = 'Anotace:';
$string['successmessage'] = 'Váš rámec byl odeslán';
$string['tabledesc'] = 'Níže je struktura pro matici SmartEvidence sbírky.';
$string['taskscompleted'] = 'Úlohy dokončeny';
$string['titledesc'] = 'Název rámce by měl být krátký. Je zobrazován jako název na stránce SmartEvidence, stejně jako ve vysouvacím menu, kde si lidé volí rámec.';
$string['titlestandard'] = 'Název této skupiny standardů. Limit je 255 znaků.';
$string['uncollapsesection'] = 'Klikněte pro zobrazení tabulkové oblasti pro standard "%s".';
$string['upgradeplugin'] = 'Pro zpřístupnění tohoto nastavení je potřeba aktualizovat SmartEvidence plugin.';
$string['uploadframeworkdesc1'] = 'Podívejte se do <a href="https://git.mahara.org/mahara/mahara/blob/16.10_STABLE/test/behat/upload_files/example.matrix">GIT úložiště Mahary</a> a na <a href="http://manual.mahara.org/en/16.10/administration/smartevidence.html#create-a-framework-file">uživatelskou příručku Mahary</a> pro vysvětlení jednotlivých komponent.';
$string['uploadframeworkdesc2'] = 'Nahrajte soubor .matrix s JSON kódováním. Můžete nahlédnout do <a href="https://git.mahara.org/mahara/mahara/-/blob/%s/test/behat/upload_files/example.matrix">git repozitáře Mahary</a> na příklady zápisu a do <a href="%s">manuálu Mahary</a> pro vysvětlení jednotlivých částí.';
$string['usedincollections'] = 'Použito ve sbírkách';
$string['validjson'] = 'Aktuální obsah formuláře je platný a je možné jej odeslat.';
