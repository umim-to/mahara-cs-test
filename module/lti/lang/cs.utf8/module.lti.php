<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['archive'] = 'Archivovat po oznámkování';
$string['archivedescription'] = 'Po přidělení hodnocení bude vytvořen snímek aktuální podoby portfolia.';
$string['autoconfiguredesc'] = 'Automaticky povolí nastavení a provede konfigurace potřebnou pro LTI.';
$string['autoconfiguretitle'] = 'Automatická konfigurace LTI';
$string['autocreateusers'] = 'Automaticky vytvářet uživatele';
$string['autocreationnotenabled'] = 'Automatické vytváření uživatelských účtů není povoleno';
$string['autocreationnotenabledredirect'] = 'Nepodařilo se vám se přihlásit, protože vaše instituce nepovoluje vytvoření účtu přes LTI. Pokud již máte účet, můžete se přihlásit přes běžný přihlašovací formulář. Pokud máte jakékoliv otázky, obraťte se prosím na správce těchto webových stránek.';
$string['configstep'] = 'Položka konfigurace';
$string['configstepstatus'] = 'Stav';
$string['configuration'] = 'Nastavení hodnocení';
$string['configurationintro'] = 'Můžete měnit nastavení hodnocení dokud nikdo neodevzdá své portfolio. Jakmile portfolio někdo odevzdá, tato možnost bude uzamčena.';
$string['configurationsaved'] = 'Nastavení hodnocení uloženo';
$string['emailtutors'] = 'Zasílat učitelům email, když student odevzdá práci';
$string['grade'] = 'Hodnocení';
$string['gradedby'] = 'Ohodnotil:';
$string['gradereceived'] = 'Bylo vám přiděleno hodnocení %s ze 100 od %s v %s';
$string['gradesubmissions'] = 'Odevzdáno k hodnocení';
$string['gradesubmitted'] = 'Známka úspěšně udělena';
$string['groupname'] = '"%s" - "%s"';
$string['institutiondenied'] = 'Přístup do \'%s\' zamítnut. Prosím, kontaktujte administrátora vaší instituce.';
$string['lock'] = 'Nechat portfolio po oznámkování zamčené';
$string['lockdescription'] = 'Uživatelé mohou portfolio po oznámkování upravovat';
$string['ltiserviceexists'] = 'Skupina pro správu LTI je povolena.';
$string['maharalti'] = 'LTI 1.1';
$string['maharaltimoodleassign'] = 'Příspěvek pro zadání z Moodlu';
$string['nocollections'] = 'Nemáte žádná portfolia, která by mohla být odeslána k hodnocení.';
$string['notconfigured'] = 'Tato aktivita aktuálně nepovoluje odevzdání. Prosím, zkuste to později.';
$string['noticeenabled'] = 'LTI API je povoleno.';
$string['noticenotenabled'] = 'LTI API <b>není</b> povoleno.';
$string['notreadylabel'] = 'Není připraven';
$string['oauthprotocolenabled'] = 'Protokol OAuth povolen';
$string['parentauthforlti'] = 'Nadřazená autorita';
$string['portfolioname'] = 'Název portfolia';
$string['portfoliosubmitted'] = 'Odeslali jste portfolio "%s" k hodnocení na %s.';
$string['portfoliosubmittedforgrading'] = 'Odeslali jste portfolium "<strong><a href="%s">%s</a></strong>" k hodnocení na %s.';
$string['portfoliosubmittedheader'] = 'Portfolio odesláno';
$string['readylabel'] = 'Připraven';
$string['restprotocolenabled'] = 'Protokol REST povolen';
$string['revokesubmission'] = 'Zrušit odevzdání';
$string['saveandrelease'] = 'Uložit a povolit odevzdávání';
$string['submitintro'] = 'Vyberte stánku nebo sbírku, kterou chcete k této aktivitě odevzdat. Pokud ještě nemáte portfolio, <a href="%sview/index.php" target="_blank">vytvořte si jej</a>. Jakmile budete hotovi, znovu aktivitu načtěte z LMS.';
$string['submitportfolio'] = 'Odeslat portfolio k hodnocení';
$string['submitto'] = 'Odeslat toto portfolio k hodnocení %s, %s';
$string['timegraded'] = 'Čas oznámkování';
$string['timesubmitted'] = 'Čas odevzdání';
$string['usernameexists1'] = 'Uživatelské jméno "%s" již existuje.';
$string['usernameexists2'] = 'Uživatelské jméno "%s" není platné.';
$string['viewsubmittedmessage1'] = '%s odevzdal "%s" ke %s Prosím ohodnoťte jej kliknutím na aktivitu  "%s" ve vašem LMS';
$string['viewsubmittedsubject1'] = 'Ohodnocení odevzdaného úkolu pro %s';
$string['webserviceauthdisabled'] = 'Autentifikace Webových služeb pro tuto instituci není povolena.';
$string['webserviceproviderenabled'] = 'Příchozí žádosti webové služby povoleny';
