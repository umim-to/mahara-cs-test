<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['appaccessrevoked'] = 'Přístup zrušen';
$string['autoconfiguredesc'] = 'Automaticky povolí nastavení a konfiguraci potřebnou pro Mobilní API.';
$string['autoconfiguretitle'] = 'Automaticky nastavit Mobilní API?';
$string['clientinfo'] = 'Aplikace';
$string['clientnotspecified'] = '(Neznámý)';
$string['configstep'] = 'Položka konfigurace';
$string['configstepstatus'] = 'Stav';
$string['generateusertoken'] = 'Vytvořit přístupový token aplikace';
$string['maharamobile'] = 'API Mahara Mobile';
$string['manualtokensdesc'] = 'Uživatelé mohou ručně vytvořit a zkopírovat přístupové tokeny webových služeb a vložit je do aplikace. Za běžných okolností by měla být aplikace schopna vytvořit tokeny pro uživatele sama. Některé zásuvné moduly to však nemusí umožnit.';
$string['manualtokenstitle'] = 'Ruční vytvoření tokenu';
$string['mobileapiserviceconfigured'] = 'Služba Mobilní API povolena, "%s" povoleno, "%s" zakázáno';
$string['mobileapiserviceexists'] = 'Služba skupina v rámci Mobilní API je registrována';
$string['mytokensmenutitle'] = 'Aplikace';
$string['mytokensmenutitle1'] = 'Mahara Mobile';
$string['mytokenspagedesc'] = 'Tyto aplikace mají přístup k vašemu účtu.';
$string['mytokenspagetitle'] = 'Aplikace';
$string['mytokenspagetitle1'] = 'Tokeny Mahara Mobile';
$string['nopersonaltokens'] = 'Nemáte povolen přístup k žádné aplikaci.';
$string['noticeenabled'] = 'Mobílní API Mahara je v současné době povoleno.';
$string['noticenotenabled'] = 'Mobilní API Mahara <b>není</b> v současné době povoleno.';
$string['notreadylabel'] = 'Nepřipraveno';
$string['readylabel'] = 'Připraveno';
$string['restprotocolenabled'] = 'Protokol REST povolen';
$string['servicenotallowed'] = 'Poskytnuté přihlašovací údaje nemají oprávnění pro přístup k této funkci.';
$string['token'] = 'Přístupový token';
$string['tokencreated'] = 'Vytvořen';
$string['tokenmanuallycreated'] = 'Vytvořen ručně';
$string['webserviceproviderenabled'] = 'Příchozí žádost webových služeb povolena';
