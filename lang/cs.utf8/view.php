<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = 'Mnohem větší prostřední sloupec';
$string['20,20,20,20,20'] = 'Stejné šířky';
$string['20,30,30,20'] = 'Větší prostřední sloupce';
$string['25,25,25,25'] = 'Stejně široké';
$string['25,25,50'] = 'Větší pravý sloupec';
$string['25,50,25'] = 'Větší prostřední sloupec';
$string['33,33,33'] = 'Stejně široké';
$string['33,67'] = 'Větší pravý sloupec';
$string['50,25,25'] = 'Větší levý sloupec';
$string['50,50'] = 'Stejně široké';
$string['67,33'] = 'Větší levý sloupec';
$string['Added'] = 'Přidáno';
$string['Browse'] = 'Procházet';
$string['Collection'] = 'Sbírka';
$string['Configure'] = 'Konfigurovat';
$string['Configure1'] = 'Upravit';
$string['Createdon'] = 'Vytvořeno';
$string['Grouphomepage'] = 'Domovská stránka skupiny';
$string['Help'] = 'Nápověda';
$string['Locked'] = 'Zamčený';
$string['Me'] = 'Se mnou';
$string['Owner'] = 'Vlastník';
$string['Pages'] = 'Stránky';
$string['Portfolio'] = 'Portfolio';
$string['Portfolios'] = 'Portfolia';
$string['Preview'] = 'Náhled';
$string['Profile'] = 'Profil';
$string['Progress'] = 'Dokončenost portfolia';
$string['Row'] = 'Řádek';
$string['Search'] = 'Hledat';
$string['Submitted'] = 'Zasláno';
$string['Template'] = 'Šablona';
$string['Untitled'] = 'Bez názvu';
$string['Updatedon'] = 'Aktualizováno';
$string['View'] = 'Stránka';
$string['Views'] = 'Stránky';
$string['Viewscollections'] = 'Stránky a sbírky';
$string['access'] = 'Přístup';
$string['accessbetweendates2'] = 'Nikdo neuvidí tuto stránku před %s nebo po %s';
$string['accessbetweendates3'] = 'Tuto stránku nemůže nikdo zobrazit před %s a po %s';
$string['accessdates'] = 'Přístup datum / čas';
$string['accessdeniedaccesss'] = 'Již nemáte oprávnění měnit povolení přístupu';
$string['accessfromdate2'] = 'Nikdo neuvidí tuto stránku před %s';
$string['accessfromdate3'] = 'Tuto stránku nemůže nikdo zobrazit před %s';
$string['accessibilitymodedescription'] = 'Tato stránka má zapnuto přístupné rozvržení. V tomto módu budou mít bloky šířku přes celou stránku a budou zobrazeny jeden po druhém. Pro změnu umístění bloku na něj najeďte, označte jej pomocí klávesy  \'Enter\' a posuňte jej dolů nebo nahoru mezi bloky za pomocí šipek.';
$string['accessibilitymodedescription1'] = 'Tato stránka má zapnuto přístupné rozvržení stránky. Pro přidání bloku na stránku klikněte na tlačítko \'Přidat nový blok\'.<br>V tomto módu mají bloky šířku přes celou stránku a jsou zobrazovány jeden za druhým. Pro změnu pozice bloku na něj najeďte, chytněte jej pomocí klávesy \'Enter\' a přesouvejte jej nahoru či dolů v seznamu bloků pomocí šipek na vaší klávesnici.';
$string['accessibleview'] = 'Přístupné rozvržení';
$string['accessibleviewdescription'] = 'Pro vytvoření rozvržení stránky pouze s jedním sloupcem a umožnění jejich úprav pomocí klávesnice místo tažení myší.';
$string['accesslist'] = 'Seznam sledování';
$string['accessrulesfor'] = 'Přístupová pravidla pro "%s"';
$string['accesssavedsuccessfully'] = 'Nastavení přístupu úspěšně uloženo';
$string['accessuntildate2'] = 'Nikdo neuvidí tuto stránku po %s';
$string['accessuntildate3'] = 'Tuto stránku nemůže nikdo zobrazit po %s';
$string['add'] = 'Přidat';
$string['addaccess'] = 'Přidat přístup pro "%s"';
$string['addaccessgroup'] = 'Přidat přístup pro skupinu "%s"';
$string['addaccessinstitution'] = 'Přidat přístup pro instituci "%s"';
$string['addarow'] = 'Přidat řádek';
$string['addblock'] = 'Přidat blok: %s';
$string['addcolumn'] = 'Přidat sloupec';
$string['addcontent'] = 'Přidat obsah';
$string['addedtowatchlist'] = 'Stránka byla přidána do seznamu sledovaných stránek';
$string['addnewblock'] = 'Potáhněte pro přidání nového bloku';
$string['addnewblockaccessibility'] = 'Klikněte pro přidání nového bloku';
$string['addnewblockaccessible'] = 'Klikněte pro přidání nového bloku';
$string['addnewblockdrag'] = 'Potáhněte pro přidání nového bloku';
$string['addnewblockhere'] = 'Přidat nový blok';
$string['addtowatchlist'] = 'Sledovat stránku';
$string['addtowatchlistartefact'] = 'Přidat stránku "%s" do sledování';
$string['addtutors'] = 'Přidat učitele';
$string['addusertogroup'] = 'Přidat tohoto uživatele do skupiny';
$string['advanced'] = 'Pokročilé';
$string['advancedoptions'] = 'Pokročilá nastavení';
$string['allow'] = 'Povolit';
$string['allowcommentsonview'] = 'Pokud je zaškrtnuté, uživatelé mohou zanechat komentář.';
$string['allowcommentsonview1'] = 'Povolit uživateům přidávání komentářů';
$string['allowcopying'] = 'Umožnit kopírování stránky';
$string['allviews'] = 'Všechny stránky';
$string['alreadyinwatchlist'] = 'Tato stránka je již na seznamu sledovaných stránek';
$string['anonymise'] = 'Anonymizovat';
$string['anonymisedescription'] = 'Skryjte svoje autorství stránky před ostatními uživateli. Správci a zaměstnanci mít možnost zobrazit vaše jméno budou.';
$string['artefacts'] = 'Položky portofolia';
$string['artefactsinthisview'] = 'Položky na této stránce';
$string['attachedfileaddedtofolder'] = 'Přiložený soubor %s byl uložen do vaší složky "%s".';
$string['attachment'] = 'Příloha';
$string['back'] = 'Zpět';
$string['basicoptions'] = 'Základní nastavení';
$string['basics'] = 'Základy';
$string['blockcell'] = 'Buňka';
$string['blockchangedbackerror'] = 'Změna bloku zpět na Dočasný blok se nepodařila.';
$string['blockchangedbacksuccess'] = 'Blok byl změněn zpět na Dočasný blok.';
$string['blockchangederror'] = 'Změna bloku na blok  \'%s\' se nepodařila.';
$string['blockchangedsuccess'] = 'Změna Dočasného bloku na blok \'%s\' proběhla úspěšně.';
$string['blockconfigurationrenderingerror'] = 'Konfigurace selhala protože blok nemůže být vypsán.';
$string['blockcopypermission'] = 'Oprávnění kopírovat blok';
$string['blockcopypermissiondesc'] = 'Pokud dovolíte ostatním uživatelům, aby si kopírovali váši stránku, můžete si zvolit, jak se má kopírovat tento blok';
$string['blockhelp'] = 'Nápověda k blokům';
$string['blockinstanceconfiguredsuccessfully'] = 'Blok úspěšně nakonfigurován';
$string['blocknotinview'] = 'Blok s ID %d není součástí stránky.';
$string['blockorder'] = 'Pozice';
$string['blockorderafter'] = 'Za %s';
$string['blockordertop'] = 'Horní část sloupce';
$string['blockordertopcell'] = 'Vrchní část buňky';
$string['blocksinstructionajax'] = 'Pomocí myši přetáhněte ikony jednotlivých položek vašeho portfolia pod tuto čáru. Tím je umístíte do bloků na této stránce. Bloky poté můžete v rámci stránky přesouvat na požadovanou pozici.';
$string['blocksinstructionajaxlive'] = 'Toto je náhled toho, jak bude stránka vypadat. Změny jsou ukládány automaticky.<br>Přetáhněte bloky na stránku pro jejich přidání. Pro změnu pozice je přetahujte v rámci stránky.';
$string['blocksinstructionajaxlive1'] = 'Tato oblast ukazuje náhled toho, jak bude vaše stránka vypadat. Změny se ukládají automaticky.<br>Potáhnutím ikony  \'Přidat nový blok\' na stránku nový blok umístíte. Následně budete moci zvolit, o jaký druh bloku se bude jednat. Poté budete také moci tažením bloků po stránce měnit jejich umístění.';
$string['blocksinstructionajaxlive2'] = 'Toto je náhled vaší stránky. Změny se ukládají automaticky.<br>Potáhněte tlačítko \'Plus\' na stránku pro vytvoření nového bloku. Vyberte, jaký typ bloku to bude. Potažením bloků na různá místa můžete měnit jejich pozice.';
$string['blocksintructionnoajax'] = 'Vyberte si blok a poté zvolte místo, kam se má na vaší stránce umístit. Bloky můžete přesouvat pomocí tlačítek v jejich záhlaví.';
$string['blockssizeupdated'] = 'Velikosti bloků byly úspěšně aktualizovány';
$string['blocktitle'] = 'Název bloku';
$string['blocktypecategory.external'] = 'Externí obsah';
$string['blocktypecategory.fileimagevideo'] = 'Soubory, obrázky a videa';
$string['blocktypecategory.general'] = 'Obecné';
$string['blocktypecategory.internal'] = 'Osobní informace';
$string['blocktypecategorydesc.blog'] = 'Klikněte pro nastavení deníku';
$string['blocktypecategorydesc.external'] = 'Klikněte pro nastavení externích zdrojů';
$string['blocktypecategorydesc.fileimagevideo'] = 'Klikněte pro nastavení médií';
$string['blocktypecategorydesc.general'] = 'Klikněte pro obecná nastavení';
$string['blocktypecategorydesc.internal'] = 'Klikněte pro nastavení osobních informací';
$string['blocktypeis'] = 'Typ bloku %s';
$string['bottom'] = 'Dole';
$string['by'] = '-';
$string['cantaddannotationinoldlayout'] = 'K této stránce nemůžete přidat anotaci. Prosím, převeďte rozvržení stránky tak, že ji nejdříve upravíte.';
$string['cantaddcolumn'] = 'Nemůžete přidat žádné další sloupce na tuto stránku';
$string['cantdeleteview'] = 'Nemůžete odstranit tuto stránku';
$string['canteditcollectionlocked'] = 'Není možné upravovat stránku portfolia, protože sbírka je zamčená.';
$string['canteditdontown'] = 'Nemůžete upravovat tuto stránku, protože nejste jeho vlastníci';
$string['canteditprogress'] = 'Není možné upravovat stránku portfolia, protože sbírka je zamčená nebo byla stránka zkopírována ze šablony.';
$string['canteditsubmitted'] = 'Tuto stránku nemůžete momentálně upravovat, protože byla odevzdána k ohodnocení v rámci skupiny "%s". Musíte počkat, dokud ji váš učitel neuvolní.';
$string['cantremovecolumn'] = 'Nemůžete smazat poslední sloupec z této stránky';
$string['cantsubmitcollectiontogroup'] = 'Nemůžete zveřejnit tuto sbírku.';
$string['cantsubmitemptycollection'] = 'Tato sbírka neobsahuje žádné stránky.';
$string['cantsubmittogroup'] = 'Nemůžete přispívat do této skupiny';
$string['cantsubmitviewtogroup'] = 'Tuto stránku nemůžete odevzdat k hodnocení v rámci této skupiny';
$string['cantversionoldlayout'] = 'Nelze uložit stránku jako časovou osu, dokud používáte starou verzi rozvržení. Je nutné stránku převést do nového rozvržení. To provedete jednoduše v rámci úprav stánky portolia.';
$string['cantversionvieweditpermissions'] = 'Nemáte povolení editovat tuto stránku.';
$string['cantversionviewgroupeditwindow'] = 'Nemůžete editovat tuto stránku. Je mimo časový rámec upravitelnosti vaší skupinou.';
$string['cantversionviewinvalid'] = 'Poskytnutá identifikace stránky je neplatná.';
$string['cantversionviewsubmitted'] = 'Nemůžete upravit tuto stránku, protože bylo odevzdána k hodnocení. Budete muset pořkat, dokud nebude uvolněna.';
$string['cellposition'] = 'Řádek %s Sloupec %s';
$string['celltitle'] = 'Buňka';
$string['changecolumnlayoutfailed'] = 'Rozvržení stránky nelze změnit. Zřejmě ji někdo jiný právě upravuje. Zkuste to později.';
$string['changemyviewlayout'] = 'Změnit rozvržení stránky';
$string['changerowlayoutfailed'] = 'Rozvržení stránky nelze změnit. Zřejmě ji někdo jiný právě upravuje. Zkuste to později.';
$string['changeviewlayout'] = 'Změnit rozvržení stránky';
$string['changeviewtheme'] = 'Motiv, který jste si zvolili pro tuto stránku vám již dále není k dispozici. Vyberte si prosím jiný motiv.';
$string['choosetemplategrouppageandcollectiondescription'] = '<p>Zde jsou šablony stránek, které tato skupina umožňuje zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat. Můžete také zkopírovat celou sbírku tak, že stisknete tlačítko "Kopírovat sbírku".</p><p><strong>Poznámka:</strong> V tuto chvíli není možné kopírovat deníky, příspěvky v deníku, plány ani životopisy.</p>';
$string['choosetemplategrouppagedescription'] = '<p>Zde jsou šablony stránek, které si můžete jako člen skupiny zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>

<p><strong>Poznámka:</strong> V rámci skupiny nelze kopírovat deníky ani příspěvky deníků.</p>';
$string['choosetemplateinstitutionpageandcollectiondescription'] = '<p>Zde jsou šablony stránek, které tato instituce umožňuje zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat. Můžete také zkopírovat celou sbírku tak, že stisknete tlačítko "Kopírovat sbírku".</p><p><strong>Poznámka:</strong> V tuto chvíli není možné kopírovat deníky, příspěvky v deníku, plány ani životopisy.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Zde jsou šablony stránek, které si můžete jako člen této instituce zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>

<p><strong>Poznámka:</strong> V rámci instituce nelze kopírovat deníky ani příspěvky deníků.</p>';
$string['choosetemplatepageandcollectiondescription'] = '<p>Zde jsou šablony stránek, které si můžete jako člen této instituce zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat. Můžete také zkopírovat celou sbírku tak, že stisknete tlačítko "Kopírovat sbírku".</p>';
$string['choosetemplatepagedescription'] = '<p>Zde jsou šablony stránek, které si můžete zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>';
$string['choosetemplatesitepageandcollectiondescription1'] = '<p>Zde jsou šablony stránek, které si můžete zkopírovat a použít je jako výchozí bod pro svou vlastní stránku. Pro náhled dané stránky klikněte na její název. Jakmile najdete vhodnou stránku, stiskněte tlačítko "Kopírovat stránku". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat. Můžete také zkopírovat celou sbírku tak, že stisknete tlačítko "Kopírovat sbírku".</p><p><strong>Poznámka:</strong> V tuto chvíli není možné kopírovat deníky, příspěvky v denících, plány ani životopisy ze stránek na úrovni webu.</p>';
$string['choosethemedesc'] = 'Zvolte motiv pro tuto stránku.';
$string['clickformoreinformation'] = 'Klikněte pro více informací a pro přidání komentáře';
$string['clickformoreinformation1'] = 'Klikněte pro více informací a pro přidání komentáře.';
$string['closeconfiguration'] = 'Zavřít konfiguraci';
$string['collectionalreadysubmitted'] = 'Tato sbírka již byla odevzdána k jinému zadání ve skupině';
$string['collectionsubmissionexceptionmessage'] = 'Tato sbírka nemůže být odevzdána z následujícího důvodu:';
$string['collectionsubmissionexceptiontitle'] = 'Nebylo možné odevzdat sbírku';
$string['collectionsubmitted'] = 'Sbírka odevzdána';
$string['collectionsubmittedtogroup'] = 'Tato sbírka byla odevzdána ve skupině <a href="%s">%s</a>.';
$string['collectionsubmittedtogroupgrade'] = 'Tato sbírka byla odevzdána k hodnocení  <a href="%s">"%s"</a> v "%s" na %s.';
$string['collectionsubmittedtogroupon'] = 'Tato sbírka byla odevzdána ve skupině <a href="%s">%s</a> na %s.';
$string['collectionsubmittedtohost'] = 'Tato sbírka byla odevzdána k hodnocení.';
$string['collectionsubmittedtohoston'] = 'Tato sbírka byla odevzdána %s.';
$string['collectionviewsalreadysubmitted'] = 'Některé stránky v této sbírce již byly odevzdány: "%s" Sbírku nelze odevzdat do jejich uvolnění nebo odebrání ze sbírky.';
$string['column'] = 'sloupec';
$string['columnlayout'] = 'Sloupcové rozvržení';
$string['columns'] = 'sloupce';
$string['comments'] = 'Komentáře';
$string['complaint'] = 'Stížnost';
$string['configureblock'] = 'Konfigurovat blok %s';
$string['configureblock1'] = 'Nastavit blok %s (ID %s)';
$string['configureblock2'] = 'Nastavit blok';
$string['configureblock3'] = 'Nastavit blok';
$string['configurethisblock'] = 'Konfigurovat tento blok';
$string['configurethisblock1'] = 'Nastavit tento blok (ID %s)';
$string['confirmadddesc'] = 'Prosím vyberte, co chcete vytvořit:';
$string['confirmaddtitle'] = 'Vytvořit stránku nebo sbírku';
$string['confirmcancelcreatingview'] = 'Tvorba stránky ještě není u konce. Opravdu chcete úpravy zrušit?';
$string['confirmcloseblockinstance'] = 'Jste si jisti, že chcete pokračovat bez uložení změn?';
$string['confirmconversionmessage'] = 'Jako součást Mahary 19.10 jsme zavedli nový způsob vytváření rozvržení stránek. Abyste mohli tuto stránku portfolia upravovat, bude potřeba převést staré rozvržení na nové. Pokud chcete takto převést pouze tuto stránku, klikněte na \'Souhlasit\'. Pokud si přejete převést všechny vaše stránky a toto upozornění již nezobrazovat, zvolte \'Souhlasit a zapamatovat\'. Zvolená možnost může být změněna ve vašem <a href="%s">Základním nastavení</a>. Pro návrat ke stránce bez jejího upravování klikněte na \'Zrušit\'.';
$string['confirmcopydesc'] = 'Prosím, zvolte, co si přejete zkopírovat:';
$string['confirmcopytitle'] = 'Potvrdit kopírování';
$string['confirmdeleteblockinstance'] = 'Opravdu chcete odstranit tento blok?';
$string['copiedblocksandartefactsfromdefaultview'] = 'Zkopírováno %s bloků a %d položek portfolia ze šablony stránky.';
$string['copiedblocksandartefactsfromtemplate'] = 'Úspěšně zkopírováno %d bloků a %d položek portfolia z %s';
$string['copyaview'] = 'Kopírovat stránku';
$string['copyforexistingmembersprogress'] = 'Kopírování portfolií existujících členů skupiny';
$string['copyfornewgroups'] = 'Kopie pro nové skupiny';
$string['copyfornewgroupsdescription'] = 'Automaticky udělej kopii této stránky ve všech skupinách tohoto typu:';
$string['copyfornewgroupsdescription1'] = 'Vytvořit kopii vybraných stránek / sbírek ve všech nových skupinách s těmito rolemi:';
$string['copyfornewmembers'] = 'Kopie pro nové členy';
$string['copyfornewmembersdescription'] = 'Automaticky udělej vlastní kopii této stránky pro všechny nové členy instituce %s.';
$string['copyfornewmembersdescription1'] = 'Automaticky vytvářet osobní kopie vybraných stránek / sbírek pro všechny nové členy skupiny %s';
$string['copyfornewmembersdescription2'] = 'Automaticky vytvořit kopii zvolených stránek / sbírek novým členům %s. Pokud chcete, aby si tito uživatelé mohli později vytvořit vlastní kopii stránek / sbírek, povolte kopírování globálně.';
$string['copyfornewusers'] = 'Kopie pro nové uživatele';
$string['copyfornewusersdescription'] = 'Automaticky udělej vlastní kopii této stránky pro každého nového uživatele těchto stránek';
$string['copyfornewusersdescription1'] = 'Jakmile je založen nový uživatel, automaticky vytvořit kopie vybraných stránek / sbírek v jeho portfoliu.';
$string['copyfornewusersdescription2'] = 'Při vytvoření nového uživatele automaticky vytvořit kopii zvolených stránek či sbírek na uživatelův účet. Pokud chcete, aby tito uživatelé mohli kopírovat zvolená stránky / sbírky, povolte kopírování globálně.';
$string['copylocked'] = 'Kopie uzamčena';
$string['copylockeddescription'] = 'Toto je kopie šablony. Změňte nastavení pro uzamčení/odemčení polí s instrukcemi pro tuto kopii.';
$string['copynewusergroupneedsloggedinaccess'] = 'Ke kopiím pro nové skupiny nebo nové uživatele musejí mít přístup všichni přihlášení uživatelé.';
$string['copythisportfolio'] = 'Kopírovat toto portfolio';
$string['copythisview'] = 'Kopírovat tuto stránku';
$string['copyview'] = 'Kopírovat stránku';
$string['copyvieworcollection'] = 'Kopírovat stránku nebo sbírku';
$string['coverimage'] = 'Úvodní obrázek';
$string['coverimagedescription'] = 'Doporučený rozměr je 180px (šířka) na 130px (výška).';
$string['coverimagefolder'] = 'Úvodní obrázky';
$string['createcustomlayout'] = 'Vytvořit vlastní rozvržení';
$string['createemptyview'] = 'Vytvořit prázdnou stránku';
$string['createnewlayout'] = 'Vytvořit novou stránku';
$string['createtags'] = 'Vytvořit pomocí štítků';
$string['createtagsdesc1'] = 'Vyhledat nebo vložit štítky pro automatické přidání obsahu do vaší stránky. Pokud vložíte více štítků než jeden, na stránce se objeví pouze obsah označený všemi těmito štítky. Následně budete mít možnost bloky přesouvat a mazat.';
$string['createview'] = 'Vytvořit stránku';
$string['dashboard'] = 'Ovládací panel';
$string['dashboardviewtitle'] = 'Stránka ovládacího panelu';
$string['date'] = 'Datum';
$string['defaultaccesslistmessage'] = 'Nikdo kromě vás nemůže zobrazit vybrané stránky / sbírky. Přidejte uživatele, kteří mají mít oprávnění.';
$string['defaultsort'] = 'Abecedně';
$string['deletedview'] = 'Stránka smazána';
$string['deletespecifiedview'] = 'Odstranit stránku "%s"';
$string['deletethisview'] = 'Odstranit tuto stránku';
$string['deleteviewconfirm'] = 'Opravdu chcete odstranit tuto stránku? Tato akce nemůže být vrácena.';
$string['deleteviewconfirm1'] = 'Opravdu chcete smazat tuto stránku? Smazání nelze vrátit zpět.';
$string['deleteviewconfirmbackup'] = 'Prosím, zvažte vytvoření zálohy této stránky tak, že ji <a href="%sexport/" target="_blank">exportujete</a>.';
$string['deleteviewconfirmbackup1'] = 'Prosím, zvažte vytvoření zálohy této stránky jejím <a href="%sexport/index.php?view=%s">vyexportováním</a>.';
$string['deleteviewconfirmnote'] = '<p><strong>Poznámka:</strong> všechny bloky s obsahem, které byly přidány na stránku nebudou smazány. Nicméně budou odstraněny jakékoliv zpětné vazby, k dané stránce vztažené. Zvažte prosím nejprve zálohu stránky v podobě jeho exportu.</p>';
$string['deleteviewconfirmnote1'] = '<strong> POZNÁMKA:</strong>Všechny vaše soubory a záznamy z deníků, které jsou propojené s touto stránkou budou stále k dispozici.<br/> Smazána však bude veškerá zpětná vazba spjatá se stránkou.';
$string['deleteviewconfirmnote2'] = 'Tato stránka je součástí kolekce <a href="%s">"%s"</a>';
$string['deleteviewconfirmnote3'] = '<strong>Poznámka:</strong> Všechny vaše soubory a záznamy v deníku, které jsou spojené s touto stránkou budou i nadále k dispozici.<br/>Dojde nicméně k odstranění všech komentářů.';
$string['description'] = 'Popis stránky';
$string['detailslinkalt'] = 'Podrobnosti';
$string['dimensionsnotset'] = 'Rozměry bloků nejsou nastaveny';
$string['displayview'] = 'Zobrazit stránku';
$string['dontaskagain'] = 'Souhlasit a zapamatovat';
$string['draft'] = 'Koncept';
$string['drafttextblockdescription'] = 'Uložte si blok jako koncept, pokud nechcete, aby ho kdokoli viděl. Jakmile je text jednou publikován, nelze z něj udělat znovu koncept.';
$string['editaccess'] = 'Upravit přístup';
$string['editaccessdescription'] = 'Stejné nastavení můžete provést pro více položek vybraných z rozevíracích nabídek. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editaccessfor'] = 'Nastavit přístup (ID %s)';
$string['editaccessgrouppagedescription'] = 'Ve výchozím nastavení, pouze členové skupiny mohou přidávat a upravovat tyto stránky a sbírky. Stejné nastavení můžete provést pro více položek vybraných z rozevíracích nabídek. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editaccessinstitutionpagedescription'] = 'Ve výchozím nastavení, jsou vaše stránky a sbírky přístupné pouze správci instituce. Stejné nastavení můžete provést pro více položek vybraných z rozevíracích nabídek. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editaccessinvalidviewset'] = 'Pokus o úpravu přístupu k neplatnému souboru stránek a sbírek';
$string['editaccesspagedescription3'] = 'Ve výchozím nastavení jsou stránky a sbírky přístupné pouze vám. S ostatními je můžete sdílet přidáním patřičných přístupových pravidel níže. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editaccesspagedescription6'] = 'Jste jediný, kdo ve výchozím stavu může zobrazit vaše stránky a sbírky. Na této stránce zvolíte, kdo další má právo je prohlížet.';
$string['editaccesssitepagedescription'] = 'Ve výchozím nastavení jsou stránky a sbírky přístupné pouze vám. Stejné nastavení můžete provést pro více položek vybraných z rozevíracích nabídek. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editblockspagedescription'] = '<p>Stránku vytvoříte tak, že chytnete a táhnete obsahové bloky z tlačítek níže. </p>';
$string['editcontent'] = 'Upravit obsah';
$string['editcontent1'] = 'Upravit';
$string['editcontentandlayout'] = 'Upravit obsah a rozvržení';
$string['editlayout'] = 'Upravit rozvržení';
$string['editsecreturlaccess'] = 'Upravit tajnou adresu URL';
$string['editsecreturlaccessfor'] = 'Upravit tajnou adresu URL (ID %s)';
$string['editsecreturlsintable'] = '<b>Tajná URL</b> musí být generována samostatně. Pro nastavení tajných URL se vraťte na <a href="%s">seznam sbírek a stránek</a>.';
$string['editthisview'] = 'Upravit tuto stránku';
$string['edittitle'] = 'Upravit titulek';
$string['edittitleanddescription'] = 'Upravit titulek a popis';
$string['empty_block'] = 'Vyberte si položku portfolia ze seznamu vlevo, kterou sem chcete umístit';
$string['emptylabel'] = 'Klikněte sem pro vložení textu popisku';
$string['entersearchquery'] = 'Vložte vyhledávací dotaz';
$string['err.addblocktype'] = 'Nelze přidat blok na vaší stránku';
$string['err.addcolumn'] = 'Nelze přidat nový sloupec';
$string['err.changetheme'] = 'Nelze aktualizovat motiv';
$string['err.moveblockinstance'] = 'Nelze přesunout blok na určenou pozici';
$string['err.removeblockinstance'] = 'Nelze odstranit blok';
$string['err.removecolumn'] = 'Nelze odstranit sloupec';
$string['everyoneingroup'] = 'Všichni ve skupině';
$string['existingURLS'] = 'Existující URL';
$string['existinggroupmembercopy'] = 'Kopírovat pro existujícíc členy skupiny';
$string['existinggroupmembercopydesc1'] = 'Zkopírovat vybrané stránky / sbírky na místo pro osobní portfolia všech existujících členů skupiny. Tento přepínač se po uložení uvede do původní polohy. Členové skupiny dostanou tedy kopii pouze jednou.';
$string['expandcontract'] = 'Rozšířit / Zmenšit seznam typů bloku';
$string['filescopiedfromviewtemplate'] = 'Zkopírované soubory z %s';
$string['forassessment'] = 'k hodnocení';
$string['forassessment1'] = 'Odevzdat k hodnocení';
$string['friend'] = 'Přítel';
$string['friends'] = 'Přátelé';
$string['general'] = 'Obecné';
$string['generatesecreturl'] = 'Vytvořit nové tajné URL pro %s';
$string['generatingpreview'] = 'Probíhá generování náhledu...';
$string['gotonextversion'] = 'Přejít k následující verzi';
$string['gotopreviousversion'] = 'Přejít k předcházející verzi';
$string['group'] = 'Skupina';
$string['grouphomepage'] = 'Domovská stránka skupiny';
$string['grouphomepagedescription'] = 'Domovská stránka skupiny je obsah, který se zobrazí na kartě "O této skupině"';
$string['grouphomepageviewtitle'] = 'Stránka domovské skupiny';
$string['groups'] = 'Skupiny';
$string['groupviews'] = 'Stránka skupiny';
$string['groupviewurltaken'] = 'Stránka s touto adresou již existuje.';
$string['image'] = 'Obrázek';
$string['imagetooltip'] = 'Vložit obrázek';
$string['in'] = '-';
$string['institutions'] = 'Instituce';
$string['institutionviews'] = 'Stránky instituce';
$string['institutionviewscollections'] = 'Stránky a sbírky instituce';
$string['instructions'] = 'Instrukce';
$string['invalidcolumn'] = 'Sloupec %s mimo rozsah';
$string['inviteusertojoingroup'] = 'pozvat tohoto uživatele do skupiny';
$string['itemdropped'] = 'Položka chycena: %s';
$string['itemgrabbed'] = 'Položka upuštěna: %s';
$string['itemreorder'] = 'Pořadí položek seznamu bylo změněno. Položka %s je nyní na pozici %s z %s.';
$string['latestcreated'] = 'Datum vytvoření';
$string['latestmodified'] = 'Naposledy změněno';
$string['latestviewed'] = 'Naposledy zobrazeno';
$string['layout'] = 'Rozvržení';
$string['layoutpreview'] = 'Náhled rozvržení';
$string['layoutpreviewimage'] = 'Náhledový obrázek rozložení';
$string['linktooriginaltemplate'] = 'Původní šablona';
$string['linktooriginaltemplatedescription'] = 'Tato stránka je postavena na šabloně. Toto je odkaz vedoucí k ní.';
$string['linktooriginaltemplatedescriptiondeleted'] = 'Tato stránka je postavena na šabloně.';
$string['listedinpages'] = 'Uvedeno na stránkách';
$string['listviews'] = 'Seznam stránek';
$string['loadingtimelinecontent'] = 'Načítání časové osy pro "%s". Pokud má stránka mnoho verzí, může tento proces chvíli trvat.';
$string['lockblocks'] = 'Zamčení bloků';
$string['lockblocks1'] = 'Zabránit odstraňování bloků';
$string['lockblocksdescription'] = 'Můžete zamknout bloky na stránce a tím zabránit jejich odstranění v případě, kdy si lidé danou stránku zkopírují.';
$string['lockblocksdescription1'] = 'Bloky můžete na stránce zamknout a tak zabránit jejich případnému odstranění, když stránku upravujete. Tento zámek můžete kdykoli odstranit, pokud budete potřebovat bloky smazat.';
$string['lockblocksdescription2'] = 'Můžete předcházet odstranění bloků během upravování stránky. Stále budete schopni měnit polohu i velikost bloků. Toto nastavení lze navíc kdykoli změnit, pokud by bylo bloky potřeba odstranit.';
$string['lockblocksdescriptioninstitution'] = 'Zamkněte bloky na stránce, aby jste zabránili jejich případnému odstranění, když si lidé zkopírují danou stránku do prostoru jejich osobních nebo skupinových portfolií. Budete mít stále možnost bloky odstranit, když budete upravovat stránku instituce nebo těchto webových stránek.';
$string['lockblocksdescriptioninstitution1'] = 'Můžete zabránit tomu, aby bylo možné mazat bloky, když lidé zkopírují stránku do jejich osobního nebo skupinového portfolia. Toto nastavení neovlivní vaše možnosti upravování stránky tohoto webu nebo institucí.';
$string['lockedgroupviewdesc'] = 'Pokud zamknete tuto stránku, jsou schopni ji upravit pouze správci skupin.';
$string['locktemplate'] = 'Šablona';
$string['locktemplatedescription'] = 'Pokud je zde nastaveno "ano", lidé při kopírování této stránky do prostoru jejich osobního účtu nebudou moci měnit instrukce ke stránce ani položce.';
$string['loggedin'] = 'Přihlášení uživatelé';
$string['manageaccess'] = 'Spravovat přístup';
$string['manageaccessfor'] = 'Spravovat přístup pro "%s"';
$string['managekeys'] = 'Spravovat tajné URL';
$string['managekeysfor'] = 'Spravovat tajné URL pro "%s"';
$string['manager'] = 'Vedoucí';
$string['matchalltags'] = 'Odpovídat všem štítkům';
$string['matchalltagsdesc'] = 'Oddělujte štítky čárkami, například: kočky,mourovaté';
$string['moderate'] = 'Střední';
$string['moreinstitutions'] = 'Více institucí';
$string['moreoptions'] = 'Pokročilé možnosti hledání';
$string['mostcomments'] = 'Nejvíce zpětné vazby';
$string['mostcomments1'] = 'Nejkomentovanější';
$string['mostvisited'] = 'Nejnavštěvovanější';
$string['moveblock'] = 'Přesunout blok %s';
$string['moveblock2'] = 'Přesunout blok';
$string['moveblockdown'] = 'Přesunout blok %s dolů';
$string['moveblockleft'] = 'Přesunout blok %s doleva';
$string['moveblockright'] = 'Přesunout blok %s doprava';
$string['moveblockup'] = 'Přesunout blok %s nahoru';
$string['movethisblock'] = 'Přesunout tento blok';
$string['movethisblockdown'] = 'Přesunout tento blok dolů';
$string['movethisblockleft'] = 'Přesunout tento blok doleva';
$string['movethisblockright'] = 'Přesunout tento blok doprava';
$string['movethisblockup'] = 'Přesunout tento blok nahoru';
$string['newsecreturl'] = 'Nová tajná adresa URL';
$string['newstartdatemustbebeforestopdate'] = 'Datum začátku pro "%s" musí být před koncovým datem';
$string['newstopdatecannotbeinpast'] = 'Koncové datum pro "%s" nemůže být v minulosti';
$string['next'] = 'Následující';
$string['nextversion'] = 'Následující verze';
$string['noaccesstoview'] = 'Nemáte oprávnění vidět tuto stránku';
$string['noartefactstochoosefrom'] = 'Žádné položky na výběr';
$string['noblocks'] = 'Žádné bloky v této kategorii';
$string['nobodycanseethisview2'] = 'Pouze vy můžete vidět tuto stránku';
$string['nocopyableviewsfound'] = 'Žádné stránky ke kopírování';
$string['nogroupviewsyet'] = 'V této skupině ještě nejsou žádné stránky';
$string['noownersfound'] = 'Vlastníci nenalezeni';
$string['nosharedviewsyet'] = 'S touto skupinou ještě nebyly sdíleny žádné stránky';
$string['nospecialrole'] = 'Žádná speciální role';
$string['nosubmissionsfrom'] = 'Členové, kteří ještě nic neodevzdali';
$string['nosubmittedviewscollectionsyet'] = 'Do této skupiny ještě nebyly odevzdány žádné stránky ani kolekce';
$string['nothemeselected'] = 'Motiv nevybrán';
$string['nothemeselected1'] = 'Použít motiv instituce';
$string['notifyadministrator'] = 'Informovat správce';
$string['notifyadministratorconfirm'] = 'Jste si jisti, že chcete nahlásit tuto stránku jako stránku obsahující nevhodný materiál?';
$string['notifysiteadministrator'] = 'Upozornit správce stránek';
$string['notifysiteadministratorconfirm'] = 'Jste si opravdu jisti, že chcete nahlásit tuto stránku z důvodu nepřípustného obsahu?';
$string['notitle'] = 'Bez názvu';
$string['notobjectionable'] = 'Není problematické';
$string['noversionsexist'] = 'Pro stránku "%s" neexistují žádné verze k zobrazení';
$string['noviewlayouts'] = 'Žádná rozvržení pro stránky se %s sloupci';
$string['noviews'] = 'Žádné stránky';
$string['noviews1'] = 'Žádné stránky nebo sbírky';
$string['nportfolios'] = array(

		0 => '%s portfolio', 
		1 => '%s portfolia', 
		2 => '%s portfolií'
);
$string['nrrows'] = array(

		0 => '%s řádek', 
		1 => '%s řádky', 
		2 => '%s řádků'
);
$string['numberofcolumns'] = 'Počet sloupců';
$string['nviews'] = array(

		0 => 'jedna stránka', 
		1 => '%s stránky',
		2 => '%s stránek'
);
$string['nviews1'] = array(

		0 => '%s stránka', 
		1 => '%s stránky', 
		2 => '%s stránek'
);
$string['nviewsandcollections'] = array(

		0 => '%s stránka nebo sbírka', 
		1 => '%s stránky nebo sbírky', 
		2 => '%s stránek nebo sbírek'
);
$string['otherpeople'] = 'Ostatní uživatelé';
$string['otherusersandgroups'] = 'Sílet s ostatními uživateli a skupinami';
$string['overrideconflict'] = 'Jedno nebo více přístupových oprávnění je v rozporu s daty zpřístupnění. Tato přístupová oprávnění nebudou platná mimo data zpřístupnění.';
$string['overridingstartstopdate'] = 'Přenastavení data zpřístupnění a znepřístupnění';
$string['overridingstartstopdatesdescription'] = 'Pokud chcete, zde můžete nastavit datum zpřístupnění a/nebo datum znepřístupnění vaší stránky. Nikdo kromě vás nebude moci stránku vidět před datem zpřístupnění nebo po datu znepřístupnění, a to bez ohledu na ostatní nastavení přístupových práv na této stránce.';
$string['ownedbygroup'] = 'Patřící této skupině';
$string['owner'] = 'vlastník';
$string['ownerformat'] = 'Formát zobrazení jména';
$string['ownerformatdescription'] = 'Jak se má zobrazovat vaše jméno lidem, kteří navštíví váši stránku?';
$string['owners'] = 'vlastníci';
$string['pageaccessrules'] = 'Pravidla přístupu ke stránce';
$string['pagepartofcollection'] = 'Vaše stránka je součástí sbírky \'%s\'. Povolení, které udělíte této stránce budou aplikovány na celou sbírku.';
$string['panelmenu'] = 'Menu';
$string['peer'] = 'Spolužák';
$string['peermanager'] = 'Spolužák a vedoucí';
$string['pending'] = 'Portfolio je revidováno';
$string['peopleinmyinstitution'] = 'Lidé z "%s"';
$string['peopleinmyinstitutions'] = 'Lidé z mých institucí';
$string['pleaseconfirmtranslate'] = 'Převést rozvržení stránky';
$string['portfolio'] = 'Portfolio';
$string['portfolios'] = 'portfolia';
$string['previousversion'] = 'Předcházející verze';
$string['print'] = 'Vytisknout';
$string['profile'] = 'profil';
$string['profileicon'] = 'Ikona profilu';
$string['profilenotshared'] = 'Plný přístup k profilu tohoto uživatele je omezen.';
$string['profileviewtitle'] = 'Uživatelský profil';
$string['progress'] = 'Dokončenost portfolia';
$string['public'] = 'Veřejnost';
$string['publicaccessnotallowed'] = 'Vaše instituce nebo správce webu zakázal veřejné stránky a tajné adresy URL. Jakékoliv tajné adresy URL, zde uvedené, jsou v současné době neaktivní.';
$string['publicaccessnotallowedforprobation'] = 'Omlouváme se, ale nově registrovaní uživatelé nemohou vytvářet tajné adresy URL.';
$string['quickedit'] = 'Rychlá úprava';
$string['reallyaddaccesstoemptyview'] = 'Vaše stránka neobsahuje žádné bloky. Opravdu chcete dát uživatelům přístup k této stránce?';
$string['reallydeletesecreturl'] = 'Jste si jisti, že chcete smazat tuto adresu URL?';
$string['registeredusers'] = 'Registrovaní uživatelé';
$string['remove'] = 'Odstranit';
$string['removeblock'] = 'Odstranit tento blok';
$string['removeblock1'] = 'Odstranit blok %s (ID %s)';
$string['removeblock2'] = 'Odstranit blok';
$string['removecolumn'] = 'Odstranit tento sloupec';
$string['removedfromwatchlist'] = 'Stránka odstraněna ze seznamu sledovaných stránek';
$string['removefromwatchlist'] = 'Přestat sledovat tuto stránku';
$string['removefromwatchlistartefact'] = 'Odstranit stránku "%s" ze sledovaných';
$string['removethisblock'] = 'Odebrat tento blok';
$string['removethisblock1'] = 'Odstranit tento blok (ID %s)';
$string['removethisrow'] = 'Odebrat tento řádek';
$string['reordercancelled'] = 'Změna v pořadí položek byla zrušena';
$string['reportobjectionablematerial'] = 'Nahlásit nevhodný obsah';
$string['reportsent'] = 'Vaše zpráva byla odeslána';
$string['retainviewrights1'] = 'Zachovat přístup pro prohlížení zkopírovaným stránkám nebo sbírkám';
$string['retainviewrightsdescription'] = 'Zaškrtněte toto políčko, pokud chcete zajistit váš přístup ke zobrazení kopií této stránky nebo sbírky, které vytvoří ostatní uživatelé. Tito uživatelé mohou přístup zrušit poté, co vytvoří jeho kopie. Ke stránkám zkopírovaným z kopie této stránky nebo sbírky mít přístup nebudete.';
$string['retainviewrightsdescription1'] = 'Zaškrtněte toto políčko, pokud chcete přidat svůj přístup ke kopiím vybraných stránek / sbírek, které vytvářejí jiní uživatelé. Tito uživatelé budou moci později tento přístup zrušit, pokud se tak rozhodnou. Stránky, které jsou zkopírovány z kopie této stránky nebo sbírky nebudou mít stejný přístup.';
$string['retainviewrightsdescription2'] = 'Povolit sám sobě přístup na zvolené stránky / sbírky, které zkopírují ostatní uživatelé. Uživatelé mohou později tento přístup zakázat. Stránky zkopírované z kopie této stránky nebo sbírky nebudou mít stejná přístupová práva.';
$string['retainviewrightsgroupdescription'] = 'Zaškrtněte toto políčko, pokud chcete zajistit členům této skupiny přístup ke zobrazení kopií této stránky, které vytvoří ostatní uživatelé. Tito uživatelé mohou přístup zrušit poté, co vytvoří jeho kopie. Ke stránkám zkopírovaným z kopie této stránky nebo sbírky mít přístup nebudete.';
$string['retainviewrightsgroupdescription1'] = 'Zaškrtněte toto políčko, pokud chcete přidat přístup členům skupiny ke kopiím vybraných stránek / sbírek, které vytvářejí jiní uživatelé. Tito uživatelé budou moci později tento přístup zrušit, pokud se tak rozhodnou. Stránky, které jsou zkopírovány z kopie této stránky nebo sbírky nebudou mít stejný přístup.';
$string['retainviewrightsgroupdescription2'] = 'Povolit členům skupiny přístup na zvolené stránky / sbírky, které zkopírují ostatní uživatelé. Uživatelé mohou později tento přístup zakázat. Stránky zkopírované z kopie této stránky nebo sbírky nebudou mít stejná přístupová práva.';
$string['retractable'] = 'Zasouvací';
$string['retractabledescription'] = 'Povolit, aby tento blok byl zasunutelný (zobrazovat a skrývat jeho obsah)';
$string['retractedonload'] = 'Automaticky zasunout (skrýt obsah)';
$string['retractedonloaddescription'] = 'Povolit automatické zasunutí tohoto bloku';
$string['returntoinstitutionportfolios'] = 'Vrátit se k stránkám a sbírkám instituce';
$string['returntositeportfolios'] = 'Vrátit se ke stránkám a sbírkám celé sítě';
$string['returntoviews'] = 'Vrátit se ke stránkám a sbírkám';
$string['review'] = 'Posudek';
$string['row'] = 'řádek';
$string['rownr'] = 'Řádek %s';
$string['rows'] = 'řádky';
$string['savedtotimeline'] = 'Uloženo do časové osy';
$string['savetimeline'] = 'Uložit do časové osy';
$string['savetimelinespecific'] = 'Uložit časovou osu pro %s';
$string['search'] = 'Vyhledávání';
$string['searchfor'] = 'Hledat...';
$string['searchowners'] = 'Prohledat vlastníky';
$string['searchviews'] = 'Prohledat stránky';
$string['searchviewsbyowner'] = 'vyhledat stránky podle vlastníka:';
$string['secreturldeleted'] = 'Vaše tajná adresa URL byla smazána.';
$string['secreturls'] = 'Tajné adresy URL';
$string['secreturlupdated'] = 'Tajná adresa URL aktualizována';
$string['selectaviewtocopy'] = 'Vyberte si stránku ke zkopírování:';
$string['settings'] = 'Nastavení';
$string['share'] = 'Co sdílím s ostatními';
$string['shareallwithmaximum'] = array(

		0 => 'Vybraná portfolia můžete sdílet s maximálně 1 osobou nebo skupinou lidí.', 
		1 => 'Vybraná portfolia můžete sdílet s maximálně %d lidmi nebo skupinami lidí.', 
		2 => 'Vybraná portfolia můžete sdílet s maximálně %d lidmi nebo skupinami lidí.'
);
$string['sharedby'] = 'Sdíleno';
$string['sharedbyme'] = 'Co sdílím';
$string['sharedtogroup'] = 'Sdíleno s touto skupinou';
$string['sharedviews'] = 'Sdílené stránky';
$string['sharedviewsdescription'] = 'Tato stránka obsahuje seznam naposledy upravených nebo komentovaných stránek, které s vámi ostatní sdílí. Mohou tak činit přímo, sdílením s přáteli vlastníka nebo sdílením s jednou z vašich skupin.';
$string['sharedwith'] = 'Sdílené s';
$string['sharedwithdescription'] = 'Zvolte, která portfolia druhých lidí chcete vidět v tomto bloku.';
$string['sharedwithellipsis'] = 'Sdíleno s...';
$string['sharedwithme'] = 'Co se mnou sdílí ostatní';
$string['sharedwithothers'] = 'Sdílet s ostatními';
$string['shareview'] = 'Sdílet stránku';
$string['shareview1'] = 'Sdílet';
$string['sharewith'] = 'Sdílet s';
$string['sharewithmaximum'] = array(

		0 => 'Portfolio můžete sdílet s maximálně 1 osobou nebo skupinou lidí.', 
		1 => 'Portfolio můžete sdílet s maximálně %d lidmi nebo skupinami lidí.', 
		2 => 'Portfolio můžete sdílet s maximálně %d lidmi nebo skupinami lidí.'
);
$string['sharewithmygroups'] = 'Sdílet s mými skupinami';
$string['sharewithmyinstitutions'] = 'Sdílet s mou institucí';
$string['sharewithusers'] = 'Sdílet s uživatelem';
$string['show'] = 'Ukázat';
$string['skin'] = 'Vzhled';
$string['startdate'] = 'Datum zpřístupnění';
$string['startdatemustbebeforestopdate'] = 'Datum začátku musí být před datumem konce';
$string['stopdate'] = 'Datum znepřístupnění';
$string['stopdatecannotbeinpast'] = 'Datum konce nemůže být v minulosti';
$string['stopdatecannotbeinpast1'] = 'Datum "do" nemůže být v minulosti';
$string['submissionstogroup'] = 'Příspěvky do této skupiny';
$string['submitaviewtogroup'] = 'Odeslat stránku do této skupiny';
$string['submitconfirm'] = 'Pokud odešlete \'%s\' učiteli %s k posouzení, nebudete moci upravovat obsah, dokud učitel neudělí známku. Skutečně to chcete udělat?';
$string['submittedforassessment'] = 'Odeslán k posouzení';
$string['submittedpendingrelease'] = 'Čeká na uvolnění z archivace';
$string['submitthiscollectionto'] = 'Zaslat tuto sbírku k hodnocení -';
$string['submitthiscollectionto1'] = 'Odevzdat tuto sbírku k ohodnocení';
$string['submitthisviewto'] = 'Zaslat tuto stránku k ohodnocení -';
$string['submitthisviewto1'] = 'Odevzdat tuto stránku k ohodnocení';
$string['submittogroup'] = 'Odevzdat stránku nebo sbírku této skupině';
$string['submitviewconfirm'] = 'Pokud odevzdáte "%s" do "%s" k ohodnocení, nebudete jej moci upravovat až do okamžiku, než jej váš učitel oznámkuje. Opravdu chcete nyní vaši stránku odeslat?';
$string['submitviewtogroup'] = 'Odevzdat "%s" do "%s k ohodnocení';
$string['success.addblocktype'] = 'Blok úspěšně přidán';
$string['success.addcolumn'] = 'Sloupec úspešně přidán';
$string['success.changetheme'] = 'Motiv byl úspěšně aktualizován';
$string['success.moveblockinstance'] = 'Blok úspěšně přesunut';
$string['success.removeblockinstance'] = 'Blok úspěšně odstraněn';
$string['success.removecolumn'] = 'Sloupec úspěšně odstraněn';
$string['tagsonly'] = 'Pouze štítky';
$string['tagsonly1'] = 'Štítky';
$string['templatedashboard'] = 'Šablona domovské stránky';
$string['templatedescription'] = 'Zaškrtněte, pokud chcete návštěvníkům vaší stránky dovolit, aby ji používali jako šablonu pro jejich vlastní stránky.';
$string['templatedescriptionplural'] = 'Zaškrtněte, pokud chcete, aby lidé, kteří vidí vaše stránky, mohli vytvářet jejich vlastní kopie spolu se všemi soubory a složkami, které obsahují.';
$string['templatedescriptionplural1'] = 'Zaškrtněte toto pole, pokud si přejete, aby uživatelé, kteří mohou zobrazit vaše stránky / sbírku mohli vytvářet jejich kopie, včetně souborů a složek v nich obsažených.';
$string['templatedescriptionplural2'] = 'Pokud mají uživatelé povolený přístup na vámi zvolené stránky / sbírky, mohou si vytvořit jejich kopie.';
$string['templategrouphomepage'] = 'Šablona domovské stránky skupiny';
$string['templateportfolio'] = 'Šablona stránky';
$string['templateportfoliodescription'] = 'Nastavte výchozí rozložení stránek, které vytvoří vaši uživatelé. Můžete také přidávat bloky. Vezměte, prosím, na vědomí, že obsah, který přidáte do výchozí stránky, bude na každé stránce, kterou vaši uživatelé vytvoří.';
$string['templateportfoliodescription1'] = 'Připravte výchozí rozvržení pro vytvářené stránky. Můžete také přidávat bloky. Pamatujte prosím, že jakýkoli obsah, který vložíte na výchozí stránku, se objeví na každé stránce vytvořené po té, co své změny uložíte.';
$string['templateportfoliotitle'] = 'Bez názvu';
$string['templateprofile'] = 'Šablona profilu';
$string['templateprogress'] = 'Šablona pro dokončenost portfolia';
$string['textbox'] = 'Textové pole';
$string['textbox1'] = 'Poznámka';
$string['textboxtooltip'] = 'Přidat textové pole';
$string['theme'] = 'Motiv vzhledu';
$string['thisviewmaybecopied'] = 'Kopírování povoleno';
$string['thisviewmaynotbecopied'] = 'Kopírování není povoleno';
$string['timeline'] = 'Časová osa';
$string['timelinespecific'] = 'Časová osa pro';
$string['timeofsubmission'] = 'Doba odevzdání';
$string['title'] = 'Název stránky';
$string['titleanddescription'] = 'Název, popis, štítky';
$string['titleanddescriptionandtagsandowner'] = 'Název, popis, štítky, vlastník';
$string['titleanddescriptionnotags'] = 'Název, popis';
$string['token'] = 'Tajná adresa URL';
$string['top'] = 'Nahoře';
$string['tutors'] = 'učitelé';
$string['undo'] = 'Zpět';
$string['unrecogniseddateformat'] = 'Neznámý formát data';
$string['updatedaccessfornumviews'] = 'Přístupová práva pro stránku (stránky) %d byla aktualizována';
$string['updatedaccessfornumviews1'] = array(

		0 => 'Na jedné stránce byla aktualizována přístupová pravidla.', 
		1 => 'Na %d stránkách byla aktualizována přístupová pravidla.',
		2 => 'Na %d stránkách byla aktualizována přístupová pravidla.'
);
$string['updatewatchlistfailed'] = 'Aktualizace seznamu sledovaných stránek selhala';
$string['user'] = 'Uživatel';
$string['users'] = 'Uživatelé';
$string['userviewurltaken'] = 'Tato URL adresa je již používána. Zvolte prosím jinou.';
$string['usesitetheme'] = 'Použít motiv těchto webových stránek';
$string['verifier'] = 'Posuzovatel';
$string['versionnumber'] = 'Verze %s';
$string['view'] = 'stránka';
$string['viewaccesseditedsuccessfully'] = 'Nastavení přístupových práv úspěšně uloženo';
$string['viewalreadysubmitted'] = 'Tato sbírka již byla odevzdána k jinému zadání ve skupině';
$string['viewauthor'] = 'od <a href="%s">%s</a>';
$string['viewcolumnspagedescription'] = 'Nejprve zvolte, do kolika sloupců chcete rozvrhnout obsah vaší stránky. Šířku sloupců budete moci změnit v dalším kroku.';
$string['viewcopywouldexceedquota'] = 'Kopírováním této stránky by mohlo dojít k překročení přidělené diskové kvóty.';
$string['viewcreatedsuccessfully'] = 'Stránka úspěšně vytvořena';
$string['viewcreatewouldexceedquota'] = 'Vytvoření této stránky by přesáhlo vaši souborovou kvótu';
$string['viewdeleted'] = 'Stránka odstraněna';
$string['viewfilesdirdesc'] = 'Soubory ze zkopírovaných stránek';
$string['viewfilesdirname'] = 'soubory_stranek';
$string['viewinformationsaved'] = 'Informace o stránce úspěšně zkopírovány';
$string['viewlayoutchanged'] = 'Rozvržení stránky bylo změněno';
$string['viewlayoutpagedescription'] = 'Vyberte si, jak chcete rozmístit sloupce na vaší stránce.';
$string['viewname'] = 'Název stránky';
$string['viewnotsubmitted'] = 'Portfolio není aktuálně odevzdáno';
$string['viewobjectionableunmark'] = 'Tato stránka nebo nějaký její obsah byl nahlášen jako závadný. Pokud je již závadný obsah odstraněn, můžete kliknout na tlačítko k odstranění tohoto oznámení a sdělit tuto skutečnost ostatním správcům.';
$string['vieworcollection'] = 'stránka nebo sbírka';
$string['views'] = 'stránky';
$string['viewsalreadysubmitted'] = 'Některé stránky této sbírky již byly odeslány:<br>%s<br> Sbírku nelze odeslat, dokud nebudou uvolněny.';
$string['viewsandcollections'] = 'stránky a sbírky';
$string['viewsavedsuccessfully'] = 'Stránka úspěšně uložena';
$string['viewscollectionssharedtogroup'] = 'Stránky a sbírky sdílené s touto skupinou';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Musíte povolit kopírování stránky, která má být automaticky kopírována do nových skupin.';
$string['viewscopiedfornewusersmustbecopyable'] = 'Musíte povolit kopírování stránky, která má být automaticky kopírována novým uživatelům.';
$string['viewsownedbygroup'] = 'Stránky vlastněné touto skupinou';
$string['viewssharedtogroup'] = 'Stránky, které sdílíte s touto skupinou';
$string['viewssharedtogroupbyothers'] = 'Stránky, které ostatní sdílí s touto skupinou';
$string['viewssubmittedtogroup'] = 'Stránky zaslané do této skupiny';
$string['viewsubmissionexceptionmessage'] = 'Tato stránka nemůže být odevzdána z následujícího důvodu:';
$string['viewsubmissionexceptiontitle'] = 'Nebylo možné odevzdat stránku.';
$string['viewsubmitted'] = 'Stránka zaslána';
$string['viewsubmittedtogroup'] = 'Tato stránka byla zaslána <a href="%s">%s</a>';
$string['viewsubmittedtogroup1'] = 'Toto portfolio bylo odesláno k <a href="%s">%s</a>.';
$string['viewsubmittedtogroupgrade'] = 'Toto portfolio bylo odesláno k hodnocení <a href="%s">"%s"</a> v "%s" %s.';
$string['viewsubmittedtogroupon'] = 'Tato stránka byla zaslána <a href="%s">%s</a> na %s';
$string['viewsubmittedtogroupon1'] = 'Toto portfolio bylo odesláno k <a href="%s">%s</a> %s.';
$string['viewsubmittedtohost'] = 'Toto portfolio bylo odevzdáno k hodnocení.';
$string['viewsubmittedtohoston'] = 'Toto portfolio bylo odevzdáno %s.';
$string['viewswithretainviewrightsmustbecopyable'] = 'Musíte povolit kopírování předtím než budete moci nastavit zachování přístupu stránky pro prohlížení.';
$string['viewtitleby'] = '%s - <a href="%s">%s</a>';
$string['viewunobjectionablebody'] = '%s se podíval na %s od %s a označil to jako již dále nezávadný materiál.';
$string['viewunobjectionablesubject'] = 'Stránka %s byla označena %s jako nezávadný materiál';
$string['viewurl'] = 'URL adresa stránky';
$string['viewurldescription'] = 'Čitelná adresa pro vaši stránku. Toto pole musí mít délku 3 až 100 znaků.';
$string['viewvisitcount'] = array(

		0 => '%d návštěva stránky od %s do %s', 
		1 => '%d návštěvy stránky od %s do %s', 
		2 => '%d návštěv stránky od %s do %s'
);
$string['watchlistupdated'] = 'Seznam sledovaných stránek byl aktualizován.';
$string['whocanseethisview'] = 'Kdo může vidět tuto stránku';
$string['whosharewith'] = 'S kým chcete sdílet?';
$string['wrongblocktype'] = 'vedené ID není platným typem bloku.';
$string['youhavenoviews'] = 'Nemáte žádné stránky';
$string['youhavenoviews1'] = 'Nemáte žádné stránky ani sbírky.';
$string['youhaventcreatedanyviewsyet'] = 'Dosud jste nevytvořili žádné stránky.';
$string['youhavenviews'] = array(

		0 => 'Máte 1 stránku.', 
		1 => 'Máte %d stránky.', 
		2 => 'Máte %d stránek.'
);
$string['youhaveoneview'] = 'Máte jednu stránku.';
$string['youhavesubmitted'] = 'Odeslali jste <a href="%s">%s</a> do této skupiny';
$string['youhavesubmittedon'] = 'Odeslali jste <a href="%s">%s</a> do této skupiny na %s';
$string['youhaveviews'] = 'Počet vašich stránek je: %s.';
$string['yoursubmissions'] = 'Odevzdali jste';
$string['100'] = 'Stejné šířky';
