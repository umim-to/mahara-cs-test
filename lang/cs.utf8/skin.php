<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Untitled'] = 'Bez názvu';
$string['addfontvariant'] = 'Přidat styl písma';
$string['addtofavorites'] = 'Přidat do oblíbených';
$string['addtofavoritesspecific'] = 'Přidat "%s" do oblíbených';
$string['allskins'] = 'Všechny vzhledy stránek';
$string['archivereadingerror'] = 'Chyba při čtení ZIP archivu.';
$string['backgroundattachment'] = 'Obrázek na pozadí';
$string['backgroundcolor'] = 'Barva na pozadí';
$string['backgroundfixed'] = 'Pevné umístění';
$string['backgroundposition'] = 'Pozice obrázku na pozadí';
$string['backgroundrepeat'] = 'Opakování obrázku na pozadí';
$string['backgroundrepeatboth'] = 'Opakovat v obou směrech';
$string['backgroundrepeatno'] = 'Neopakovat';
$string['backgroundrepeatx'] = 'Opakovat pouze horizontálně';
$string['backgroundrepeaty'] = 'Opakovat pouze vertikálně';
$string['backgroundscroll'] = 'Posun s obsahem';
$string['blockheaderfontcolor'] = 'Barva textu záhlaví bloků';
$string['blockheaderfontfamily'] = 'Písmo záhlaví bloků';
$string['blockheading'] = 'Blok záhlaví';
$string['bodybackgroundcolour'] = 'Barva pozadí stránky';
$string['bodybackgroundimage'] = 'Obrázek na pozadí stránky';
$string['bodybgcolor'] = 'Barva pozadí vzhledu';
$string['bodybgcolor1'] = 'Barva pozadí';
$string['bodybgimage'] = 'Obrázek pozadí vzhledu';
$string['bodybgimage1'] = 'Obrázek pozadí';
$string['bold'] = 'Tučné';
$string['bolditalic'] = 'Tučné s kurzívou';
$string['bottom'] = 'Dole';
$string['bottomleft'] = 'Vlevo dole';
$string['bottomright'] = 'Vpravo dole';
$string['buttontextcolor'] = 'Barva textu v tlačítku';
$string['cantdeletefont'] = 'Nemůžete odstranit toto písmo.';
$string['cantdeleteskin'] = 'Nemůžete odstranit tento vzhled stránky.';
$string['cantremoveskinfromfavorites'] = 'Nemůžete odstranit vzhled z oblíbených';
$string['centre'] = 'Uprostřed';
$string['chooseskin'] = 'Vyberte vzhled';
$string['chooseviewskin'] = 'Vyberte vzhled stránky';
$string['clickimagetoedit'] = 'Klikněte pro editaci obrázku';
$string['clicktoedit'] = 'Klikněte pro editaci vzhledu';
$string['closemetadata'] = 'Zavřít informace o vzhledu';
$string['createskin'] = 'Vytvořit vzhled stránek';
$string['creationdate'] = 'Vytvořeno';
$string['currentskin'] = 'Aktuální vzhled';
$string['deletefont'] = 'Odstranit písmo';
$string['deletefontconfirm1'] = 'Opravdu chcete odstranit toto písmo?';
$string['deletefontconfirm2'] = 'Tuto akci nelze vrátit zpět.';
$string['deletefontconfirmused'] = array(

		0 => 'Tento styl písma používá %s vzhled.', 
		1 => 'Tento styl písma používají %s vzhledy.', 
		2 => 'Tento styl písma používá %s vzhledů.'
);
$string['deleteskinconfirm'] = 'Opravdu chcete odstranit tento vzhled? Tuto akci nelze vrátit zpět.';
$string['deleteskinusedinpages'] = array(

		0 => 'Vzhled, který se chystáte odstranit se používá na %d stránce.', 
		1 => 'Vzhled, který se chystáte odstranit se používá na %d stránkách.', 
		2 => 'Vzhled, který se chystáte odstranit se používá na %d stránkách.'
);
$string['deletespecifiedfont'] = 'Odstranit písmo \'%s\'';
$string['deletespecifiedskin'] = 'Odstranit vzhled \'%s\'';
$string['deletethisskin'] = 'Odstranit tento vzhled';
$string['description'] = 'Popis';
$string['displayname'] = 'Vlastník';
$string['editfont'] = 'Upravit písmo';
$string['editproperties'] = 'Upravit vlastnosti písma';
$string['editsiteskin?'] = 'Toto je vzhled webu. Chcete ho upravit?';
$string['editskin'] = 'Upravit vzhled';
$string['editthisskin'] = 'Upravit tento vzhled';
$string['emphasizedcolor'] = 'Barva zvýrazněného textu';
$string['emphasizedcolordescription'] = 'Toto je barva pro dílčí záhlaví stránky a zvýrazněný text';
$string['eotdescription'] = 'Písmo Embedded OpenType (pro Internet Explorer 4 a vyšší)';
$string['exportskins'] = 'Exportovat vzhled';
$string['exportskinsmenu'] = 'Export';
$string['exportspecific'] = 'Exportovat "%s"';
$string['exportthisskin'] = 'Exportovat tento vzhled';
$string['favoriteskins'] = 'Oblíbené vzhledy';
$string['filepathnotwritable'] = 'Nelze zapisovat soubory do \'%s\'';
$string['font'] = 'písmo';
$string['fontdeleted'] = 'Písmo odstraněno';
$string['fontedited'] = 'Písmo bylo úspěšně upraveno';
$string['fontfileeot'] = 'Písmo EOT';
$string['fontfilelicence'] = 'Licenční soubor';
$string['fontfilemissing'] = 'ZIP archiv neobsahuje soubor písma \'%s\'';
$string['fontfiles'] = 'Písma';
$string['fontfilesvg'] = 'Písmo SVG';
$string['fontfilettf'] = 'Písmo TTF';
$string['fontfilewoff'] = 'Písmo WOFF';
$string['fontfilezip'] = 'Archiv ZIP';
$string['fontinstalled'] = 'Písmo bylo úspěšně nainstalováno';
$string['fontlicence'] = 'Licence písma';
$string['fontlicencenotfound'] = 'Licence písma nebyla nalezena';
$string['fontname'] = 'Název písma';
$string['fontnotice'] = 'Popis písma';
$string['fontnoticedescription'] = 'Jeden řádek přidaný do CSS souboru popisuje písmo a jeho autora.';
$string['fonts'] = 'písma';
$string['fontsize'] = 'Velikost písma';
$string['fontsizelarge'] = 'velká';
$string['fontsizelarger'] = 'vetší';
$string['fontsizelargest'] = 'největší';
$string['fontsizemedium'] = 'střední';
$string['fontsizesmall'] = 'malá';
$string['fontsizesmaller'] = 'menší';
$string['fontsizesmallest'] = 'nejmenší';
$string['fontsort.alpha'] = 'Abeceda';
$string['fontsort.date'] = 'Datum přidání';
$string['fontsort.popularity'] = 'Popularita';
$string['fontsort.style'] = 'Počet stylů';
$string['fontsort.trending'] = 'Trendy';
$string['fontstyle'] = 'Řez písma';
$string['fonttype'] = 'Typ písma';
$string['fonttype.google'] = 'Písmo Google web';
$string['fonttype.site'] = 'Místní písmo';
$string['fonttype.t_maroon'] = 'Písmo motivů: Maroon';
$string['fonttype.t_modern'] = 'Písmo motivů: Modern';
$string['fonttype.t_ocean'] = 'Písmo motivů: Ocean';
$string['fonttype.t_primaryschool'] = 'Písmo motivů: Primary school';
$string['fonttype.t_raw'] = 'Písmo motivů: Raw';
$string['fonttypes.all'] = 'Všechna písma';
$string['fonttypes.google'] = 'Písma Google web';
$string['fonttypes.site'] = 'Lokální písma';
$string['fonttypes.theme'] = 'Písma motivů';
$string['fontuploadinstructions'] = '<br />Pro nahrání potřebných souborů písem můžete buď nahrát ZIP archiv vygenerovaný pomocí <a href="http://www.fontsquirrel.com/fontface/generator/" target="_blank">FontSquirrel Online Generator</a>, nebo nahrát EOT, SVG, TTF, WOFF a licenční soubory zvlášť.';
$string['fontvariantadded'] = 'Styl písma byl úspěšně přidán';
$string['genericfontfamily'] = 'Základní rodiny písem';
$string['gwfinstructions'] = '<ol> <li>Navštivte <a href="http://www.google.com/fonts/" target="_blank">Google fonts</a></li> <li>Přidejte písma a vyberte je do své sbírky</li> <li>Stáhněte písma ve sbírce jako ZIP soubor</li> <li>Nahrajte ZIP soubor  prostřednictvím formuláře</li> <li>Nainstalujte vybraná písma Google</li> </ol>';
$string['gwfontadded'] = 'Písma Google byla úspěšně nainstalována.';
$string['gwfontsnotavailable'] = 'Písma Google nejsou momentálně k dispozici.';
$string['gwfzipdescription'] = 'Platný ZIP soubor obsahuje všechna vybraná písma Google k instalaci.';
$string['gwfzipfile'] = 'Platný ZIP soubor';
$string['header'] = 'Záhlaví';
$string['headerbackgroundcolor'] = 'Barva pozadí záhlaví';
$string['headerbackgroundcolordescription'] = 'Barva pozadí pro záhlaví stránky. Nebude použita, pokud nastavíte také obrázek na pozadí záhlaví.';
$string['headerbackgroundimage'] = 'Obrázek na pozadí záhlaví';
$string['headerbackgroundimagedescription'] = 'Minimální šířka je 1832 px a minimální výška je 232 px.';
$string['headerlogoimage1'] = 'Logo';
$string['headerlogoimagedark1'] = 'Tmavé logo Mahara a text (vhodné pro světlejší pozadí záhlaví)';
$string['headerlogoimagelight1'] = 'Bíle logo Mahara a text (vhodné pro tmavší pozadí záhlaví)';
$string['headerlogoimagenormal'] = 'Výchozí logo vzhledu';
$string['headingandtext'] = 'Záhlaví a text';
$string['headingcolor'] = 'Barva záhlaví stránky';
$string['headingcolor1'] = 'Barva textu záhlaví';
$string['headingcolordescription'] = 'Toto je barva záhlaví stránky.';
$string['headingcolordescription2'] = 'Toto je barva textu pro oblast záhlaví.';
$string['headingfontfamily'] = 'Písmo záhlaví';
$string['headingonly'] = 'Pouze záhlaví';
$string['hoverbuttoncolor'] = 'Zvýrazněná barva tlačítka';
$string['hoverlinkcolor'] = 'Zvýrazněná barva odkazu';
$string['hoverlinkcolor1'] = 'Barva odkazu při najetí myši';
$string['hoverlinkunderlined'] = 'Podtrhávat odkaz při najetí myši';
$string['import'] = 'Import';
$string['importskins'] = 'Importovat vzhled';
$string['importskinsmenu'] = 'Import';
$string['importskinsnotice'] = 'Vyberte prosím platný XML soubor, pro import, který obsahuje definici vzhledu stránky.';
$string['installfont'] = 'Instalovat písma';
$string['installfontinstructions'] = '<p> Přidat písma, která umožňují vkládání písem do webových stránek prostřednictvím CSS pravidla @font-face. Pamatujte, že ne všichni autoři nebo vlastníci práv to dovolují. </p> <p> Pokud najdete vhodné volné písmo, u něhož je povoleno vkládání do vlastních webových stránek, je nutné jej převést do jednoho z následujících formátů: <br />Písma TrueType, Embeed OpenType, Web Open Font Format a SVG. </p> <p>Ke konverzi můžete použít např. <a href="http://www.fontsquirrel.com/fontface/generator/" target="_blank">online generátor FontSquirrel</a>. </p>';
$string['installfontinstructions1'] = '<p> Přidejte fonty, které umožňují vnoření fontů do webových stránek, přes CSS pravidlo @font-face. Pamatujte, že ne všichni autoři / tvůrci toto povolují. </p> <p> Když najdete vhodný font zdarma, který je povoleno vnořit do webové stránky, musíte jej nejdříve zkonvertovat do následujích formátů: <br />TrueType Font, Embedded OpenType Font, Web Open Font Format Font and Scalable Vector Graphic Font. </p> <p> Pro tuto konverzi můžete použít <a href="https://www.fontsquirrel.com/tools/webfont-generator/">FontSquirrel Online Generator</a>. </p> <p> Alternativně můžete nainstalovat nějaký z Google fontů pomocí následujících kroků: <ol> <li>Navštivte <a href="https://google-webfonts-helper.herokuapp.com">Google webfonts helper</a>. Je to však na vaše vlastní riziko, tato služba není poskytována Google Fonty. </li> <li>Zvolte font, který hledáte.</li> <li>Zvolte sadu znaků, kterou vyžadujete.</li> <li>Zvolte styly, které chcete mít k dispozici.</li> <li>Stáhněte výsledný soubor ZIP z této stránky ve 4. kroku.</li> <li>Nahrajte tento ZIP soubor pomocí tohoto formuláře.</li> </ol> </p>';
$string['installgwfont'] = 'Instalovat písma Google';
$string['invalidfonttitle'] = 'Neplatný název písma. Musí obsahovat alespoň jeden alfanumerický znak.';
$string['italic'] = 'Kurzíva';
$string['left'] = 'Vlevo';
$string['linkunderlined'] = 'Podtržení odkazu';
$string['manageskins'] = 'Správa vzhledů';
$string['metatitle'] = 'Informace o vzhledu';
$string['modifieddate'] = 'Upraveno';
$string['myskins'] = 'Vzhled stránek';
$string['nfonts'] = array(

		0 => '%s font', 
		1 => '%s fonty', 
		2 => '%s fontů'
);
$string['nofonts'] = 'Nejsou k dispozici žádná písma.';
$string['normalbuttoncolor'] = 'Barva normálího tlačítka';
$string['normallinkcolor'] = 'Barva normálního odkazu';
$string['normallinkunderlined'] = 'Podtrhávat normální odkaz';
$string['noskin'] = 'Žádný vzhled';
$string['noskins'] = 'Nejsou k dispozici žádné vzhledy';
$string['nosuchfont'] = 'Písmo s daným názvem neexistuje.';
$string['notcompatiblewithpagetheme'] = 'Šablona této stránky "%s" nepodporuje vzhledy stránek. Proto vzhled stránek, který zvolíte, nebude mít vliv na výsledný vzhled stránek, dokud nezvolíte šablonu jinou.';
$string['notcompatiblewiththeme'] = 'Šablona vaší Mahary "%s" nepodporuje vzhledy stránek. To znamená, že pokud vyberete jiný vzhled stránek, neuvidíte žádnou změnu. Pro uživatele s jinou šablonou ovšem tato změna viditelná být může.';
$string['notsavedyet'] = 'Dosud neuloženo.';
$string['notvalidfontfile'] = 'Toto není platný %s soubor písma.';
$string['notvalidxmlfile'] = 'Nahraný soubor není platný XML soubor.';
$string['notvalidzipfile'] = 'Toto není platný ZIP soubor.';
$string['nskins'] = array(

		0 => '%s vzhled stránky', 
		1 => '%s vzhledy stránek', 
		2 => '% vzhledů stránek'
);
$string['pluginname'] = 'Vzhled stránky';
$string['preview'] = 'Náhled';
$string['previewheading'] = 'Lorem ipsum';
$string['previewsubhead1'] = 'Scriptum';
$string['previewsubhead2'] = 'Imago';
$string['previewtextline1'] = 'Lorem ipsum dolor sit amet,';
$string['previewtextline2'] = 'consectetur adipiscing elit.';
$string['previewtextline3'] = 'Donec cursus orci turpis.';
$string['previewtextline4'] = 'Donec et bibendum augue.';
$string['previewtextline5'] = 'Vestibulum ante ipsum primis';
$string['previewtextline6'] = 'in faucibus orci luctus et';
$string['previewtextline7'] = 'ultrices posuere cubilia Curae;';
$string['previewtextline8'] = 'Cras odio enim, sodales at';
$string['previewtextline9'] = 'rutrum et, sollicitudin non nisi.';
$string['privateskinaccess'] = 'Toto je soukromý vzhled';
$string['publicskinaccess'] = 'Toto je veřejný vzhled';
$string['publicskins'] = 'Veřejné vzhledy stránek';
$string['regular'] = 'Obyčejné';
$string['removefromfavorites'] = 'Odstranit z oblíbených';
$string['removefromfavoritesspecific'] = 'Odstranit "%s" z oblíbených';
$string['right'] = 'Vpravo';
$string['samplefonttitle'] = 'Název písma';
$string['samplesize'] = 'Velikost';
$string['samplesort'] = 'Třídění';
$string['sampletext'] = 'Text';
$string['sampletext11'] = 'Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz';
$string['sampletext12'] = 'Àà Áá Ââ Ãã Ää Åå Ææ Çç Èè Éé Êê Ëë Ìì Íí Îî Ïï Ðð Ññ Òò Óó Ôô Õõ Öö Øø Ùù Úú Ûû Üü Ýý Þþ ß';
$string['sampletext13'] = 'Āā Ăă Ąą Ćć Čč Ďď Đđ Ēē Ėė Ęę Ěě Ğğ Ģģ Īī Ĭĭ Įį İı Ķķ Ĺĺ Ļļ Ľľ Łł Ńń Ņņ Ňň Ōō Őő Œœ Ŕŕ Ŗŗ Řř Śś Şş Šš Ţţ Ťť Ūū Ŭŭ Ůů Űű Ųų Źź Żż Žž ſ';
$string['sampletext14'] = 'Аа Бб Вв Гг Дд Ее Ёё Жж Зз Ии Йй Кк Лл Мм Нн Оо Пп Рр Сс Тт Уу Фф Хх Цц Чч Шш Щщ Ъъ Ыы Ьь Ээ Юю Яя';
$string['sampletext15'] = 'Αα Ββ Γγ Δδ Εε Ζζ Ηη Θθ Ιι Κκ Λλ Μμ Νν Ξξ Οο Ππ Ρρ Σσς Ττ Υυ Φφ Χχ Ψψ Ωω';
$string['sampletext18'] = '1 2 3 4 5 6 7 8 9 0 ¼ ½ ¾ ⅓ ⅔ ⅛ ⅜ ⅝ ⅞ ¹ ² ³';
$string['sampletext19'] = '& ! ? » « @ $ € § * # %% / ()  {} []';
$string['sampletext20'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
$string['sampletext21'] = 'Nechť již hříšné saxofony ďáblů rozzvučí síň úděsnými tóny waltzu, tanga a quickstepu.';
$string['sampletext22'] = 'Příliš žluťoučký kůň úpěl ďábelské ódy.';
$string['sampletitle11'] = 'Latinka (pouze ASCII)';
$string['sampletitle12'] = 'Latinka (ISO/IEC 8859-1)';
$string['sampletitle13'] = 'Latinka (ISO/IEC 8859-2)';
$string['sampletitle14'] = 'Cyrilice (ISO/IEC 8859-5)';
$string['sampletitle15'] = 'Řecká abeceda (ISO/IEC 8859-7)';
$string['sampletitle18'] = 'Čísla a zlomky';
$string['sampletitle19'] = 'Interpunkce';
$string['sampletitle20'] = 'Lorem ipsum...';
$string['sampletitle21'] = 'Nechť již hříšné saxofony...';
$string['sampletitle22'] = 'Příliš žluťoučký kůň...';
$string['showfonts'] = 'Zobrazit';
$string['sitefonts'] = 'Písma';
$string['sitefontsdescription'] = '<p>Následující písma byla nainstalována na vašem webu a lze je využít ve vzhledech stránek.</p>';
$string['sitefontsmenu'] = 'Písma';
$string['siteskinaccess'] = 'Toto je vzhled stránky webu';
$string['siteskinmenu'] = 'Vzhledy stránek';
$string['siteskins'] = 'Vzhledy stránek webu';
$string['skin'] = 'Vzhled';
$string['skinaccessibility'] = 'Přístupnost vzhledu';
$string['skinaccessibility1'] = 'Přístup ke vzhledu';
$string['skinaddedtofavorites'] = 'Vzhled byl přidán do oblíbených';
$string['skinbackgroundoptions'] = 'Pozadí vzhledu';
$string['skinbackgroundoptions1'] = 'Pozadí';
$string['skincustomcss'] = 'Vlastní CSS';
$string['skincustomcssdescription'] = 'Vlastní CSS se neprojeví v náhledu vzhledu stránek.';
$string['skindeleted'] = 'Vzhled odstraněn';
$string['skindescription'] = 'Popis vzhledu';
$string['skingeneraloptions'] = 'Obecné';
$string['skinimported'] = 'Vzhled byl úspěšně importován';
$string['skinnotselected'] = 'Vzhled nebyl vybrán';
$string['skinpreview'] = 'Náhled vzhledu "%s"';
$string['skinpreviewedit'] = 'Náhled vzhledu "%s" - pro editaci vzhledu klikněte';
$string['skinremovedfromfavorites'] = 'Vzhled byl odstraněn z oblíbených';
$string['skins'] = 'vzhledy';
$string['skinsaved'] = 'Vzhled byl úspěšně uložen';
$string['skintitle'] = 'Název vzhledu';
$string['svgdescription'] = 'Písmo SVG (pro iPad a iPhone)';
$string['tableborder'] = 'Barva ohraničení tabulky';
$string['tableevenrows'] = 'Barva pozadí pro sudé řádky';
$string['tableheader'] = 'Barva pozadí v hlavičce';
$string['tableheadertext'] = 'Barva textu v hlavičce';
$string['tableoddrows'] = 'Barva pozadí pro liché řádky';
$string['textcolor'] = 'Barva textu';
$string['textcolordescription'] = 'Toto je barva běžného textu.';
$string['textfontfamily'] = 'Písmo';
$string['themedefault'] = 'Výchozí motiv';
$string['title'] = 'Název';
$string['top'] = 'Nahoře';
$string['topleft'] = 'Vlevo nahoře';
$string['topright'] = 'Vpravo nahoře';
$string['ttfdescription'] = 'Písmo TrueType (pro Firefox 3.5, Operu 10, Safari 3.1, Chrome 4 a vyšší)';
$string['userskins'] = 'Moje vzhledy stránek';
$string['validxmlfile'] = 'Platný XML soubor';
$string['viewadvancedoptions'] = 'Rozšířené';
$string['viewbackgroundoptions'] = 'Pozadí stránky';
$string['viewbgcolor'] = 'Barva pozadí stránky';
$string['viewbgimage'] = 'Obrázek pozadí stránky';
$string['viewcontentoptions'] = 'Písma a barvy stránky';
$string['viewcontentoptions1'] = 'Písma a barvy';
$string['viewfontspecimen'] = 'Zobrazit ukázku písma';
$string['viewfontspecimenfor'] = 'pro "%s"';
$string['viewheaderoptions'] = 'Hlavička stránky';
$string['viewmetadata'] = 'Zobrazit informace o vzhledu';
$string['viewmetadataspecific'] = 'Zobrazit informace o "%s"';
$string['viewskinchanged'] = 'Vzhled stránky změněn';
$string['viewtableoptions'] = 'Tabulky a tlačítka stránky';
$string['viewwidth'] = 'Šířka stránky';
$string['woffdescription'] = 'Písmo Web Open Font Format (pro Firefox 3.6, Internet Explorer 9, Chrome 5 a vyšší)';
$string['zipdescription'] = 'ZIP archiv obsahující EOT, SVG, TTF, WOFF a licenční soubory písma';
$string['zipfontfiles'] = 'Soubory písma v ZIP archivu';
