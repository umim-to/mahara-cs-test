<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, David Mudrák, Adam Pátek, Matouš Trča, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2018
 *
 */

defined('INTERNAL') || die();

$string['errorbehatcommand'] = 'Chyba při spuštění CLI příkazu Behat. Pro další informace zkuste ručně spustit "{$ a} --help" z CLI.';
$string['errorcomposer'] = 'Závislosti pro Composer nejsou nainstalovány.';
$string['errordataroot'] = '$CFG->behat_dataroot není nastaven nebo je neplatný.';
$string['errorsetconfig'] = 'V config.php je třeba nastavit $CFG>behat_dataroot, $CFG->behat_dbprefix a $CFG->behat_wwwroot';
$string['erroruniqueconfig'] = 'Hodnoty $CFG->behat_dataroot, $CFG->behat_dbprefix and $CFG->behat_wwwroot musí být jiné než pro $CFG->dataroot, $CFG->dbprefix, $CFG->wwwroot, $CFG->phpunit_dataroot, and $CFG->phpunit_prefix.';
