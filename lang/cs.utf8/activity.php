<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['addtowatchlist'] = 'Přidat na seznam sledovaných';
$string['adminnotificationerror'] = 'Chyba v upozorněních uživatele je pravděpodobně způsobena konfigurací vašeho serveru.';
$string['adminnotificationerror1'] = 'Chyba v upozorněních byla pravděpodobně zapříčiněna nastavením vašeho serveru.';
$string['alltypes'] = 'Všechny typy';
$string['artefacts'] = 'Položky portfolia';
$string['attime'] = 'v';
$string['blockinstancenotification'] = 'Blok "%s" byl přidán nebo změněn.';
$string['date'] = 'Datum';
$string['deleteallnotifications'] = 'Smazat všechna upozornění';
$string['deletednotifications'] = 'Počet odstraněných upozornění: %s';
$string['deletednotifications1'] = array(

		0 => 'Smazáno %s upozornění', 
		1 => 'Smazána %s upozornění', 
		2 => 'Smazáno %s upozornění'
);
$string['failedtodeletenotifications'] = 'Nelze odstranit upozornění';
$string['failedtomarkasread'] = 'Nelze označit upozornění jako přečtené';
$string['groups'] = 'Skupiny';
$string['institutioninvitemessage'] = 'Příslušnost k této instituci můžete potvrdit na stránce Nastavení institucí:';
$string['institutioninvitesubject'] = 'Byla vám nabídnuta příslušnost k instituci %s';
$string['institutionrequestmessage'] = 'Přiřadit uživatele k této instituci můžete na stránce Členové instituce:';
$string['institutionrequestsubject'] = '%s požádal(a) o příslušnost k %s';
$string['markasread'] = 'Označit jako přečtené';
$string['markedasread'] = 'Upozornění označena jako přečtená';
$string['messageaccessfrom1'] = 'Tuto stránku můžete zobrazit za %s.';
$string['messageaccessfromto1'] = 'Tuto stránku můžete zobrazit od %s do %s.';
$string['messageaccessto1'] = 'Tuto stránku můžete zobrazit do %s.';
$string['messagetype'] = 'Typ zprávy';
$string['missingparam'] = 'Chybí požadovaný parametr %s u typu činnosti %s';
$string['monitored'] = 'Monitorováno';
$string['newaccesssubject'] = array(

		0 => 'Byl Vám udělen přístup k %s portfoliu', 
		1 => 'Byl vám udělen přístup k %s portfoliím', 
		2 => 'Byl vám udělen přístup k %s portfoliím'
);
$string['newaccesssubjectname'] = array(

		0 => 'Byl Vám udělen přístup k %s portfoliu. Udělil jej: %s', 
		1 => 'Byl vám udělen přístup ke %s portfoliím. Udělil jej: %s', 
		2 => 'Byl vám udělen přístup ke %s portfoliím. Udělil jej: %s'
);
$string['newaccessubjectdefault'] = 'Byl Vám udělen přístup';
$string['newcollectionaccessmessage'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení sbírky  "%s" uživatelem %s';
$string['newcollectionaccessmessagenoowner'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení sbírky "%s"';
$string['newcollectionaccessmessagenoownerviews'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení stránek "%s" ve sbírce "%s"';
$string['newcollectionaccessmessageviews'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení stránek  "%s" ve sbríce "%3$s" uživatelem %2$s';
$string['newcollectionaccesssubject'] = 'Nový přístup ke sbírce "%s"';
$string['newcontactus'] = 'Nový kontakt';
$string['newcontactusfrom'] = 'Nový kontakt z';
$string['newgroupmembersubj'] = '%s je nyní členem skupiny!';
$string['newviewaccessmessage'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení stránky "%s" uživatelem %s';
$string['newviewaccessmessagenoowner'] = 'Byli jste přidání mezi uživatele oprávněné k prohlížení stránky "%s"';
$string['newviewaccessmessagenoownerviews'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení stránek "%s"';
$string['newviewaccessmessageviews'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení stránek "%s" uživatelem %s';
$string['newviewaccesssubject'] = 'Přístup ke stránce';
$string['newviewaccesssubject1'] = 'Nový přístup ke stránce "%s"';
$string['newviewaccesssubjectviews'] = 'Nový přístup ke stránkám "%s"';
$string['newviewmessage'] = '%s vytvořil(a) novou stránku "%s"';
$string['newviewsubject'] = 'Vytvořena nový stránka';
$string['newwatchlistmessage'] = 'Nová činnost na vašem seznamu sledování';
$string['newwatchlistmessageunsubscribe'] = 'Pro ukončení odběru upozornění na úpravy stránky %s prosím pokračujte na odkaz %s';
$string['newwatchlistmessageview'] = '%s změnil(a) novou stránku "%s"';
$string['newwatchlistmessageview1'] = 'Stránka "%s" patřící k %s byla změněna';
$string['nodelete'] = 'Žádná upozornění k odstranění';
$string['nonamegiven'] = 'nebylo zadáno jméno';
$string['noresultsfound'] = 'Nenalezeny žádné zprávy dle zadaných parametrů.';
$string['objectionablecontentview'] = 'Nevhodný obsahv stránky "%s" ohlášen uživatelem %s';
$string['objectionablecontentviewartefact'] = 'Nevhodný obsah na stránce "%s" v "%s" ohlášen uživatelem %s';
$string['objectionablecontentviewartefacthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nevhodný obsah stránky "%s" v "%s" ohlášen uživatelem %s<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Stížnost se týká: <a href="%s">%s</a></p> <p>Ohlášeno uživatelem: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewartefacttext'] = 'Nevhodný obsah na "%s" v "%s" ohlášen uživatelem %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete stránku vidět, navštivte odkaz: %s Pro zobrazení profilu nahlašovatele pak odkaz: %s';
$string['objectionablecontentviewhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nevhodný obsah na stránce "%s" ohlášen uživatelem %s<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Stížnost se týká: <a href="%s">%s</a></p> <p>Ohlášeno uživatelem: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewtext'] = 'Nevhodný obsah na "%s" ohlášen uživatelem %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete stránku vidět, navštivte odkaz: %s Pro zobrazení profilu nahlašovatele pak odkaz: %s';
$string['objectionablereviewview'] = 'Kontrola nevhodnosti obsahu na stránce  "%s" vyžádána %s';
$string['objectionablereviewviewartefact'] = 'Kontrola nevhodnosti obsahu na stránce "%s" v "%s" vyžádána  %s';
$string['objectionablereviewviewartefacthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Kontrola nevhodnosti obsahu na "%s" v "%s" vyžádána  %s<br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Žádost se vztahuje k: <a href="%s">%s</a></p> <p>Vyžádál: <a href="%s">%s</a></p> </div>';
$string['objectionablereviewviewartefacttext'] = 'Kontrola nevhodnosti obsahu na "%s" v "%s" vyžádána  %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pro zobrazení dané stránky klikněte na tento odkaz: %s Pro zobrazení profilu vlastníka, klikněte na tento odkaz: %s';
$string['objectionablereviewviewhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Kontrola nevhodnosti obsahu na "%s" vyžádána  %s <br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Žádost se vztahuje k: <a href="%s">%s</a></p> <p>Vyžádal: <a href="%s">%s</a></p> </div>';
$string['objectionablereviewviewtext'] = 'Kontrola nevhodnosti obsahu na "%s" vyžádána  %s %s------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pro zobrazení dané stránky klikněte na tento odkaz: %s Pro zobrazení profilu vlastníka, klikněte na tento odkaz: %s';
$string['ongroup'] = 'na skupinu';
$string['ownedby'] = 'vlastník';
$string['prefsdescr'] = 'Ať už si vyberete kteroukoliv z možností zasílání e-mailu, budou nové události zobrazeny v Přehledu událostí, ale budou automaticky označeny jako přečtené.';
$string['read'] = 'Přečíst';
$string['reallydeleteallnotifications'] = 'Skutečně chcete smazat všechna svá upozornění?';
$string['recurseall'] = 'Rekurzivně vše';
$string['removedgroupmembersubj'] = '%s již není členem skupiny';
$string['removefromwatchlist'] = 'Odstranit ze seznamu sledování';
$string['selectall'] = 'Vybrat vše';
$string['selectalldelete'] = 'Všechna upozornění k odstranění';
$string['selectallread'] = 'Všechna nepřečtená upozornění';
$string['stillobjectionablecontent'] = 'Zahrnuje nevhodný obsah.';
$string['stillobjectionablecontentsuspended'] = 'Přístup k této stránce byl dočasně znemožněn, dokud nebude nevhodný obsah odstraněn.';
$string['stopmonitoring'] = 'Zastavit monitorování';
$string['stopmonitoringfailed'] = 'Zastavení monitorování selhalo';
$string['stopmonitoringsuccess'] = 'Monitorování úspěšně zastaveno';
$string['subject'] = 'Předmět';
$string['type'] = 'Typ';
$string['typeadminmessages'] = 'Zprávy pro správce';
$string['typecontactus'] = 'Kontaktujte nás';
$string['typefeedback'] = 'Zpětná vazba';
$string['typegroupmessage'] = 'Zpráva pro skupinu';
$string['typeinstitutionmessage'] = 'Zpráva pro instituci';
$string['typemaharamessage'] = 'Systémová zpráva';
$string['typenewpost'] = 'Diskusní příspěvek';
$string['typeobjectionable'] = 'Nevhodný obsah';
$string['typeusermessage'] = 'Zpráva od jiných uživatelů';
$string['typeviewaccess'] = 'Nový přístup na stránku';
$string['typeviewaccessrevoke'] = 'Odebraný přístup na stránku';
$string['typevirusrelease'] = 'Upozornění na virus';
$string['typevirusrepeat'] = 'Ukladání zavirovaného obsahu';
$string['typewatchlist'] = 'Sledování';
$string['unread'] = 'Nepřečteno';
$string['viewmodified'] = 'změnil(a) svou stránku';
$string['viewsubmittedmessage'] = 'Uživatel %s odevzdal stránku "%s" k ohodnocení ve skupině %s';
$string['viewsubmittedmessage1'] = '%s byla odevzdána "%s" do %s';
$string['viewsubmittedsubject'] = 'Stránka odevzdána k ohodnocení v rámci skupiny %s';
$string['viewsubmittedsubject1'] = 'Odevzdání do %s';
$string['yourinboxisempty'] = 'Vaše přijatá pošta je prázdná';
$string['youroutboxisempty'] = 'Vaše odeslaná pošta je prázdná';
