<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['active'] = 'Aktivní';
$string['addauthority'] = 'Přidat autoritu';
$string['alternativelogins'] = 'Správcovské přihlášení';
$string['application'] = 'Aplikace';
$string['authloginmsg'] = 'Vložte zprávu, která se zobrazí uživatelům při pokusu o přihlášení se na tyto stránky pomocí přihlašovacího formuláře Mahary';
$string['authloginmsg2'] = 'Když jste si nevybrali nadřazenou autoritu, zadejte alespoň zprávu, která se zobrazí poté, co se uživatel pokusí přihlásit prostřednictvím přihlašovacího formuláře.';
$string['authloginmsgnoparent'] = 'Vložte zprávu, která se zobrazí, když se uživatel snaží přihlásit přes přihlašovací formulář.';
$string['authname'] = 'Jméno autority';
$string['cannotjumpasmasqueradeduser'] = 'Nemůžete přejít do jiné aplikace, zatimco se vydáváte za cizího uživatele.';
$string['cannotremove'] = 'Tento autentizační modul nelze odebrat, protože je jediný pro tuto instituci';
$string['cannotremoveinuse'] = 'Tento autentizační modul nelze odebrat, protože je používán některými uživateli. Nejprve je třeba upravit jejich nastavení.';
$string['cantretrievekey'] = 'Nepodařilo se získat veřejný klíč vzdáleného serveru.<br />Ujistěte se, že máte správně vyplněna pole Aplikace a kořenový adresář WWW a že vzdálený server má zapnutou podporu síťových služeb.';
$string['changepasswordurl'] = 'URL ke změně hesla';
$string['duplicateremoteusername'] = 'Uživatelské jméno předané externí autentifikací je již používáno uživatelem %s. Externí uživatelská jména musí být unikátní v rámci všech autentifikačních metod.';
$string['duplicateremoteusernameformerror'] = 'Externí uživatelská jména musí být unikátní v rámci všech autentifikačních metod.';
$string['editauthority'] = 'Upravit autoritu';
$string['errnoauthinstances'] = 'Vypadá to, že nemáme nakonfigurovanou žádnou instanci autentizačního modulu pro hostitele %s';
$string['errnoxmlrpcinstances'] = 'Vypadá to, že nemáme nakonfigurovanou žádnou instanci autentizačního modulu XMLRPC pro hostitele %s';
$string['errnoxmlrpcuser'] = 'Je mi líto, ale nepodařilo se vás přihlásit. Možná, že vaše SSO přihlášení vypršelo. Vraťte se na partnerský server a klikněte na příslušný odkaz, který vás znovu přivede na tento portál. Dalším důvodem může být, že nemáte oprávnění přihlásit se na tento portál přes SSO. V případě nejasností se spojte s vaším správcem.';
$string['errnoxmlrpcuser1'] = 'Nebyli jsme schopni vás v tuto chvíli ověřit. To může být pravděpodobně proto, že: * Vaše relace SSO vypršela. Vraťte se do jiné aplikace a klikněte na odkaz pro přihlášení do %s znovu. * Nemáte povolen přístup skrze SSO na %s. Prosím, poraďte se se svým správcem, pokud si myslíte, že by vám měl být umožněn.';
$string['errnoxmlrpcwwwroot'] = 'Nemáme záznam pro žádného hostitele na %s';
$string['errorcertificateinvalidwwwroot'] = 'Tento certifikát byl vydán pro %s, ale vy se jej snažíte použít pro %s.';
$string['errorcouldnotgeneratenewsslkey'] = 'Nemohu vygenerovat nový SSL klíč. Jste si jistí, že máte na tomto počítači nainstalováno OpenSSL a pro něj také příslušný modul PHP?';
$string['errornotvalidsslcertificate'] = 'Neplatný SSL certifikát';
$string['errorunabletologin'] = 'Nemůžete se přihlásit';
$string['host'] = 'Jméno hostitele nebo adresa';
$string['hostwwwrootinuse'] = 'kořenový adresář WWW je již používán jinou institucí (%s)';
$string['ipaddress'] = 'IP adresa';
$string['mobileuploadnotenabled'] = 'Omlouváme se, ale upload z mobilních zařízení není povolen.';
$string['mobileuploadtokennotfound'] = 'Omlouváme se, ale tento token nebyl nalezen. Prosím zkontrolujte svojí stránku a nastavení mobilní aplikace.';
$string['mobileuploadtokennotset'] = 'Váš token pro nahrávání nemůže být prázdný. Zkontrolujte nastavení vaší mobilní aplikace a zkuste to znovu.';
$string['mobileuploadusernamenotset'] = 'Vaše uživatelské jméno pro nahrávání nemůže být prázdné. Zkontrolujte nastavení vaší mobilní aplikace a zkuste to znovu.';
$string['name'] = 'Název stránek';
$string['noauthpluginconfigoptions'] = 'Tento modul nemá žádné konfigurační parametry';
$string['nodataforinstance'] = 'Nelze najít data pro instanci autentizačního modulu';
$string['nodataforinstance1'] = 'Nepodařilo se nalézt data pro autentikační instanci "%s".';
$string['nullprivatecert'] = 'Nebylo možné vygenerovat privátní klíč';
$string['nullpubliccert'] = 'Nebylo možné vygenerovat veřejný certifikát';
$string['parent'] = 'Nadřazená autoria';
$string['port'] = 'Číslo portu';
$string['primaryemaildescription'] = 'Primární emailová adresa. Budete dostávat zprávy obsahující odkaz - Klikněte na něj pro potvrzení správnosti vaší adresy a přihlašte se do systému';
$string['protocol'] = 'Protokol';
$string['requiredfields'] = 'Požadovaná pole profilu';
$string['requiredfieldsset'] = 'Požadovaná pole profilu nastavena';
$string['saveinstitutiondetailsfirst'] = 'Nejprve uložte podrobnosti o instituci a poté nastavte autentizační moduly';
$string['shortname'] = 'Krátký název vašich stránek';
$string['ssodirection'] = 'směr SSO';
$string['theyautocreateusers'] = 'Oni automaticky vytvářejí uživatele';
$string['theyssoin'] = 'Jejich uživatelé se k nám mohou hlásit přes SSO';
$string['toomanytries'] = 'Překročili jste maximální počet pokusů o přihlášení. Tento účet byl uzamčen na dobu pěti minut.';
$string['unabletosigninviasso'] = 'Nelze se přihlásit přes SSO';
$string['updateuserinfoonlogin'] = 'Automaticky aktualizovat informace o uživateli';
$string['updateuserinfoonlogindescription'] = 'Znovu získat informace o uživateli ze vzdáleného serveru a aktualizovat lokální záznamy při každém přihlášení';
$string['validationprimaryemailsent'] = 'Ověřující email byl odeslán. Klikněte na odkaz, který je v něm obsažen.';
$string['warninstitutionregistration'] = '$cfg->usersuniquebyusername je zapnuto, ale instituce má povolené registrace. Z bezpečnostních důvodů musí mít všechny instituce deaktivované registrace. Pro změnu tohoto nastavení z webového rozhraní budete muset dočasně nastavit hodnotu $cfg->usersuniquebyusername = false.';
$string['warninstitutionregistrationinstitutions'] = array(

		0 => 'Následující instituce má povolené registrace: %2$s', 
		1 => 'Následující instituce mají povolené registrace: %2$s', 
		2 => 'Následující instituce mají povolené registrace: %2$s'
);
$string['warnmultiinstitutionsoff'] = '$cfg->usersuniquebyusername je zapnuto, ale globální volba \'Uživatelé jsou oprávněni spadat pod vícero institucí\' je deaktivovaná. To nedává smysl, protože to způsobí změnu instituce uživatele při každém přihlášení odjinud. Zapněte prosím tuto volbu v sekci "Správa → Nastavení stránek → Nastavení institucí".';
$string['weautocreateusers'] = 'My automaticky vytváříme uživatele';
$string['weimportcontent'] = 'My importujeme obsah (pouze z některých aplikací)';
$string['weimportcontentdescription'] = '(pouze z některých aplikací)';
$string['wessoout'] = 'Naši uživatelé se k nim mohou hlásit přes SSO';
$string['wwwroot'] = 'Kořenová WWW adresa';
$string['xmlrpccouldnotlogyouin'] = 'Je mi líto, ale nepodařilo se vás přihlásit :-(';
$string['xmlrpccouldnotlogyouindetail'] = 'Bohužel, vaše přihlášení se nezdařilo. Zkuste to, prosím, za chvíli znovu a pokud problém přetrvá, spojte se s vaším správcem.';
$string['xmlrpccouldnotlogyouindetail1'] = 'Je mi líto, ale nepodařilo se vás přihlásit do %s. Prosím, zkuste to zakrátko znovu. Pokud problém přetrvává, obraťte se na správce.';
$string['xmlrpcserverurl'] = 'URL adresa serveru XML-RPC';
