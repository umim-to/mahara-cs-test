<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = 'Přístup odepřen';
$string['accessdeniedbadge'] = 'Nemáte přístup k tomuto odznaku.';
$string['accessdeniedexception'] = 'Nemáte právo zobrazit tuto stránku.';
$string['accessdeniednourlsecret'] = 'Nemáte přístup k této funkcionalitě. Zahrňte prosím do URL hodnotu "urlsecret" ze souboru config.php.';
$string['accessdeniedobjection'] = 'Přístup odepřen. Stížnost již byla vyřešena jiným správcem.';
$string['accessdeniedsuspension'] = 'Toto portfolio je nyní revidováno.';
$string['apcstatoff'] = 'Váš server je spuštěn s hodnotou APC apc.stat = 0. Mahara nepodporuje tuto konfiguraci. Je třeba nastavit apc.stat = 1 v souboru php.ini. Pokud používáte sdílený hosting, je malá pravděpodobnost, že budete moci tuto změnu provést, proto se obraťte na svého poskytovatele hostingu. Možná byste měli uvažovat o přemístění na jiný počítač.';
$string['artefactnotfound'] = 'Položka portfolia s identifikátorem %s nebyla nalezena';
$string['artefactnotfoundmaybedeleted'] = 'Položka portfolia s identifikátorem %s nebyla nalezena. Možná již byla odstraněna.';
$string['artefactnotinview'] = 'Položka portfolia %s není na stránce %s';
$string['artefactonlyviewableinview'] = 'Položka portfolia tohoto typu je zobrazitelná jen na stránce.';
$string['artefactpluginmethodmissing'] = 'Modul %s musí implementovat metodu %s.';
$string['artefactsnotfound'] = 'Nebyly nalezeny položky portfolia s následujícím(i) id: %s';
$string['artefacttypeclassmissing'] = 'Typy položek portfolia musí implementovat nějakou třídu. Chybí %s.';
$string['artefacttypemismatch'] = 'Neshoda v položkách portfolia, pokoušíte se použít %s jako %s.';
$string['artefacttypenametaken'] = 'Typ položky portfolia %s je již zabrán modulem %s.';
$string['badsessionhandle'] = 'Obslužná relace pro ukládání "%s" není správně nastavena. Zkontrolujte prosím nastavení v souboru "config.php".';
$string['blockconfigdatacalledfromset'] = 'Konfigurační data (configdata) by se neměly nastavovat přímo. Použijte PluginBlocktype::instance_config_save.';
$string['blockinstancednotfound'] = 'Instance bloku s identifikátorem %s nebyl nalezen';
$string['blockinstancenotfound'] = 'Instance bloku s id %s nenalezena.';
$string['blocktypelibmissing'] = 'V bloku %s modulu %s chybí knihovna lib.php.';
$string['blocktypemissingconfigform'] = 'Blok %s musí implementovat instance_config_form.';
$string['blocktypenametaken'] = 'Blok %s je již zabrán modulem %s.';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'Toto bude nainstalováno v průběhu instalace modulu %s.';
$string['cannotputblocktypeintoview'] = 'Typ bloku %s nebylo možné vložit do této stránky';
$string['classmissing'] = 'Chybějící třída %s typu %s v modulu %s.';
$string['collectionnotfound'] = 'Sbírka s id %s nebyla nalezena';
$string['couldnotmakedatadirectories'] = 'Z nějakého důvodu nemohly být některé z hlavních datových adresářů vytvořeny. K tomu by nemělo dojít, protože Mahara si ověřila, že má právo zápisu do datového adresáře. Zkontrolujte nastavení přístupových práv k datovému adresáři pro proces webového serveru.';
$string['cssnotpresent'] = 'Ve složce htdocs/theme/raw/style nejsou CSS soubory. Pokud spouštíte Maharu pomocí git checkout, použijte "make css" pro kompilaci CSS souborů. Jestli spouštíte Maharu ze ZIP archivu, zkuste jej stáhnout a rozbalit znovu.';
$string['curllibrarynotinstalled'] = 'Nemáte nainstalováno PHP rozšíření cURL. Tato knihovna je potřebná pro integraci s Moodle a pro odebírání externích RSS kanálů. Ujistěte se, že je cURL nainstalováno a povoleno v php.ini.';
$string['datarootinsidedocroot'] = 'Do nastaveného adresáře s daty (data root) %s nelze zapisovat. To znamená, že data relací, uživatelské soubory ani nic dalšího, co je potřeba na server nahrát, není možné uložit. Pokud tento adresář neexistuje, vytvořte ho a nastavte mu oprávnění totožné s uživatelským účtem, který používá webový server.';
$string['datarootnotwritable'] = 'Do nastaveného datového adresáře %s (dataroot) nelze zapisovat. Mahara potřebuje, aby proces webového serveru mohl do tohoto adresáře ukládat soubory session, soubory uživatelů.';
$string['dbconnfailed'] = 'Mahara se nemůže připojit ke své databázi.
* Pokud jste běžný uživatel a právě jste pracovali s těmito stránkami, vyčkejte chvilku a akci opakujte
* Pokud jste správce stránek, zkontrolujte konfiguraci připojení k databázi a ujistěte se, že je databáze dostupná. Následuje popis chyby:';
$string['dbnotutf8'] = 'Vaše databáze není v kódování UTF-8. Mahara ukládá všechna data v UTF-8. Je třeba znovu založit databázi s korektně nastaveným kódováním.';
$string['dbnotutf8mb4'] = 'Vaše databáze nepoužívá sadu znaků utf8mb4 (4-Byte UTF-8 Unicode Encoding). Mahara však interně ukládá data jako utf8mb4. O aktualizaci se můžete pokusit, nicméně doporučujeme převedení vaší databáze do utf8mb4.';
$string['dbversioncheckfailed'] = 'Mahara vyžaduje vyšší verzi vašeho databázového serveru. Momentálně máte %s %s, ale Mahara potřebuje alespoň verzi %s.';
$string['domextensionnotloaded'] = 'Konfigurace vašeho serveru neobsahuje rozšíření dom. Ten Mahara vyžaduje k tomu, aby mohla procházet XML data z různých zdrojů.';
$string['errorat'] = 'v';
$string['fileuploadtoobig'] = 'Soubor je příliš velký, protože jeho velikost přesahuje "%s"';
$string['fileuploadtoomany'] = 'Snažíte se nahrát příliš mnoho souborů. Můžete jich nahrát maximálně %s.';
$string['gdextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření GD. Mahara vyžaduje tuto knihovnu pro manipulaci s grafikou, např. změna rozměrů nahraných obrázků apod. Buď povolte rozšíření "gd" v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['gdfreetypenotloaded'] = 'Rozšíření GD neobsahuje podporu fontů Freetype. Tyto fonty jsou potřeba pro generování kontrolních CAPTCHA obrázků. Opravte konfiguraci PHP a gd.';
$string['gdlibrarylacksgifsupport'] = 'Instalované rozšíření GD pro PHP nepodporuje vytváření a čtení formátu GIF. Pro ukládání obrázků GIF je potřeba jejich plná podpora.';
$string['gdlibrarylacksjpegsupport'] = 'Instalované rozšíření GD pro PHP nepodporuje formát JPEG/JPG. Pro ukládání obrázků JPEG/JPG je potřeba jejich plná podpora.';
$string['gdlibrarylackspngsupport'] = 'Instalované rozšíření GD pro PHP nepodporuje formát PNG. Pro ukládání obrázků PNG je potřeba jejich plná podpora.';
$string['institutionprivacystatementnotfound'] = 'Prohlášení o ochraně osobních údajů pro "%s" s ID "%s" nebylo nalezeno.';
$string['interactioninstancenotfound'] = 'Instance aktivity s identifikátorem %s nenalezena.';
$string['intlextensionnotloaded'] = 'Nastavení vašeho serveru nevyužívá rozšíření intl pro PHP. Mahara vyžaduje toto rozšíření k analýze informací související s místními jazyky.';
$string['invaliddirection'] = 'Neplatný směr %s.';
$string['invalidlayoutselection'] = 'Pokusili jste se zvolit rozvržení, které neexistuje.';
$string['invalidnumrows'] = 'Pokusili jste se vytvořit rozvržení s větším počtem řádků než je povoleno.';
$string['invaliduser'] = 'Zvolen neplatný uživatel';
$string['invalidviewaction'] = 'Neplatná operace %s';
$string['isolatedinstitutionsremoverules'] = 'Máme skrytá pravidla přístupu %s vzhledem k tomu, že jsou v účinnosti jednotlivé instituce. Tato skrytá pravidla budou odstraněna, jakmile bude formulář uložen.';
$string['jsonextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření JSON. Mahara vyžaduje tuto knihovnu pro AJAX komunikaci mezi prohlížečem a serverem. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['magicquotesgpc'] = 'Máte povoleno PHP nastavení magic_quotes_gpc, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['magicquotesruntime'] = 'Máte povoleno PHP nastavení magic_quotes_runtime, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['magicquotessybase'] = 'Máte povoleno PHP nastavení magic_quotes_sybase, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['mahararootusermissing'] = 'V databázi chybí uživatel "root", proto není možné pokračovat. Tento uživatel je nezbytný pro správné fungování Mahary. Pro přidání uživatele "root" proveďte prosím jinou instalaci Vámi užívané verze Mahary a podívejte se, co obsahují tabulky "usr" a "usr_custom_layout" u uživatele s id = 0. Vraťte tato data zpět do vaší instalace Mahary než se znovu pokusíte provést aktualizaci.';
$string['mbstringextensionnotloaded'] = 'Nastavení Vašeho serveru neobsahuje rozšíření mbstring. Mahara jej vyžaduje pro rozbor více-bytových řetězců určených pro různé jazyky.';
$string['mbstringneeded'] = 'Nainstalujte rozšíření mbstring pro PHP. Je potřeba v případě, že máte uživatelská jména v kódování UTF-8. Jinak se uživatelům nemusí podařit přihlášení.';
$string['memcacheusememcached'] = 'Úložiště relací "memcache" je zastaralé. Prosím, použijte "memcached".';
$string['missingparamblocktype'] = 'Nejprve vyberte typ bloku, který chcete přidat.';
$string['missingparamcolumn'] = 'Chybí určení sloupce';
$string['missingparamid'] = 'Chybějící id';
$string['missingparamorder'] = 'Chybí určení pořadí';
$string['missingparamrow'] = 'Chybí specifikace řádku';
$string['mysqldbextensionnotloaded'] = 'Vaše PHP nemá podporu MySQL v podobě rozšíření mysqli nebo mysql. Mahara vyžaduje tuto knihovnu pro ukládání dat ve stejnojmenné  databázi. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['mysqlmodulenolongersupported'] = 'Vaše PHP nemá podporu MySQL. Mahara vyžaduje tuto knihovnu pro ukládání dat ve stejnojmenné  databázi v podobě rozšíření mysqli. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením. Počínaje verzí 16.10 nebude Mahara podporovat rozšíření mysql.';
$string['mysqlmodulenolongersupported1'] = 'Vaše PHP nemá podporu MySQL v podobě rozšíření mysqli nebo mysql. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením. Mahara přestala podporovat rozšíření mysql ve verzi 16.10.';
$string['mysqlnotriggerprivilege'] = 'Mahara vyžaduje oprávnění k vytvoření databázových triggerů, ale nemůže tak činit. Ujistěte se, že byla udělena oprávnění pro triggery příslušnému uživateli v instalaci MySQL. Pokyny, jak to provést, naleznete na https://wiki.mahara.org/index.php/System_Administrator\'s_Guide/Granting_Trigger_Privilege';
$string['nomemcachedserver'] = 'Server memcache "%s" není dostupný. Prosím, zkontrolujte hodnotu $cfg->memcacheservers, abyste se ujistili, že je zadána správně.';
$string['nomemcacheserversdefined'] = 'Obslužná relace pro ukládání "%s" nemá definované žádné související servery. Nastavte prosím hodnotu $cfg->memcacheservers např. jako "localhost:11211".';
$string['nopasswordsaltset'] = 'Nebyla nastavena kryptografická sůl pro šifrování hesel. Upravte svůj config.php a nastavte parametru "passwordsaltmain" nějakou rozumnou tajnou frázi.';
$string['nophpextension'] = 'PHP rozšíření "%s" není povoleno. Prosím, povolte toto rozšíření a restartujte Váš webový server nebo zvolte jinou možnost relace.';
$string['noreplyaddressmissingorinvalid'] = 'Nastavení adresy noreply je buď prázdné nebo obsahuje neplatnou e-mailovou adresu. Zkontrolujte prosím konfiguraci e-mailu v <a href="%s">nastavení správy stránek</a>.';
$string['notartefactowner'] = 'Nejste vlastníkem této položky portfolia.';
$string['notenoughsessionentropy'] = 'Hodnota nastavení vaší direktivy session.entropy_length je příliš malá. Měli byste ji v php.ini nastavit nejméně na 16, aby bylo zajištěno, že generované ID relace bude dostatečně náhodné a nepředvídatelné.';
$string['notfound'] = 'Nenalezeno';
$string['notfoundexception'] = 'Stránka, kterou hledáte, nebyla nalezena.';
$string['notinstitutionmember'] = 'Tuto stránku nemůžete zobrazit, protože nejste členem instituce, které tato stránka patří.';
$string['notinthesamegroup'] = 'Profil tohoto uživatele nemůžete zobrazit, protože nejste členem stejné skupiny.';
$string['notinthesameinstitution'] = 'Profil tohoto uživatele nemůžete zobrazit, protože nejste členem stejné instituce.';
$string['notproductionsite'] = 'Stránka není v produkčním režimu. Některá data nemusí být k dispozici, nebo mohou být zastaralá.';
$string['onlyoneblocktypeperview'] = 'Na stránku nemůžete vložit více jak jeden blok typu %s.';
$string['onlyoneprofileviewallowed'] = 'Je dovoleno mít pouze jednu stránku s profilem.';
$string['openbasedirenabled'] = 'Váš server má v nastavení php povolené omezení v direktivě open_basedir.';
$string['openbasedirpaths'] = 'Mahara může otevřít pouze soubory v rámci uvedených cest: %s.';
$string['openbasedirwarning'] = 'Některé požadavky na externí webové stránky mohou selhat před dokončením. Tomu by mohlo mimo jiné zamezit zastavení aktualizace určitých kanálů.';
$string['parameterexception'] = 'Chybí povinný parametr.';
$string['passwordsaltweak'] = 'Nebyla nastavena dostatečně silná kryptografická sůl pro šifrování hesel. Upravte svůj config.php a nastavte parametru "passwordsaltmain" delší tajnou frázi.';
$string['pgsqldbextensionnotloaded'] = 'Vaše PHP nemá podporu PostgreSQL v podobě rozšíření pgsql. Mahara vyžaduje tuto knihovnu pro ukládání dat ve stejnojmenné  databázi. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['phpversion'] = 'Mahara nebude fungovat na PHP verzi menší než %s. Buď aktualizujte vaši instalaci PHP nebo Maharu nainstalujte na jiném serveru.';
$string['pleaseloginforjournals'] = 'Musíte se odhlásit a znovu přihlásit předtím, než uvidíte všechny své deníky a příspěvky.';
$string['plpgsqlnotavailable'] = 'Jazyk PL/pgSQL není povolen ve vaší instalaci Postgres a Mahara ho povolit nemůže. Nainstalujte prosím PL/pqSQL pro vaší databázi ručně. Pro instrukce, jak na to se podívejte na https://wiki.mahara.org/index.php/System_Administrator\'s_Guide/Enabling_Plpgsql';
$string['pluginnotactive1'] = 'Modul "%s" není povolen. Povolit ho můžete v sekci "Administrátorské menu"  → "Rozšíření" → "Správa modulů".';
$string['postmaxlessthanuploadmax'] = 'Vaše nastavení PHP post_max_size (%s) je menší než nastavení upload_max_filesize nastavení (%s). Soubory větší než %s se neuloží bezchybně. Obvykle bývá post_max_size mnohem větší, než upload_max_filesize.';
$string['previewimagegenerationfailed'] = 'Při generování náhledu obrázku došlo k chybě.';
$string['randomkeyminlength'] = 'Délka musí být kladné celé číslo rovné osmi nebo vyšší';
$string['registerglobals'] = 'Máte povoleno PHP nastavení register_globals, což je bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['resavecustomthemes'] = 'Poslední přechod na novější verzi mohl způsobit, že vaše konfigurovatelné motivy se nyní nezobrazují správně. Pro jejich aktualizaci přejděte prosím do Správcovské menu → Instituce -> Nastavení, proveďte potřebnou konfiguraci a formulář uložte. 
<br>Konfigurovatelné motivy využívají následující instituce:';
$string['safemodeon'] = 'Váš server pravděpodobně běží v režimu safe_mode. Maharu není možné provozovat v tomto režimu. Musíte jej vypnout v php.ini nebo v konfiguraci Apache. Pokud provozujete Maharu u externího poskytovatele, budete muset požádat jejich technickou podporu. Případně zvažte instalaci na jiném serveru.';
$string['sessionextensionnotloaded'] = 'Konfigurace vašeho serveru nezahrnuje rozšíření pro relace. Mahara vyžaduje tuto knihovnu pro přihlašování uživatelů. Povolte toto rozšíření v php.ini nebo nainstalujte PHP s jeho podporou.';
$string['sessionpathnotwritable'] = 'Do vašeho adresáře %s se soubory relací nelze zapisovat. Pokud tento adresář neexistuje, vytvořte ho a nastavte mu oprávnění totožné s uživatelským účtem, který používá webový server.';
$string['sideblockmenuclash'] = 'Postranní blok jménem "%s" je již používán. Prosím, vyberte si jiný.';
$string['siteoutofsyncfor'] = 'Tyto stránky mají novější databázové informace než by dle %s souboru měly být.';
$string['siteprivacystatementnotfound'] = 'Prohlášení o ochraně osobních údajů vyžadované těmito webovými stránkami s ID %s nebylo nalezeno.';
$string['smallpostmaxsize'] = 'Vaše nastavení PHP post_max_size o velikosti (%s) je velmi malé. Soubory větší než %s se neuloží bezchybně.';
$string['switchtomysqli'] = '<strong>mysqli</strong> PHP rozšíření není na vašem serveru nainstalováno. Proto Mahara používá zastaralé rozšíření <strong>mysql</strong>. Doporučujeme nainstalovat <a href="http://php.net/manual/en/book.mysqli.php">mysqli</a>.';
$string['themenameinvalid'] = 'Motiv jménem \'%s\' obsahuje neplatné znaky.';
$string['timezoneidentifierunusable'] = 'PHP na vašem hostingu nevrací platnou hodnotu pro identifikátor časové zóny (%%z) - určité akce s tímto spojené, jako např. export LEAP2A neproběhnou korektně. %%z je kód PHP pro formátování datumu. Tento problém je obvykle dán omezením PHP běžícím na systému Windows.';
$string['unabletosetmultipleblogs'] = 'Povolování více deníků pro uživatele %s při kopírování stránky %s selhalo. To lze nastavit ručně na stránce <a href="%s">nastavení</a>.';
$string['unknowndbtype'] = 'V konfiguraci je uveden neplatný typ databáze. Možné hodnoty jsou "postgres8" nebo "mysql5". Opravte nastavení v souboru config.php.';
$string['unrecoverableerror'] = 'Vyskytla se neošetřená chyba. Pravděpodobně jste odhalili chybu v systému.';
$string['unrecoverableerrortitle'] = '%s - stránky nejsou dostupné';
$string['updatesitetimezone'] = 'Nastavení časového pásma těchto stránek je nyní potřeba nastavit v "Administrátorské menu" → "Nastavení stránek" → "Nastavení stránek". Prosíme, nastavte časové pásmo zde a smažte řádek $cfg->dbtimezone z Vašeho souboru config.php.';
$string['urlsecretweak'] = 'Hodnota $cfg->urlsecret pro tyto stránky je ve výchozím nastavení. Změňte soubor config.php tak, aby byla $cfg->urlsecret jiná (nebo prázdná, pokud nechcete používat urlsecret).';
$string['versionphpmissing'] = 'V modulu %s %s chybí soubor version.php';
$string['versionphpmissing1'] = 'Plugin %s %s postrádá version.php. Pokud s pluginem %s nepočítáte, prosím smažte adresář v %s.';
$string['viewnotfound'] = 'Stránka s identifikátorem %s nebyla nalezena.';
$string['viewnotfoundbyname'] = 'Stránka %s %s nebyla nalezena.';
$string['viewnotfoundexceptionmessage'] = 'Pokoušíte se zobrazit neexistující stránku.';
$string['viewnotfoundexceptiontitle'] = 'Stránka nenalezena';
$string['viewtemplatenotfound'] = 'Výchozí šablona stránky nenalezena.';
$string['wrongparamtype'] = 'Funkce nebo metoda obdržela špatný typ argumentu.';
$string['wrongsessionhandle'] = 'Obslužná relace pro ukládání "%s" není v Mahaře podporována.';
$string['wwwrootnothttps'] = 'Vámi nastavený wwwroot, %s, nemá HTTPS přístup. Další nastavení (například sslproxy) pro vaši instalaci vyžadují, aby váš wwwroot byl přístupný prostřednictvím HTTPS. Prosím aktualizujte své nastavení wwwroot, aby bylo skrze HTTPS přístupné, nebo opravte nesprávné nastavení.';
$string['xmlextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření %s. Mahara vyžaduje tuto knihovnu pro parsování XML dat z různých zdrojů. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['youcannotviewthisusersprofile'] = 'Nemůžete si prohlédnout profil tohoto uživatele.';
