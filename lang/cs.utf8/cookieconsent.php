<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['additionalmodifications'] = 'Pokud chcete, aby byla Správa Cookies plně funkční, musíte změnit nebo aktualizovat soubory hlavičky tématu nebo nastavení <tt>$cfg->additionalhtmlhead</tt>.';
$string['advertisingDefaultDescription'] = 'Reklamy budou voleny automaticky na základě vašeho minulého chování a zájmů.';
$string['advertisingDefaultTitle'] = 'Reklamy';
$string['allSitesSettingsDialogSubtitle'] = 'Tyto cookies můžete povolit na všech webech používajících tento zásuvný modul';
$string['allSitesSettingsDialogTitleA'] = 'Nastavení soukromí';
$string['allSitesSettingsDialogTitleB'] = 'pro všechny weby';
$string['allowCookies'] = 'Povolit cookies';
$string['allowCookiesImplicit'] = 'Zavřít';
$string['allowForAllSites'] = 'Povolit pro všechny weby';
$string['analyticsDefaultDescription'] = 'Pro zlepšení kvality služeb anonymně zaznamenáváme váš pohyb na tomto webu';
$string['analyticsDefaultTitle'] = 'Analytika';
$string['backToSiteSettings'] = 'Zpět do nastavení stránek';
$string['bannerposition'] = 'Pozice lišty';
$string['bannerpositionbottom'] = 'Dole';
$string['bannerpositiondesc'] = 'Zvolte, zda se bude lišta Souhlasu s použitím cookies zobrazovat na vršku, nebo na spodku stránky';
$string['bannerpositionpush'] = 'Vysunout shora (experimentální)';
$string['bannerpositiontop'] = 'Nahoře';
$string['changeForAllSitesLink'] = 'Změnit nastavení pro všechny weby';
$string['closeWindow'] = 'Zavřít okno';
$string['consentmode'] = 'Varianta souhlasu';
$string['consentmodedesc1'] = 'Souhlas s použitím cookies vždy použije explicitní mód, pokud prohlížeč odesílá požadavek "do not track" (pokud toto není přetíženo v "Nastavení funkcí" níže).';
$string['consentmodedesc2'] = 'Odesílání požadavku "do not track" je ve výchozím nastavení aktivováno v posledních verzích prohlížeče Internet Explorer.';
$string['consentmodeexplicit'] = 'Explicitní - do souhlasu uživatele nebudou odeslány žádné cookies';
$string['consentmodeimplicit'] = 'Implicitní - umožňuje uživatelům vyvázat se z používání cookies';
$string['cookieconsent'] = 'Souhlas s použitím cookies';
$string['cookieconsent2'] = 'Doplňkové modifikace stránek';
$string['cookieconsentdisabled'] = 'Souhlas s použitím cookies vypnut';
$string['cookieconsentdismiss'] = 'Rozumím.';
$string['cookieconsentenable'] = 'Aktivovat Souhlas s použitím cookies';
$string['cookieconsentenabled'] = 'Souhlas s použitím cookies byl aktivován a nastavení bylo uloženo';
$string['cookieconsentintro1'] = 'Směrnice 2009/136/EC  Evropského parlamentu a Evropské rady vyžaduje souhlas s použitím cookies v rámci Evropské unie. V principu jde o dodatek předchozí směrnice Directive 2002/58/EC a zabývá se ochranou data a soukromí na webu i ve zbytku elektronické komunikace.';
$string['cookieconsentintro2'] = 'Nová směrnice vešla v platnost 25. května 2011. Text směrnice je dlouhý přibližně 26 stran, ale nejdůležitější odstavec o cookies se nachází na straně 20, která mění článek 5 (3) předchozí směrnice 2002/58/EC:';
$string['cookieconsentintro3'] = '"Členské státy zajistí, aby uchovávání informací nebo získávání přístupu k již uchovávaným informacím bylo v koncovém zařízení účastníka nebo uživatele povoleno pouze pod podmínkou, že dotčený účastník či uživatel poskytl svůj souhlas poté, co mu byly poskytnuty jasné a úplné informace v souladu se směrnicí 95/46/ES, mimo jiné o účelu zpracování. To nebrání technickému ukládání nebo takovému přístupu, jehož jediným účelem je provedení přenosu sdělení prostřednictvím sítě elektronických komunikací, nebo je-li to nezbytně nutné k tomu, aby mohl poskytovatel služeb informační společnosti poskytovat služby, které si účastník nebo uživatel výslovně vyžádal."';
$string['cookieconsentintro4'] = 'Ve zkratce to znamená, že předtím, než je možné ukládat nebo získávat informace z počítače, mobilního telefonu nebo jiného zařízení, uživatel musí poskytnout informovaný souhlas. Záměrem je zvýšit soukromí koncového uživatele a zabránit organizacím v získávání informací o lidech bez jejich vědomí.';
$string['cookieconsentintro51'] = 'Nejprve níže povolte plugin %sCookie Consent od Silktide%s , poté nastavte požadovanou konfiguraci a uložte změny. Budete přesměrováni na jinou stránku s podrobnými instrukcemi týkajícími se úpravy nebo aktualizace hlavičkových souborů tématu nebo konfigurace <tt>$cfg->additionalhtmlhead</tt>, aby byl zásuvný modul Cookie Control plně funkční.';
$string['cookieconsentintro52'] = 'Níže povolte %Cookie Consent%s. Uživatelé uvidí na spodní straně obrazovky panel Souhlas s použitím cookies s odkazem na vaši stránku s informacemi o ochraně osobních údajů. Ujistěte se, že tato stránka obsahuje informace o využití cookies.';
$string['cookieconsentlearnmore'] = 'Zjistit více';
$string['cookieconsentmessage'] = 'Tato stránka využívá cookies pro zajištění lepší použitelnosti.';
$string['cookietypes'] = 'Druhy cookies';
$string['cookietypesadvertising'] = 'Reklama';
$string['cookietypesanalytics'] = 'Analytika';
$string['cookietypesdesc'] = 'Zvolte druh cookies používaných na vašich stránkách';
$string['cookietypesnecessary'] = 'Jen nezbytně nutné';
$string['cookietypessocial'] = 'Sociální sítě';
$string['customCookie'] = 'Tyto stránky používají speciální druh cookies, který potřebuje zvláštní povolení';
$string['defaultDescription'] = 'Výchozí popis cookie.';
$string['defaultTitle'] = 'Výchozí název cookie.';
$string['directive2009136'] = 'Směrnice 2009/136/EC';
$string['example'] = 'Příklad';
$string['example1advertising'] = 'Google AdSense a jiné zásuvné moduly pro cílenou reklamu';
$string['example1analytics'] = 'Google Analytics a StatCounter';
$string['example1necessary'] = 'Google AdSense a jiné zásuvné moduly pro cílenou reklamu';
$string['example1social'] = 'Doplňky pro Twitter a Facebook';
$string['exampleafter'] = 'Po (změny jsou tučným písmem)';
$string['examplebefore'] = 'Před';
$string['featureoptions'] = 'Nastavení funkcí';
$string['generaloptions'] = 'Obecná nastavení';
$string['hideDetails'] = 'skrýt podrobnosti';
$string['hideprivacytab'] = 'Skrýt panel nastavení soukromí';
$string['hideprivacytabdesc'] = 'Chcete-li použít vlastní in-line odkaz na nastavení soukromí (např. v rámci šablony tématu), mohli byste chtít skrýt standardní panel nastavení soukromí.';
$string['hideprivacytabdesc1'] = 'Pokud chcete na stránku pro nastavení soukromí odkazovat in-line (např. v rámci motivu stránek), můžete skrýt standardní panel nastavení soukromí.';
$string['ignoredonottrack'] = 'Ignorovat požadavek "do not track"';
$string['ignoredonottrackdesc'] = 'Aktivace tohoto nastavení znamená, že Souhlas s použitím cookies ignoruje hlavičky "do not track" z prohlížečů uživatelů.';
$string['ignoredonottrackdesc1'] = 'Po aktivaci této volby nebude Cookie Consent brát ohled na "do not track" hlavičky z prohlížečů.';
$string['instructiontext1'] = 'Vyhledat jakékoli javascriptové prvky nastavující %s cookies.
Příklady mohou obsahovat %s.';
$string['instructiontext2'] = 'Upravit tag <tt><script></tt> tak, aby byl jeho atribut type "text/plain" místo "text/javascript"';
$string['instructiontext2-1'] = 'Upravte tag <tt><script></tt> tak, aby atribut typu byl "text/plain" místo "application/javascript"';
$string['instructiontext3'] = 'Přidat třídu %s do tagu <tt><script></tt>';
$string['itdidntwork'] = 'To nevyšlo.';
$string['itdidntwork1'] = 'Nejprve, zkuste nahradit <b>%s</b> za <b>%s</b>. Tím se opraví některé Javascriptové zásuvné moduly využívající <tt>document.write()</tt>.';
$string['itdidntwork2'] = 'Pokud to nepomůže, %sprohlédněte si stránky s příklady kódu pro Cookie Consent%s, nebo požádejte o pomoc v %sCookie Consent LinkedIn skupině%s.';
$string['learnMore'] = 'Zjistit více';
$string['necessaryDefaultDescription'] = 'Některé cookies na těchto stránkách jsou nezbytné a nemohou být deaktivovány.';
$string['necessaryDefaultTitle'] = 'Nezbytné';
$string['notUsingCookies'] = 'Tyto stránky nepoužívají žádné cookies.';
$string['notificationTitle'] = 'Povolením cookies zlepšíte použitelnost těchto stránek.';
$string['notificationTitleImplicit'] = 'Používáme cookies, abychom zajistili co nejpříjemnější používání stránek.';
$string['pagerefresh'] = 'Obnovení stránky';
$string['pagerefreshdesc'] = 'Pokud máte serverovou aplikaci, která potřebuje informace o souhlasu s použitím cookies, zvolením této možnosti zajistíte obnovení stránky po udělení souhlasu.';
$string['pagerefreshdesc1'] = 'Pokud na straně serveru provozujete nějakou aplikaci, která musí brát ohled na souhlas s používáním cookies, aktivace této volby vynutí opětovné načtení stránky po povolení cookies uživatelem.';
$string['pluginstyle'] = 'Styl';
$string['pluginstyledark'] = 'Tmavý';
$string['pluginstyledesc'] = 'Změnit vzhled Souhlasu s cookies';
$string['pluginstylelight'] = 'Světlý';
$string['preferenceAlways'] = 'Vždy povolit';
$string['preferenceAsk'] = 'Vždy se znovu dotázat';
$string['preferenceConsent'] = 'Souhlasím';
$string['preferenceDecline'] = 'Nesouhlasím';
$string['preferenceNever'] = 'Nikdy nepovolit';
$string['preferenceUseGlobal'] = 'Použít globální nastavení';
$string['privacySettings'] = 'Nastavení soukromí';
$string['privacySettingsDialogSubtitle'] = 'Některé funkce těchto stránek vyžadují váš souhlas pro zapamatování vaší identity.';
$string['privacySettingsDialogTitleA'] = 'Nastavení soukromí';
$string['privacySettingsDialogTitleB'] = 'pro tyto stránky';
$string['readdirectiveBG'] = 'Přečíst směrnici 2009/136/EC v bulharštině';
$string['readdirectiveCS'] = 'Přečíst směrnici 2009/136/EC v češtině';
$string['readdirectiveDA'] = 'Přečíst směrnici 2009/136/EC v dánštině';
$string['readdirectiveDE'] = 'Přečíst směrnici 2009/136/EC v němčině';
$string['readdirectiveEL'] = 'Přečíst směrnici 2009/136/EC v řečtině';
$string['readdirectiveEN'] = 'Přečíst směrnici 2009/136/EC v angličtině';
$string['readdirectiveES'] = 'Přečíst směrnici 2009/136/EC ve španělštině';
$string['readdirectiveET'] = 'Přečíst směrnici 2009/136/EC v estonštině';
$string['readdirectiveFI'] = 'Přečíst směrnici 2009/136/EC ve finštině';
$string['readdirectiveFR'] = 'Přečíst směrnici 2009/136/EC ve francouštině';
$string['readdirectiveHU'] = 'Přečíst směrnici 2009/136/EC v maďarštině';
$string['readdirectiveIT'] = 'Přečíst směrnici 2009/136/EC v italštině';
$string['readdirectiveLT'] = 'Přečíst směrnici 2009/136/EC v litevštině';
$string['readdirectiveLV'] = 'Přečíst směrnici 2009/136/EC v lotyšštině';
$string['readdirectiveMT'] = 'Přečíst směrnici 2009/136/EC v maltštině';
$string['readdirectiveNL'] = 'Přečíst směrnici 2009/136/EC v holandštině';
$string['readdirectivePL'] = 'Přečíst směrnici 2009/136/EC v polštině';
$string['readdirectivePT'] = 'Přečíst směrnici 2009/136/EC v portugalštině';
$string['readdirectiveRO'] = 'Přečíst směrnici 2009/136/EC v rumunštině';
$string['readdirectiveSK'] = 'Přečíst směrnici 2009/136/EC ve slovenštině';
$string['readdirectiveSL'] = 'Přečíst směrnici 2009/136/EC ve slovinštině';
$string['readdirectiveSV'] = 'Přečíst směrnici 2009/136/EC ve švédštině';
$string['readfulltext1'] = 'Přečíst celý text směrnice';
$string['saveForAllSites'] = 'Uložit pro všechny stránky';
$string['savePreference'] = 'Uložit preferenci';
$string['seeDetails'] = 'zobrazit podrobnosti';
$string['seeDetailsImplicit'] = 'změnit nastavení';
$string['socialDefaultDescription'] = 'Aby Facebook, Twitter a jiné sociální sítě dokázaly správně fungovat, potřebují znát vaši identitu.';
$string['socialDefaultTitle'] = 'Sociální sítě';
$string['stylingoptions'] = 'Nastavení vzhledu';
$string['tabposition'] = 'Pozice panelů';
$string['tabpositionbottomleft'] = 'Vlevo dole';
$string['tabpositionbottomright'] = 'Vpravo dole';
$string['tabpositiondesc'] = 'Zvolte, kde se má panel s nastavením soukromí zobrazit.';
$string['tabpositionverticalleft'] = 'Levý bok';
$string['tabpositionverticalright'] = 'Pravý bok';
$string['usessl'] = 'Použít SSL';
$string['usessldesc'] = 'Pokud některá ze stránek obsahujících Souhlas s použitím cookies používá SSL, musíte zaškrtnout tuto položku.';
$string['usessldesc1'] = 'Pokud některé stránky, na kterých se objevuje výzva k souhlasu s použitím cookies, používají SSL, je třeba zapnout tuto volbu.';
