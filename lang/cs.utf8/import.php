<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2015
 *
 */

defined('INTERNAL') || die();

$string['Import'] = 'Importovat';
$string['addnew'] = 'Přidat nový';
$string['append'] = 'Připojit';
$string['duplicateditem'] = 'Duplicitní položka';
$string['entries'] = 'Záznamy';
$string['entry'] = 'Záznam';
$string['existingitem'] = 'Stávající položka';
$string['howimportportfoliodescription'] = 'V tomto kroku se můžete rozhodnout, jak sloučit importovaný obsah s vaším stávajícím obsahem.';
$string['howimportyourportfolio'] = 'Vyberte si způsob, jak chcete importovat položky svého portfolia';
$string['howtoimport'] = 'Jak importovat';
$string['ignore'] = 'Ignorovat';
$string['importartefactplugindata'] = 'Importuji data z modulu položky portfolia';
$string['importartefacts'] = 'Importuji položky portfolia';
$string['importartefactsprogress'] = 'Importuji položky portfolia: %s/%s';
$string['importentry'] = 'Importovat záznam';
$string['importexceedquota'] = 'Celková velikost nových souborů v nahraném Leap2A souboru by překročil váš přidělený diskový prostor. Odstraňte některé soubory a zkuste to znovu.';
$string['importfailed'] = '<p><strong> Je nám líto - import vašeho Leap2A souboru selhal</strong></p><p> Mohlo se to stát proto, že jste nevybrali platný Leap2A soubor k nahrání nebo proto, že použitá verze vašeho Leap2A souboru není podporována touto verzí Mahary. Mohlo se také stát, že neúspěšný import zapříčinila chyba v Mahaře a váš soubor ve formátu Leap2A je platný.</p><p>Prosím, vraťte se na předcházející stránku a zkuste to znovu</a>Pokud problém přetrvává, můžete se s žádostí o pomoc obrátit na <a href="http://mahara.org/forums/">Diskusní fórum Mahara</a>. Buďte připraveni, že můžete být požádáni o kopii daného souboru.</p>';
$string['importfolder'] = 'Importovat složku %s';
$string['importfooter'] = 'Importuji zápatí';
$string['importportfoliodescription'] = 'Pomocí souboru Leap2A můžete importovat obsah vašeho portfolia. Tento soubor typicky získáte exportem portfolia na stránkách fungujících na systému Mahara. Nelze importovat nastavení stránek nebo obsah, který nahráli nebo vytvořili ve skupinách.';
$string['importresult'] = 'Výsledky importu';
$string['importsuccessfully'] = 'Vaše portfolio bylo úspěšně importováno';
$string['importviews'] = 'Importuji stránky';
$string['importviewsprogress'] = 'Importuji stránky: %s/%s';
$string['importwitherrors'] = 'Vaše portfolio bylo importováno s chybami.';
$string['importyourportfolio'] = 'Importovat vaše portfolio';
$string['noimportentry'] = 'Neexistuje žádný záznam import.';
$string['noimportpluginsenabled'] = 'Správce webu nepovolil zásuvný modul pro import. Proto ho nemůžete použít.';
$string['noleapimportpluginsenabled'] = 'Správce webu nepovolil zásuvný modul pro Leap2A. Proto ho nemůžete použít.';
$string['replace'] = 'Nahradit';
