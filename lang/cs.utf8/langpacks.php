<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['addlangpack'] = 'Přidat jazyk';
$string['addlangpackdescription'] = 'Vyberte jazyk, kterých chcete přidat na webové stránky.';
$string['brokenlangpack'] = 'Konfigurační soubor je poškozen v %s. Buď jej manuálně opravte nebo smažte adresář %s, abyste odstranili tento jazyk úplně.';
$string['bulkselect'] = 'Vyberte jazyk k aktualizaci';
$string['code'] = 'Jazykový kód';
$string['confirmsync'] = 'Jste si jisti, že chcete aktualizovat nainstalované jazyky?';
$string['fileorigin'] = 'Původ souboru';
$string['filetoinstall'] = 'Soubor';
$string['langalreadyinstalled'] = '%s je již nainstalován';
$string['langpackadded'] = '%s přidán';
$string['langpacks'] = 'Jazyky';
$string['langpackuptodate'] = '%s je aktuální';
$string['languagepackdescription'] = 'Přidejte a aktualizujte jazyky, které mohou držitelé účtů používat na těchto webových stránkách.';
$string['languagepacks_title'] = 'Jazyky';
$string['languagesyncsuccessfully'] = 'Jazyky úspěšně aktualizovány';
$string['languagesyncunsuccessful'] = 'Aktualizace %s selhala';
$string['nolanguagepacksfound'] = 'Nejsou nainstalovány žádné jazyky. Pro přidání nového jazyka prosím vyberte nějaký níže.';
$string['nolanguageselected'] = 'Nebyly vybrány žádné jazyky';
$string['notvalidlangpack'] = '%s není platná volba jazyka';
$string['selectlang'] = 'Zvolte jazyk "%s"';
$string['unreadablelangpack'] = 'Není možné přečíst jazykový adresář %s.';
$string['updatelangpacks'] = 'Aktualizovat';
