<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['accessiblepagecreation'] = 'Vytváření přístupné stránky';
$string['accessiblepagecreationdescription'] = 'Tato volba umožňuje vytvářet a upravovat stránky tak, aby k tomu stačila čtečka displeje nebo klávesnice.';
$string['accountdeleted'] = 'Váš účet byl odstraněn.';
$string['accountoptionsdesc'] = 'Zde můžete nastavit obecné parametry účtu';
$string['badmobileuploadtoken'] = 'Tento token je neplatný. Všechny tokeny obsahovat alespoň 6 znaků.';
$string['canceldeletionadminemailhtml'] = '<p>Vážený správče,</p> <p>uživatel %s zrušil svou žádost o smazání jeho účtu z této sítě.</p> <p>Vy jste zapsán jako správce instituce, do které tento uživatel patří.</p> <p>Podrobnosti o zrušení této žádosti:</p> <p>Jméno: %s</p> <p>Email: %s</p> <pre>-- S pozdravem,  %s tým</pre>';
$string['canceldeletionadminemailsubject'] = 'Zrušení žádosti o smazání účtu %s';
$string['canceldeletionadminemailtext'] = 'Vážený správce, uživatel %s zrušil svou žádost o smazání jeho účtu z této sítě. Vy jste zapsán jako správce instituce, do které tento uživatel patří.Podrobnosti o zrušení této žádosti: Jméno: %s Email: %s -- S pozdravem,  %s tým';
$string['canceldeletionrequest'] = 'Zrušit žádost o smazání';
$string['canceldeletionrequestconfirmation'] = 'Tímto zrušíte žádost pro správce instituce ohledně smazání účtu %s. Jste si jistí, že chcete pokračovat?';
$string['cancelrequest'] = 'Zrušit žádost';
$string['changepassworddesc'] = 'Pokud si přejete změnit heslo, vyplňte následující údaje';
$string['changepasswordotherinterface'] = 'Heslo si můžete změnit na <a href="%s">jiné stránce</a>';
$string['changeprofileurl'] = 'Změnit URL profilu';
$string['changeusername'] = 'Nové uživatelské jméno';
$string['changeusernamedesc'] = 'Uživatelské jméno, které používáte na %s. Uživatelská jména mají 3 až 30 znaků a mohou obsahovat písmena, číslice a většinu běžných znaků kromě mezery.';
$string['changeusernameheading'] = 'Změna uživatelského jména';
$string['deleteaccount'] = 'Zrušit účet';
$string['deleteaccountdescription'] = 'Pokud odstraníte svůj účet, vaše profilové informace a vaše portfolia nebudou dále viditelné ostatním uživatelům. Obsah všech příspěvků ve fórech, které jste napsali, bude nadále viditelný, nebude už u nich však uvedeno vaše jméno.';
$string['deleteaccountuser'] = 'Smazat účet %s';
$string['deleterequestcanceled'] = 'Žádost o smazání účtu vašeho uživatele byla zrušena';
$string['devicedetection'] = 'Detekce mobilních zařízení';
$string['devicedetectiondescription'] = 'Povolit detekci mobilních zařízení při procházení této stránky.';
$string['disabled'] = 'Zakázat';
$string['disableemail'] = 'Zakázat zasílání e-mailů';
$string['disablemultipleblogserror'] = 'Nemůžete zakázat více deníků, protože máte jen jeden';
$string['enabled'] = 'Povolit';
$string['enablemultipleblogs'] = 'Povolit více deníků';
$string['enablemultipleblogs1'] = 'Více deníků';
$string['enablemultipleblogsdescription'] = 'Standardně můžete mít jeden deník. Pokud jich chcete mít více, zaškrtněnte tuto volbu.';
$string['enablemultipleblogsdescription1'] = 'Ve výchozím stavu máte nastavený jeden deník. Pokud chcete vést více deníků, aktivujte tuto možnost.';
$string['friendsauth'] = 'Noví přátelé vyžadují mou autorizaci';
$string['friendsauto'] = 'Noví přátelé jsou automaticky autorizováni';
$string['friendsdescr'] = 'Nastavení přátel';
$string['friendsnobody'] = 'Nikdo mě nemůže přidat mezi své přátele';
$string['hiderealname'] = 'Skrýt skutečné jméno';
$string['hiderealnamedescription'] = 'Zaškrtněte toto políčko, pokud jste nastavili jméno pro zobrazení a nechcete, aby vás ostatní uživatelé mohli najít podle vašeho skutečného jména při vyhledávání uživatelů.';
$string['language'] = 'Jazyk';
$string['licensedefault'] = 'Výchozí licence';
$string['licensedefaultdescription'] = 'Výchozí licence pro váš obsah.';
$string['licensedefaultinherit'] = 'Použít výchozí licenci instituce';
$string['maildisabled'] = 'E-mail zakázán';
$string['maildisabledbounce'] = 'Odeslání e-mailů na vaši adresu bylo zakázáno, protože se nám z ní vrátilo příliš mnoho zpráv jako nedoručitelných. Zkontrolujte si prosím, jestli váš účet funguje, předtím, než ho znovu povolíte v nastavení účtu na %s.';
$string['maildisableddescription'] = 'Odesílání e-mailů na váš účet bylo zakázáno. Můžete ho <a href="%s">znovu povolit</a> na stránce s nastavením účtu.';
$string['messagesallow'] = 'Kdokoliv mi může poslat zprávu';
$string['messagesdescr'] = 'Zprávy od ostatních uživatelů';
$string['messagesfriends'] = 'Mí přátelé mi mohou poslat zprávu';
$string['messagesnobody'] = 'Nikdo mi nemůže poslat zprávu';
$string['mobileuploadtoken'] = 'Token pro nahrávání mobilem';
$string['mobileuploadtokendescription'] = 'Vložte token sem a do vašeho telefonu, aby bylo možné nahrávání povolit (poznámka: bude jiný pro každé nahrávání). <br/>Pokud se setkáte s nějakými problémy, jednoduše token resetujte zde i na telefonu.';
$string['noprivacystatementsaccepted'] = 'Tento účet zatím nesouhlasil s žádným z aktuálních prohlášení o soukromí.';
$string['off'] = 'Vypnuto';
$string['oldpasswordincorrect'] = 'Toto není stávající heslo';
$string['on'] = 'Zapnuto';
$string['pendingdeletionadminemailhtml'] = '<p>Vážený správce,</p> <p>uživatel %s zažádal o smazání svého účtu z tohoto webu.</p> <p>Vy jste uveden jako správce instituce, do které tento uživatel patří. Proto je na vás schválení nebo zamítnutí této žádosti. Můžete tak učinit na tomto odkaze: <a href=\'%s\'>%s</a></p> <p>Podrobnosti ohledně této žádosti:</p> <p>Jméno: %s</p> <p>Email: %s</p> <p>Důvod: %s</p> <pre>-- S pozdravem, %s tým</pre>';
$string['pendingdeletionadminemailsubject'] = 'Žádost o smazání podána %s';
$string['pendingdeletionadminemailtext'] = '<p>Vážený správce,</p> <p>uživatel %s zažádal o smazání svého účtu z tohoto webu.</p> <p>Vy jste uveden jako správce instituce, do které tento uživatel patří. Proto je na vás schválení nebo zamítnutí této žádosti. Můžete tak učinit na tomto odkaze: <a href=\'%s\'>%s</a></p> <p>Podrobnosti ohledně této žádosti:</p> <p>Jméno: %s</p> <p>Email: %s</p> <p>Důvod: %s</p> <pre>-- S pozdravem, %s tým</pre>';
$string['pendingdeletionemailsent'] = 'Poslat upozornění správcům instituce';
$string['pendingdeletionsince'] = 'Čekající smazání účtu od %s';
$string['prefsnotsaved'] = 'Ukládání vašich předvoleb selhalo!';
$string['prefssaved'] = 'Předvolby uloženy';
$string['profileurl'] = 'URL adresa profilu';
$string['profileurldescription'] = 'Vlastní URL adresa pro váš profil. Musí mít 3-30 znaků.';
$string['resenddeletionadminemailhtml'] = '<p>Vážený správce,</p> <p>dovolujeme si vám připomenout, že uživatel % zažádal o smazání svého účtu z tohoto webu.</p> <p>Vy jste uveden jako správce instituce, do které tento uživatel patří. Proto je na vás schválení nebo zamítnutí této žádosti. Můžete tak učinit na tomto odkaze: <a href=\'%s\'>%s</a></p><p>Podrobnosti ohledně této žádosti:</p> <p>Jméno: %s</p> <p>Email: %s</p> <p>Zpráva: %s</p> <pre>-- S pozdravem, %s tým</pre>';
$string['resenddeletionadminemailsubject'] = 'Připomenutí žádosti o smazání účtu z %s';
$string['resenddeletionadminemailtext'] = '<p>Vážený správce,</p> <p>dovolujeme si vám připomenout, že uživatel % zažádal o smazání svého účtu z tohoto webu.</p> <p>Vy jste uveden jako správce instituce, do které tento uživatel patří. Proto je na vás schválení nebo zamítnutí této žádosti. Můžete tak učinit na tomto odkaze: <a href=\'%s\'>%s</a></p><p>Podrobnosti ohledně této žádosti:</p> <p>Jméno: %s</p> <p>Email: %s</p> <p>Zpráva: %s</p> <pre>-- S pozdravem, %s tým</pre>';
$string['resenddeletionnotification'] = 'Znovu odeslat upozornění ohledně smazání';
$string['resizeonuploaduserdefault1'] = 'Zmenšení obrázků při nahrávání.';
$string['resizeonuploaduserdefaultdescription1'] = 'Pokud je zaškrtnuto, bude zmenšování obrázků nastaveno jako výchozí. Obrázky překračující povolené rozměry budou po nahrání automaticky zmenšeny. Toto výchozí nastavení můžete zakázat pro každý nahrávaný obrázek individuálně.';
$string['resizeonuploaduserdefaultdescription2'] = 'Automatické zmenšování obrázků je ve výchozím stavu povoleno. Obrázky větší než jsou nastavené maximální rozměry budou při nahrání zmenšeny. Tuto možnost lze vypnout při nahrávání každého obrázku.';
$string['sendnotificationdescription'] = 'Bude zasláno upozornění správci, jehož souhlas je potřeba ke zrušení vašeho účtu. Pokud zažádáte o zrušení svého účtu, váš obsah včetně všech nahraných souborů, zápisů do deníků a stránek a sbírek, které jste vytvořili, bude trvale vymazán. Tento krok je nevratný. Co se týče souborů, které jste nahráli do skupin, a vašich příspěvků ve fóru, zůstanou nadále na webu, ale nebude u nich již zobrazeno vaše jméno.';
$string['showhomeinfo'] = 'Zobrazit informace o Mahaře na titulní stránce';
$string['showhomeinfo1'] = 'Informace na domovské stránce';
$string['showhomeinfo2'] = 'Informace na domovské stránce';
$string['showhomeinfodescription'] = 'Na domovské stránce zobrazí informace o tom, jak používat %s.';
$string['showhomeinfodescription1'] = 'Zobrazit informace o používání %s na domovské stránce';
$string['showlayouttranslatewarning'] = 'Potvrdit před změnou rozvržení stránky portfolia';
$string['showlayouttranslatewarningdescription'] = 'Zobrazovat varování a vyžadovat souhlas před změnou rozvržení stránky portfolia na nový typ rozvržení.';
$string['showprogressbar'] = 'Grafické zobrazení úplnosti profilu';
$string['showprogressbardescription'] = 'Zobrazit do jaké míry je váš %s profil úplný a tipy k jeho doplnění.';
$string['showviewcolumns'] = 'Při úpravách stránky zobrazovat prvky pro přidání a odebrání sloupců';
$string['tagssideblockmaxtags'] = 'Maximum štítků v přehledu';
$string['tagssideblockmaxtagsdescription'] = 'Maximální počet položek zobrazných ve vašem přehledu štítků';
$string['updatedfriendcontrolsetting'] = 'Nastavení přátel upraveno';
$string['urlalreadytaken'] = 'Tato URL adresa profilu je již používána. Zvolte prosím jinou.';
$string['usernameexists'] = 'Uživatelské jméno již existuje, vyberte prosím jiné.';
$string['usernameexists1'] = 'Toto uživatelské jméno nelze použít, zvolte si prosím jiné.';
$string['wysiwygdescr'] = 'Editor HTML';
