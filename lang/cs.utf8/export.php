<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Done'] = 'Hotovo';
$string['Export'] = 'Exportovat';
$string['Starting'] = 'Spouštění';
$string['addedleap2atoexportqueueall'] = 'Vaše data byla přidána do fronty na export.';
$string['addedleap2atoexportqueuecollections'] = 'Některé z vašich sbírek byly přidány do fronty na export.';
$string['addedleap2atoexportqueueviews'] = 'Některé z vašich stránek byly přidány do fronty na export.';
$string['allmydata'] = 'Všechna má data';
$string['archivedsubmissionfailed'] = 'Informace o archivovaném příspěvku nelze zapsat do databáze';
$string['chooseanexportformat'] = 'Vyberte formát pro export';
$string['clickheretodownload'] = 'Klikněte sem pro stažení';
$string['clicktopreview'] = 'Klepněte pro náhled';
$string['collectionstoexport'] = 'Sbírky k exportu';
$string['continue'] = 'Pokračovat';
$string['couldnotcopyattachment'] = 'Nelze zkopírovat přílohu "%s"';
$string['couldnotcopyfilesfromto'] = 'Nelze zkopírovat soubory ze složky "%s" do "%s"';
$string['couldnotcopystaticfile'] = 'Nelze zkopírovat soubor "%s"';
$string['couldnotcreatedirectory'] = 'Nelze vytvořit složku "%s"';
$string['couldnotcreatestaticdirectory'] = 'Nelze vytvořit složku "%s"';
$string['couldnotwriteLEAPdata'] = 'Nelze zapsat Leap2A data do souboru';
$string['creatingzipfile'] = 'Vytvářím soubor typu ZIP';
$string['deleteexportqueueitems'] = 'Selhalo odebrání položek z databázové tabulky položek čekajících na export.';
$string['deleteexportqueuerow'] = 'Selhalo odebrání položek z tabulky položek čekajících na export';
$string['exportarchivedescription1'] = 'Obdržíte ZIP soubor, ve kterém najdete obsah, který jste vybrali k exportu, ve formátu HTML i Leap2A. Své portfolio můžete zobrazit v prohlížeči přes soubor index.html nebo ho importovat na jinou platformu pro portfolia, která podporuje formát Leap2A.';
$string['exportarchivedescriptionpdf'] = 'Obdržíte soubor ZIP s obsahem, který jste pro export vybrali, a to ve formátech HTML, Leap2A a PDF. Vaše portfolio pak budete moci zobrazit pomocí prohlížeče skrze soubor index.html, z exportovaných souborů PDF nebo jej bude možné importovat do jiné platformy pro portfolio, která podporuje format Leap2A.';
$string['exportarchivesavefailed'] = 'Informace o archivu exportu nelze zapsat do databáze.';
$string['exportgeneratedsuccessfully'] = 'Export byl vygenerován úspěšně. %sKlepněte zde pro jeho stažení%s';
$string['exportgeneratedsuccessfully1'] = 'Export byl úspěšně vygenerován';
$string['exportgeneratedsuccessfullyjs'] = 'Export byl vygenerován úspěšně. %sPokračovat%s';
$string['exportgeneratedwitherrors'] = 'Export byl vygenerován s nějakými chybami.';
$string['exportingartefactplugindata'] = 'Exportovat data položek portfolia';
$string['exportingartefacts'] = 'Exportovat položky portfolia';
$string['exportingartefactsprogress'] = 'Exportuji položky portfolia: %s%s';
$string['exportingcollections'] = 'Exportuji sbírky';
$string['exportingfooter'] = 'Exportuji zápatí';
$string['exportingviews'] = 'Exportuji stránku';
$string['exportingviewsprogress'] = 'Exportuji stránky: %s%s';
$string['exportingviewsprogresshtml'] = 'Exportování stránek do HTML: %s/%s';
$string['exportingviewsprogressleap'] = 'Exportování stránek do Leap2A: %s/%s';
$string['exportingviewsprogresspdf'] = 'Vytváření PDF souborů: %s/%s';
$string['exportlitenotwritable'] = 'Do exportního adresáře nelze zapisovat.';
$string['exportpagedescription'] = 'Tento nástroj exportuje informace ze všech vašich portfolií a stránek, ale neexportuje nastavení webu.';
$string['exportportfoliodescription'] = 'Tento nástroj vyexportuje všechny informace týkající se vašeho portfolia a stránkek. Obsah nahraný nebo vytvořený ve skupinách a vaše uživatelská nastavení exportována nebudou.';
$string['exportportfoliodescription1'] = '<p class="lead">Tento nástroj exportuje všechny informace o vašem portfoliu a stránky portfolia. Neexportuje ale žádné vaše nastavení v rámci těchto webových stránek ani obsah, který jste nahráli nebo vytvořili ve svých skupinách.</p><p class="lead">Můžete exportovat obsah svých osobních portfolií. Nastavení vašeho účtu a obsah nahraný nebo vytvořený ve skupinách exportovány nebudou.</p>';
$string['exportqueueerrorsadminmessage'] = 'Nelze exportovat řádek "%s": %s';
$string['exportqueueerrorsadminsubject'] = 'Chyba spouštění fronty pro export';
$string['exportqueuenotempty'] = 'Položky ve frontě na export pro tohoto uživatele. Prosím, počkejte, až skončí archivace.';
$string['exportyourportfolio'] = 'Exportovat vaše portfolio';
$string['exportzipfileerror'] = 'Vytváření ZIP souboru selhalo: %s';
$string['generateexport'] = 'Exportovat';
$string['includecomments'] = 'Zahrnout komentáře';
$string['includecommentsdescription'] = 'V HTML exportu budou obsaženy všechny komentáře.';
$string['includefeedback'] = 'Zahrnout zpětnou vazbu uživatelů';
$string['includefeedbackdescription'] = 'HMTL export bude obsahovat všechny komentáře uživatelů.';
$string['includeprivatefeedback'] = 'Zahrnout soukromé komentáře';
$string['includeprivatefeedbackdesc1'] = 'Pokud se rozhodnete zahrnout soukromé komentáře, lidé, se kterými budete exportovaný soubor sdílet, je uvidí. Pokud je chcete zachovat jako soukromé, exportujte soubor úplně bez komentářů nebo nezahrnujte soukromé komentáře.';
$string['justsomecollections'] = 'Jen některé z mých sbírek';
$string['justsomeviews'] = 'Jen některé z mých stránek';
$string['noexportpluginsenabled'] = 'Tuto vlastnost nemůžete použít, protože žádné z rozšíření pro export nebylo správcem povoleno';
$string['nonexistentfile'] = 'Pokus o přidání neexistujícího souboru: \'%s\'';
$string['nonexistentprofileicon'] = 'Pokusili jste se o přidání neexistující profilové ikony "%s"';
$string['nonexistentresizedprofileicon'] = 'Pokusili jste se o přidání neexistující profilové ikony (zmenšené) "%s"';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Počkejte prosím, zatímco je váš export generován...';
$string['requeue'] = 'Znovu zařadit do fronty';
$string['reverseselection'] = 'Obrátit výběr';
$string['selectall'] = 'Vybrat vše';
$string['setupcomplete'] = 'Nastavení kompletní';
$string['startinghtmlexport'] = 'Zahajování exportu HTML';
$string['startingleapexport'] = 'Zahajování exportu Leap2A';
$string['startingpdfexport'] = 'Zahajování exportu PDF';
$string['submissiondirnotwritable'] = 'Nelze zapsat do složky archivu příspěvků: %s';
$string['submissionreleasefailed'] = 'Selhalo uvolnění příspěvku po archivaci';
$string['unabletocopyartefact'] = 'Není možné zkopírovat soubor "%s"';
$string['unabletocopyprofileicon'] = 'Není možné zkopírovat profilovou ikonu "%s"';
$string['unabletocopyresizedprofileicon'] = 'Není možné zkopírovat profilovou ikonu (zmenšenou) "%s"';
$string['unabletoexportportfoliousingoptions'] = 'Nelze generovat export s použitím zvolených možností';
$string['unabletoexportportfoliousingoptionsadmin'] = 'Položka není objekt typu sbírka nebo stránka';
$string['unabletogenerateexport'] = 'Nelze generovat export';
$string['viewstoexport'] = 'Stránky k exportu';
$string['whatdoyouwanttoexport'] = 'Co chcete exportovat?';
$string['writingfiles'] = 'Zapisuji soubory';
$string['youarehere'] = 'Nacházíte se zde';
$string['youmustselectatleastonecollectiontoexport'] = 'Musíte vybrat alespoň jednu sbírku pro export';
$string['youmustselectatleastoneviewtoexport'] = 'Musíte vybrat alespoň jednu stránku k exportu';
$string['zipnotinstalled'] = 'Váš systém neobsahuje příkaz zip. Nainstalujte prosím zip k povolení této vlastnosti.';
