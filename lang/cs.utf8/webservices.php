<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['accesskey'] = 'Přístupový klíč';
$string['aimscreenname'] = 'Uživatelské jméno v AIM';
$string['allowcomments'] = 'Povolit komentáře';
$string['apilevel'] = 'Úroveň API';
$string['approvecomments'] = 'Schvalovat komentáře';
$string['archiveonrelease'] = 'Chcete archivovat odevzdané portfolio po jeho uvolnění?';
$string['authplugins'] = 'Moduly autentizace zahrnují manual, ldap, imap, etc';
$string['authuserfirstname'] = 'Křestní jméno autentizované osoby';
$string['authuserfullname'] = 'Celé jméno autentizované osoby';
$string['authuserid'] = 'ID autentizované osoby';
$string['authuserlastname'] = 'Příjmení autentizované osoby';
$string['authuserusername'] = 'Uživatelské jméno autentizované osoby';
$string['availfunctions'] = 'Dostupné funkce';
$string['blog'] = 'Deník';
$string['blogaddress'] = 'Webová adresa deníku';
$string['blogauthor'] = 'Autor deníku';
$string['blogcreatetime'] = 'Čas vytvoření deníku';
$string['blogdesc'] = 'Popis deníku';
$string['blogid'] = 'ID deníku';
$string['blogmodtime'] = 'Čas úpravy deníku';
$string['blogofparent'] = 'ID nadřazeného deníku';
$string['blogowner'] = 'Vlastník deníku';
$string['blogowneremail'] = 'Emailová adresa vlastníka deníku';
$string['blogownerid'] = 'ID vlastníka deníku';
$string['blogownerremusername'] = 'Externí uživatelské jméno vlastníka deníku';
$string['blogownerusername'] = 'Uživatelské jméno vlastníka deníku';
$string['blogpost'] = 'Příspěvek v deníku';
$string['blogpostauthor'] = 'Autor příspěvku v deníku';
$string['blogpostcount'] = 'Počet příspěvků v deníku';
$string['blogpostcreatetime'] = 'Čas vytvoření příspěvku v deníku';
$string['blogpostdesc'] = 'Popis příspěvku v deníku';
$string['blogpostdraft'] = 'Pracovní mód příspěvku v deníku';
$string['blogpostid'] = 'ID příspěvku v deníku';
$string['blogpostmodtime'] = 'Čas úpravy příspěvku v deníku';
$string['blogpostowner'] = 'Vlastník příspěvku v deníku';
$string['blogposts'] = 'Příspěvky v deníku';
$string['blogpostsids'] = 'Jednotlivé ID příspěvků v deníku';
$string['blogposttitle'] = 'Název příspěvku v deníku';
$string['blogs'] = 'Deníky';
$string['blogscount'] = 'Počet deníků';
$string['blogsids'] = 'Jednotlivé ID deníků';
$string['blogtitle'] = 'Název deníku';
$string['businessnumber'] = 'Pracovní telefonní číslo';
$string['city'] = 'Město bydliště osoby';
$string['collections'] = 'Sbírky';
$string['collectionscount'] = 'Počet sbírek';
$string['context_id'] = 'LTI kontext ID';
$string['context_label'] = 'LTI kontext značka';
$string['context_title'] = 'LTI kontext název';
$string['context_type'] = 'LTI kontext typ';
$string['country'] = 'Kód země dané osoby, jako třeba AU nebo CZ';
$string['deleteuserid'] = 'ID osoby ke smazání';
$string['deleteusername'] = 'Uživatelské jméno osoby ke smazání';
$string['displayname'] = 'Zobrazované jméno osoby';
$string['displaytitle'] = 'Zobrazovaný název stránky';
$string['emailaddress'] = 'Emailová adresa osoby';
$string['emailvalid'] = 'Platná a jedinečná emailová adresa';
$string['ext_lms'] = 'LTI externí LMS';
$string['ext_user_username'] = 'LTI externí uživatelské jméno';
$string['externalfilesource'] = 'Identifikátor řetězce, odkud byl soubor nahrán.';
$string['externalfullurl'] = 'Celá URL adresa webových stránek, kde byly odevzdané příspěvky vytvořeny.';
$string['favourites'] = 'Oblíbené';
$string['favshortname'] = 'Krátké jméno pro oblíbené';
$string['favsownerid'] = 'ID vlastníka oblíbených';
$string['favsownerusername'] = 'Uživatelské jméno vlastníka oblíbených';
$string['favuserid'] = 'ID oblíbené osoby';
$string['favusername'] = 'Uživatelské jméno oblíbeného';
$string['faxnumber'] = 'Číslo faxu';
$string['feedbacknotify'] = 'Upozorňování na komentáře povoleno:';
$string['feedbacknotifyexplicit'] = 'Upozornění na komentáře';
$string['filedescription'] = 'Popis souboru, např. "Certifikát pro kurz 101, 2021"';
$string['fileid'] = 'ID nově vytvořeného souboru';
$string['filetags'] = 'Pole volitelných značek, např. [2021, 101, certifikát]';
$string['filetitle'] = 'Název souboru, např. "Certifikát 101"';
$string['filetoupload'] = 'Název souboru';
$string['fileuploadfail'] = 'Nahrání souboru selhalo';
$string['fileuploadmessagebody'] = 'Soubor "%s" byl nahrán automaticky od %s. Je ve složce "%s".';
$string['fileuploadmessagesubject'] = 'Nový soubor nahrán do prostoru vašich "Souborů".';
$string['fileuploadstatus'] = 'Úspěch nebo selhání při nahrávání souboru';
$string['fileuploadsuccess'] = 'Nahrání souboru proběhlo úspěšně';
$string['fileurl'] = 'URL adresa pro stažení zip souboru. Poznámka: Musíte poskytnout platný token webové služby, aby bylo možné soubor stáhnout.';
$string['firstname'] = 'První (křestní) jméno/a dané osoby';
$string['foldername'] = 'Jméno složky, do které má být uložen soubor. Pokud taková ještě neexistuje, bude vytvořena.';
$string['forcegroupcategory'] = 'Vytvoří kategorii skupiny, pokud ještě neexistuje';
$string['forcepasswordchange'] = 'Vynutit změnu hesla po prvním přihlášení';
$string['fullurl'] = 'Celá URL adresa';
$string['functiondocuri'] = 'URI dokumentace k funkci';
$string['functionname'] = 'Název funkce';
$string['groupcaneditroles'] = 'Role, které mají práva k úpravám:';
$string['groupcategory'] = 'Kategorie skupiny – název již existující kategorie skupiny';
$string['groupdesc'] = 'Popis skupiny';
$string['grouphidden'] = 'Skrýt skupinu';
$string['groupid'] = 'ID skupiny';
$string['groupmemberactions'] = 'Akce členství ve skupině';
$string['groupmembership'] = 'Členství ve skupině';
$string['groupname'] = 'Jméno skupiny';
$string['groupparticipationreports'] = 'Hlášení o účasti';
$string['groupshortname'] = 'Krátké jméno skupiny pro skupiny ovládané pouze přes API';
$string['grouptype'] = 'Typ skupiny:';
$string['grouptypectrl'] = 'Řízený – Správci skupiny mohou přidávat lidi do skupin bez jejich souhlasu, a členové nemají možnost skupinu opustit';
$string['grouptypeopen'] = 'Otevřený – Lidé se mohou ke skupině připojit, aniž by potřebovali schválení správce skupiny';
$string['grouptypepublic'] = 'Veřejná skupina';
$string['grouptypereq'] = 'Na žádost – Lidé mohou zasílat správcům skupiny žádosti o udělení členství ve skupině';
$string['grouptypesubmitpage'] = 'Povolit příspěvky – Členové mohou nahrávat do skupiny stránky';
$string['hidemembership'] = 'Skrýt členství';
$string['homenumber'] = 'Telefonní číslo domů';
$string['icqnumber'] = 'Číslo ICQ';
$string['idownersubmitportfolio'] = 'ID v rámci Mahary od osoby, která odevzdává své portfolio k hodnocení';
$string['industry'] = 'Průmysl';
$string['institution'] = 'Instituce Mahary';
$string['institutioncontextauthuser'] = 'Kontext instituce autentizované osoby';
$string['institutionforctrlgroups'] = 'Instituce Mahary - vyžadováno pro skupiny ovládané přes API';
$string['institutionnameauthuser'] = 'Celé jméno kontextu instituce autentizované osoby';
$string['internal'] = 'interní';
$string['introduction'] = 'Představení vlastníka účtu';
$string['isacollection'] = 'Je sbírka';
$string['iscollection'] = 'Je toto ID v rámci Mahary ID sbírky?';
$string['jabberusername'] = 'Uživatelské jméno Jabber/XMPP';
$string['lastname'] = 'Příjmení osoby';
$string['launch_presentation_locale'] = 'Spuštění prezentace lokálního LTI';
$string['launch_presentation_return_url'] = 'Návratové URL spuštění prezentace LTI';
$string['lis_person_contact_email_primary'] = 'LTI primární kontaktní email';
$string['lis_person_name_family'] = 'LTI příjmení osoby';
$string['lis_person_name_full'] = 'LTI celé jméno osoby';
$string['lis_person_name_given'] = 'LTI křestní jméno osoby';
$string['lis_person_sourcedid'] = 'ID osoby dle zdroje LTI';
$string['liteexporttype'] = 'Typ exportu, který má být použit. Platnými možnostmi jsou \'htmllite\' nebo \'pdflite\'.';
$string['lock'] = 'Má mít odevzdaný příspěvek zamčenou možnost úprav?';
$string['locked'] = 'Zamčeno';
$string['lti_message_type'] = 'Typ zprávy LTI';
$string['lti_version'] = 'Verze LTI';
$string['mahara'] = 'Mahara';
$string['memberactionexplicit'] = 'Akce se členy: přidat, nebo odebrat';
$string['memberrole'] = 'Role člena:';
$string['memberroleexplicit'] = 'Role člena: správce, učitel, člen';
$string['memberroleonlyadmin'] = 'Role člena: správce';
$string['memberuserid'] = 'ID člena';
$string['memberusername'] = 'Uživatelské jméno člena';
$string['mobilenumber'] = 'Telefonní číslo na mobil';
$string['msnnumber'] = 'Číslo MSN';
$string['occupation'] = 'Profese';
$string['officialwebsite'] = 'Oficiální webové stránky osoby';
$string['owneremailsubmitportfolio'] = 'Emailová adresa osoby, zasílající své portfolio k hodnocení';
$string['passwordvalid'] = 'Musí být dlouhé aspoň 6 znaků. Musí se lišit od uživatelského jména.';
$string['personalwebsite'] = 'Osobní webové stránky';
$string['plaintxtpassword'] = 'Heslo z prostého textu obsahující jakékoli znaky';
$string['portfolioowneremail'] = 'Emailová adresa vlastníka portfolia';
$string['portfolioownerid'] = 'ID vlastníka portfolia v rámci Mahary';
$string['portfolioownerusername'] = 'Uživatelské jméno vlastníka portfolia';
$string['portfolioquery'] = 'Filtr pro použití k požadavkům na portfolio';
$string['portfolioremoteuser'] = 'Vzdálené uživatelské jméno vlastníka portfolia';
$string['preferredname'] = 'Upřednostňované jméno vlastníka účtu';
$string['relativeurl'] = 'Relativní URL adresa';
$string['releaseremail'] = 'Emailová adresa osoby, která uvolnila tento odevzdaný příspěvek.';
$string['releaserid'] = 'ID osoby uvolňující tento odevzdaný příspěvek.';
$string['releaserremoteusername'] = 'Vzdálené uživatelské jméno osoby, která uvolňuje tento odevzdaný příspěvek.';
$string['releaserusername'] = 'Uživatelské jméno osoby, která uvolňuje tento odevzdaný příspěvek.';
$string['remoteuser'] = 'Vlastník vzdáleného účtu';
$string['remoteuserid'] = 'ID vlastníka vzdáleného účtu';
$string['remoteusername'] = 'Uživatelské jméno vzdáleného účtu';
$string['remoteusersconnected'] = 'Vzdáleně připojení lidé';
$string['remoteusersubmitportfolio'] = 'Přihlašovací jméno ke vzdálenému účtu osoby odevzdávající své portfolio k hodnocení';
$string['requestedapilvl'] = 'Požadovaná úroveň API';
$string['resource_link_description'] = 'Popis zdrojového odkazu LTI';
$string['resource_link_id'] = 'LTI ID zdrojového odkazu';
$string['resource_link_title'] = 'Název odkazu LTI zdroje';
$string['roles'] = 'LTI role';
$string['sitename'] = 'Název webových stránek';
$string['siteurl'] = 'URL webových stránek';
$string['skypeusername'] = 'Uživatelské jméno na Skype';
$string['socialprofilevalid'] = 'Profil na sociální síti vyžaduje vyplnění typu i URL adresy';
$string['storagequota'] = 'Volitelná kvóta úložiště';
$string['streetaddress'] = 'Adresa ulice osoby';
$string['studentid'] = 'Libovolné ID číslo pro studenta';
$string['studentidinst'] = 'Libovolné ID číslo, pocházející asi z instituce';
$string['submissionextid'] = 'Externí ID, ke kterému se vztahuje odevzdaný příspěvek';
$string['submissionextname'] = 'Externí jméno, ke kterému se vztahuje odevzdaný příspěvek';
$string['submittedhost'] = 'Externí hostitel, kterému byl odevzdán';
$string['submittedtime'] = 'Čas odevzdání';
$string['tag'] = 'Štítek';
$string['tags'] = 'Štítky';
$string['tool_consumer_info_product_family_code'] = 'Rodina produktů odběratele pro nástroj LTI';
$string['tool_consumer_info_version'] = 'Verze odběratele pro nástroj LTI';
$string['tool_consumer_instance_guid'] = 'GUID instance odběratele pro nástroj LTI';
$string['tool_consumer_instance_name'] = 'Název instance odběratele pro nástroj LTI';
$string['town'] = 'Město bydliště osoby';
$string['user_id'] = 'LTI ID osoby';
$string['useraddress'] = 'Adresa osoby';
$string['userfavs'] = 'Oblíbené vlastníka účtu';
$string['userid'] = 'ID vlastníka účtu';
$string['useridsort'] = 'Řazení dle ID. Buďto vzestupně [asc] nebo sestupně [desc]';
$string['username'] = 'Uživatelské jméno vlastníka účtu';
$string['usernamesubmitportfolio'] = 'Uživatelské jméno osoby, zasílající své portfolio k hodnocení';
$string['usernamevalid1'] = 'Délka mezi 3 a 255 znaky. Povoleny jsou písmena, čísla a většina standardních symbolů.';
$string['userresultslimit'] = 'Konec výsledků';
$string['userresultsoffset'] = 'Začátek výsledků';
$string['usersautoadded'] = 'Automatické přidávání lidí';
$string['userstotalcount'] = 'Celkový počet výsledků';
$string['view'] = 'Stránka';
$string['viewcollid'] = 'ID sbírky stránek';
$string['viewcreatetime'] = 'Čas vytvoření stránky';
$string['viewdesc'] = 'Popis stránky';
$string['viewfullurl'] = 'Úplná URL adresa stránky';
$string['viewid'] = 'ID stránky';
$string['viewidsubmit'] = 'ID odevzdané stránky nebo sbírky v rámci Mahary.';
$string['viewidtotest'] = 'Portfolio odevzdané k exportování';
$string['viewmodtime'] = 'Čas úpravy stránky';
$string['viewnotify'] = 'Povolena oznámení sdílené stránky:';
$string['viewoutcomes'] = 'Výsledky související s tímto portfoliem';
$string['viewrelativeurl'] = 'Relativní URL adresa stránky';
$string['views'] = 'Stránky';
$string['viewscount'] = 'Počet stránek';
$string['viewsids'] = 'ID jednotlivých stránek';
$string['viewtitle'] = 'Název stránky';
$string['viewtype'] = 'Typ stránky';
$string['wwwroot'] = 'URN klienta k rozlišení vzdálených zámků';
$string['yahoochat'] = 'Chat Yahoo';
