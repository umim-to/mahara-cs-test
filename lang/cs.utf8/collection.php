<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Collection'] = 'Sbírka';
$string['Collections'] = 'Sbírky';
$string['about'] = 'O';
$string['access'] = 'Přístup';
$string['accesscantbeused'] = 'Potlačení přístupu nebylo uložené. Vybraný přístup ke stránce (tajné URL) nemuže být použit pro více zobrazení.';
$string['accessdeniedundo'] = 'Vyjádření bylo smazáno. K portfoliu nemáte již nadále přístup.';
$string['accessignored'] = 'Některé typy tajných URL odkazů byly ignorovány.';
$string['accessoverride'] = 'Přístup potlačen';
$string['accesssaved'] = 'Přístup ke sbírce byl úspěšně uložen.';
$string['add'] = 'Přidat';
$string['addviews'] = 'Přidat stránky';
$string['addviewstocollection'] = 'Přidat stránky do sbírky';
$string['andparticle'] = 'a %s';
$string['autocopytemplate'] = 'Stávající automaticky kopírovaná šablona';
$string['autocopytemplatedesc'] = 'Nastavte na \'Ano\', pokud si přejete, aby tato sbírka byla zkopírována pro každý nově vytvořený účet (a také na základě práce cronu, pokud ho takto nastavíte). Oprávnění ke sdílení bude nastaveno automaticky tak, aby umožňovalo kopírování sbírky. Také byste měli nastavit \'Šablona\' na \'Ano\'.';
$string['back'] = 'Zpět';
$string['by'] = 'od';
$string['cantcreatecollection'] = 'Nemáte povoleno vytvořit tuto sbírku';
$string['cantdeletecollection'] = 'Nemůžete odstranit tuto sbírku.';
$string['canteditcollection'] = 'Nemáte povoleno upravit tuto sbírku';
$string['canteditdontown'] = 'Nemůžete upravit tuto sbírku, protože nejste jejím vlastníkem.';
$string['canteditgroupcollections'] = 'Nemáte povoleno upravovat sbírky skupiny.';
$string['canteditinstitutioncollections'] = 'Nemáte povoleno upravovat sbírky instituce.';
$string['canteditsubmitted'] = 'Nemůžete upravovat tuto sbírku, protože byla odeslána k hodnocení %s. Musíte počkat, dokud nebude uvolněna.';
$string['cantlistgroupcollections'] = 'Nemáte povoleno zobrazit sbírky skupiny.';
$string['cantlistinstitutioncollections'] = 'Nemáte povoleno zobrazit sbírky instituce.';
$string['collection'] = 'sbírka';
$string['collectionaccess'] = 'Přístup ke sbírce';
$string['collectionaccesseditedsuccessfully'] = 'Přístup ke sbírce byl úspěšně uložen';
$string['collectionaccessrules'] = 'Přístupová práva ke sbírce';
$string['collectionconfirmdelete'] = 'Stránky v této sbírce nebudou odstraněny. Jste si jisti, že chcete odstranit tuto sbírku?';
$string['collectionconfirmdelete1'] = '<p>Opravdu chcete smazat tuto sbírku? Smažete tak také všechny stránky, které obsahuje. Pokud chcete pouze smazat určité stránky, tuto akci neprovádějte a smažte pouze ty stránky, které chcete odstranit. </p><p>Prosím, zvažte vytvoření zálohy <a href="%sexport/index.php?collection=%s">vyexportováním</a> vašeho portfolia před tím, než cokoli smažete.</p> <p><strong>Poznámka:</strong> Pokud se rozhodnete smazat tuto sbírku, všechny vaše soubory a záznamy v deníku, na které je na stránkách portfolia odkazováno, zůstanou i nadále dostupné. Nicméně všechny textové bloky a komentáře umístěné na stránkách portfolia budou smazány spolu se stránkami. </p>';
$string['collectioncopywouldexceedquota'] = 'Kopírováním této sbírky bude překročen váš limit místa na disku.';
$string['collectioncreatedsuccessfully'] = 'Sbírka byla úspěšně vytvořena.';
$string['collectioncreatedsuccessfullyshare'] = 'Vaše sbírka byla úspěšně vytvořena. Podělte se o své sbírky s ostatními prostřednictvím níže uvedených odkazy.';
$string['collectiondeleted'] = 'Sbírka byla úspěšně odstraněna.';
$string['collectiondescription'] = 'Sbírka je soubor stránek, které jsou navzájem propojené a mají stejné oprávnění k přístupu. Můžete vytvořit tolik sbírek, kolik chcete, ale stránka se nemůže objevit ve více než jedné sbírce.';
$string['collectiondragupdate1'] = 'Přetáhněte jména stránek z pole "Přidat stránky do sbírky" nebo stránky označte zaškrtnutím a klikněte na "Přidat stránky" pro přesunutí stránek do pole "Stránky ve sbírce"';
$string['collectioneditaccess'] = 'Právě upravujete přístup pro stránku %d v této sbírce';
$string['collections'] = 'Sbírky.';
$string['collectionsaved'] = 'Sbírka byla uložena.';
$string['collectionssharedtogroup'] = 'Sbírky sdílené s touto skupinou';
$string['collectiontitle'] = 'Titulek sbírky';
$string['completionpercentage'] = 'Dokončenost';
$string['confirmcancelcreatingcollection'] = 'Tato sbírka nebyla dokončena. Opravdu chcete zrušit?';
$string['copiedblogpoststonewjournal'] = 'Zkopírované příspěvky v deníku byly vloženy do nového samostatného deníku.';
$string['copiedpagesblocksandartefactsfromtemplate'] = 'Zkopírováno %d stránek, %d bloků a %d položek portfolia z %s';
$string['copiedparticle'] = 'Zkopírováno %s';
$string['copyacollection'] = 'Kopírovat sbírku';
$string['copycollection'] = 'Kopírovat sbírku';
$string['countartefacts'] = array(

		0 => '%d položka portfolia', 
		1 => '%d položky portfolia', 
		2 => '%d položek portfolia'
);
$string['countblocks'] = array(

		0 => '%d blok', 
		1 => '%d bloky', 
		2 => '%d bloků'
);
$string['countpages'] = array(

		0 => '%d stránka', 
		1 => '%d stránky', 
		2 => '%d stránek'
);
$string['created'] = 'Vytvořeno';
$string['deletecollection'] = 'Odstranit sbírku';
$string['deletespecifiedcollection'] = 'Odstranit sbírku \'%s\'';
$string['deleteview'] = 'Odstranit stránku ze sbírky';
$string['deletingcollection'] = 'Mažu sbírku';
$string['description'] = 'Popis sbírky';
$string['editaccess'] = 'Upravit přístup ke sbírce';
$string['editcollection'] = 'Upravit sbírku';
$string['editingcollection'] = 'Upravuji sbírku';
$string['edittitleanddesc'] = 'Upravit název a popis';
$string['editviewaccess'] = 'Upravit přístup ke stránce';
$string['editviews'] = 'Upravit stránky sbírky';
$string['emptycollection'] = 'Prázdná sbírka';
$string['emptycollectionnoeditaccess'] = 'Nemůžete měnit přístup k prázdné sbírkce. Nejprve přidejte nějaké stránky.';
$string['fromtemplate'] = 'z "%s"';
$string['groupcollections'] = 'Sbírky skupiny';
$string['institutioncollections'] = 'Sbírky instituce';
$string['lockedcollection'] = 'Zamčeno do %s';
$string['manage'] = 'Správa';
$string['manageviews'] = 'Spravovat stránky';
$string['manageviewsspecific'] = 'Spravovat stránky v "%s"';
$string['name'] = 'Název sbírky';
$string['navtopage'] = 'Přejít na stránku:';
$string['ncollections'] = array(

		0 => '%s sbírka', 
		1 => '%s sbírky', 
		2 => '%s sbírek'
);
$string['needssignedoff'] = 'Potřebuje schválení vlastníkem';
$string['needsverified'] = 'Potřebuje potvrzení vedoucím';
$string['needtoselectaview'] = 'Musíte zvolit stránku pro přidání do sbírky';
$string['newcollection'] = 'Nová sbírka';
$string['nexteditaccess'] = 'Další: Upravit přístup';
$string['nextpage'] = 'Další stránka';
$string['nocollections'] = 'Prozatím není dostupná žádná sbírka.';
$string['nocollectionsaddone'] = 'Dosud nejsou vytvořeny žádné sbírky. %sVytvořte nějaké%s!';
$string['nooverride'] = 'Žádné potlačení';
$string['nosharedcollectionsyet'] = 'V této skupině nebyly ještě sdíleny žádné sbírky';
$string['notifyappointed'] = 'Požadavek znovu zadán';
$string['noviews'] = 'Žádné stránky.';
$string['noviewsaddsome'] = 'Ve sbírce nejsou žádné stránky. %sPřidejte nějaké!%s';
$string['noviewsavailable'] = 'Žádné stránky nejsou k dispozici pro přidání.';
$string['noviewsincollection'] = 'Ve sbírce nejsou žádné stránky.';
$string['numviewsincollection'] = array(

		0 => '%s stránka ve sbírce', 
		1 => '%s stránky ve sbírce', 
		2 => '%s stránek ve sbírce'
);
$string['onlyactivetemplatewarning'] = 'Toto je jediná automaticky kopírovaná šablona této instituce. Po změnění tohoto nastavení nebude připravena žádná šablona k nakopírování do nových účtů (ani na základě práce cronu, pokud je nějaká nastavena).';
$string['overallcompletion'] = 'Dokončenost schválení vlastníkem a potvrzení vedoucími';
$string['overrideaccess'] = 'Potlačit přístup';
$string['ownerhasremovedaccess'] = '%s odebral/a váš přístup k jeho/jejímu portfoliu "%s".';
$string['ownerhasremovedaccesssubject'] = '%s odebral/a váš přístup k "%s"';
$string['pageincollectiontitle'] = 'Tato stránka je součástí sbírky \'%s\'.';
$string['pluginname'] = 'Sbírky';
$string['portfoliocompletion'] = 'Dokončenost portfolia';
$string['potentialviews'] = 'Potenciální stránky';
$string['prevpage'] = 'Předchozí stránka';
$string['progresscompletiondesc'] = 'Vložit stránku \'Dokončenost portfolia\' na začátek této sbírky.';
$string['progressnotavailable'] = 'Posun v dokončenosti portfolia "%s" od %s zatím nelze zobrazit';
$string['progresspage'] = 'Dokončenost portfolia';
$string['progresspagedescription'] = 'Nastavte výchozí rozložení stránky dokončenosti portfolia pro sbírku.';
$string['progressportfolios'] = 'Počítadlo portfolií';
$string['progressverifiers'] = 'Procento posuzovatelů';
$string['reasonforundo'] = 'Důvod pro smazání vyjádření:';
$string['removeaccess'] = 'Odebrat můj přístup k portfoliu "%s" od %s';
$string['removemyaccess'] = 'Odebrat můj přístup';
$string['removemyaccessiconaria'] = 'Odebrat můj přístup k "%s", které vlastní %s';
$string['removemyaccesssubmit'] = 'Pokračovat';
$string['revokedbyowner'] = 'Vlastník odebral přístup';
$string['revokemessagesent'] = 'Přístup odebrán';
$string['revokemyaccessconfirm'] = 'Ztratíte přístup k:';
$string['revokemyaccessdescription'] = 'Pokud budete pokračovat, odeberete si přístup k celému tomuto portfoliu. Nebudete ho nadále již moci zobrazit ani s ním jinak nakládat. Vlastník portfolia obdrží upozornění, že již nemáte přístup. <br>Můžete přidat doplňující zprávu.';
$string['revokemyaccessformtitle'] = 'Zrušit přístup k portfoliu';
$string['revokemyaccessreason'] = 'Zpráva';
$string['revokemyaccessreasontextbox'] = 'Zpráva';
$string['saveapply'] = 'Použít a uložit';
$string['savecollection'] = 'Uložit sbírku';
$string['sharedviewverifiedchecked'] = '%s od %s je zkontrolováno';
$string['sharedviewverifiedunchecked'] = '%s od %s ještě nebylo posouzeno';
$string['signedoff'] = 'Schváleno vlastníkem';
$string['sitecollections'] = 'Sbírka stránek.';
$string['smartevidence'] = 'SmartEvidence';
$string['smartevidencedesc'] = 'Spravovat SmartEvidence rámce';
$string['template'] = 'Šablona';
$string['templatedesc'] = 'Nastavte na \'Ano\', pokud chcete, aby všechny stránky této sbírky byly předělány na šablony bez potřeby to nastavovat pro každou stránku zvlášť. Stránky přidané do šablony budou také automaticky přeměněny na šablony. Automaticky je také zabráněno odstraňování bloků, je to ale možné změnit.';
$string['undonemessage'] = '%s smazal vyjádření "%s" v portfoliu "%s" na žádost osoby, která jej původně potvrdila.';
$string['undonesubject'] = 'Žádost o smazání posudkového vyjádření byla vyplněna';
$string['undoreportmessage'] = 'Vyjádření "%s" v portfoliu "%s" bylo potvrzeno. Nicméně posuzovatel %s si ho přeje smazat. Uvedl následující zdůvodnění: %s';
$string['undoreportnotsent'] = 'Žádost o smazání nebyla zaslána, protože není komu ji zaslat. Prosím, kontaktujte správce.';
$string['undoreportsent'] = 'Žádost o smazání zaslána.';
$string['undoreportsubject'] = 'Žádost o smazání vyjádření k portfoliu';
$string['undoverification'] = 'Smazat vyjádření';
$string['undoverificationdescription'] = 'Zvolte jedno z vašich vyjádření, které chcete smazat. V přápadě pokračování pak bude zasláno upozornění tomu, kdo vaše vyjádření smazat může. Vaše schválení pak bude odstraněno, jakmile kontaktovaná osoba vyhoví vaší žádosti, a budete jej moci udělit znovu.';
$string['undoverificationformtitle'] = 'Smazání vyjádření';
$string['update'] = 'Aktualizovat';
$string['updatingautocopytemplatewarning'] = 'Pouze jedna sbírka může být automaticky kopírovanou šablonou pro instituci. Tím, že nastavíte tuto sbírku jako automaticky kopírovanou pro instituci "%s", nynější automaticky kopírovaná sbírka "%s" již nebude aktivní. Nebude nadále s institucí sdílena.';
$string['usecollectionname'] = 'Použít jméno sbírky?';
$string['usecollectionnamedesc'] = 'Pokud chcete použít jméno sbírky namísto titulku bloku, ponechejte tuto volbu zaškrtnutou.';
$string['userhasremovedaccess'] = '%s už nemá přístup k portfoliu "%s".';
$string['userhasremovedaccesssubject'] = '%s odebral(a) svůj přístup k "%s"';
$string['userrevokereason'] = 'Uvedli následující zdůvodnění:';
$string['verification'] = 'Potvrzení';
$string['verificationdone'] = 'Vyjádření k portfoliu "%s%" od %s potvrzeno';
$string['verificationtobedone'] = 'Potvrďte vyjádření k portfoliu "%s" od %s';
$string['verified'] = 'Potvrzeno vedoucím';
$string['verifiedbyme'] = 'Má vyjádření';
$string['verifiedbymedescription'] = 'Zvolte vyjádření k odstranění. Poté uveďte důvod této změny.';
$string['verifiednotavailable'] = 'Nemůžete potvrdit hlavní vyjádření k portfoliu "%s" od %s';
$string['verifiednotavailabledate'] = 'Vyjádření k portfoliu "%s" od %s nemůže být potvrzeno před %s';
$string['viewaddedsecreturl'] = 'Veřejně dostupné skrze tajné URL';
$string['viewaddedtocollection'] = 'Stránky byly přidány do sbírky. Sbírka je aktualizována, aby zahrnovala přístup k novým stránkám.';
$string['viewcollection'] = 'Zobrazit detaily sbírky';
$string['viewconfirmremove'] = 'Opravdu si přejete odstranit tuto stránku ze sbírky?';
$string['viewcount'] = 'Stránky';
$string['viewingpage'] = 'Jste na stránce';
$string['viewnavigation'] = 'Zobraz navigační lištu';
$string['viewnavigationdesc'] = 'Přidej horizontální navigační lištu ke každé stránce v této sbírce jako výchozí nastavení.';
$string['viewremovedsuccessfully'] = 'Stránka byla úspěšně odstraněna.';
$string['viewsaddedaccesschanged'] = 'Přístupová práva změněna pro tyto stránk:';
$string['viewsaddedtocollection'] = 'Stránky byly přidány do sbírky. Sbírka je aktualizována, aby zahrnovala přístup k novým stránkám.';
$string['viewsaddedtocollection1'] = array(

		0 => '%s stránka přidána do sbírky.', 
		1 => '%s stránky přidány do sbírky.', 
		2 => '%s stránek přidáno do sbírky.'
);
$string['viewsaddedtocollection1different'] = array(

		0 => '%s stránka přidána do sbírky. Sdílení se změnilo pro všechny stránky ve sbírce.', 
		1 => '%s stránky přidány do sbírky. Sdílení se změnilo pro všechny stránky ve sbírce.', 
		2 => '%s stránek přidáno do sbírky. Sdílení se změnilo pro všechny stránky ve sbírce.'
);
$string['viewsincollection'] = 'Stránky ve sbírce';
$string['viewstobeadded'] = 'Stránky byly přidány';
$string['youhavebeengivenaccess'] = 'Byl vám přidělen přístup k následujícímu:';
$string['youhavecollections'] = 'Máte %s sbírek.';
$string['youhavencollections'] = array(

		0 => 'Máte 1 sbírku.', 
		1 => 'Máte %d sbírky.', 
		2 => 'Máte %d sbírek.'
);
$string['youhavenocollections'] = 'Nemáte žádné sbírky.';
$string['youhaveonecollection'] = 'Máte jednu sbírku.';
