<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2017
 *
 */

defined('INTERNAL') || die();

$string['cached'] = 'v mezipaměti';
$string['csys'] = 'csys';
$string['cuser'] = 'cuser';
$string['dbqueries'] = 'DB dotazů';
$string['included'] = 'Zahrnutých souborů';
$string['memoryused'] = 'Paměť';
$string['reads'] = 'čtení';
$string['seconds'] = 'sekund';
$string['serverload'] = 'Zátěž serveru';
$string['sys'] = 'sys';
$string['ticks'] = 'ticks';
$string['timeused'] = 'Doba provádění';
$string['user'] = 'user';
$string['writes'] = 'zápisů';
