<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, David Mudrák, Adam Pátek, Matouš Trča, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2018
 *
 */

defined('INTERNAL') || die();

$string['aboutdefaultcontent'] = '<h2>O těchto stránkách</p>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara vznikla v roce 2006 jako výsledek společného úsilí několika institucí na Novém Zélandu: Tertiary Education Commission\'s e-learning Collaborative Development Fund (eCDF), Massey University, Auckland University of Technology, The Open Polytechnic of New Zealand a Victoria University of Wellington.</p>

<p>Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Slovo mahara znamená "myslet" nebo "myšlenka" v jazyce původních obyvatel Te Reo Maori. Reprezentuje záměr svých tvůrců vytvořit prostředí zaměřené na celoživotní vzdělávání a osobnostní rozvoj uživatelů a zároveň přesvědčení, že podobnou technologii nelze vyvinout bez zohlednění řady pedagogických aspektů.</p>

<p>Mahara je poskytována zdarma jako software s otevřeným kódem (open source) a je možno ji dále modifikovat a šířit za podmínek daných všeobecnou veřejnou licencí GNU GPL.</p>

<p>V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>';
$string['homedefaultcontent'] = '<h2>Vítejte na těchto stránkách</h2>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Přečtěte si více <a href="about.php">o těchto stránkách</a>. V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>';
$string['licensedisplaynameby'] = 'Creative Commons Attribution 3.0';
$string['licensedisplaynamebync'] = 'Creative Commons Attribution Non Commercial 3.0';
$string['licensedisplaynamebyncnd'] = 'Creative Commons Attribution Non Commercial No Derivatives 3.0';
$string['licensedisplaynamebyncsa'] = 'Creative Commons Attribution Non Commercial Share Alike 3.0';
$string['licensedisplaynamebynd'] = 'Creative Commons Attribution No Derivatives 3.0';
$string['licensedisplaynamebysa'] = 'Creative Commons Attribution Share Alike 3.0';
$string['licensedisplaynamegfdl'] = 'GNU Free Documentation License v1.3';
$string['licenseshortnameby'] = 'CC-BY-3.0';
$string['licenseshortnamebync'] = 'CC-BY-NC-3.0';
$string['licenseshortnamebyncnd'] = 'CC-BY-NC-ND-3.0';
$string['licenseshortnamebyncsa'] = 'CC-BY-NC-SA-3.0';
$string['licenseshortnamebynd'] = 'CC-BY-ND-3.0';
$string['licenseshortnamebysa'] = 'CC-BY-SA-3.0';
$string['licenseshortnamegfdl'] = 'GFDL-1.3';
$string['loggedouthomedefaultcontent'] = '<h2>Vítejte na těchto stránkách</h2>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Přečtěte si více <a href="about.php">o těchto stránkách</a>. V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>';
$string['privacydefaultcontent'] = '<h2>Prohlášení o ochraně osobních údajů</h2>';
$string['staticpageconfigdefault'] = '"Statické stránky" v "Nastavení stránek" v sekci "Administrace"';
$string['staticpageconfigdefaults'] = '"Administrátorské menu"→ "Nastavení stránek" → <a href="%s">"<b>Statické stránky</b>"</a>';
$string['staticpageconfiginstitution'] = '"Statické stránky" v "Instituce" v sekci "Administrace"';
$string['staticpageconfiginstitutions'] = '"Administrátorské menu"→ "Instituce" → <a href="%s">"<b>Statické stránky</b>"</a>';
$string['termsandconditionsdefaultcontent'] = '<h2>Podmínky používání těchto stránek</h2>';
$string['uploadcopyrightdefaultcontent'] = 'Tímto prohlašuji, že soubor, který ukládám na server, jsem vytvořil/-a já osobně nebo jsem oprávněn/-a jej reprodukovat a dále šířit. Zároveň prohlašuji, že nahráním tohoto souboru neporušuji platné zákony týkající se ochrany autorských práv ani další ujednání vztahující se k tomuto souboru. Obsah souboru nesmí být v rozporu s podmínkami používání těchto stránek.';
$string['versionedpageconfigdefault'] = '"Administrátorské menu" → "Nastavení stránek" → <a href="%s">"<b>Právní ustanovení</b>"</a>';
