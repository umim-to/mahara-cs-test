<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'B';
$string['element.bytes.gigabytes'] = 'GB';
$string['element.bytes.invalidvalue'] = 'Hodnota musí být číselná';
$string['element.bytes.kilobytes'] = 'kB';
$string['element.bytes.megabytes'] = 'MB';
$string['element.calendar.datefrom'] = 'Datum od';
$string['element.calendar.dateto'] = 'Datum do';
$string['element.calendar.format.arialabel'] = 'K úpravě tohoto pole používejte následující formát: RRRR / MM / DD mezera hodiny : minuty (ve 24 hodinovém formátu).';
$string['element.calendar.format.help.12hour1digit'] = 'H';
$string['element.calendar.format.help.12hour2digits'] = 'HH';
$string['element.calendar.format.help.24hour1digit'] = 'H';
$string['element.calendar.format.help.24hour2digits'] = 'HH';
$string['element.calendar.format.help.ampmlowercase'] = 'dop.';
$string['element.calendar.format.help.ampmuppercase'] = 'dop.';
$string['element.calendar.format.help.dayofmonth1digit'] = 'D';
$string['element.calendar.format.help.dayofmonth2digits'] = 'DD';
$string['element.calendar.format.help.minute2digits'] = 'MM';
$string['element.calendar.format.help.month2digit'] = 'MM';
$string['element.calendar.format.help.second2digits'] = 'SS';
$string['element.calendar.format.help.year2digit'] = 'RR';
$string['element.calendar.format.help.year4digit'] = 'RRRR';
$string['element.calendar.invalidvalue'] = 'Neplatný datum/čas';
$string['element.calendar.opendatepicker'] = 'Otevřít kalendář';
$string['element.color.or'] = 'nebo';
$string['element.color.transparent'] = 'Použít barvu tématu';
$string['element.color.transparent1'] = 'Výchozí motiv';
$string['element.date.at'] = 'v';
$string['element.date.monthnames'] = 'leden,únor,březen,duben,květen,červen,červenec,srpen,září,říjen,listopad,prosinec';
$string['element.date.notspecified'] = 'Neurčeno';
$string['element.date.or'] = 'nebo';
$string['element.date.specify'] = 'Nastavit datum';
$string['element.expiry.days'] = 'dnů';
$string['element.expiry.days.lowercase'] = array(

		0 => '%s den', 
		1 => '%s dny', 
		2 => '%s dnů'
);
$string['element.expiry.months'] = 'měsíců';
$string['element.expiry.months.lowercase'] = array(

		0 => '%s měsíc', 
		1 => '%s měsíce', 
		2 => '%s měsíců'
);
$string['element.expiry.noenddate'] = 'nikdy';
$string['element.expiry.noenddate.lowercase'] = 'nikdy';
$string['element.expiry.weeks'] = 'týdnů';
$string['element.expiry.weeks.lowercase'] = array(

		0 => '%s týden', 
		1 => '%s týdny', 
		2 => '%s týdnů'
);
$string['element.expiry.years'] = 'roků';
$string['element.expiry.years.lowercase'] = array(

		0 => '%s rok', 
		1 => '%s roky', 
		2 => '%s roků'
);
$string['element.files.addattachment'] = 'Přidat přílohu';
$string['element.font.themedefault'] = 'Použít písmo motivu';
$string['element.passwordpolicy.ul'] = 'Velká a malá písmena';
$string['element.passwordpolicy.uln'] = 'Velká a malá písmena, číslice';
$string['element.passwordpolicy.ulns'] = 'Velká a malá písmena, číslice, symboly';
$string['element.select.other'] = 'Jiné';
$string['element.select.remove'] = 'Odebrat "%s"';
$string['oneoffields'] = 'Pole označená jako \'%s\' vyžadují, aby aspoň jedno z nich bylo vyplněno.';
$string['requiredfields'] = 'Pole označená jako \'%s\' jsou povinná.';
$string['rule.before.before'] = 'Zadaná hodnota nemůže být pozdější než v poli "%s"';
$string['rule.email.email'] = 'Neplatná e-mailová adresa';
$string['rule.float.float'] = 'Toto pole přijímá pouze desetinná čísla';
$string['rule.integer.integer'] = 'Musíte zadat celé číslo';
$string['rule.maxlength.maxlength'] = 'Můžete zadat nejvýše %d znaků';
$string['rule.maxvalue.maxvalue'] = 'Hodnota nemůže být větší než %d';
$string['rule.minlength.minlength'] = 'Musíte zadat nejméně %d znaků';
$string['rule.minvalue.minvalue'] = 'Nemůžete zadat hodnotu menší než %d';
$string['rule.oneof.oneof'] = 'Jedno z polí v této skupině je nutné vyplnit.';
$string['rule.regex.regex'] = 'Neplatný formát';
$string['rule.required.required'] = 'Povinné pole';
$string['rule.safetext.invalidchars'] = 'Toto pole obsahuje neplatné znaky.';
$string['rule.validateoptions.validateoptions'] = 'Neplatný výběr "%s"';
$string['switchbox.false'] = 'Ne';
$string['switchbox.no'] = 'Ne';
$string['switchbox.off'] = 'Vypnuto';
$string['switchbox.on'] = 'Zapnuto';
$string['switchbox.true'] = 'Ano';
$string['switchbox.yes'] = 'Ano';
