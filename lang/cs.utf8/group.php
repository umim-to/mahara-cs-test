<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['About'] = 'Informace';
$string['Admin'] = 'Správce';
$string['Collections'] = 'Sbírky';
$string['Controlled'] = 'Řížené';
$string['Created'] = 'Vytvořeno';
$string['Everyone'] = 'Vše';
$string['Files'] = 'Soubory';
$string['Friends'] = 'Přátelé';
$string['Group'] = 'Skupina';
$string['Joined'] = 'Připojeni';
$string['Members'] = 'Členové';
$string['Membership'] = 'Členství';
$string['Open'] = 'Otevřené';
$string['Portfolios'] = 'Portfolia';
$string['Public'] = 'Veřejné';
$string['Recommendations'] = 'Doporučení';
$string['Reply'] = 'Odpovědět';
$string['Role'] = 'Role';
$string['Roles'] = 'Typ skupiny';
$string['Type'] = 'Typ';
$string['Views'] = 'Stránky';
$string['Viewscollections'] = 'Stránky a sbírky';
$string['aboutgroup'] = 'O %s';
$string['acceptfriendshiprequestfailed'] = 'Žádost o přátelství se nepodařilo přijmout.';
$string['acceptinvitegroup'] = 'Souhlasit';
$string['addedtofriendslistmessage'] = '%s vás přidal(a) mezi své přátele! To také znamená, že se %s bude zobrazovat na seznamu vašich přátel. Pomocí následujícího odkazu otevřete uživatelské profily vašich přátel.';
$string['addedtofriendslistsubject'] = 'Nový přítel';
$string['addedtogroupmessage'] = '%s vás přidal(a) do skupiny "%s". Pomocí následujícího odkazu otevřete stránku s informacemi o této skupině.';
$string['addedtogroupsmessage'] = '%s vás přidal do skupin(y): %s';
$string['addedtogroupsubject'] = 'Byli jste přidáni do skupiny';
$string['addedtongroupsmessage'] = array(

		0 => '%2$s vás přidal do skupiny: %3$s', 
		1 => '%2$s vás přidal do skupin: %3$s', 
		2 => '%2$s vás přidal do skupin: %3$s'
);
$string['addgrouplabel'] = 'Přiřadit skupině značky';
$string['addgrouplabeldescription'] = 'Přiřaďte této skupině jednu nebo více značek, které budou viditelné pouze vám. Přidáním značky si můžete uspořádat vaše skupiny podle svých potřeb. Také si můžete určit, které skupiny chcete zobrazovat v postranním panelu a na vaší profilové stránce.';
$string['addgrouplabelfilter'] = 'Přidat filtr značek skupin "%s"';
$string['addlabel'] = 'Přidat značku';
$string['addmembers'] = 'Přidat členy';
$string['addnewinteraction'] = 'Přidat %s';
$string['addtofriendsfailed'] = 'Přidání %s do vašeho seznamu přátel selhalo.';
$string['addtofriendslist'] = 'Přidat mezi přátele';
$string['addtomyfriends'] = 'Přidej se mezi mé přátele!';
$string['adduserfailed'] = 'Přidání uživatele selhalo';
$string['addusertogroup'] = 'Přidat do';
$string['agrouplabeltoolong'] = 'Jedna nebo více značek skupiny jsou moc dlouhé, mohou mít maximálně %s znaků.';
$string['agrouplabeltooshort'] = 'Jedna nebo více značek skupiny jsou příliš krátké, musí mít minimálně %s znaků.';
$string['allcategories'] = 'Všechny kategorie';
$string['allexceptmember'] = 'Všichni kromě řadových členů';
$string['allfriends'] = 'Všichni přátelé';
$string['allgroupmembers'] = 'Všichni členové skupiny';
$string['allgroups'] = 'Všechny skupiny';
$string['allmygroups'] = 'Všechny mé skupiny';
$string['allowsarchives'] = 'Povolit archivaci příspěvků';
$string['allowsarchivesdescription'] = 'Stránky / sbírky jsou během zveřejnění archivovány jako komprimované Leap2A soubory.';
$string['allowsarchivesdescription1'] = 'Stránky / kolekce jsou během procesu jejich uvolňování archivovány jako ZIP soubory.';
$string['allowsarchiveserror'] = 'Můžete povolit archivaci pouze pokud jsou povoleny příspěvky.';
$string['allowsendnow'] = 'Okamžitě odesílat příspěvky z fóra';
$string['allowsendnowdescription'] = 'Zaškrtnutí umožní kterémukoli členovi skupiny okamžitě odesílat diskuzní příspěvky. Pokud není zaškrtnuto, mohou to dělat jen skupinoví správci, učitelé nebo moderátoři.';
$string['allowsendnowdescription1'] = 'Jakýkoli člen skupiny může zvolit okamžité odesílání příspěvků do diskuse. Pokud je tato volba nastavená na "Vypnuto", mohou to udělat jen správci, učitelé a moderátoři.';
$string['allowssubmissions'] = 'Povoluje příspěvky';
$string['allowssubmissionsdescription'] = 'Členové mohou zveřejnit stránku do této skupiny.';
$string['allowssubmissionsdescription1'] = 'Členové mohou poslat stránky do skupiny, čímž se zamknou. Tyto stránky pak nelze upravovat, dokud nebudou uvolněny učitelem skupiny nebo správcem.';
$string['allowsubmissions'] = 'Povolit příspěvky';
$string['alreadyfriends'] = 'Již jste přátelé s uživatelem %s.';
$string['approve'] = 'Schválit';
$string['approverequest'] = 'Schválit požadavek';
$string['archives'] = 'Archiv';
$string['associatewithaninstitution'] = 'Přidružit skupinu  \'%s\' k instituci.';
$string['associatewithinstitution'] = 'Přidružit k instituci';
$string['backtofriendslist'] = 'Zpět na seznam přátel';
$string['cannotinvitetogroup'] = 'Tohot uživatele nelze do dané skupiny přidat.';
$string['cannotrequestfriendshipwithself'] = 'Nemůžete požádat sebe sama o přátelství.';
$string['cannotrequestjoingroup'] = 'Nemůžete požádat o členství v této skupině.';
$string['cantdeletegroup'] = 'Nemůžete odstranit tuto skupinu.';
$string['cantdenyrequest'] = 'Neplatná nabídka přátelství';
$string['canteditdontown'] = 'Nejte vlastníkem této skupiny, takže ji nemůžete upravovat.';
$string['cantleavegroup'] = 'Nemůžete opustit tuto skupinu.';
$string['cantmessageuser'] = 'Tomuto uživateli nemůžete zasílat zprávy';
$string['cantmessageuserdeleted'] = 'Tomuto uživateli nelze zaslat zprávu, protože jeho účet byl smazán.';
$string['cantremovefriend'] = 'Tohoto uživatele nemůžete odebrat ze seznamu svých přátel.';
$string['cantremovemember'] = 'Učitel nemůže odebírat členy.';
$string['cantremovememberfromgroup'] = 'Nemůžete odebrat uživatele z %s.';
$string['cantremoveuserisadmin'] = 'Učitel nemůže odebírat správce ani ostatní tutory.';
$string['cantrequestfriendship'] = 'Tohoto uživatele nemůžete požádat o přátelství.';
$string['cantviewmessage'] = 'Tuto zprávu nemůžete zobrazit.';
$string['categoryunassigned'] = 'Kategorie nepřiřazena';
$string['changedgroupmembership'] = 'Členství ve skupinách bylo úspěšně aktualizováno.';
$string['changedgroupmembershipsubject'] = 'Vaše členství ve skupinách bylo změněno';
$string['changerole'] = 'Změnit roli';
$string['changerolefromto'] = 'Změnit roli z %s na';
$string['changeroleofuseringroup'] = 'Změnit roli uživatele %s ve skupině %s';
$string['changerolepermissions'] = 'Nastavit roli %s pro %s';
$string['collectionreleasedmessage'] = 'Vaše sbírka "%s" byla zveřejněna od %s jako %s';
$string['collectionreleasedmessage1'] = 'Vaše sbírka "%s" byla zveřejněna v rámci skupiny "%s" členem %s.';
$string['collectionreleasedpending'] = 'Sbírky budou zveřejněny po archivaci';
$string['collectionreleasedsubject'] = 'Vaše sbírka "%s" byla zveřejněna od %s jako %s';
$string['collectionreleasedsubject1'] = 'Vaše sbírka "%s" byla zveřejněna v rámci skupiny "%s" členem %s.';
$string['collectionreleasedsuccess'] = 'Sbírka byla úspěšně zveřejněna';
$string['commentnotify'] = 'Oznámení o komentáři';
$string['commentnotifydescription1'] = 'Vyberte, kteří členové skupiny by měli obdržet upozornění, když jsou komentovány skupinové stránky a položky portfolia.';
$string['confirmremovefriend'] = 'Opravdu chcete';
$string['controlleddescription'] = 'Správci skupin mohou přidat uživatele do skupiny bez jejich souhlasu a ti nemohou danou skupinu opustit.';
$string['copygroup'] = 'Kopírovat skupinu "%s"';
$string['couldnotjoingroup'] = 'Nemůžete se přidat k této skupině.';
$string['couldnotleavegroup'] = 'Nemůžete opustit tuto skupinu.';
$string['couldnotrequestgroup'] = 'Nelze zaslat žádost o vstup do skupiny';
$string['creategroup'] = 'Vytvořit skupinu';
$string['current'] = 'Stávající';
$string['currentarchivereleasedsubmittedhostmessage'] = 'Vaše portfolio "%s" bylo uvolněno z "%s" od "%s". Své portfolio můžete odevzdat znovu, pokud je třeba.';
$string['currentfriends'] = 'Stávající přátelé';
$string['currentrole'] = 'Stávající role';
$string['declineinvitegroup'] = 'Zamítnout';
$string['declinerequest'] = 'Zamítnout žádost';
$string['declinerequestsuccess'] = 'Členství ve skupině bylo úspěšně odmítnuto.';
$string['deletegroup'] = 'Skupina úspěšně odstraněna';
$string['deletegroup1'] = 'Odstranit skupinu';
$string['deletegroupnotificationmessage'] = 'Byli jste členy skupiny %s na %s. Tato skupina byla odstraněna.';
$string['deletegroupnotificationsubject'] = 'Skupina "%s" byla odstraněna';
$string['deleteinteraction'] = 'Odstranit %s "%s"';
$string['deleteinteractionsure'] = 'Jste si tím opravdu jisti? Tato akce nemůže být vrácena zpět.';
$string['deletespecifiedgroup'] = 'Odstranit skupinu "%s"';
$string['deny'] = 'Zamítnout';
$string['denyfriendrequest'] = 'Zamítnout požadavek na přátelství';
$string['denyfriendrequestlower'] = 'Zamítnout požadavek na přátelství';
$string['denyrequest'] = 'Zamítnout požadavek';
$string['displayonlylabels'] = 'Zobrazit pouze skupiny se značkou';
$string['editability'] = 'Úpravy';
$string['editable'] = 'Upravitelné';
$string['editgroup'] = 'Upravit skupinu';
$string['editgroupmembership'] = 'Upravit členství ve skupině';
$string['editmembershipforuser'] = 'Upravit členství uživatele %s';
$string['editroles'] = 'Vyvářet a upravovat stránky';
$string['editroles1'] = 'Vytváření a úpravy';
$string['editrolesdescription'] = 'Role s oprávněním vytvářet a upravovat stránky skupiny.';
$string['editrolesdescription1'] = 'Role s povolením vytvářet a upravovat skupinové stránky, deníky a soubory.';
$string['editrolesdescription2'] = 'Role s oprávněním vytvářet a upravovat obsah a zařazovat ho do portfolií skupiny.';
$string['editwindowbetween'] = 'Mezi %s a %s';
$string['editwindowendbeforestart'] = 'Datum konce musí být po datu začátku';
$string['editwindowfrom'] = 'Od %';
$string['editwindowuntil'] = 'Do %';
$string['exceedsgroupmax'] = 'Přidání tohoto množství skupin by překročilo limit počtu skupin pro vaší instituci. V rámci vašeho limitu můžete přidat ještě %s skupin. Zkuste přidat méně skupin nebo se spojte se správcem těchto webových stránek a proberte možnost navýšení vašeho limitu.';
$string['existingfriend'] = 'stávající přítel';
$string['extcommenters'] = 'Zapojení nečlenové';
$string['feedbacknotify'] = 'Upozornění na zpětnou vazbu';
$string['feedbacknotifydescription1'] = 'Zvolte, kteří členové skupiny mají dostávat upozornění, když je ke skupinové stránce nebo položce portfolia přidána zpětná vazba.';
$string['filterbygrouplabel'] = 'Filtrovat podle značek';
$string['findnewfriends'] = 'Vyhledat nové přátele';
$string['friend'] = 'přítel';
$string['friendformacceptsuccess'] = 'Nabídka přátelství přijata';
$string['friendformaddsuccess'] = '%s přidán na seznam vašich přátel';
$string['friendformrejectsuccess'] = 'Nabídka přátelství zamítnuta';
$string['friendformremovesuccess'] = '%s odebrán ze seznamu přátel';
$string['friendformrequestsuccess'] = 'Nabídka přátelství uživateli %s zaslána';
$string['friendinvitations'] = 'Nabídky přátelství';
$string['friendlistfailure'] = 'Nelze upravit váš seznam přátel';
$string['friendrequestacceptedmessage'] = 'Uživatel %s přijal vaši nabídku přátelství a byl přidán do seznamu vašich přátel';
$string['friendrequestacceptedsubject'] = 'Nabídka přátelství přijata';
$string['friendrequestrejectedmessage'] = 'Uživatel %s zamítnul vaši nabídku přátelství';
$string['friendrequestrejectedmessagereason'] = 'Uživatel %s zamítnul vaši nabídku přátelství s následujícím odůvodněním:';
$string['friendrequestrejectedsubject'] = 'Nabídka přátelství zamítnuta';
$string['friendrequests'] = 'Nabídky přátelství';
$string['friends'] = 'přátelé';
$string['friendshipalreadyrequested'] = 'Uživateli %s jste již zaslali nabídku přátelství.';
$string['friendshipalreadyrequestedowner'] = 'Uživatel %s vám již zaslal nabídku přátelství.';
$string['friendshiprequested'] = 'Nabídka přátelství!';
$string['group'] = 'skupina';
$string['groupadmins'] = 'Správci skupiny';
$string['groupalreadyexists'] = 'Skupina tohoto jména již existuje';
$string['groupalreadyexistssuggest'] = 'Skupina s takovým jménem již existuje. Je dostupné jméno "%s".';
$string['grouparchivereports'] = 'Přístup k archivu odevzdaných příspěvků';
$string['grouparchivereportsdesc'] = 'Správci skupiny mohou přistupovat k archovovaným odevzdaným souborům.';
$string['grouparchivereportserror'] = 'Povolit archivování hlášení o odevzdaných příspěvcích můžete pouze pokud jsou povoleny odevzdávané příspěvky.';
$string['grouparchivereportsheading'] = 'Archiv odevzdaných příspěvků';
$string['groupassociated'] = 'Skupina byla přidružena úspěšně přidružena k instituci';
$string['groupcategory'] = 'Kategorie skupiny';
$string['groupconfirmdelete'] = 'Jste si jisti, že chcete odstranit tuto skupinu?';
$string['groupconfirmleave'] = 'Jste si jisti, že chcete opustit tuto skupinu?';
$string['groupcreatedviewsscrolled'] = 'Byl překročen konec výpisu stránek skupiny.';
$string['groupdescription'] = 'Popis skupiny';
$string['grouphasntcreatedanyviewsyet'] = 'Tato skupina zatím nevytvořila žádné stránky.';
$string['grouphaveinvite'] = 'Bylo vám nabídnuto členství v této skupině.';
$string['grouphaveinvitewithrole'] = 'Bylo vám nabídnuto členství v této skupině v roli';
$string['groupinteractions'] = 'Činnost skupiny';
$string['groupinviteaccepted'] = 'Pozvání úspěšně přijato! Stali jste se členy skupiny.';
$string['groupinvitedeclined'] = 'Pozvání úspěšně zamítnuto!';
$string['groupinvitesfrom'] = 'Nabídku ke vstupu do skupin:';
$string['groupjointypecontrolled'] = 'Členství v této skupině je řízeno. Nemůžete se stát členy této skupiny.';
$string['groupjointypeinvite'] = 'Členy této skupiny se můžete stát jen na základě jejich pozvání.';
$string['groupjointypeopen'] = 'Členství v této skupině je otevřené. Přidejte se!';
$string['groupjointyperequest'] = 'O členství v této skupině musíte požádat.';
$string['grouplabeladded'] = 'Značka skupiny přidána';
$string['grouplabelnotmember'] = 'Aktuálně nejste členem této skupiny. Prosím, načtěte stránku znovu.';
$string['grouplabelupdated'] = 'Značka skupiny upravena';
$string['groupmaxreached'] = 'K této instituci nemohou být přidány skupiny, protože bylo dosaženo maximálního počtu skupin povolených pro tuto instituci. Prosím, spojte se se <a href="%sinstitution/index.php?institution=%s">správcem instituce</a>  pro navýšení tohoto limitu.';
$string['groupmaxreachednolink'] = 'K této instituci nemohou být přidány skupiny, protože bylo dosaženo maximálního počtu skupin povolených pro tuto instituci. Prosím, spojte se se správcem instituce pro navýšení tohoto limitu.';
$string['groupmemberrequests'] = 'Nevyřízené žádosti o členství';
$string['groupmembershipchangedmessageaddedmember'] = 'Stali jste se členy této skupiny';
$string['groupmembershipchangedmessageaddedtutor'] = 'Stali jste se učitelem v této skupině';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Vaše žádost o vstup do této skupiny byla zamítnuta';
$string['groupmembershipchangedmessagemember'] = 'Přestali jste být učiteli v této skupině';
$string['groupmembershipchangedmessageremove'] = 'Byli jste vyřazeni z této skupiny';
$string['groupmembershipchangedmessagetutor'] = 'Stali jste se učiteli v této skupině';
$string['groupmembershipchangesubject'] = 'Členství ve skupině: %s';
$string['groupname'] = 'Název skupiny';
$string['groupnotfound'] = 'Skupina s identifikátorem %s nebyla nalezena';
$string['groupnotfoundname'] = 'Skupina %s nebyla nalezena';
$string['groupnotinvited'] = 'Nebyli jste pozváni do této skupiny';
$string['groupnovalidlabelsupplied'] = 'Značka musí sestávat z alespoň dvou znaků.';
$string['groupoptionsset'] = 'Nastavení možností skupin bylo aktualizováno.';
$string['groupparticipationreports'] = 'Hlášení o spolupráci';
$string['groupparticipationreportsdesc'] = 'Pokud je zaškrtnuto, správce skupiny si může prohlédnout záznamy všech skupin a sdílených stránek včetně informace, kdo na nich zanechal komentář.';
$string['groupparticipationreportsdesc1'] = 'Správci skupiny mají přístup ke shrnutí všech skupinových a sdílených stránek a toho, kdo je komentoval.';
$string['grouprequestmessage'] = 'Uživatel %s by se rád stal členem vaší skupiny %s';
$string['grouprequestmessagereason'] = 'Uživatel %s by se rád stal členem vaší skupiny %s s následujícím odůvodněním: %s';
$string['grouprequestsent'] = 'Žádost o členství ve skupině byla odeslána';
$string['grouprequestsubject'] = 'Nová žádost o členství ve skupině';
$string['groups'] = 'skupiny';
$string['groupsaved'] = 'Skupina úspěšně uložena';
$string['groupsharedviewsscrolled'] = 'Překročili jste konec sdíleného seznamu stránek.';
$string['groupshortname'] = 'Krátký název';
$string['groupshortnamealreadyexists'] = 'Skupina s tímto krátkým jménem již existuje.';
$string['groupsicanjoin'] = 'Skupiny, do kterých se mohu připojit';
$string['groupsimin'] = 'Skupiny, v nichž jsem členem';
$string['groupsiminvitedto'] = 'Skupiny, do nichž jsem pozván';
$string['groupsiown'] = 'Skupiny, které vlastním';
$string['groupsiwanttojoin'] = 'Skupiny, do nichž chci vstoupit';
$string['groupsnotin'] = 'Skupiny, v nichž nejsem';
$string['grouptype'] = 'Typ skupiny';
$string['groupurl'] = 'URL domovské stránky skupiny';
$string['groupurldescription'] = 'URL domovské stránky vaší skupiny. Pole musí mít 3 - 30 znaků.';
$string['groupurltaken'] = 'Tato adresa je již používána jinou skupinou.';
$string['hasbeeninvitedtojoin'] = 'byl pozván do této skupiny';
$string['hasrequestedmembership'] = 'požádal o členství v této skupině';
$string['hiddengroup'] = 'Skrýt skupinu';
$string['hiddengroupdescription'] = 'Nezobrazovat tuto skupinu na stránce "Najít skupiny".';
$string['hiddengroupdescription1'] = 'Skrýt tuto skupinu ze stránky "Vyhledat skupiny"';
$string['hiddengroupdescription2'] = 'Skrýt tuto skupinu na stránce skupin';
$string['hidegroupmembers'] = 'Skrýt členy';
$string['hidemembers'] = 'Skrýt členství';
$string['hidemembersdescription'] = 'Skrýt členství ve skupinách zobrazované nečlenům.';
$string['hidemembersfrommembers'] = 'Skrýt členství členů';
$string['hidemembersfrommembersdescription'] = 'Členové nejsou zobrazeni. Členství se zobrazí pouze správcům skupiny. Správci jsou uvedeni na domovské stránce skupiny.';
$string['hidemembersfrommembersdescription1'] = 'Skrýt členy skupiny. Jen správci budou mít možnost zobrazit kompletní seznam členů. Správci skupiny budou stále zobrazeni všem.';
$string['hideonlygrouptutors'] = 'Skrýt vyučující';
$string['interactiondeleted'] = '%s úspěšně odstraněna';
$string['interactionsaved'] = '%s úspěšně uložena';
$string['invalidgroup'] = 'Taková skupina neexistuje';
$string['invalidshortname'] = 'Neplatný krátký název skupiny';
$string['invitationssent'] = 'Odeslána pozvánka uživateli %d';
$string['invite'] = 'Pozvat';
$string['invitefriends'] = 'Pozvat přátele';
$string['invitefriendsdescription'] = 'Pokud je toto pole zatrženo, členové mají povoleno pozvat přátele, aby se připojili ke skupině. Bez ohledu na toto nastavení, správci skupiny mohou vždy zaslat pozvání komukoliv.';
$string['invitefriendsdescription1'] = 'Povolit členům pozvat své přátele do skupiny. Správci skupiny mohou pozvánky rozesílat nezávisle na tomto nastavení.';
$string['invitemembersdescription'] = 'Uživatele do skupiny můžete pozvat prostřednictvím jejich profilových stránek nebo <a href="%s">zaslat více pozvánek najednou</a>.';
$string['invitemembertogroup'] = 'Pozvat uživatele %s do skupiny "%s"';
$string['invites'] = 'Pozvaní';
$string['invitetogroupmessage'] = 'Uživatel %s vám nabízí členství ve skupině "%s". Klikněte na následují odkaz pro více informací.';
$string['invitetogroupsubject'] = 'Byli jste pozváni do skupiny';
$string['inviteuserfailed'] = 'Nelze pozvat tohoto uživatele';
$string['inviteusertojoingroup'] = 'Nabídnout členství ve skupině';
$string['joinedgroup'] = 'Stali jste se členy skupiny';
$string['joingroup'] = 'Přidat se k této skupině';
$string['label'] = 'Značky';
$string['labelfor'] = 'Přiřadit značku skupině "%s"';
$string['leavegroup'] = 'Opustit tuto skupinu';
$string['leavespecifiedgroup'] = 'Opustit skupinu "%s"';
$string['leftgroup'] = 'Opustili jste tuto skupinu';
$string['leftgroupfailed'] = 'Nelze opustit skupinu';
$string['member'] = 'člen';
$string['memberchangefailed'] = 'Nelze aktualizovat některé informace o členství';
$string['memberchangesuccess'] = 'Stav členství úspěšně změněn';
$string['membercommenters'] = 'Zapojení členové';
$string['memberrequests'] = 'Žádost o členství';
$string['members'] = 'členové';
$string['membersdescription:controlled'] = 'Toto je skupina s říženým členstvím. Můžete přidat uživatele prostřednictvím jeho stránky s profilem nebo <a href="%s">přidat více uživatelů najednou</a>.';
$string['membersdescription:invite'] = 'Tato skupina je pouze na pozvání. Musíte pozvat uživatele prostřednictvím jeho stránky s profilem nebo <a href="%s">zaslat skupinovou pozvánku</a> pro více uživatelů.';
$string['membershipbyinvitationonly'] = 'Členství v této skupině je možné pouze na základě pozvání.';
$string['membershipcontrolled'] = 'Členství v této skupině je řízené.';
$string['membershipopencontrolled'] = 'Členství nemůže být zároveň otevřené a řízené.';
$string['membershipopenrequest'] = 'Skupiny s otevřeným členstvím nepřijímají žádosti o členství.';
$string['membershiprequests'] = 'Žádost o členství';
$string['membershiptype'] = 'Typ členství';
$string['membershiptype.abbrev.approve'] = 'Obvyklá';
$string['membershiptype.abbrev.controlled'] = 'Řízená';
$string['membershiptype.abbrev.invite'] = 'Pozvání';
$string['membershiptype.abbrev.open'] = 'Otevřená';
$string['membershiptype.abbrev.request'] = 'Žádost';
$string['membershiptype.approve'] = 'Schvalované členství';
$string['membershiptype.controlled'] = 'Řízené členství';
$string['membershiptype.invite'] = 'Pouze na pozvání';
$string['membershiptype.open'] = 'Otevřené členství';
$string['membershiptype.request'] = 'Pouze na požádání';
$string['memberslist'] = 'Seznam členů:';
$string['messagebody'] = 'Zaslat zprávu';
$string['messagenotsent'] = 'Nelze zaslat zprávu';
$string['messagesent'] = 'Zpráva odeslána!';
$string['moregroups'] = 'Více skupin';
$string['mygrouplabel'] = 'Mé značky skupiny';
$string['myinstitutions'] = 'Moje instituce';
$string['newmembersadded'] = 'Přidat %d nových členů';
$string['newusermessage'] = 'Nová zpráva od uživatele %s';
$string['newusermessageemailbody'] = '%s vám zasla novou zprávu. Pro zobrazení zprávy navštivte %s';
$string['nfriends'] = array(

		0 => '%s přítel', 
		1 => '%s přátelé', 
		2 => '% přátel'
);
$string['ngroups'] = array(

		0 => '%s skupina', 
		1 => '%s skupiny', 
		2 => '%s skupin'
);
$string['nmembers'] = array(

		0 => 'Jeden člen', 
		1 => '%s členové',
		2 => '%s členů'
);
$string['nmembers1'] = array(

		0 => '% člen', 
		1 => '%s členové', 
		2 => '%s členů'
);
$string['nnonmembers'] = array(

		0 => 'Jeden nečlen', 
		1 => '%s nečlenové', 
		2 => '%s nečlenů'
);
$string['nobodyawaitsfriendapproval'] = 'Žádná čekající nabídka přátelství';
$string['nocategoryselected'] = 'Žádná kategorie nebyla vybrána';
$string['nogroups'] = 'Žádné skupiny';
$string['nogroupsfound'] = 'Nebyly nalezeny žádné skupiny';
$string['nointeractions'] = 'Žádné činnosti v této skupině';
$string['nosearchresultsfound'] = 'Žádné výsledky vyhledávání';
$string['notallowedtodeleteinteractions'] = 'Nemáte oprávnění odstraňovat činnosti v této skupině';
$string['notallowedtoeditinteractions'] = 'Nemáte oprávnění přidávat nebo upravovat činnosti v této skupině';
$string['notamember'] = 'Nejste členem této skupiny';
$string['notcategorised'] = 'Nezařazena do kategorie';
$string['notinanygroups'] = 'Není v žádných skupinách';
$string['notmembermayjoin'] = 'Pro zobrazení této stránky se musíte stát členem skupiny "%s".';
$string['notpublic'] = 'Tato skupina není veřejná.';
$string['noviewssharedwithgroupyet'] = 'S touto skupinou nejsou prozatím sdílené žádné stránky.';
$string['noviewstosee'] = 'Žádné dostupné';
$string['nrecommendationssent'] = array(

		0 => '1 doporučení odesláno', 
		1 => '%d doporučení odeslány', 
		2 => '%d doporučení odesláno'
);
$string['nusers'] = array(

		0 => '%s člověk', 
		1 => '%s lidé', 
		2 => '%s lidí'
);
$string['opendescription'] = 'Uživatelé se mohou stát členy skupiny bez souhlasu jejího správce.';
$string['pending'] = 'čeká';
$string['pendingfriend'] = 'Čekající přítel';
$string['pendingfriends'] = 'Čekající přátelé';
$string['pendingmembers'] = 'Čekající členové';
$string['pendingsince'] = 'čekající od %s';
$string['portfolioreleasedmessage'] = 'Vaše portfolio "%s" bylo uvolněno z "%s" od "%s".';
$string['portfolioreleasedpending'] = 'Portfolio bude uvolněno po archivaci';
$string['portfolioreleasedsubject'] = 'Portfolio "%s" uvolněno';
$string['portfolioreleasedsuccess'] = 'Portfolio bylo úspěšně uvolněno';
$string['portfolioreleasedsuccesswithname'] = 'Portfolio "%s" bylo úspěšně uvolněno';
$string['portfolioreleasefailed'] = 'Uvolnění "%s" po archivaci selhalo';
$string['potentialmembers'] = 'Možní členové';
$string['publiclyviewablegroup'] = 'Veřejně viditelná skupina?';
$string['publiclyviewablegroupdescription'] = 'Povolit komukoliv (včetně návštěvníků, kteří na těchto stránkách nemají účet) vidět tuto skupinu a její diskusní fóra?';
$string['publiclyviewablegroupdescription1'] = 'Umožnit všem online zobrazit tuto skupinu včetně diskusí.';
$string['publiclyvisible'] = 'Veřejně viditelná';
$string['reason'] = 'Odůvodnění';
$string['reasonoptional'] = 'Odůvodnění (volitelné)';
$string['recommendationssent'] = '% doporučení zasláno';
$string['reject'] = 'Zamítnout';
$string['rejectfriendshipreason'] = 'Důvod pro zamítnutí nabídky';
$string['releasecollection'] = 'Uvolnit sbírku';
$string['releaseview'] = 'Uvolnit stránku';
$string['remove'] = 'Odebrat';
$string['removedfromfriendslistmessage'] = 'Uživatel %s vás odebral ze seznamu svých přátel.';
$string['removedfromfriendslistmessagereason'] = 'Uživatel %s vás odebral ze seznamu svých přátel s odůvodněním:';
$string['removedfromfriendslistsubject'] = 'Odebrání ze seznamu přátel';
$string['removedfromgroupsmessage'] = 'Uživatel %s vás odstranil ze skupin(y): %s';
$string['removedfromngroupsmessage'] = array(

		0 => '%2$s vás odebral ze skupiny: %3$s', 
		1 => '%2$s vás odebral ze skupin: %3$s', 
		2 => '%2$s vás odebral ze skupin: %3$s'
);
$string['removefriend'] = 'Odebrat přítele';
$string['removefromfriends'] = 'Odebrat uživatele %s ze seznamu přátel';
$string['removefromfriendslist'] = 'Odebrat ze seznamu přátel';
$string['removefromgroup'] = 'Odebrat ze skupiny';
$string['removegrouplabelfilter'] = 'Odstranit filtr značek skupin "%s"';
$string['report'] = 'Hlášení';
$string['request'] = 'Na žádost';
$string['requestdescription'] = 'Uživatelé mohou žádat o členství ve skupině bez souhlasu jejího správce.';
$string['requestedfriendlistinboxmessage'] = '%s vás požádal o přidání do přátel. To můžete udělat buď pomocí kliknutí na následující link, nebo na stránce vašeho seznamu přátel.';
$string['requestedfriendlistmessage'] = 'Uživatel %s vám nabízí přátelství. Nabídku můžete přijmout buď pomocí následujícího odkazu nebo na stránce vašich přátel';
$string['requestedfriendlistmessageexplanation'] = '%s vás požádal o přidání do přátel. To můžete udělat buď pomocí kliknutí na následující link, nebo na stránce vašeho seznamu přátel. Jako úvod uvedl:';
$string['requestedfriendlistmessagereason'] = 'Uživatel %s vám nabízí přátelství. Nabídku můžete přijmout buď pomocí následujícího odkazu nebo na stránce vašich přátel. Uvedené odůvodnění přátelství je:';
$string['requestedfriendlistsubject'] = 'Nová nabídka přátelství';
$string['requestedfriendship'] = 'nabízené přátelství';
$string['requestedmembershipin'] = 'Požadované člentví v:';
$string['requestedsince'] = 'požadováno od %s';
$string['requestedtojoin'] = 'Požádali jste o členství v této skupině';
$string['requestfriendship'] = 'Nabídnout přátelství';
$string['requestjoingroup'] = 'Žádost o vstup do této skupiny';
$string['requestjoinspecifiedgroup'] = 'Žádost o vstup do skupiny "%s"';
$string['requestmembership'] = 'Žádost o členství';
$string['requests'] = 'Požadavky';
$string['returntogrouppages'] = 'Vrátit se na skupinové stránky';
$string['returntogroupportfolios'] = 'Vrátit se ke stránkám a sbírkám skupiny';
$string['rolechanged'] = 'Role změněna';
$string['savegroup'] = 'Uložit skupinu';
$string['seeallviews'] = 'Zobrazit všechny stránky (celkem %s)';
$string['sendfriendrequest'] = 'Zaslat nabídku přátelství';
$string['sendfriendshiprequest'] = 'Zaslat uživateli %s nabídku přátelství';
$string['sendinvitation'] = 'Zaslat pozvání';
$string['sendinvitations'] = 'Zaslat pozvánky';
$string['sendmessage'] = 'Zaslat zprávu';
$string['sendmessageto'] = 'Zaslat zprávu uživateli %s';
$string['shortnameformat'] = 'Krátké názvy skupin mohou být 2 až 255 znaků a obsahovat pouze alfanumerické znaky a znaky ".", "-", a "_"';
$string['shortnameformat1'] = 'Krátká skupinová jména mohou být od 2 do 255 znaků dlouhé a obsahovat pouze malá písmena a čísla, ".", "-", a "_".';
$string['showintroduction'] = 'Představení';
$string['submit'] = 'Odeslat';
$string['submitted'] = 'Odesláno';
$string['submittedviews'] = 'Odevzdané stránky';
$string['suggestfriendsdescription'] = 'Pokud je toto pole zaškrtnuto, mohou členové snadno zaslat doporučení pro vstup do této skupiny svým přátelům prostřednictvím tlačítka umístěného na domovské stránce skupiny.';
$string['suggestfriendsdescription1'] = 'Povolit členům skupiny zasílání doporučení k přidání se do skupiny svým přátelům. Toto doporučení se posílá pomocí tlačítka na domovské stránce skupiny.';
$string['suggestfriendsrequesterror'] = 'Můžete povolit doporučování přátelům pouze v otevřených skupinách nebo ve skupinách na žádost.';
$string['suggestgroupnotificationmessage'] = '%s vám navrhl vstoupit do skupiny "%s" na %s';
$string['suggestgroupnotificationsubject'] = '%s vám navrhl vstoupit do skupiny';
$string['suggestinvitefriends'] = 'Nemůžete povolit zároveň pozvání a doporučování přátelům.';
$string['suggesttofriends'] = 'Doporučit přátelům';
$string['title'] = 'Titulek';
$string['trysearchingforfriends'] = 'Zkuste %snajít nové přátele%s a rozšiřte svoji síť!';
$string['trysearchingforgroups'] = 'Zkuste %snajít skupiny%s, do nichž byste mohli vstoupit!';
$string['trysearchingforgroups1'] = 'Vyzkoušejte <a href="%sgroup/index.php?filter=canjoin">hledání skupin</a>, ke kterým se lze připojit.';
$string['updatemembership'] = 'Aktualizovat členství';
$string['user'] = 'uživatel';
$string['useradded'] = 'Uživatel přidán';
$string['useralreadyinvitedtogroup'] = 'Tento uživatel byl již do této skupiny pozván, nebo už je jejím členem.';
$string['usercannotchangetothisrole'] = 'Uživateli nelze přiřadit tuto roli.';
$string['usercantleavegroup'] = 'Tento uživatel nemůže opustit tuto skupinu.';
$string['userdoesntwantfriends'] = 'Tento uživatel nehledá nové přátele';
$string['userinvited'] = 'Pozvání odesláno';
$string['userremoved'] = 'Uživatel odstraněn';
$string['users'] = 'uživatelé';
$string['usersautoadded'] = 'Automaticky přidávat uživatele?';
$string['usersautoaddeddescription'] = 'Mají se všichni noví uživatelé automaticky stávat členy v této skupině?';
$string['usersautoaddeddescription1'] = 'Automaticky do skupiny přidávat všechny uživatele, kteří se registrují na stránky.';
$string['userstobeadded'] = 'Uživatelé byli přidáni';
$string['userstobeinvited'] = 'Uživatelé byli vybráni';
$string['userstosendrecommendationsto'] = 'Uživatelé, kterým budou zaslána doporučení';
$string['viewmessage'] = 'Zobrazit zprávu';
$string['viewnotify'] = 'Zobrazit oznámení';
$string['viewnotifydescription'] = 'Pokud je toto pole zatrženo, bude všem členům skupiny zasláno oznámení vždy, když některý člen skupiny začně sdílet jednu ze svých stránek s ostatními. Povolení tohoto nastavení ve velmi velkých skupinách může znamenat produkování velkého množství oznámení.';
$string['viewnotifydescription2'] = 'Zvolte, kteří členové skupiny mají dostávat upozornění na vytvoření skupinových stránek a stárnky sdílené do skupiny jejími členy. Členové skupiny, kteří tyto stránky sdíleli, upozornění neobdrží. Pro rozsáhlé skupiny je ideální vynechat z tohoto nastavení běžné členy, neboť může být zdrojem velkého množství upozornění.';
$string['viewnotifydescription3'] = 'Zvolte, kteří členové skupiny mají dostat upozornění, když je vytvořeno nové portfolio skupiny nebo když člen skupiny sdílí některé ze svých portfolií skupině. Člen skupiny sdílející dané portfolio upozornění neobdrží. U velkých skupin by bylo vhodné omezit tato upozornění jen pro jiné než běžné členy, protože toto nastavení u nich může zapříčinit příliš velké množství upozornění.';
$string['viewreleasedmessage'] = 'Stránka, kterou jste odevzdal(a) skupině %s vám byla uvolněna uživatelem %s';
$string['viewreleasedmessage1'] = 'Vaše stránka "%s" byla zveřejněna v rámci skupiny "%s" členem %s.';
$string['viewreleasedpending'] = 'Stránka bude zveřejněna po archivaci';
$string['viewreleasedsubject'] = 'Vaše stránka byla uvolněna';
$string['viewreleasedsubject1'] = 'Vaše stránka "%s" byla zveřejněna v rámci skupiny "%s" členem %s.';
$string['viewreleasedsuccess'] = 'Stránka úspěšně zveřejněna';
$string['whymakemeyourfriend'] = 'Toto je důvod, proč bychom se měli stát přáteli:';
$string['windowdatedescriptionadmin'] = 'Nastavte toto datum pouze v případě, že je vyžadováno a hromadně přidáváte skupiny. Nezapomeňte toto pole vymazat, jakmile budete hotovi.';
$string['windowend'] = 'Datum konce';
$string['windowenddesc'] = 'Skupina nemůže být upravována po tomto datu';
$string['windowenddescription'] = 'Po tomto datu nemůže být skupina upravována běžnými členy skupiny. Toto datum bude zároveň použito jako výchozí datum dokončení pro všechny importované plány.';
$string['windowstart'] = 'Datum začátku';
$string['windowstartdesc'] = 'Skupina nemůže být upravována před tímto datem';
$string['windowstartdescription'] = 'Před tímto datem nemůže být skupina upravována běžnými členy skupiny. Toto datum bude zároveň použito jako výchozí datum začátku pro všechny importované plány.';
$string['youaregroupadmin'] = 'Jste správce této skupiny';
$string['youaregroupmember'] = 'Jste členy této skupiny';
$string['youaregrouptutor'] = 'Jste tutor této skupiny';
$string['youowngroup'] = 'Vlastníte tuto skupinu.';
