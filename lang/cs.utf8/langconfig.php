<?php

defined('INTERNAL') || die();  

$string['locales'] = 'cs_CZ.utf8';
$string['strfdaymonthyearshort'] = '%%d.%%m.%%Y';
$string['strftimedate'] = '%%e %%B %%Y';
$string['strftimedateshort'] = '%%d %%B';
$string['strftimedatetime'] = '%%d %%B %%Y, %%l:%%M %%p';
$string['strftimedatetimeshort'] = '%%Y/%%m/%%d %%H:%%M';
$string['strftimedaydate'] = '%%A, %%d %%B %%Y';
$string['strftimedaydatetime'] = '%%A, %%d %%B %%Y, %%l:%%M %%p';
$string['strftimedayshort'] = '%%A, %%d %%B';
$string['strftimedaytime'] = '%%a, %%k:%%M';
$string['strftimemonthyear'] = '%%B %%Y';
$string['strftimerecent'] = '%%d %%b, %%k:%%M';
$string['strftimerecentfull'] = '%%a, %%d %%b %%Y, %%l:%%M %%p';
$string['strftimetime'] = '%%l:%%M %%p';
$string['strftimew3cdate'] = '%%Y-%%m-%%d';
$string['strftimew3cdatetime'] = '%%Y-%%m-%%dT%%T%%z';
$string['thislanguage'] = 'Čeština';

