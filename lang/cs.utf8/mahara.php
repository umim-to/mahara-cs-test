<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Vše';
$string['Allinstitutions'] = 'Všechny instituce';
$string['Artefact'] = 'Položka';
$string['Artefacts'] = 'Položky';
$string['Close'] = 'Zavřít';
$string['Content'] = 'Obsah';
$string['Copyof'] = 'Kopie - %s';
$string['Create'] = 'Vytvořit';
$string['Created'] = 'Vytvořeno';
$string['Details'] = 'Podrobnosti';
$string['Engage'] = 'Skupiny';
$string['Failed'] = 'Selhalo';
$string['From'] = 'Od';
$string['Help'] = 'Nápověda';
$string['Hide'] = 'Schovat';
$string['Hide2'] = 'Skrýt blok s informacemi';
$string['Invitations'] = 'Pozvánky';
$string['Manage'] = 'Spravovat';
$string['Memberships'] = 'Členství';
$string['Organise'] = 'Organizovat';
$string['Permissions'] = 'Oprávnění';
$string['Query'] = 'Dotaz';
$string['Requests'] = 'Požadavky';
$string['Results'] = 'Výsledky';
$string['Site'] = 'Stránky';
$string['Tag'] = 'Štítek';
$string['Title'] = 'Titulek';
$string['To'] = 'Do';
$string['Total'] = 'Celkem';
$string['Updated'] = 'Aktualizováno';
$string['Visibility'] = 'Viditelnost';
$string['Visits'] = 'Navštíveno';
$string['about'] = 'O těchto stránkách';
$string['acccountaddappsdescription'] = 'Aktuálně nejsou aktivní žádné pluginy, které umožňují správu tokenů.';
$string['acccountappsdescription'] = 'Zde můžete spravovat všechny aplikace, které vyžadují pro svůj přístup token.';
$string['acccountchooseappsdescription'] = 'Pro správu tokenů vaší aplikace, prosím, zvolte příslušnou aplikaci z panelu "Připojené aplikace".';
$string['accept'] = 'Souhlasit';
$string['accessforbiddentoadminsection'] = 'Nemáte přístup k administraci stránek';
$string['accesstotallydenied_institutionexpired'] = 'Platnost instituce %s vypršela. Dokud nebude obnovena, nebudete se moci do %s přihlásit. Prosím, kontaktujte svou instituci pro další pomoc.';
$string['accesstotallydenied_institutioninactive'] = 'Autentizační metoda pro instituci "%s" není aktivní. Dokud nebude zaktivována, nebude možné se přihlásit do %s. Prosím, kontaktujte administrátora vaší instituce s žádostí o pomoc.';
$string['accesstotallydenied_institutionsuspended'] = 'Vaše instituce %s byla pozastavena. Dokud nebude její činnost znovu obnovena, není možné se k %s přihlásit. Prosím kontaktujte svou instituci pro další pomoc.';
$string['account'] = 'Můj účet';
$string['accountcreated'] = '%s: Nový účet';
$string['accountcreatedchangepasswordhtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Na stránkách <a href="%s">%s</a> byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:</p>
<ul>
<li><strong>Uživatelské jméno:</strong> %s</li>
<li><strong>Heslo:</strong> %s</li>
</ul>
<p>Poté, co se poprvé přihlásíte, budete vyzváni ke změně tohoto hesla.</p>

<p>Navštivte <a href="%s">%s</a> a začněte budovat své e-portfolio!</p>

<p>S pozdravem, správce stránek %s</p>';
$string['accountcreatedchangepasswordtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Na stránkách %s byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:

Uživatelské jméno: %s
Heslo: %s

Poté, co se poprvé přihlásíte, budete vyzváni ke změně tohoto hesla.
Navštivte nyní %s a začněte budovat své e-portfolio!

S pozdravem, správce stránek %s';
$string['accountcreatedhtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Na stránkách <a href="%s">%s</a> byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:</p>
<ul>
<li><strong>Uživatelské jméno:</strong> %s</li>
<li><strong>Heslo:</strong> %s</li>
</ul>
<p>Navštivte <a href="%s">%s</a> a začněte budovat své e-portfolio!</p>

<p>S pozdravem, správce stránek %s</p>';
$string['accountcreatedtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Na stránkách %s byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:

Uživatelské jméno: %s
Heslo: %s

Navštivte nyní %s a začněte budovat své e-portfolio!

S pozdravem, správce stránek %s';
$string['accountdeleted'] = 'Bohužel, váš účet byl odstraněn. Můžete <a href="%scontact.php">kontaktovat správce stránek</a>.';
$string['accountexpired'] = 'Bohužel, platnost vašeho účtu vypršela.  Můžete <a href="%scontact.php">kontaktovat správce stránek</a> pro jeho reaktivaci.';
$string['accountexpirywarning'] = 'Upozornění na vypršení platnosti';
$string['accountexpirywarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost vašeho účtu na stránkách %s vyprší během %s.</p>

<p>Doporučujeme vám uložit si obsah vašeho portfolia pomocí nástroje pro export. Návod k používání tohoto nástroje jsou k dispozici v uživatelské dokumentaci.</p>

<p>Pokud si přejete prodloužit platnost vašeho účtu nebo máte dotaz k uvedeným pokynům, neváhejte <a href="%s">nás kontaktovat</a>.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['accountexpirywarninghtml1'] = '<p>Vážený %s,</p> <p>platnost Vašeho účtu na %s vyprší %s.</p> <p>Pokud chcete zachovat obsah Vašeho portfolia i po té, co Vašemu účtu vyprší platnost, doporučujeme Vám Vaše portfolio exportovat přes <a href="%s">nástroj pro export</a>.</p> <p>Pokud si přejete platnost Vašeho účtu prodloužit nebo máte k výše zmíněnému jakékoli otázky, neváhejte <a href="%s">nás kontaktovat</a>.</p> <p>S pozdravem,<br> %s správce stránek</p>';
$string['accountexpirywarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost vašeho účtu na stránkách %s vyprší během %s.

Doporučujeme vám uložit si obsah vašeho portfolia pomocí nástroje pro export. Návod k používání tohoto nástroje jsou k dispozici v uživatelské dokumentaci.

Pokud si přejete prodloužit platnost vašeho účtu nebo máte dotaz k uvedeným pokynům, neváhejte nás kontaktovat: %s.

S pozdravem, %s, správce stránek';
$string['accountexpirywarningtext1'] = 'Vážený %s, platnost Vašeho účtu na %s vyprší %s. Pokud chcete zachovat obsah Vašeho portfolia i po té, co Vašemu účtu vyprší platnost, doporučujeme Vám Vaše portfolio exportovat přes %s. Pokud si přejete platnost Vašeho účtu prodloužit nebo máte k výše zmíněnému jakékoli otázky, neváhejte nás kontaktovat: %s 
S pozdravem, %s správce stránek';
$string['accountinactive'] = 'Je mi líto, váš účet není momentálně aktivní.';
$string['accountinactivewarning'] = 'Upozornění na deaktivaci účtu';
$string['accountinactivewarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Váš účet na stránkách %s bude deaktivován během %s.</p>

<p>S neaktivním uživatelským účtem se nebudete moci na stránky přihlásit. Reaktivovat účet může pouze váš správce.</p>

<p>Chcete-li předejít deaktivaci vašeho účtu, přihlaste se na uvedených stránkách.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['accountinactivewarninghtml1'] = '<p>Vážený %s,</p> <p>Váš účet na %s bude deaktivován %s.</p> <p>Jakmile bude účet neaktivní, ztratíte možnost se přihlásit dokud Vám správce účet znovu neaktivuje. </p> <p>Zabránit deaktivaci Vašeho účtu můžete tak, že se přihlásíte.</p> <p>S pozdravem,<br> %s správce stránek</p>';
$string['accountinactivewarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Váš účet na stránkách %s bude deaktivován během %s.

S neaktivním uživatelským účtem se nebudete moci na stránky přihlásit. Reaktivovat účet může pouze váš správce.

Chcete-li předejít deaktivaci vašeho účtu, přihlaste se na uvedených stránkách.

S pozdravem, %s, správce stránek';
$string['accountinactivewarningtext1'] = 'Vážený %s, Váš účet na %s bude deaktivován %s. Jakmile bude účet neaktivní, ztratíte možnost se přihlásit dokud Vám správce účet znovu neaktivuje. Zabránit deaktivaci Vašeho účtu můžete tak, že se přihlásíte. S pozdravem, %s správce stránek';
$string['accountprefs'] = 'Předvolby';
$string['accountsuspended'] = 'Váš účet byl pozastaven ke dni %s. Důvod pozastavení je: <blockquote>%s</blockquote>';
$string['active'] = 'Aktivní';
$string['activityprefs'] = 'Předvolby upozorňování';
$string['add'] = 'Přidat';
$string['addemail'] = 'Přidat e-mailovou adresu';
$string['addone'] = 'Přidat';
$string['addspecific'] = 'Přidat "%s"';
$string['admin'] = 'Správce';
$string['adminfirst'] = 'Správci jako první';
$string['administration'] = 'Administrace';
$string['adminmenu'] = 'Administrátorské menu';
$string['adminofinstitutions'] = 'Správce %s';
$string['adminphpuploaderror'] = 'Chyba při nahrávání souboru byla pravděpodobně způsobená konfigurací vašeho serveru.';
$string['after'] = 'po';
$string['alignment'] = 'Zarovnání';
$string['allonline'] = 'Zobrazit všechny připojené uživatele';
$string['allowinstitutiontags'] = 'Povolit štítky institucí';
$string['allowinstitutiontagsdescription'] = 'Správcům bude umožněno vytvořit sadu štítků, které budou moci používat členové jejich instituce.';
$string['allowpublicaccess'] = 'Povolit přístup i nepřihlášeným uživatelům';
$string['allpeopleonline'] = 'Zobrazit připojené uživatele';
$string['alltags'] = 'Všechny štítky';
$string['allusers'] = 'Všichni uživatelé';
$string['alphabet'] = 'A,Á,B,C,Č,D,Ď,E,É,Ě,F,G,H,I,Í,J,K,L,M,N,Ň,O,Ó,P,Q,R,Ř,S,Š,T,Ť,U,Ú,Ů,V,W,X,Y,Ý,Z,Ž';
$string['anonymoususer'] = '(Jméno autora je skryto)';
$string['applychanges'] = 'Uložit změny';
$string['approvalrequired'] = 'Vyžadováno povolení';
$string['artefact'] = 'položka portfolia';
$string['artefactnotfound'] = 'Položka portfolia s identifikátorem %s nenalezena';
$string['artefactnotpublishable'] = 'Položka portfolia %s nebyla zveřejněna na stránce %s';
$string['artefactnotrendered'] = 'Nevykreslená položka portfolia';
$string['artefacts'] = 'Položky portfolia';
$string['ascending'] = 'Vzestupně';
$string['at'] = '-';
$string['attachedimage'] = 'Obrázková příloha';
$string['attachment'] = 'Příloha';
$string['authentication'] = 'Autentifikace';
$string['back'] = 'Zpět';
$string['backto'] = 'Zpět do %s';
$string['before'] = 'před';
$string['belongingto'] = 'Patří k:';
$string['betweenxandy'] = 'Mezi %s a %s';
$string['blacklisteddomaininurl'] = 'URL v tomto poli obsahuje doménu %s umístěnou na černé listině.';
$string['blockinstanceconfigownerauto'] = 'Tento blok bude automaticky naplněn daty, jakmile bude zkopírován jako uživatelova stránka portfolia.';
$string['blockinstanceconfigownerchange'] = 'Tento blok bude nutné upravit/nastavit, jakmile bude zkopírován jako uživatelova stránka portfolia, aby se obsah zobrazil.';
$string['blockinstanceownerchange'] = 'Pro výběr obsahu ke zobrazení přejděte do úprav tohoto bloku.';
$string['blocks'] = 'Bloky';
$string['border'] = 'Šířka ohraničení';
$string['bulkselect'] = 'Zvolit uživatele pro úpravu / sestavy';
$string['bytes'] = 'bajtů';
$string['cancel'] = 'Zrušit';
$string['cancelrequest'] = 'Zrušit požadavek';
$string['cannotcompletemigrationwithuser'] = 'Bude nutné se přihlásit pomocí přihlašovacích údajů vaší aktuální instituce, nikoli pomocí údajů instituce, do které se chcete přesunout. Až to provedete, znovu klikněte na odkaz v emailu.';
$string['cannotremovedefaultemail'] = 'Nemůžete odstranit vaši primární e-mailovou adresu.';
$string['cannotrenametempfile'] = 'Není možné přejmenovat dočasný soubor.';
$string['cantbedeleted'] = 'Tento blok nemůže být smazán, protože on nebo jeho podbloky jsou součástí schvalovaných stránek.';
$string['cantbedeletedbeingusedascoverimage'] = 'Tato položka nemůže ýt smazána, protože se používá jako úvodní obrázek u existujícího portfolia.';
$string['cantchangepassword'] = 'Bohužel, pomocí tohoto rozhraní si nemůžete změnit heslo. Použijte rozhraní vaší instituce.';
$string['cantlistinstitutiontags'] = 'Není vám dovoleno vypsat seznam štítků instituce.';
$string['cantmoveitem'] = 'Tuto položku portfolia nelze přesunout.';
$string['change'] = 'Změnit';
$string['changepassword'] = 'Změna hesla';
$string['changepasswordinfo'] = 'Před pokračováním si musíte změnit své stávající heslo.';
$string['chooseinstitution'] = 'Vyberte vaši instituci';
$string['chooselanguage'] = 'Vybrat jazyk';
$string['choosetheme'] = 'Vyberte vzhled...';
$string['chooseusernamepassword'] = 'Vyberte si své uživatelské jméno a heslo';
$string['chooseusernamepasswordinfo'] = 'Pro přihlášení k %s potřebujete uživatelské jméno a heslo. Prosím zvolte je nyní.';
$string['clambroken'] = 'Na těchto stránkách se používá antivirová kontrola nahrávaných souborů, ale objevila se chyba v konfiguraci této služby. Váš soubor se NEPODAŘILO nahrát. Váš správce byl na tuto událost upozorněn e-mailem. Pokuste se nahrát soubor později.';
$string['clamdeletedfile'] = 'Soubor byl odstraněn.';
$string['clamdeletedfilefailed'] = 'Soubor nemohl být odstraněn.';
$string['clamemailsubject'] = '%s :: upozornění Clam AV';
$string['clamfailed'] = 'Nepodařilo se spustit Clam AV. Vrácené chybové hlášení zní: %s. Následuje výstup programu Clam:';
$string['clamlost'] = 'U nahrávaných souborů se má provádět antivirová kontrola, ale nastavená cesta k programu Clam AV (%s) není platná.';
$string['clammovedfile'] = 'Soubor byl přesunut do karantény.';
$string['clamnotset'] = 'Aktivovali jste antivirovou kontrolu, ale nenastavili jste hodnotu "Cesta ke ClamAV". Kontrola virů nezačne fungovat, dokud nenakonfigurujete cestu ke ClamAV přidáním hodnoty "$cfg->pathtoclam" do vašeho konfiguračního souboru "config.php".';
$string['clamunknownerror'] = 'Neznámá chyba programu clam.';
$string['cleanurlallowedcharacters'] = 'Jsou povolena pouze malá písmena a-z, číslice a pomlčka.';
$string['cli_incorrect_value'] = 'Nesprávná hodnota, prosím, opakujte zadání';
$string['clickformore'] = '(Pro zobrazení více informací stiskněte \'enter\')';
$string['closehelp'] = 'Zavřít nápovědu';
$string['collapse'] = 'Sbalit';
$string['collapsespecific'] = 'Sbalit %s';
$string['complaint'] = 'Stížnost';
$string['complete'] = 'Hotovo';
$string['config'] = 'Konfigurovat';
$string['configfor'] = 'Konfigurace';
$string['confirmdeletetag'] = 'Jste si skutečně jistí, že chcete vymazat tento štítek ze všech položek svého portfolia?';
$string['confirminvitation'] = 'Potvrdit pozvání';
$string['confirmmigration'] = 'Přesunout účet';
$string['confirmpassword'] = 'Potvrdit heslo';
$string['connectedapps'] = 'Připojené aplikace';
$string['connectspecific'] = 'Připojit "%s"';
$string['constrain'] = 'Omezit';
$string['contactus'] = 'Kontakt';
$string['content'] = 'Obsah';
$string['controlyourprivacy'] = 'Ovládání a ochrana vašeho soukromí';
$string['controlyourprivacylinked'] = 'Ovládejte své <a href="%s">soukromí</a>';
$string['cookiesnotenabled'] = 'Pro přihlášení se na těchto stránkách se používají cookies. Váš prohlížeč buď nemá cookies povoleny, nebo blokuje cookies zaslané z tohoto serveru.';
$string['copy'] = 'Kopírovat';
$string['copytoclipboard'] = 'Zkopírovat tajné URL do schránky';
$string['couldnotgethelp'] = 'Vyskytla se chyba při načítání stránky s nápovědou';
$string['country.ad'] = 'Andorra';
$string['country.ae'] = 'Spojené arabské emiráty';
$string['country.af'] = 'Afghánistán';
$string['country.ag'] = 'Antigua a Barbuda';
$string['country.ai'] = 'Anguilla';
$string['country.al'] = 'Albánie';
$string['country.am'] = 'Arménie';
$string['country.an'] = 'Nizozemské Antily';
$string['country.ao'] = 'Angola';
$string['country.aq'] = 'Antarktida';
$string['country.ar'] = 'Argentina';
$string['country.as'] = 'Americká Samoa';
$string['country.at'] = 'Rakousko';
$string['country.au'] = 'Austrálie';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Ålandy';
$string['country.az'] = 'Ázerbájdžán';
$string['country.ba'] = 'Bosna a Hercegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = 'Bangladéš';
$string['country.be'] = 'Belgie';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = 'Bulharsko';
$string['country.bh'] = 'Bahrajn';
$string['country.bi'] = 'Burundi';
$string['country.bj'] = 'Benin';
$string['country.bl'] = 'Svatý Bartoloměj';
$string['country.bm'] = 'Bermudy';
$string['country.bn'] = 'Brunej';
$string['country.bo'] = 'Bolívie';
$string['country.bq'] = 'Bonaire';
$string['country.br'] = 'Brazílie';
$string['country.bs'] = 'Bahamy';
$string['country.bt'] = 'Bhútán';
$string['country.bv'] = 'Bouvetův ostrov';
$string['country.bw'] = 'Botswana';
$string['country.by'] = 'Bělorusko';
$string['country.bz'] = 'Belize';
$string['country.ca'] = 'Kanada';
$string['country.cc'] = 'Kokosové ostrovy';
$string['country.cd'] = 'Demokratická republika Kongo';
$string['country.cf'] = 'Středoafrická republika';
$string['country.cg'] = 'Kongo';
$string['country.ch'] = 'Švýcarsko';
$string['country.ci'] = 'Pobřeží slonoviny';
$string['country.ck'] = 'Cookovy ostrovy';
$string['country.cl'] = 'Chile';
$string['country.cm'] = 'Kamerun';
$string['country.cn'] = 'Čína';
$string['country.co'] = 'Kolumbie';
$string['country.cr'] = 'Kostarika';
$string['country.cs'] = 'Srbsko a Černá Hora';
$string['country.cu'] = 'Kuba';
$string['country.cv'] = 'Kapverdy';
$string['country.cw'] = 'Curaçao';
$string['country.cx'] = 'Vánoční ostrov';
$string['country.cy'] = 'Kypr';
$string['country.cz'] = 'Česká republika';
$string['country.de'] = 'Německo';
$string['country.dj'] = 'Džibutsko';
$string['country.dk'] = 'Dánsko';
$string['country.dm'] = 'Dominika';
$string['country.do'] = 'Dominikánská republika';
$string['country.dz'] = 'Alžírsko';
$string['country.ec'] = 'Ekvádor';
$string['country.ee'] = 'Estonsko';
$string['country.eg'] = 'Egypt';
$string['country.eh'] = 'Západní Sahara';
$string['country.er'] = 'Eritrea';
$string['country.es'] = 'Španělsko';
$string['country.et'] = 'Etiopie';
$string['country.fi'] = 'Finsko';
$string['country.fj'] = 'Fidži';
$string['country.fk'] = 'Falklandy (Malvíny)';
$string['country.fm'] = 'Mikronésie';
$string['country.fo'] = 'Faerské ostrovy';
$string['country.fr'] = 'Francie';
$string['country.ga'] = 'Gabun';
$string['country.gb'] = 'Velká Británie';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Georgia';
$string['country.gf'] = 'Francouzská Guyana';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = 'Grónsko';
$string['country.gm'] = 'Gambie';
$string['country.gn'] = 'Guinea';
$string['country.gp'] = 'Guadeloupe';
$string['country.gq'] = 'Rovníková Guinea';
$string['country.gr'] = 'Řecko';
$string['country.gs'] = 'Jižní Georgie a Jižní Sandwichovy ostrovy';
$string['country.gt'] = 'Guatemala';
$string['country.gu'] = 'Guam';
$string['country.gw'] = 'Guinea-Bissau';
$string['country.gy'] = 'Guyana';
$string['country.hk'] = 'Hongkong';
$string['country.hm'] = 'Heardův ostrov a McDonaldovy ostrovy';
$string['country.hn'] = 'Honduras';
$string['country.hr'] = 'Chorvatsko';
$string['country.ht'] = 'Haiti';
$string['country.hu'] = 'Maďarsko';
$string['country.id'] = 'Indonésie';
$string['country.ie'] = 'Irsko';
$string['country.il'] = 'Izrael';
$string['country.im'] = 'Ostrov Man';
$string['country.in'] = 'Indie';
$string['country.io'] = 'Britské indickooceánské území';
$string['country.iq'] = 'Irák';
$string['country.ir'] = 'Írán';
$string['country.is'] = 'Island';
$string['country.it'] = 'Itálie';
$string['country.je'] = 'Jersey';
$string['country.jm'] = 'Jamajka';
$string['country.jo'] = 'Jordánsko';
$string['country.jp'] = 'Japonsko';
$string['country.ke'] = 'Keňa';
$string['country.kg'] = 'Kyrgyzstán';
$string['country.kh'] = 'Kambodža';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Komory';
$string['country.kn'] = 'Svatý Kryštof a Nevis';
$string['country.kp'] = 'Korejská lidově demokratická republika';
$string['country.kr'] = 'Korea';
$string['country.kw'] = 'Kuvajt';
$string['country.ky'] = 'Kajmanské ostrovy';
$string['country.kz'] = 'Kazachstán';
$string['country.la'] = 'Laos';
$string['country.lb'] = 'Libanon';
$string['country.lc'] = 'Svatá Lucie';
$string['country.li'] = 'Lichtenštejnsko';
$string['country.lk'] = 'Srí Lanka';
$string['country.lr'] = 'Libérie';
$string['country.ls'] = 'Lesotho';
$string['country.lt'] = 'Litva';
$string['country.lu'] = 'Lucembursko';
$string['country.lv'] = 'Lotyšsko';
$string['country.ly'] = 'Libye';
$string['country.ma'] = 'Maroko';
$string['country.mc'] = 'Monako';
$string['country.md'] = 'Moldávie';
$string['country.me'] = 'Černá Hora';
$string['country.mf'] = 'Svatý Martin (francouzská část)';
$string['country.mg'] = 'Madagaskar';
$string['country.mh'] = 'Marshallovy ostrovy';
$string['country.mk'] = 'Makedonie';
$string['country.ml'] = 'Mali';
$string['country.mm'] = 'Myanmar';
$string['country.mn'] = 'Mongolsko';
$string['country.mo'] = 'Macao';
$string['country.mp'] = 'Severní Mariany';
$string['country.mq'] = 'Martinik';
$string['country.mr'] = 'Mauritánie';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = 'Malta';
$string['country.mu'] = 'Mauricius';
$string['country.mv'] = 'Maledivy';
$string['country.mw'] = 'Malawi';
$string['country.mx'] = 'Mexiko';
$string['country.my'] = 'Malajsie';
$string['country.mz'] = 'Mosambik';
$string['country.na'] = 'Namibie';
$string['country.nc'] = 'Nová Kaledonie';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Norfolk';
$string['country.ng'] = 'Nigérie';
$string['country.ni'] = 'Nikaragua';
$string['country.nl'] = 'Nizozemsko';
$string['country.no'] = 'Norsko';
$string['country.np'] = 'Nepál';
$string['country.nr'] = 'Nauru';
$string['country.nu'] = 'Niue';
$string['country.nz'] = 'Nový Zéland';
$string['country.om'] = 'Omán';
$string['country.pa'] = 'Panama';
$string['country.pe'] = 'Peru';
$string['country.pf'] = 'Francouzská Polynésie';
$string['country.pg'] = 'Papua-Nová Guinea';
$string['country.ph'] = 'Filipíny';
$string['country.pk'] = 'Pákistán';
$string['country.pl'] = 'Polsko';
$string['country.pm'] = 'Saint-Pierre a Miquelon';
$string['country.pn'] = 'Pitcairn';
$string['country.pr'] = 'Portoriko';
$string['country.ps'] = 'Palestina';
$string['country.pt'] = 'Portugalsko';
$string['country.pw'] = 'Palau';
$string['country.py'] = 'Paraguay';
$string['country.qa'] = 'Katar';
$string['country.re'] = 'Réunion';
$string['country.ro'] = 'Rumunsko';
$string['country.rs'] = 'Srbsko';
$string['country.ru'] = 'Ruská federace';
$string['country.rw'] = 'Rwanda';
$string['country.sa'] = 'Saúdská Arábie';
$string['country.sb'] = 'Šalamounovy ostrovy';
$string['country.sc'] = 'Seychely';
$string['country.sd'] = 'Súdán';
$string['country.se'] = 'Švédsko';
$string['country.sg'] = 'Singapur';
$string['country.sh'] = 'Svatá Helena';
$string['country.si'] = 'Slovinsko';
$string['country.sj'] = 'Špicberky';
$string['country.sk'] = 'Slovenská republika';
$string['country.sl'] = 'Sierra Leone';
$string['country.sm'] = 'San Marino';
$string['country.sn'] = 'Senegal';
$string['country.so'] = 'Somálsko';
$string['country.sr'] = 'Surinam';
$string['country.ss'] = 'Jižní Súdán';
$string['country.st'] = 'Svatý Tomáš a Princův ostrov';
$string['country.sv'] = 'Salvador';
$string['country.sx'] = 'Svatý Martin (nizozemská část)';
$string['country.sy'] = 'Sýrie';
$string['country.sz'] = 'Svazijsko';
$string['country.tc'] = 'Turks a Caicos';
$string['country.td'] = 'Čad';
$string['country.tf'] = 'Francouzská jižní území';
$string['country.tg'] = 'Togo';
$string['country.th'] = 'Thajsko';
$string['country.tj'] = 'Tádžikistán';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Východní Timor';
$string['country.tm'] = 'Turkmenistán';
$string['country.tn'] = 'Tunisko';
$string['country.to'] = 'Tonga';
$string['country.tr'] = 'Turecko';
$string['country.tt'] = 'Trinidad a Tobago';
$string['country.tv'] = 'Tuvalu';
$string['country.tw'] = 'Tchaj-wan';
$string['country.tz'] = 'Tanzanie';
$string['country.ua'] = 'Ukrajina';
$string['country.ug'] = 'Uganda';
$string['country.um'] = 'Menší odlehlé ostrovy USA';
$string['country.us'] = 'Spojené státy americké';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Uzbekistán';
$string['country.va'] = 'Vatikán';
$string['country.vc'] = 'Svatý Vincenc a Grenadiny';
$string['country.ve'] = 'Venezuela';
$string['country.vg'] = 'Panenské ostrovy (V. Británie)';
$string['country.vi'] = 'Panenské ostrovy (USA)';
$string['country.vn'] = 'Vietnam';
$string['country.vu'] = 'Vanuatu';
$string['country.wf'] = 'Wallisovy ostrovy';
$string['country.ws'] = 'Samoa';
$string['country.xk'] = 'Kosovo';
$string['country.ye'] = 'Jemen';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = 'Jižní Afrika';
$string['country.zm'] = 'Zambie';
$string['country.zw'] = 'Zimbabwe';
$string['countryisocustomise'] = 'Pro úpravu jakýchkoli jmen zemí pro váš web prosím upravte jazykový soubor "mahara.php" v jednotlivých překladech nebo <a href="https://wiki.mahara.org/wiki/Developer_Area/Language_strings#Custom_lang_strings_in_.2Flocal">vytvořte řetězce v místním adresáři</a>.';
$string['countryisodisclaimer'] = 'Jména zemí jsou zobrazována v souladu s ISO 3166, kterou vydala Mezinárodní organizace pro normalizaci.';
$string['create'] = 'Tvořit';
$string['createcollect'] = 'Vytvářet a shromažďovat';
$string['createcollectsubtitle'] = 'Rozvíjejte své portfolio';
$string['createdetail'] = 'Pracujte na svém elektronickém portfoliu ve flexibilním prostředí pro osobní rozvoj';
$string['createresume'] = 'Vytvořit svůj životpis';
$string['createsubtitle'] = 'Rozvíjejte své portfolio';
$string['createtag'] = 'Vytvořit štítek';
$string['createyourresume'] = 'Vytvořte svůj <a href="%s">životopis</a>';
$string['currentinstitutionmembership'] = 'Aktuální instituce';
$string['dashboarddescription'] = 'Stránku ovládacího panelu uvidíte vždy na titulní stránce, když se poprvé přihlásíte. Pouze vy k němu máte přístup.';
$string['dashboarddescription1'] = 'Váš ovládací panel je stránka, kterou vidíte na své domovské stránce, když se přihlásíte. Máte k němu přístup pouze vy.';
$string['date'] = 'Datum';
$string['dateformatguide'] = 'Použijte formát RRRR/MM/DD';
$string['dateformatguide1'] = 'Použít formát %s';
$string['dateofbirthformatguide'] = 'Použijte formát RRRR/MM/DD';
$string['dateofbirthformatguide1'] = 'Použít formát %s';
$string['datepicker_amNames'] = '[\'dop.\', \'dop.\']';
$string['datepicker_clear'] = 'Zrušit výběr';
$string['datepicker_clearText'] = 'Vyčistit';
$string['datepicker_close'] = 'Zavřít výběrový nástroj';
$string['datepicker_closeStatus'] = 'Zavřít bez provedení změn';
$string['datepicker_closeText'] = 'Hotovo';
$string['datepicker_currentStatus'] = 'Zobrazit aktuální měsíc';
$string['datepicker_currentText'] = 'Teď';
$string['datepicker_dateStatus'] = 'Zvolit DD, MM d, yy';
$string['datepicker_dayNames'] = '[\'neděle\',\'pondělí\',\'úterý\',\'středa\',\'čtvrtek\',\'pátek\',\'sobota\']';
$string['datepicker_dayNamesMin'] = '[\'ne\',\'po\',\'út\',\'st\',\'čt\',\'pá\',\'so\']';
$string['datepicker_dayNamesShort'] = '[\'ne\',\'po\',\'út\',\'st\',\'čt\',\'pá\',\'so\']';
$string['datepicker_dayStatus'] = 'Použít DD jako první den týdne';
$string['datepicker_decrementHour'] = 'Snížit o hodinu';
$string['datepicker_decrementMinute'] = 'Snížit o minutu';
$string['datepicker_decrementSecond'] = 'Snížit o sekundu';
$string['datepicker_hourText'] = 'Hodina';
$string['datepicker_incrementHour'] = 'Navýšit o hodinu';
$string['datepicker_incrementMinute'] = 'Navýšit o minutu';
$string['datepicker_incrementSecond'] = 'Navýšit o sekundu';
$string['datepicker_initStatus'] = 'Zvoit datum';
$string['datepicker_millisecText'] = 'Milisekunda';
$string['datepicker_minuteText'] = 'Minuta';
$string['datepicker_monthNames'] = '[\'leden\',\'únor\',\'březen\',\'duben\',\'květen\',\'červen\',\'červenec\',\'srpen\',\'září\',\'říjen\',\'listopad\',\'prosinec\']';
$string['datepicker_monthNamesShort'] = '[\'led\',\'ún\',\'břez\',\'dub\',\'květ\',\'čer\',\'čec\',\'srp\',\'zář\',\'říj\']';
$string['datepicker_monthStatus'] = 'Zobrazit jiný měsíc';
$string['datepicker_nextCentury'] = 'Následující století';
$string['datepicker_nextDecade'] = 'Následující desetiletí';
$string['datepicker_nextMonth'] = 'Následující měsíc';
$string['datepicker_nextStatus'] = 'Zobrazit následující měsíc';
$string['datepicker_nextText'] = 'Následující';
$string['datepicker_nextYear'] = 'Následující rok';
$string['datepicker_pickHour'] = 'Vybrat hodinu';
$string['datepicker_pickMinute'] = 'Vybrat minutu';
$string['datepicker_pickSecond'] = 'Vybrat sekundu';
$string['datepicker_pmNames'] = '[\'odp.\', \'odp.\']';
$string['datepicker_prevCentury'] = 'Předcházející století';
$string['datepicker_prevDecade'] = 'Předcházející desetiletí';
$string['datepicker_prevMonth'] = 'Předcházející měsíc';
$string['datepicker_prevStatus'] = 'Zobrazit předchozí měsíc';
$string['datepicker_prevText'] = 'Předchozí';
$string['datepicker_prevYear'] = 'Předcházející rok';
$string['datepicker_secondText'] = 'Druhý';
$string['datepicker_selectDecade'] = 'Vybrat desetiletí';
$string['datepicker_selectMonth'] = 'Vybrat měsíc';
$string['datepicker_selectTime'] = 'Vybrat čas';
$string['datepicker_selectYear'] = 'Vybrat rok';
$string['datepicker_timeOnlyTitle'] = 'Zvolit čas';
$string['datepicker_timeText'] = 'Čas';
$string['datepicker_timezoneText'] = 'Časové pásmo';
$string['datepicker_today'] = 'Přejít na dnešek';
$string['datepicker_togglePeriod'] = 'Přepnout časové období';
$string['datepicker_weekHeader'] = 'Týd.';
$string['datepicker_yearStatus'] = 'Zobrazit jiný rok';
$string['datetimeformatguide'] = 'Použijte formát RRRR/MM/DD HH:MM';
$string['datetimeformatguide1'] = 'Použijte formát %s';
$string['day'] = 'den';
$string['days'] = 'dní';
$string['debugemail'] = 'POZNÁMKA: Tento e-mail je určen pro %s <%s>, ale byl zaslán vám díky nastavení "sendallemailto".';
$string['decline'] = 'Zamítnout';
$string['default'] = 'Výchozí';
$string['defaulthint'] = 'Zadejte hledaný výraz';
$string['delete'] = 'Odstranit';
$string['deleteaccount'] = 'Odstraněn účet: %s / %s';
$string['deleteaccount1'] = 'Odstranit účet';
$string['deleted'] = 'odstraněný';
$string['deleteduser'] = 'Odstraněný uživatel';
$string['deleteduser1'] = 'Účet smazán';
$string['deleteinstitutiontag'] = 'Smazat štítek instituce';
$string['deleteinstitutiontagspecific'] = 'Smazat štítek instituce "%s"';
$string['deleteitem'] = 'Odstranit';
$string['deletespecific'] = 'Odstranit "%s"';
$string['deletespecificfrom'] = 'Odstranit "%s" s URL "%s"';
$string['deletetag'] = 'Smazat <a href="%s">%s</a>';
$string['deletetagdescription'] = 'Smazat tento štítek ze všech položek ve vašem portfoliu';
$string['denylisteddomaininurl'] = 'URL adresa v tomto poli obsahuje doménu %s, která je na seznamu zakázaných.';
$string['descending'] = 'Sestupně';
$string['description'] = 'Popis';
$string['details'] = 'podrobnosti';
$string['dimensions'] = 'Rozměry';
$string['disable'] = 'Zakázat';
$string['disabled'] = 'Zakázáno';
$string['disabledspecific'] = '"%s" je zakázáno';
$string['discusstopics'] = 'Diskusní témata';
$string['discusstopicslinked'] = 'Diskutujte o <a href="%s">tématech</a>';
$string['displayname'] = 'Zobrazované jméno';
$string['divertingemailto'] = 'Přesměrování e-mailu na %s';
$string['done'] = 'Hotovo';
$string['dropdownmenu'] = 'navigace';
$string['duplicatenamedfield'] = 'Vložená hodnota již existuje';
$string['earliest'] = 'Nejdřívější';
$string['edit'] = 'Upravit';
$string['editaccess'] = 'Upravit přístup';
$string['editdashboard'] = 'Upravit';
$string['editing'] = 'Úprava';
$string['editinstitutiontag'] = 'Upravit štítek instituce';
$string['editmyprofilepage'] = 'Upravit stránku s profilem';
$string['editspecific'] = 'Upravit "%s"';
$string['editspecificfrom'] = 'Upravit "%s" s URL "%s"';
$string['edittag'] = 'Upravit <a href="%s">%s</a>';
$string['edittagdescription'] = 'Všechny položky ve vašem portfoliu označené štítkem "%s" byly aktualizovány';
$string['edittags'] = 'Upravit štítky';
$string['editthistag'] = 'Upravit tento štítek';
$string['email'] = 'E-mail';
$string['emailaddress'] = 'E-mailová adresa';
$string['emailaddressdescription'] = 'Adresa elektronické pošty';
$string['emailaddressorusername'] = 'E-mailová adresa nebo uživatelské jméno';
$string['emailinvalid'] = 'Emailová adresa není platná.';
$string['emailnotsent'] = 'Nepodařilo se zaslat e-mail. Chybová zpráva: "%s"';
$string['emails'] = 'E-maily';
$string['emailtoolong'] = 'E-mailová adresa nemůže být delší než 255 znaků.';
$string['enable'] = 'Povolit';
$string['enabled'] = 'Povoleno';
$string['enabledspecific'] = '"%s" je povoleno';
$string['engage'] = 'Zapojit se';
$string['engagedetail'] = 'Zúčastněte se diskuze s ostatními lidmi a spolupracujte s nimi ve skupinách';
$string['engagesubtitle'] = 'Seznamte se s ostatními a přidávejte se ke skupinám';
$string['error:duplicatetag'] = 'Tento štítek instituce již existuje.';
$string['error:emptytag'] = 'Štítek instituce nemůže být prázdný.';
$string['errorLoading'] = 'Výsledek nemohl být načten.';
$string['errorprocessingform'] = 'Při odesílání tohoto formuláře došlo k chybě. Překontrolujte označená pole a pokuste se uložit formulář znovu.';
$string['expand'] = 'Rozbalit';
$string['expandspecific'] = 'Rozbalit %s';
$string['exportfiletoobig'] = 'Vytvářený soubor bude příliš velký. Prosím, uvolněte místo na disku.';
$string['externalmanual'] = 'Externí manuál';
$string['facebookdescription'] = 'Mahara je webová open source aplikace pro správu vlastního e-portfolia a sociální sítě. Poskytuje uživatelům nástroje pro vytváření a správu jejich vzdělávacího digitálního portfolia s funkcemi sociální sítě pro usnadnění vzájemné interakce.';
$string['failedmovingfiletodataroot'] = 'Nemohu přesunout nahraná data do adresáře s uživatelskými daty.';
$string['false'] = 'Neplatný';
$string['filenotfound'] = 'Soubor nenalezen';
$string['filenotfoundmaybeexpired'] = 'Soubor nenalezen. Váš exportovaný soubor existuje pouze 25 hodin po vytvoření. Musíte opakovat export obsahu.';
$string['filenotimage'] = 'Formát nahraného souboru není přípustný formát obrázku, musí se jednat o PNG, JPEG nebo GIF.';
$string['fileunknowntype'] = 'Typ nahraného souboru nebyl rozpoznán. Buď je váš soubor poškozen nebo se objevila chyba v konfiguraci serveru. Prosím, spojte se s vaším správcem.';
$string['filter'] = 'Filtr';
$string['filterresultsby'] = 'Fitrovat výsledek podle:';
$string['findfriends'] = 'Najít přátele';
$string['findfriendslinked'] = 'Najděte <a href="%s">přátele</a>';
$string['findgroups'] = 'Najít skupiny';
$string['findpeople'] = 'Najít lidi';
$string['first'] = 'První';
$string['firstjoined'] = 'Prvně připojen';
$string['firstname'] = 'Křestní jméno';
$string['firstnameall'] = 'Všechna křestní jména';
$string['firstnamedescription'] = 'Křestní jméno uživatele';
$string['firstpage'] = 'První stránka';
$string['forgotpassemailsendunsuccessful'] = 'Bohužel, email se nepodařilo odeslat. Prosím, zkuste to později.';
$string['forgotpassemailsentanyway'] = 'E-mail byl odeslán na adresu evidovanou u tohoto uživatele, což neznamená, že je platná nebo jí server uživatele nevrátí zpět. Pokud jste e-mail neobdrželi, obraťte se na správce Mahary za účelem nastavení nového hesla.';
$string['forgotpassemailsentanyway1'] = 'E-mail byl odeslán na adresu evidovanou u tohoto uživatele, ale adresa nemusí být správná nebo ji server nemusí doručit. V takovém případě se prosím obraťte se na správce %s.';
$string['forgotpassnosuchemailaddressorusername'] = 'Tato e-mailová adresa nebo uživatelské jméno nepatří žádnému z našich uživatelů';
$string['forgotpassuserusingexternalauthentication'] = 'Tento uživatel používá metodu externí autentifikace.  <a href="%s">Požádejte administrátora</a> o pomoc se změnou hesla, nebo zvolte jiné uživatelské jméno / emailovou adresu.';
$string['forgotpasswordenternew'] = 'Před pokračováním zadejte vaše nové heslo.';
$string['forgotusernamepassword'] = 'Zapomněli jste své uživatelské jméno nebo heslo?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Toto je automaticky generovaný email pro: %s</p>

<p>Na stránkách %s byla podána žádost o zaslání zapomenutého uživatelského jména nebo hesla pro přihlášení.</p>

<p>Vaše uživatelské jméno je <strong>%s</strong>.</p>

<p>Pokud si přejete resetovat vaše heslo, přejděte na následující stránku:</p>

<p><a href="%s">%s</a></p>

<p>Pokud jste o zaslání zapomenutého jména nebo hesla nepožádal/a, ignorujte tento e-mail.</p>

<p>V případě jakýchkoliv dotazů nás neváhejte <a href="%s">kontaktovat</a>.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['forgotusernamepasswordemailmessagetext'] = 'Toto je automaticky generovaný email pro: %s

Na stránkách %s byla podána žádost o zaslání zapomenutého uživatelského jména nebo hesla pro přihlášení.

Vaše uživatelské jméno je: %s

Pokud si přejete resetovat vaše heslo, přejděte na následující stránku:

%s

Pokud jste o zaslání zapomenutého jména nebo hesla nepožádal/a, ignorujte tento e-mail.

V případě jakýchkoliv dotazů nás neváhejte kontaktovat: %s

S pozdravem, %s, správce stránek';
$string['forgotusernamepasswordemailsubject'] = 'Přihlašovací údaje pro %s';
$string['forgotusernamepasswordtext'] = '<p>Pokud jste zapomněli své uživatelské jméno nebo heslo, zadejte vaši e-mailovou adresu z vašeho uživatelského profilu. Bude vám zaslána zpráva, pomocí níž budete moci získat nové heslo.</p>

<p>Pokud si uživatelské jméno pamatujete a zapomněli jste pouze heslo, můžete namísto e-mailu použít své uživatelské jméno.</p>';
$string['forgotusernamepasswordtextprimaryemail'] = '<p>Pokud jste zapomněli své uživatelské jméno nebo heslo, zadejte primární e-mailovou adresu uvedenou ve svém profilu a my vám zašleme zprávu, kterou můžete použít pro nastavení nového hesla.</p> <p>Pokud znáte své uživatelské jméno a zapomněli jste pouze své heslo, můžete ho zadat namísto primárního e-mailu.</p>';
$string['formatpostbbcode'] = 'Pro formátování můžete použít syntaxi BBCode. %sVíce podrobností%s';
$string['formerror'] = 'Došlo k chybě při zpracování vašeho podání. Zkuste to prosím znovu.';
$string['formerroremail'] = 'Kontaktujte nás na %s pokud vaše problémy přetrvávají.';
$string['fullname'] = 'Celé jméno';
$string['general'] = 'Povolání';
$string['go'] = 'Přejít';
$string['goto'] = 'Přejít na \'%s\'';
$string['gotoinbox'] = 'Přejít k doručené poště';
$string['gotomore'] = 'Zobrazit víc...';
$string['groupquota'] = 'Diskový prostor skupiny';
$string['groups'] = 'Skupiny';
$string['height'] = 'Výška';
$string['heightshort'] = 'v';
$string['helpfor'] = 'Nápověda pro "%s"';
$string['hidden'] = 'skrytý';
$string['hide'] = 'Skrýt';
$string['historical'] = 'Historická data';
$string['home'] = 'Domů';
$string['howtodisable'] = 'Vaše informační plocha je skrytá. Její viditelnost můžete měnit v <a href="%s">nastavení</a>.';
$string['hspace'] = 'Vodorovné místo';
$string['image'] = 'Obrázek';
$string['imagebrowserdescription'] = 'Pro volbu nebo nahrání vlastního obrázku vložte adresu k externímu obrázku, nebo zvolte procházení obrázky níže.';
$string['imagebrowsertitle'] = 'Vložte nebo zvolte obrázek';
$string['imageformattingoptions'] = 'Možnosti formátování obrázku';
$string['imagexofy'] = 'Obrázek {x} z {y}';
$string['importedfrom'] = 'Importováno z %s';
$string['inbox'] = 'Doručená pošta';
$string['inboxblocktitle'] = 'Zprávy';
$string['incomingfolderdesc'] = 'Soubory importované z jiných serverů';
$string['infofor'] = 'Informace pro';
$string['inputTooLong'] = 'Příliš mnoho znaků';
$string['inputTooShort'] = 'Vložte vyhledávaný dotaz';
$string['installedplugins'] = 'Nainstalované moduly';
$string['institution'] = 'Instituce';
$string['institutionadministration'] = 'Správa instituce';
$string['institutioncontacts'] = '\'%s\' – kontakty na instituci';
$string['institutionexpirywarning'] = 'Upozornění na vypršení členství v instituci';
$string['institutionexpirywarninghtml_institution'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost členství %s v %s vyprší za %s.</p>

<p>Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím <a href="%s">kontaktovat</a>.</p>

<p>S pozdravem, správce stránek %s</p>';
$string['institutionexpirywarninghtml_institution1'] = '<p>Vážený %s,</p> <p>Členství %s z %s vyprší %s.</p> <p>Pokud si přejete prodloužit členství ve vaší instituci nebo máte k výše zmíněnému jakékoliv dotazy, neváhejte se prosím <a href="%s"> na nás obrátit.  </a>.</p> <p>S pozdravem,<br> %s správce sítě</p>';
$string['institutionexpirywarninghtml_site'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost instituce %s vyprší za %s.</p>

<p> Možná nás budete chtít kontaktovat za účelem prodloužení %s.</p>

<p>S pozdravem, správce stránek %s</p>';
$string['institutionexpirywarninghtml_site1'] = '<p>Vážený %s,</p> <p>Instituci \'%s\' vyprší platnost %s.</p> <p> Možná je chcete kontaktovat, aby si prodloužili jejich členství  v %s.</p> <p>S pozdravem,<br> %s správce sítě</p>';
$string['institutionexpirywarningtext_institution'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost členství %s v %s vyprší za %s.

Pokud chcete prodloužit své členství nebo máte nějaké dotazy, neváhejte nás kontatovat na: %s

S pozdravem, správce stránek %s';
$string['institutionexpirywarningtext_institution1'] = 'Vážený %s, členství %s z %s vyprší %s.Pokud si přejete prodloužit členství ve vaší instituci nebo máte k výše zmíněnému jakékoliv dotazy, neváhejte se prosím na nás obrátit: %s  S pozdravem,  %s správce sítě';
$string['institutionexpirywarningtext_site'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost instituce %s vyprší za %s. Možná nás budete chtít kontaktovat za účelem prodloužení %s.

S pozdravem, správce stránek %s';
$string['institutionexpirywarningtext_site1'] = 'Vážený %s, instituci \'%s\' vyprší platnost %s. Možná je chcete kontaktovat, aby si prodloužili jejich členství  v %s. S pozdravem,<br> %s správce sítě</p>';
$string['institutionfilledreplymessage'] = 'Vážený %s, děkujeme za vaši trpělivost. Nyní můžete svůj účet přesunout do instituce "%s" pomocí tohoto odkazu %s S pozdravem, tým %s';
$string['institutionfilledreplysubject'] = 'Přesunutí účtu je opět možné';
$string['institutionfull'] = 'Vybraná instituce již nepřijímá žádné nové členy.';
$string['institutioninformation'] = 'Informace o instituci';
$string['institutionlink'] = '<a href="%s">%s</a>';
$string['institutionmaxusersexceededrequest'] = 'Tato instituce je plná a nelze se k ní tedy nyní přidat. Byl o tom informován správce instituce. Zkuste to prosím později. Pokud problém přetrvává, <a href="%s">kontaktujte správce instituce</a>.';
$string['institutionmemberconfirmmessage'] = 'Bylo vám přiděleno členství v instituci %s.';
$string['institutionmemberconfirmsubject'] = 'Potvrzení členství v instituci';
$string['institutionmemberrefusedprivacy'] = 'Vážený %s, uživatel %s, s uživatelským jménem %s, odmítl %s. Jeho účet byl tedy pozastaven. %s %s Prosím, kontaktujte tohoto uživatele emailem na %s pokud si přejete danou situaci prodiskutovat. S pozdravem, %s tým';
$string['institutionmemberrejectmessage'] = 'Vaše žádost o členství v %s byla zamítnuta.';
$string['institutionmemberrejectsubject'] = 'Členství v instituci zamítnuto';
$string['institutionmembership'] = 'Členství v institucích';
$string['institutionmembershipdescription'] = 'Zde se zobrazuje seznam všech institucí, jejichž jste členem. Můžete rovněž požádat o členství v nějaké instituci. Případně, pokud vám nějaká instituce nabídla členství, zde můžete přijmout, nebo odmítnout tuto nabídku.';
$string['institutionmembershipexpirywarning'] = 'Varování pro vypršení členství v instituci';
$string['institutionmembershipexpirywarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>

Vaše členství v %s na %s vyprší za %s.</p> <p> Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím <a href="%s">kontaktovat</a>.</p> <p>

S pozdravem, správce stránek %s</p>';
$string['institutionmembershipexpirywarninghtml1'] = '<p>Vážený %s,</p> <p>Vaše členství %s na %s vyprší %s.</p> <p>Pokud si přejete své členství prodloužit nebo máte jakékoli otázky k výše uvedenému, neváhejte <a href="%s">nás kontaktovat</a>.</p> <p>S pozdravem,<br> %s správce stránek</p>';
$string['institutionmembershipexpirywarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Vaše členství v %s na %s vyprší za %s. Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím kontaktovat na: %s.

S pozdravem, správce stránek %s';
$string['institutionmembershipexpirywarningtext1'] = 'Vážený %s, Vaše členství %s na %s vyprší %s. Pokud si přejete své členství prodloužit nebo máte jakékoli otázky k výše uvedenému, neváhejte nás kontaktovat: %s
S pozdravem, %s správce stránek';
$string['institutionmembershipfullmessagetext'] = 'Dobrý den %s, byl dosaženo maximálního počtu uživatelů pro %s za %s. Promažte prosím stávájící uživatelské účty nebo požádejte o navýšení maximálního počtu uživatelů pro vaši instituci. Tento limit  může navýšit jakýkoliv ze správců. S pozdravem, Tým %s';
$string['institutionmembershipfullmessagetextuser'] = 'Vážený %s, %s by se rád přesunul do instituce "%s" na %s, ale byl dosažen maximální počet členů instituce. Buď prosím odstraňte nepoužívané účty nebo požádejte správce stránek o navýšení počtu účtů pro tuto instituci. Jakmile tento problém vyřešíte, prosím dejte vědět %s, že může přesun zkusit udělat znovu pomocí kliknutí na tento odkaz %s S pozdravem, tým %s';
$string['institutionmembershipfullsubject'] = 'Bylo dosaženo maximálního počtu uživatelských účtu pro tuto instituci';
$string['institutionmembershipinvitedescription'] = 'Správce vás pozval k připojení se k následujícím institucím.';
$string['institutionmembershipjoindescription'] = 'Máte možnost poslat žádost o připojení se k instituci. Pole "ID instituce" slouží k nastavení studentského ID pro tuto instituci.';
$string['institutionmembershiprequestsdescription'] = 'Požádal jste o připojení k následujícím institucím.';
$string['institutionnotfound'] = 'Instituce "%s" nenalezena';
$string['institutions'] = 'Instituce';
$string['institutiontag'] = 'Štítek instituce';
$string['institutiontagcantbesaved'] = 'Štítek instituce nelze uložit.';
$string['institutiontagdeleted'] = 'Štítek instituce úspěšně smazán.';
$string['institutiontagdeletefail'] = 'Nepodařilo se smazat štítek instituce.';
$string['institutiontagdesc'] = 'Štítky lze vkládat pouze po jednom.';
$string['institutiontags'] = 'Štítky instituce';
$string['institutiontagsaved'] = 'Štítek instituce uložen.';
$string['institutiontagsdescription'] = 'Štítky instituce jsou štítky předdefinované institucí, které jsou k dispozici všem jejím členům. Můžete vytvořit tolik štítků, kolik chcete.';
$string['invalidkeytoken'] = 'Něco se pokazilo. Buď vypršela platnost vašeho potvrzovacího odkazu nebo byl zaslán nový potvrzovací email. Prosím, zkontrolujte nejnovější potvrzovací email nebo zažádejte o nový.';
$string['invalidsesskey'] = 'Neplatný klíč sezení (session key)';
$string['invitedgroup'] = 'skupina nabízející členství';
$string['invitedgroups'] = 'skupiny nabízející členství';
$string['itemdeleted'] = 'Položka byla smazána';
$string['itemstaggedwith'] = 'Položka byla označena štítkem jako "%s"';
$string['itemupdated'] = 'Položka byla upravena';
$string['javascriptnotenabled'] = 'Váš prohlížeč neumožňuje nebo nedovoluje použití Javascriptu na těchto stránkách. Před přihlášením je nutné Javascript povolit.';
$string['joingroups'] = 'Zapojte se do <a href="%s">skupin</a>';
$string['joininstitution'] = 'Stát se členem instituce';
$string['joinsomegroups'] = 'Stát se členem skupin';
$string['language'] = 'Jazyk';
$string['last'] = 'Poslední';
$string['lastjoined'] = 'Naposled připojen';
$string['lastminutes'] = 'Posledních %s minut';
$string['lastname'] = 'Příjmení';
$string['lastnameall'] = 'Všechna příjmení';
$string['lastnamedescription'] = 'Příjmení uživatele';
$string['lastobjection'] = 'Poslední stížnost';
$string['lastobjectionartefact'] = 'Poslední stížnost na "%s"';
$string['lastpage'] = 'Poslední stránka';
$string['lastupdate'] = 'Poslední změna';
$string['lastupdateorcomment'] = 'Poslední změna komentáře';
$string['latest'] = 'Poslední';
$string['leaveinstitution'] = 'Opustit instituci';
$string['legal'] = 'Právní ustanovení';
$string['license'] = 'Licence';
$string['licensedesc'] = 'Licence pro tento obsah.';
$string['licensemandatoryerror'] = 'Pole licence je povinné.';
$string['licensenocustomerror'] = 'Toto není licence povolená na této stránce.';
$string['licensenone'] = 'Nic nevybráno';
$string['licensenone1'] = 'Všechna práva vyhrazena';
$string['licensenonedetailed'] = '%s nezvolil licenci pro tento obsah.';
$string['licensenonedetailed1'] = '© %s, všechna práva vyhrazena';
$string['licensenonedetailedowner'] = 'Nezvolili jste licenci pro tento obsah.';
$string['licenseother'] = 'Jiná licence (zadejte URL)';
$string['licenseotherurl'] = 'Vložte adresu';
$string['licensingadvanced'] = 'Pokročilé licencování';
$string['licensor'] = 'Poskytovatel licence';
$string['licensordesc'] = 'Původní poskytovatel licence pro tento obsah.';
$string['licensorurl'] = 'Původní URL';
$string['licensorurldesc'] = 'Původní URL pro tento obsah.';
$string['link'] = '<a href="%s">%s</a>';
$string['linksandresources'] = 'Odkazy a zdroje';
$string['loading'] = 'Načítám...';
$string['loadingMore'] = 'Načítání dalších výsledků...';
$string['loggedinusersonly'] = 'Povolit přístup pouze pro přihlášené uživatele';
$string['loggedoutok'] = 'Byli jste úspěšně odhlášeni';
$string['login'] = 'Přihlásit se';
$string['loginfailed'] = 'Nezadali jste správné přihlašovací údaje. Ujistěte se, že používáte správné uživatelské jméno a heslo.';
$string['logins'] = 'Přihlašovací jména';
$string['loginto'] = 'Přihlásit se na %s';
$string['logout'] = 'Odhlásit';
$string['lostusernamepassword'] = 'Zapomenuté údaje';
$string['maildisabled'] = 'Email zakázán';
$string['mainmenu'] = 'Hlavní menu';
$string['managespecific'] = 'Spravovat "%s"';
$string['maximumSelected'] = 'Maximum označených položek';
$string['maxitemsperpage'] = 'Maximum položek na stránku:';
$string['maxitemsperpage1'] = 'Výsledků na stránku:';
$string['memberofinstitutions'] = 'Členství v %s';
$string['members'] = 'Členové';
$string['membershipexpiry'] = 'Končí platnost členství';
$string['menu'] = 'Navigace';
$string['message'] = 'Zpráva';
$string['messagesent'] = 'Vaše zpráva byla odeslána';
$string['migrateaccountconfirm'] = 'Potvrďte přesun vašeho účtu do jiné instituce na %s.';
$string['migrateaccountconfirminfo'] = 'Váš účet bude přesunut z "%s" do "%s" a již se nebudete moci přihlásit pomocí vašich starých přihlašovacích údajů. Pokud se pomocí nich pokusíte přihlásit, bude vám vytvořen další účet.';
$string['migrateaccounttoinstitution'] = 'Přesunout účet do jiné instituce';
$string['migratecancelled'] = 'Přesunutí vašeho účtu bylo zrušeno';
$string['migrateemailsubject'] = 'Potvrzení přesunu účtu %s';
$string['migrateemailtext'] = 'Vážený %s, vyjádřil jste zájem o přesunutí vašeho účtu na %s z instituce "%s" do instituce "%s". Prosím, potvrďte tuto akci kliknutím na tento odkaz %s Odkaz je platný po dobu 30 minutes. S pozdravem, tým %s';
$string['migrateemailtext_html'] = '<p>Vážený %s,</p> <p> vyjádřil jste zájem o přesunutí vašeho účtu na %s z instituce "%s" do instituce "%s".Prosím, <a href="%s">potvrďte přesun vašeho účtu</a>.</p> <p>Tento odkaz je platný po dobu 30 minut.</p> <p>S pozdravem,<br> tým %s</p>';
$string['migrateemailtextexistinguser'] = 'Vážený %s, vyjádřil jste zájem o přesunutí vašeho účtu na %s z instituce "%s" do instituce "%s". To však udělat nemůžete, protože existuje jiný účet se stejným uživatelským jménem a údaji o emailové adrese. K vyřešení tohoto problému je potřeba kontaktovat správce instituce "%s": %s S pozdravem, tým %s';
$string['migrateemailtextexistinguser_html'] = '<p>Vážený %s,</p> <p>Vyjádřil jste zájem o přesunutí vašeho účtu na %s z instituce "%s" do instituce "%s".</p> <p> To však udělat nemůžete, protože existuje jiný účet se stejným uživatelským jménem a údaji o emailové adrese. </p> <p>K vyřešení tohoto problému prosím <a href="%s">kontaktujte správce</a> instituce "%s".</p> <p>S pozdravem,<br> tým %s</p>';
$string['migrateinstitutiondescription'] = '<div class="lead">Máte možnost zvolit si novou instituci, ke které se připojíte.</div><p>Tento krok bude vyžadovat vaše přihlašovací údaje a budete přesměrování zpět k %s. Obdržíte email s odkazem pro potvrzení přesunutí vašeho účtu. Prosím, kontrolujte vaši emailovou schránku kvůli této zprávě a po jejím obdržení dokončete přesun vašeho účtu.</p>';
$string['migrateinstitutionpagelink'] = 'Přejděte na stránku <a href="%s">Přesunout účet</a> pro změnu vaší instituce a způsobu přihlášení do %s.';
$string['migratesuccess'] = 'Přesunutí vaše účtu do instituce "%s" proběhlo úspěšně. Prosím, přihlaste se pomocí přihlašovacích údajů pro tuto instituci. Pozor, pokud se přihlásíte do vaší předchozí instituce, vytvoří se tím nový prázdný účet a budete mít účty dva.';
$string['migrationcancelled'] = 'Přesunutí vašeho účtu bylo zrušeno.';
$string['missingparent'] = 'Pro téma "%s" chybí rodičovské téma "%s".';
$string['modified'] = 'Upraveno';
$string['month'] = 'měsíc';
$string['months'] = 'měsíců';
$string['more...'] = 'více...';
$string['moreoptions'] = 'Více možností';
$string['moreoptionsfor'] = 'Více možností pro "%s"';
$string['move'] = 'Přesunout';
$string['moveitemdown'] = 'Přesunout dolů';
$string['moveitemup'] = 'Přesunout nahru';
$string['mustspecifycurrentpassword'] = 'Musíte zadat stávající heslo, aby bylo možné změnit Vaše uživatelské jméno.';
$string['mustspecifyoldpassword'] = 'Musíte zadat stávající heslo.';
$string['mydashboard'] = 'Můj ovládací panel';
$string['myfriends'] = 'Moji přátelé';
$string['mygroups'] = 'Moje skupiny';
$string['mymessages'] = 'Mé zprávy';
$string['myportfolio'] = 'Moje portfolio';
$string['mytags'] = 'Mé štítky';
$string['name'] = 'Jméno';
$string['nameatoz'] = 'Jméno A až Z';
$string['namedfieldempty'] = 'Požadované pole "%s" je prázdné';
$string['nameztoa'] = 'Jméno Z až A';
$string['newpassword'] = 'Nové heslo';
$string['newuserscantpostlinksorimages'] = 'Omlouváme se, ale nově registrovaní uživatelé nemohou  ve svých příspěvcích používat odkazy. Odstraňte je proto prosím ze svého příspěvku.';
$string['newuserscantpostlinksorimages1'] = 'Bohužel, anonymní nebo nově registrovaní uživatelé nemají povoleno zveřejňovat odkazy. Odstraňte prosím ze svého příspěvku všechny odkazy a URL a zkuste to znovu.';
$string['next'] = 'Další';
$string['nextpage'] = 'Další stránka';
$string['nitems'] = array(

		0 => '%s položka', 
		1 => '%s položky', 
		2 => '%s položek'
);
$string['no'] = 'Ne';
$string['noResults'] = 'Nebyly nalezeny žádné výsledky';
$string['nocountryselected'] = 'Žádná země nebyla vybrána';
$string['nodeletepermission'] = 'Nemáte oprávnění odstranit tuto položku portfolia.';
$string['noeditpermission'] = 'Nemáte oprávnění upravovat tuto položku portfolia.';
$string['noenddate'] = 'Bez termínu';
$string['nohelpfound'] = 'Nebyla nalezena žádná nápověda k této položce.';
$string['nohelpfoundpage'] = 'Nebyla nalezena žádná nápověda k této stránce';
$string['noinputnamesupplied'] = 'Vstupní název není k dispozici.';
$string['noinstitutionadminfound'] = 'Žádní správci instituce nebyli nalezeni.';
$string['noinstitutionoldpassemailmessagehtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončil vaše členství v %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.</p> <p><a href="%sforgotpass.php">%sforgotpass.php</a></p> <p>V případě jakýchkoliv dotazů nás neváhejte <a href="%scontact.php">kontaktovat</a>.</p> <p>S pozdravem, %s, správce stránek</p>';
$string['noinstitutionoldpassemailmessagetext'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončil vaše členství v: %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.%sforgotpass.php V případě jakýchkoliv dotazů nás neváhejte scontact.php kontaktovat</a>. S pozdravem, %s, správce stránek';
$string['noinstitutionoldpassemailsubject'] = '%s: Členství v %s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončilo vaše členství v: %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.</p> <p><a href="%sforgotpass.php">%sforgotpass.php</a></p> <p>V případě jakýchkoliv dotazů nás neváhejte <a href="%scontact.php">kontaktovat</a>.</p> <p>S pozdravem, %s, správce stránek</p>';
$string['noinstitutionsetpassemailmessagetext'] = 'Toto je automaticky generovaný e-mail pro: %s

Skončilo vaše členství v: %s

Stránky %s můžete i nadále používat pod svým uživatelským jménem "%s", ale budete si muset nastavit nové heslo.

Přejděte na následující stránku pro získání nového hesla:

%sforgotpass.php?key=%s

V případě jakýchkoliv dotazů nás neváhejte kontaktovat: %scontact.php

S pozdravem, %s, správce stránek';
$string['noinstitutionsetpassemailsubject'] = '%s: Členství v %s';
$string['noinstitutionstafffound'] = 'Žádní zaměstnanci instituce nebyli nalezeni.';
$string['nomigrateoptions'] = 'Žádné možnosti k přesunu.';
$string['none'] = 'Žádné';
$string['noonlineusersfound'] = 'Žádní připojení uživatelé nebyli nalezeni';
$string['nopathfound'] = 'Nebyla nalezena cesta k položce portfolia.';
$string['nopeopleonlinefound'] = 'Nenalezení žádní připojení uživatelé';
$string['noprogressitems'] = 'Pro tuto instituci nebyly nalezeny žádné kroky k dokončení profilu.';
$string['nopublishpermissiononartefact'] = 'Nemáte oprávnění zveřejnit %s.';
$string['norelatedtaggeditemstoview'] = 'Není zde nic k zobrazení se štítkem "%s", vlastníka "%s".';
$string['norelatedtaggeditemstoviewfiltered'] = 'Není zde žádné %s k zobrazení se štítkem "%s", vlastníka "%s".';
$string['noresultsfound'] = 'Nenalezeno';
$string['nosendernamefound'] = 'Nebylo vloženo jméno odesilatele';
$string['nosessionreload'] = 'Pro přihlášení obnovte tuto stránku';
$string['nosuchpasswordrequest'] = 'Nebyla nalezena taková žádost o změnu hesla';
$string['notags'] = 'Žádné štítky pro tuto instituci.';
$string['notifications'] = 'Přehled událostí';
$string['notifyadministrator'] = 'Upozornit správce';
$string['notifyadministratorconfirm'] = 'Jste si jistí, že chcete nahlásit tuto stránku pro pochybný obsah?';
$string['notifyadministratorreview'] = 'Jste si jistí, že je tato stránka připravena na novou kontrolu správce?';
$string['notinstallable'] = 'Nelze instalovat';
$string['notinstalledplugins'] = '<span class="error">Nenainstalované moduly</span>';
$string['notobjectionable'] = 'Nezpochybnitelné';
$string['notphpuploadedfile'] = 'Soubor se v průběhu nahrávaní ztratil. To by se nemělo stát, spojte se s vaším správcem.';
$string['nresults'] = array(

		0 => '%s výsledek', 
		1 => '%s výsledky', 
		2 => '% výsledků'
);
$string['numitems'] = 'Počet položek: %s';
$string['nusers'] = array(

		0 => '1 osoba', 
		1 => '%s osoby', 
		2 => '%s osob'
);
$string['objectionablematerialreported'] = 'Nahlášeno jako nevhodný materál';
$string['objectionablematerialreportedowner'] = 'Někdo nahlásil Vaši stránku jako nevhodný obsah. Prosím, zkontrolujte svou stránku a upravte ji, jak je potřeba.';
$string['objectionablematerialreportedreply'] = 'Klikněte na tlačítko "Zkontrolovat nevhodný obsah", abyste dali správci vědět, že jste již svou stránku upravili nebo pokud se chcete zeptat na upřesnění.';
$string['objectionablematerialreportreplied'] = 'Správce opět překontroloval vaši stránku a stále shledává, že obsahuje nevhodný obsah. Prosím, podívejte se do svých upozornění pro více informací. Poté můžete udělat změny a poslat správci zprávu kliknutím na tlačítko "Zkontrolovat nevhodný obsah" nebo se zeptat na upřesnění.';
$string['objectionnotreviewed'] = 'Stížnost zatím nebyla přezkoumána';
$string['objectionnotreviewedreply'] = 'Reagovat na dosud nepřezkoumanou stížnost: %s';
$string['objectionreview'] = 'Zkontrolovat nevhodný materiál';
$string['objectionreviewonview'] = 'Reagovat na nevhodný materiál lze pouze přes danou stránku';
$string['objectionreviewsent'] = 'Kontrola stížnosti byla odeslána vlastníkovi stránky. Klikněte na "Stále nevhodné", pokud požadujete další úpravy a zaslání nové kontroly.';
$string['off'] = 'Vypnuto';
$string['oldpassword'] = 'Stávající heslo';
$string['on'] = 'Zapnuto';
$string['onlineusers'] = 'Připojení uživatelé';
$string['optionalinstitutionid'] = 'Identifikátor instituce (volitelné)';
$string['organisedescription'] = 'Uspořádejte své portfolio do podoby <a href="%s">stránek</a>. Vytvořte stránky pro různé účely - můžete zvolit, jaké prvky budou obsahovat.';
$string['organisesubtitle'] = 'Předveďte své portfolio';
$string['orientation'] = 'Orientace';
$string['orloginvia'] = 'Nebo se přihlásit přes:';
$string['overview'] = 'Přehled';
$string['password'] = 'Heslo';
$string['passwordchangedok'] = 'Vaše heslo bylo úspěšně změněno.';
$string['passworddescription.ul'] = 'Musí obsahovat velká a malá písmena.';
$string['passworddescription.uln'] = 'Musí obsahovat velká písmena, malá písmena a číslice.';
$string['passworddescription.ulns'] = 'Musí obsahovat velká písmena, malá písmena, číslice a symboly.';
$string['passworddescription1.ul'] = 'Musí obsahovat velká písmena [A-Z] a malá písmena [a-z].';
$string['passworddescription1.uln'] = 'Musí obsahovat velká písmena [A-Z], malá písmena [a-z] a čísla [0-9].';
$string['passworddescription1.ulns'] = 'Musí obsahovat velká písmena [A-Z], malá písmena [a-z], čísla [0-9] a symboly [např. ! . @ # $ & * - _ ].';
$string['passworddescriptionbase'] = 'Minimální délka hesla je %s znaků.';
$string['passwordhelp'] = 'Heslo, které musíte zadat pro přihlášení se na tyto stránky';
$string['passwordnotchanged'] = 'Nezměnili jste heslo. Prosím, zvolte nové heslo.';
$string['passwordresetexpired'] = 'Klíč pro změnu hesla vypršel';
$string['passwordsaved'] = 'Nové heslo bylo uloženo';
$string['passwordsdonotmatch'] = 'Hesla neodpovídají.';
$string['passwordstrength1'] = 'Velmi slabé';
$string['passwordstrength2'] = 'Slabé';
$string['passwordstrength3'] = 'Silné';
$string['passwordstrength4'] = 'Velmi silné';
$string['passwordtooeasy'] = 'Takové heslo je příliš snadné uhádnout! Zvolte si lepší heslo.';
$string['pendingfriend'] = 'čekající žádost o přátelství';
$string['pendingfriends'] = 'čekající žádosti o přátelství';
$string['people'] = 'Lidé';
$string['peopleonline'] = 'Připojení uživatelé';
$string['phpuploaderror'] = 'Vyskytla se chyba při nahrávání souboru: %s (Kód chyby %s).';
$string['phpuploaderror_1'] = 'Velikost souboru přesahuje hodnotu definovanou parametrem upload_max_filesize v souboru php.ini.';
$string['phpuploaderror_2'] = 'Velikost souboru přesahuje hodnotu definovanou parametrem MAX_FILE_SIZE v HTML formuláři.';
$string['phpuploaderror_3'] = 'Soubor byl nahrán pouze zčásti.';
$string['phpuploaderror_4'] = 'Nebyl nahrán žádný soubor.';
$string['phpuploaderror_6'] = 'Chybí dočasný adresář.';
$string['phpuploaderror_7'] = 'Nepodařilo se zapsat soubor na disk. Zkontrolujte volné místo na souborovém systému s uživatelskými daty (a) nebo nastavení proměnné \'upload_tmp_dir\' v PHP.';
$string['phpuploaderror_8'] = 'Nahrávání souboru bylo ukončeno rozšiřujícím modulem.';
$string['pleasedonotreplytothismessage'] = 'Prosíme, neodpovídejte na tuto zprávu.';
$string['pluginbrokenanddisabled'] = 'Uživatel se pokusil načíst zásuvný modul %s, který nemohl být nahrán.
Pro zamezení budoucích chyb byl zásuvný modul zakázán. Vygenerovaná chybová zpráva zásuvného modulu byla: ---------------------------------------------------------------------------- %s ---------------------------------------------------------------------------- Chcete-li znovu povolit plugin, navštivte stránku "Rozšíření".';
$string['pluginbrokenanddisabledtitle'] = 'Poškozený zásuvný modul (%s) byl zakázán';
$string['pluginbrokenanddisabledtitle1'] = 'Rozbitý zásuvný modul "%s" byl zakázán.';
$string['plugindisabled'] = 'Zásuvný modul byl skryt.';
$string['plugindisableduser'] = 'Zásuvný modul %s byl zakázán. Pro jeho zapnutí kontaktujte svého správce.';
$string['pluginenabled'] = 'Zásuvný modul je nyní povolen.';
$string['pluginexplainaddremove'] = 'Zásuvné moduly jsou v Mahaře vždy nainstalované a uživatelé k nim mají přístup, pokud znají příslušnou URL adresu nebo k nim mají přistup. Moduly se nezapínají a nevypínají, spíše se upravuje jejich viditelnost (buď jsou viditelné nebo neviditelné). Viditelnost lze měnit kliknutím na příslušnou volbu (Zobrazit/Skrýt) vedle jejich názvu.';
$string['pluginexplainartefactblocktypes'] = 'Pokud skryjete zásuvný modul typu "artefact", systém Mahara přestane zobrazovat jemu odpovídající modul typu "blocktype".';
$string['pluginnotenabled'] = 'Zásuvný modul je skrytý. Nejprve musíte zásuvný modul %s zviditelnit.';
$string['pluginnotinstallable'] = 'Plugin %s %s není možné nainstalovat:';
$string['plugintype'] = 'Typ zásuvného modulu';
$string['postformresponse'] = 'Na vaši primární emailovou adresu byl zaslán potvrzovací email. Prosím, klikněte na odkaz v něm pro potvrzení vašeho přesunu do nově zvolené instituce.';
$string['posts'] = 'Příspěvky';
$string['preferences'] = 'Základní nastavení';
$string['preferredname'] = 'Zobrazované jméno';
$string['preferrednamedisplay'] = 'Zobrazované jméno';
$string['previous'] = 'Předchozí';
$string['prevpage'] = 'Předchozí stránka';
$string['primaryemailinvalid'] = 'Vaše primární e-mailová adresa není platná.';
$string['privacystatement'] = 'Prohlášení o ochraně osobních údajů';
$string['processing'] = 'Zpracovávám';
$string['profile'] = 'profil';
$string['profilecompleteness'] = 'Úplnost profilu';
$string['profilecompletenesspreview'] = 'Náhled úplnosti profilu';
$string['profilecompletenesstips'] = 'Kroky k dokončení profilu';
$string['profilecompletionforwhichinstitution'] = 'pro';
$string['profiledescription'] = 'Vaše stránka s profilem je to, co vidí ostatní, když kliknou na vaše jméno nebo na ikonu profilu.';
$string['profileicon'] = 'Profilový obrázek';
$string['profileimage'] = 'Obrázek v profilu';
$string['profileimagetext'] = 'Profilový obrázek uživatele %s';
$string['profileimagetextanonymous'] = 'Anonymní obrázek v profilu';
$string['profileimagetexttemplate'] = 'Zde patří uživatelův profilový obrázek';
$string['profilepage'] = 'Profilová stránka';
$string['progressbargenerictask'] = array(

		0 => 'Přidat jeden: %2$s', 
		1 => 'Přidat %d: %s', 
		2 => 'Přidat %d: %s'
);
$string['progresspagedescription'] = 'Stránka o dokončenosti portfolia pro sbírku.';
$string['publishablog'] = 'Pište si <a href="%s">deník</a>';
$string['publishblog'] = 'Publikovat příspěvek';
$string['pwchangerequestsent'] = 'Během krátké chvíle byste měli obdržet e-mail v odkazem na stránku, na které si budete moci změnit vaše heslo.';
$string['pwchangerequestsentfullinfo'] = 'Na váš e-mail byl zaslán odkaz umožňující změnu hesla k vašemu účtu.<br>
Pokud e-mail neobdržíte, zřejmě jste zadali chybné informace o svém účtu nebo používáte k přihlašování na tyto stránky autentizaci pomocí externí aplikace.';
$string['quarantinedirname'] = 'karanténa';
$string['query'] = 'Dotaz';
$string['querydescription'] = 'Slova, která mají být hledána';
$string['quota'] = 'Diskový prostor';
$string['quotausage'] = 'Využíváte <span id="quota_used">%s</span> z přiděleného diskového prostoru <span id="quota_total">%s</span>.';
$string['quotausagegroup'] = 'Skupina využívá <span id="quota_used">%s</span> z přiděleného diskového prostoru <span id="quota_total">%s</span>.';
$string['reallyleaveinstitution'] = 'Opravdu chcete opustit tuto instituci?';
$string['reason'] = 'Důvod';
$string['recentactivity'] = 'Poslední aktivita';
$string['register'] = 'Registrovat';
$string['registeragreeterms'] = 'Musíte také souhlasit s <a href="terms.php">podmínkami</a>.';
$string['registeringdisallowed'] = 'Bohužel, na těchto stránkách se nemůžete sami registrovat.';
$string['registerprivacy'] = 'Údaje, které shromažďujeme, zde budou uloženy v souladu s naším <a href="privacy.php">prohlášením o ochraně soukromí</a>.';
$string['registerprivacy1'] = 'Data, která zde nasbíráme, budou uchována dle našeho ujednání o ochraně soukromí.';
$string['registerstep3fieldsmandatory'] = '<h3>Vyplňte povinná pole uživatelského profilu</h3>

<p>Následující pole jsou povinná. Musíte je vyplnit pro dokončení registrace.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Do profilu můžete vložit váš obrázek</h3>

<p>Zdárně jste se registrovali na stránkách %s. Nyní si můžete zvolit fotografii či obrázek, který bude zobrazován ve vašem profilu.</p>';
$string['registerwelcome'] = 'Vítejte! Chcete-li používat tuto stránku, musíte se nejprve zaregistrovat.';
$string['registrationcomplete'] = 'Děkujeme za registraci na stránkách %s';
$string['registrationnotallowed'] = 'Vámi vybraná instituce neumožňuje, abyste se sami registrovali.';
$string['reject'] = 'Odmítnout';
$string['relatedtags'] = 'Štítkem označený obsah od %s';
$string['relatedtagsinview'] = 'Štítkem označený obsah od %s v portfoliu "%s"';
$string['reloadtoview'] = 'Znovu načíst stránku ke zobrazení';
$string['remotehost'] = 'Vzdálený hostitel %s';
$string['remove'] = 'Odebrat';
$string['removeaccess'] = 'Odebrat přístup';
$string['removeaccessdesc'] = 'Okamžitě zrušit přístup k této stránce (a sbírce, pokud je stránka v nějaké umístěna) dokud nebude nevhodný materiál odstraněn.';
$string['removefooterlinksupgradewarning'] = 'Vaše stránky užívají vlastní odkazy k podmínkám použití těchto stránek nebo k prohlášení o ochraně osobních údajů. Následující odkazy "%s" byly nyní odstraněny. Bude potřeba jejich obsah vložit přímo na tyto stránky do sekce "Administrátorské menu" → "Nastavení stránek" → "Právní ustanovení".';
$string['replyingtoobjection'] = 'Reagování na stížnost:  "%s" %s';
$string['reportobjectionablematerial'] = 'Nahlásit nevhodný obsah';
$string['reportsent'] = 'Vaše oznámení bylo odesláno.';
$string['republish'] = 'Publikovat';
$string['requestmembershipofaninstitution'] = 'Požádat o členství v instituci';
$string['requiredfieldempty'] = 'Požadované pole je prázdné';
$string['restartmigration'] = 'Prosím začněte proces přesunutí účtu znovu na stránce <a href="%s">“vyberte instituci”</a>';
$string['result'] = 'výsledek';
$string['results'] = 'výsledky';
$string['resultsperpage'] = 'Výsledků na stránku';
$string['returntosite'] = 'Zpět na titulní stránku';
$string['reviewcomplaint'] = 'Kontrola stížnosti';
$string['reviewcomplaintdesc'] = 'Vložte zprávu, ve které autorovi sdělíte více podrobnosti ohledně toho, co by měl změnit. Pokud toto pole necháte prázdné, odešle se místo této zprávy původní stížnost.';
$string['reviewnotification'] = 'Správci bylo odesláno upozornění, aby zrevidoval portfolio a zkontroloval, zda stále obsahuje nevhodný materiál.';
$string['reviewnotificationdesc'] = 'Prosím zkontrolujte, zda nevhodný materiál stále existuje.';
$string['reviewrequestsent'] = 'Vaše žádost o kontrolu byla odeslána.';
$string['samepage'] = 'Stejná stránka';
$string['save'] = 'Uložit';
$string['scroll_to_top'] = 'Zpět nahoru';
$string['search'] = 'Vyhledat';
$string['searching'] = 'Vyhledávání...';
$string['searchresultsfor'] = 'Výsledky hledání';
$string['searchtype'] = 'Typ vyhledávání';
$string['searchusers'] = 'Vyhledat uživatele';
$string['searchusers1'] = 'Hledat lidi';
$string['searchwithin'] = 'Hledat v rámci';
$string['select'] = 'Označit';
$string['selectall'] = 'Označit vše';
$string['selectatagtoedit'] = 'Vyberte šítek k úpravě';
$string['selected'] = 'vybrané';
$string['selectnone'] = 'Odznačit vše';
$string['selfmigrate'] = 'Přesunout účet k jiné instituci';
$string['selfmigration'] = 'Přesunutí účtu';
$string['selfsearch'] = 'Prohledat moje portfolio';
$string['send'] = 'Poslat';
$string['senddeletenotification'] = 'Poslat žádost';
$string['sendmessage'] = 'Poslat zprávu';
$string['sendrequest'] = 'Poslat žádost';
$string['sessiontimedout'] = 'Vaše přihlášení vypršelo. Pro pokračování musíte znovu zadat své přihlašovací údaje.';
$string['sessiontimedoutpublic'] = 'Vaše přihlášení vypršelo. Pro pokračování se musíte <a href="%s">přihlásit</a>.';
$string['sessiontimedoutreload'] = 'Vaše přihlášení vypršelo. Obnovte stránku pro opětovné přihlášení.';
$string['setblocktitle'] = 'Nastavit název bloku';
$string['settings'] = 'Nastavení';
$string['settingssaved'] = 'Nastavení uloženo';
$string['settingssavefailed'] = 'nastavení se nepodařilo uložit';
$string['settingsspecific'] = 'Nastavení pro "%s"';
$string['share'] = 'Sdílet';
$string['sharedetail'] = 'Sdílejte své úspěchy a rozvoj takovou formou, která je blízká vám';
$string['sharenetwork'] = 'Sdílet a síťovat';
$string['sharenetworkdescription'] = 'Můžete jemně doladit, kdo bude mít přístup ke každému vaší stránce a na jak dlouho.';
$string['sharenetworksubtitle'] = 'Pozvěte přátele a přidejte se do skupin';
$string['sharesubtitle'] = 'Mějte pod kontrolou své soukromí';
$string['show'] = 'Zobrazit';
$string['showadminmenu'] = 'Zobrazit administrátorské menu';
$string['showmainmenu'] = 'Zobrazit hlavní menu';
$string['showmenu'] = 'Zobrazit menu pro %s';
$string['showmore'] = 'Zobrazit více';
$string['showsearch'] = 'Zobrazit vyhledávání';
$string['showtags'] = 'Zobrazit moje štítky';
$string['showusermenu'] = 'Zobrazit uživatelské menu';
$string['showusermenu1'] = 'Zobrazit uživatelské menu';
$string['siteadministration'] = 'Správa stránek';
$string['siteclosed'] = 'Stránky jsou momentálně uzavřeny z důvodu upgrade databáze. Přihlásit se mohou pouze správci.';
$string['siteclosedlogindisabled'] = 'Stránky jsou momentálně uzavřeny z důvodu upgrade databáze. <a href="%s">Spustit upgrade.</a>';
$string['sitecontentnotfound'] = 'Text "%s" není dostupný';
$string['siteinformation'] = 'Informace o stránkách';
$string['sizeb'] = 'B';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'kB';
$string['sizemb'] = 'MB';
$string['skipmenu'] = 'Přeskočit na hlavní obsah';
$string['sortalpha'] = 'Seřadit štítky podle abecedy';
$string['sortby'] = 'Řadit dle:';
$string['sortedby'] = 'řazeno dle:';
$string['sortfreq'] = 'Seřadit štítky podle frekvence používání';
$string['sortorder'] = 'Řazení souborů';
$string['sortresultsby'] = 'Seřadit výsledky podle:';
$string['spamtrap'] = 'Past na spam';
$string['staffofinstitutions'] = 'Zaměstnanci z %s';
$string['status'] = 'Stav';
$string['stillobjectionable'] = 'Stále nevhodné';
$string['strftimenotspecified'] = 'Neurčeno';
$string['studentid'] = 'Identifikátor';
$string['style'] = 'Styl (CSS)';
$string['styleguide_description'] = 'Tato příručka popisuje všechny druhy komponent použité v Mahaře. Všechny komponenty jsou dostupné na jednom místě, takže můžete snadno zkontrolovat, jestli jste všem nastavili správný styl při vytváření nového tématu nebo zásuvného modulu. Zvolte jiné téma stránek pro zjištění, jak jednotlivé komponenty vypadají.';
$string['styleguide_title'] = 'Příručka stylování';
$string['subject'] = 'Předmět';
$string['submit'] = 'Odeslat';
$string['supportadminofinstitutions'] = 'Pomocný správce pro %s';
$string['system'] = 'Systém';
$string['tab'] = 'panel';
$string['tabgroup'] = 'Skupina';
$string['tabinstitution'] = 'Instituce';
$string['tabs'] = 'Panely';
$string['tag'] = 'Štítek';
$string['tagdeletedsuccessfully'] = 'Štítek byl úspěšně odstraněn';
$string['tagfilter_all'] = 'Vše';
$string['tagfilter_blog'] = 'Deník';
$string['tagfilter_blogpost'] = 'Příspěvek v deníku';
$string['tagfilter_collection'] = 'Kolekce';
$string['tagfilter_external'] = 'Externí';
$string['tagfilter_file'] = 'Soubory';
$string['tagfilter_image'] = 'Obrázky';
$string['tagfilter_plan'] = 'Plán';
$string['tagfilter_resume'] = 'Životopis';
$string['tagfilter_task'] = 'Plánovaný úkol';
$string['tagfilter_text'] = 'Text';
$string['tagfilter_view'] = 'Stránky';
$string['tags'] = 'Štítky';
$string['tagsdesc'] = 'Vložte seznam štítků (oddělených čárkami) pro tuto položku.';
$string['tagsdescblock'] = 'Hledat/vložit štítky pro tento blok.';
$string['tagsdescprofile'] = 'Vložte seznam štítků (oddělených čárkami) pro tuto položku. Položky nesoucí štítek "profile" budou zobrazeny v postranním bloku.';
$string['tagupdatedsuccessfully'] = 'Štítky byly úspěšně aktualizovány';
$string['termsandconditions'] = 'Podmínky používání těchto stránek';
$string['theme'] = 'Motiv';
$string['thereasonis'] = 'Uživatelův důvod je:';
$string['thisistheprofilepagefor'] = 'Toto je stránka s profilem uživatele %s';
$string['timelapsestringhour'] = array(

		0 => 'před %2$s hodinou a %s minutou', 
		1 => 'před %2$s hodinami a %s minutami', 
		2 => 'před %2$s hodinami a %s minutami'
);
$string['timelapsestringhours'] = array(

		0 => 'před %2$s hodinami a %s minutou', 
		1 => 'před %2$s hodinami a %s minutami', 
		2 => 'před %2$s hodinami a %s minutami'
);
$string['timelapsestringminute'] = array(

		0 => 'před %s minutou', 
		1 => 'před %s minutami', 
		2 => 'před %s minutami'
);
$string['timelapsestringseconds'] = array(

		0 => 'před %s sekundou', 
		1 => 'před %s sekundami', 
		2 => 'před %s sekundami'
);
$string['timesused'] = 'Počet užití';
$string['toggletoolbarsoff'] = 'Vypnout nástrojové lišty a zobrazit základní tlačítka.';
$string['toggletoolbarson'] = 'Zapnout nástrojové lišty a zobrazit kompletní seznam tlačítek.';
$string['topicsimfollowing'] = 'Sledovaná témata';
$string['true'] = 'Pravda';
$string['units'] = 'Jednotky';
$string['unknownerror'] = 'Vyskytla se neznámá chyba (0x20f91a0)';
$string['unread'] = '%s nepřečteno';
$string['unreadmessage'] = 'nepřečtená zpráva';
$string['unreadmessages'] = 'nepřečtených zpráv';
$string['update'] = 'Upravit';
$string['updatefailed'] = 'Úprava selhala';
$string['updateprofile'] = 'Upravit profil';
$string['updateyourprofile'] = 'Aktualizujte svůj <a href="%s">profil</a>';
$string['upload'] = 'Nahrát';
$string['uploadedfiletoobig'] = 'Soubor je příliš velký. Požádejte o pomoc vašeho správce.';
$string['uploadedfiletoobig1'] = 'Soubor překročil maximální velikost nahrávání, která je %s. Prosím, zvolte k nahrání menší soubor.';
$string['uploadfiles'] = 'Nahrát soubory';
$string['uploadyourfiles'] = 'Nahrajte své <a href="%s">soubory</a>';
$string['url'] = 'Adresa obrázku';
$string['usedtagscantbedeleted'] = 'Používané štítky nelze smazat';
$string['usermenu'] = 'Uživatelské menu';
$string['usermenu1'] = 'Uživatelské menu';
$string['username'] = 'Uživatelské jméno';
$string['usernamehelp'] = 'Uživatelské jméno, pod kterým se přihlašujete na tyto stránky.';
$string['users'] = 'Uživatelé';
$string['usersdashboard'] = 'Ovládací panel uživatele %s';
$string['usersprofile'] = 'Profil uživatele %s';
$string['version.'] = 'v.';
$string['view'] = 'Stránka';
$string['viewartefact'] = 'Zobrazit';
$string['viewartefactdatavuamodal'] = 'Data %s položky "%s" nelze takto již zobrazit. Prosím, přejděte pro jejich zobrazení na stránku "%s".';
$string['viewmyprofilepage'] = 'Zobrazit stránku s profilem';
$string['views'] = 'Stránky';
$string['viewtags'] = 'Štítky stránky';
$string['virusfounduser'] = 'Soubor %s je infikován počítačovým virem - není možné jej nahrát!';
$string['virusrepeatmessage'] = 'Uživatel %s se pokusil nahrát několik souborů infikovaných počítačovým virem.';
$string['virusrepeatsubject'] = 'Upozornění: %s opakovaně nahrává zavirované soubory.';
$string['vspace'] = 'Vertikální prostor';
$string['wanttoleavewithoutsaving?'] = 'Provedli jste změny - skutečně si přejete opustit stránku bez uložení?';
$string['weeks'] = 'týdnů';
$string['width'] = 'Šířka';
$string['widthshort'] = 'š';
$string['year'] = 'rok';
$string['years'] = 'roků';
$string['yes'] = 'Ano';
$string['youareamemberof'] = 'Jste členem %s.';
$string['youaremasqueradingas'] = 'Vydáváte se za uživatele %s.';
$string['youhavebeeninvitedtojoin'] = 'Bylo vám nabídnuto členství v %s.';
$string['youhavenottaggedanythingyet'] = 'Zatím jste štítkem nic neoznačili';
$string['youhaverequestedmembershipof'] = 'Požádal jste o členství v %s.';
$string['youraccounthasbeensuspended'] = 'Váš účet byl pozastaven.';
$string['youraccounthasbeensuspendedreasontext'] = 'Váš účet na stránkách %s byl pozastaven uživatelem %s. Důvod: %s.';
$string['youraccounthasbeensuspendedreasontextcron'] = 'Váš účet %s byl pozastaven. Důvod: %s';
$string['youraccounthasbeensuspendedtext2'] = 'Váš účet na stránkách %s byl pozastaven uživatelem %s.';
$string['youraccounthasbeensuspendedtext3'] = 'Váš účet na %s byl pozastaven z důvodu Vašeho odmítnutí souhlasit s %s.';
$string['youraccounthasbeensuspendedtextcron'] = 'Váš účet %s byl pozastaven.';
$string['youraccounthasbeenunsuspended'] = 'Pozastavení platnosti vašeho účtu bylo zrušeno.';
$string['youraccounthasbeenunsuspendedtext2'] = 'Pozastavení platnosti vašeho účtu na stránkách %s bylo zrušeno. Můžete se opět přihlásit.';
$string['yournewpassword'] = 'Nové heslo. Heslo musí být dlouhé alespoň šest znaků a nesmí se shodovat s vaším uživatelským jménem. Rozlišují se malá a velká písmena.<br/> Pro zvýšení bezpečnosti zvažte vyplnění přístupové fráze, resp. věty, která může být např. vaším oblíbeným citátem nebo seznamem vašich dvou (či více!] oblíbených věcí oddělených mezerami.';
$string['yournewpassword1'] = 'Vaše nové heslo. Heslo musí být aspoň %s znaků dlouhé. U hesel se rozlišují malá a velká písmena a musí se lišit od vašeho uživatelského jména. %s<br/> Pro lepší zabezpečení zvažte užití fráze - tím myslíme, abyste si za heslo zvolili spíše větu nežli jedno slovo. Můžete použít například oblíbený citát nebo vyjmenovat několik vašich oblíbených věcí za sebou a oddělit je mezerami.';
$string['yournewpasswordagain'] = 'Nové heslo (znovu)';
