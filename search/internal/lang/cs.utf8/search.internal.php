<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['exactusersearch'] = 'Přesné vyhledávání uživatelů';
$string['exactusersearchdescription'] = 'Je-li zaškrtnuto, výsledky vyhledávání na stránkách "Vyhledat uživatele" a "Vyhledat přátele" zahrnují pouze uživatele, jejichž profilová pole odpovídají celým hledaným výrazům.';
$string['exactusersearchdescription1'] = 'Pokud je zapnuto, výsledky vyhledávání uživatelů a přátel vrací jen výsledky přesně odpovídající zadaným kritériím.';
$string['exactusersearchdescription2'] = 'Jako výsledky hledání z pole "Najít uživatele" a stránky "Najít přátele" jsou zobrazeni pouze ti uživatelé, jejichž profil se přesně shoduje s hledanými výrazy.';
$string['exactusersearchdescription3'] = 'Výsledky ze sekce "Hledat uživatele" a ze stánky "Najít lidi" obsahují pouze uživatele, jejichž pole v profilu odpovídají všem podmínkám vyhledávání.';
$string['exactusersearchdescription4'] = 'Výsledky z pole "Vyhledat uživatele" a stránky "Lidé" jsou vráceny pouze pro lidi, jejichž pole v profilu souhlasí s kompletním zadáním vyhledávání.';
$string['pluginname'] = 'Interní';
