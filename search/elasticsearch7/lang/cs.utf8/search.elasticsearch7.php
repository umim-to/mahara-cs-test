<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Group'] = 'Skupina';
$string['Media'] = 'Média';
$string['Portfolio'] = 'Portfolio';
$string['Text'] = 'Text';
$string['Users'] = 'Lidé';
$string['admin'] = 'Správce';
$string['all'] = 'Vše';
$string['analyzer'] = 'Elisaticsearch analyzér';
$string['analyzerdescription'] = 'Třída Elasticsearch analyzéru, která má být použita. Výchozí je "%s".';
$string['artefacttypes'] = 'Typy položek';
$string['artefacttypesdescription'] = 'Zatrhněte typy položek, které chcete zahrnout do indexu. Pouze ty typy artefaktů, které mají definovanou hierarchii, jsou platné. Aby se projevily změny, proveďte reset položek ve frontě.';
$string['artefacttypesmap'] = 'Hierarchie typů položek';
$string['artefacttypesmapdescription'] = 'Zadejte hierarchii pro každý typ položky, oddělte je pomocí znaku | (jeden typ položky na řádek)';
$string['atoz'] = 'Od A do Z';
$string['blogpost'] = 'Příspěvek v deníku';
$string['bypassindexname'] = 'Bypass index';
$string['bypassindexnamedescription'] = 'Pokud je uveden, Mahara načte data do tohoto indexu namísto hlavního indexu (volitelné).';
$string['clicheckingsearchsucceededmessage'] = 'Ve frontě Elasticsearch 7 nejsou žádné nezpracované nebo neúspěšné záznamy.';
$string['cligetfailedqueuesizemessage'] = 'Ve frontě Elasticsearch 7 jsou neúspěšné záznamy starší více jak jednu hodinu.';
$string['cliisqueueolderthanmessage'] = array(

		0 => 'Ve frontě Elasticsearch 7 jsou nezpracované záznamy starší než 1 hodinu.', 
		1 => 'Ve frontě Elasticsearch 7 jsou nezpracované záznamy starší než %s hodiny.', 
		2 => 'Ve frontě Elasticsearch 7 jsou nezpracované záznamy starší než %s hodin.'
);
$string['clusterconfig'] = 'Nastavení klastru';
$string['clusterstatus'] = 'Stav klastru: %s';
$string['collection'] = 'Sbírka';
$string['confignotset'] = '(nenastaveno)';
$string['connectionerror'] = 'Chyba připojení';
$string['contains'] = 'Obsah';
$string['createdby'] = 'Vytvořil %s';
$string['cronlimit'] = 'Limit cronových záznamů';
$string['cronlimitdescription'] = 'Maximální počet záznamů, které mohou být přesunuty z fronty Elasticsearch serveru při každém z běhů cronu (nechte prázdé nebo zadejte 0 pro ponechání bez limitu).';
$string['cronstatedescription'] = 'Toto vám umožňuje zapnout nebo vypnout indexování položek ve frontě během běhu cronu.';
$string['cronstatetitle'] = 'Indexování na cronu';
$string['dateoldestfirst'] = 'Datum (od nejstaršího)';
$string['daterecentfirst'] = 'Datum (od nejnovějšího)';
$string['deleted'] = 'Smazané';
$string['deletedforumpost'] = 'Smazané příspěvky z fóra';
$string['document'] = 'Dokument';
$string['error'] = 'Chyba:';
$string['errorunknown'] = 'Neznámá chyba';
$string['filterresultsby'] = 'Filtrovat výsledky podle';
$string['forum'] = 'Fórum';
$string['forumpost'] = 'Příspěvek ve fóru';
$string['forumpostedby'] = '%s na %s';
$string['forumpostedbylabel'] = 'Poslal/a';
$string['forumtopic'] = 'Téma fóra';
$string['host'] = 'Hostitel';
$string['hostdescription'] = 'Název hostitele Elasticsearch serveru. Výchozí je %s.';
$string['indexingpassword'] = 'Autentizační heslo pro zápis';
$string['indexingpassworddescription'] = 'Heslo, které se předává Elasticsearchi pro zápis do indexu prostřednictvím základního ověřování HTTP, pokud se liší od hesla pro čtení z indexu (volitelné).';
$string['indexingrunning'] = 'Cron právě provádí indexaci. Zkuste to prosím později.';
$string['indexingrunningtry'] = 'Pokud víte, že cron ve skutečnosti nebeží, podívejte se na ./mashsearch-reset-cron-lock';
$string['indexingusername'] = 'Autentizační uživatelské jméno pro zápis';
$string['indexingusernamedescription'] = 'Uživatelské jméno, které se předává Elasticsearchi pro zápis do indexu prostřednictvím základního ověřování HTTP, pokud se liší od uživatelského jména pro čtení z indexu (volitelné).';
$string['indexname'] = 'Název indexu';
$string['indexnamedescription'] = 'Název Elasticsearch indexu. Výchozí je “%s”.';
$string['indexstatusbad'] = 'Stav indexu (%s):%s';
$string['indexstatusunknown'] = 'Aktuální index “%s” je v neznámém stavu z důvodu HTTP odpovědi: “%s”.';
$string['license'] = 'Licence';
$string['monitorfailedqueuesize'] = 'Počet neúspěšných záznamů za více než jednu hodinu';
$string['monitorqueuehasolditems'] = array(

		0 => 'Nezpracované záznamy ve frontě Elasticsearch 7 starší než %s hodinu.', 
		1 => 'Nezpracované záznamy ve frontě Elasticsearch 7 starší než %s hodiny.', 
		2 => 'Nezpracované záznamy ve frontě Elasticsearch 7 starší než %s hodin.'
);
$string['monitorqueuestatus'] = 'Stav Elasticsearch 7 fronty';
$string['monitorsubnavtitle'] = 'Elasticsearch 7';
$string['monitorunprocessedqueuesize'] = 'Celkový počet nezpracovaných záznamů';
$string['none'] = 'žádné';
$string['noticeenabled'] = 'Zásuvný modul Elasticsearch 7 je aktuálně zapnutý. Abyste jej deaktivovali, zrušte jeho výběr v <a href="%s" class="elasticsearch-status">"Nastavení vyhledávání"</a>.';
$string['noticenotactive'] = 'Server Elasticsearch 7 není dostupný přes hostitele %s a port %s. Prosím, ujistěte se, že je v provozu.';
$string['noticenotenabled'] = 'Zásuvný modul Elasticsearch 7 není aktuálně zapnutý. Abyste jej aktivovali, vyberte jej v <a href="%s">"Nastavení vyhledávání"</a>.';
$string['owner'] = 'Vlastník';
$string['page'] = 'Stránka';
$string['pages'] = 'Stránky';
$string['pagetitle'] = 'Vyhledávání';
$string['password'] = 'Autentifikační heslo';
$string['passworddescription'] = 'Heslo pro Elasticsearch 7, které má být předáno přes základní HTTP autentifikaci (nepovinné)';
$string['passwordlength'] = '(délka hesla: %s)';
$string['pluginstatus'] = 'Stav modulu: %s';
$string['pluginstatusmessageindex404'] = 'Index měl být v této chvíli již vytvořen, ale nestalo se tak. Zkuste znovunačtení stránky nebo zkontrolujte váš Elsaticsearch 7 server.';
$string['pluginstatustitleaccess'] = 'Přístup serveru';
$string['pluginstatustitlecluster_health'] = 'Zdraví klastru';
$string['pluginstatustitlecron'] = 'Cron';
$string['pluginstatustitleindexstatus'] = 'Stav indexu';
$string['pluginstatustitleserver_health'] = 'Zdraví serveru';
$string['port'] = 'Port pro Elasticsearch';
$string['portdescription'] = 'Port, na kterém kontaktovat Elasticsearch. Výchozím je %s.';
$string['record'] = 'záznam';
$string['records'] = 'záznamy';
$string['relevance'] = 'Důležitost';
$string['replicashards'] = 'Kopie částí';
$string['replicashardsdescription'] = 'Počet kopií částí, které mají být vytvořeny.  Poznámka: Pokud máte pouze jeden uzel, nastavte počet kopií na 0.';
$string['reset'] = 'Obnovit';
$string['resetallindexes'] = 'Obnovit VŠECHNY indexy';
$string['resetdescription'] = 'Tato tabulka ukazuje počet záznamů každého typu, které jsou aktuálně ve frontě k zaslání Elasticsearch serveru. Prvky jsou zasílány Elasticsearch serveru pokaždé, když běží úloha cronu vyhledávacího modulu (každých 5 minut). Klikněte na tlačítko dole pro resetování vyhledávacího indexu, smazání všech záznamů a jejich znovuuspořádání do fronty.';
$string['resetitemsinindex'] = 'V indexu';
$string['resetitemsinqueue'] = 'Ve frontě';
$string['resetlegend'] = 'Reset indexu';
$string['resettype'] = 'Typ';
$string['resume'] = 'Životopis';
$string['scheme'] = 'Schéma';
$string['schemedescription'] = 'Schéma Elasticsearch 7 serveru. Výchozí je %s.';
$string['servererror'] = 'Problém s připojováním k serveru "%s"';
$string['shards'] = 'Části';
$string['shardsdescription'] = 'Počet částí indexu, které mají být vytvořeny.';
$string['sortby'] = 'Seřadit dle';
$string['systemmessage'] = 'Systémová zpráva';
$string['tags'] = 'Značky';
$string['tagsonly'] = 'Pouze značky';
$string['types'] = 'Elasticsearch typy';
$string['typesdescription'] = 'Čárkami oddělený výčet prvků k indexaci. Výchozí je "%s".';
$string['unassignedshards'] = 'Nepřidělené části: %s';
$string['usedonpage'] = 'Použito na stránce';
$string['usedonpages'] = 'Použito na stránkách';
$string['username'] = 'Ověřovací uživatelské jméno';
$string['usernamedescription'] = 'Uživatelské jméno, které se předává Elasticsearchi 7 prostřednictvím základního ověřování HTTP (volitelné).';
$string['xsearchresults'] = 'Zobrazuji %s výsledků hledání';
$string['xsearchresultsfory'] = 'Zobrazuji %s výsledků hledání pro "%s"';
$string['ztoa'] = 'Od Z do A';
