<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Group'] = 'Skupina';
$string['Media'] = 'Média';
$string['Portfolio'] = 'Portfolio';
$string['Text'] = 'Text';
$string['Users'] = 'Uživatelé';
$string['admin'] = 'Správce';
$string['all'] = 'Všichni';
$string['analyzer'] = 'Analyzátor Elasticsearch';
$string['analyzerdescription'] = 'Používaná třída pro Analyzátor Elasticsearch. Výchozí hodnota je mahara_analyzer.';
$string['artefacttypedescription'] = 'Vyberte položky portfolia, které chcete zahrnout do indexu. Platné jsou jen ty položky portfolia, které mají definovanou hierarchii. Aby se změny projevily, bude třeba znovu zařadit všechny položky portfolia čekající frontě.';
$string['artefacttypelegend'] = 'Typy položek portfolia';
$string['artefacttypemapdescription'] = 'Zadejte hierarchii pro každý typ položky portfolia oddělené | (jednoho typ portfolia na řádku).';
$string['artefacttypemaplegend'] = 'Hierarchie typů položek portfolia';
$string['atoz'] = 'A až Z';
$string['blog'] = 'Deník';
$string['blogpost'] = 'Příspěvek v deníku';
$string['bypassindexname'] = 'Obejít index';
$string['bypassindexnamedescription'] = '(Volitelné) Pokud je povoleno, Mahara načte data do tohoto indexu namísto hlavního indexu.';
$string['clusterstatus'] = 'Objevil se problém s klastrem Elasticsearch. Jejich aktuální stav je "%s", přičemž "%s" částí je nepřiřazených.';
$string['collection'] = 'Sbírka';
$string['confignotset'] = '(není nastaveno)';
$string['contains'] = 'Obsahuje';
$string['createdby'] = 'Vytvořeno uživatelem %s';
$string['createdbyanon'] = 'Vytvořeno (skrytý autor)';
$string['cronlimit'] = 'Omezení počtu záznamů';
$string['cronlimitdescription'] = 'Maximální počet záznamů, které mají být předány z fronty na server Elasticsearch při každém běhu cronu (prázdné nebo 0 až nekonečno).';
$string['dateoldestfirst'] = 'Datum (od nejstarších)';
$string['daterecentfirst'] = 'Datum (od nejnovějších)';
$string['deleted'] = 'Odstraněno';
$string['deletedforumpost'] = 'Odstraněný diskusní příspěvek';
$string['document'] = 'Dokument';
$string['elasticsearchtooold'] = 'Vaše verze Elasticsearch %s je příliš stará. Je potřeba mít nainstalovánu %s nebo vyšší.';
$string['filterresultsby'] = 'Filtrování výsledků podle';
$string['forum'] = 'Fórum';
$string['forumpost'] = 'Příspěvek ve fóru';
$string['forumpostedby'] = '%s na %s';
$string['forumpostedbylabel'] = 'Přidáno uživatelem';
$string['forumtopic'] = 'Téma fóra';
$string['host'] = 'Hostitel';
$string['hostdescription'] = 'Jméno hostitele serveru Elasticsearch. Výchozí hodnota je 127.0.0.1.';
$string['html'] = 'Text';
$string['indexingpassword'] = 'Heslo pro zápis';
$string['indexingpassworddescription'] = '(Volitelné) Heslo, které bude předáno Elasticsearch přes HTTP Basic auth pro zápis do indexu, liší-li se nějak od toho, jaké se používá pro jeho čtení';
$string['indexingrunning'] = 'Probíhá indexovací proces cronu. Prosím, opakujte pokus za pár minut.';
$string['indexingusername'] = 'Uživatelské jméno pro zápis';
$string['indexingusernamedescription'] = '(Volitelné) Uživatelské jméno, které bude předáno Elasticsearch přes HTTP Basic auth pro zápis do indexu, liší-li se nějak od toho, jaké se používá pro jeho čtení';
$string['indexname'] = 'Název indexu';
$string['indexnamedescription'] = 'Název pro index serveru Elasticsearch. Výchozí je mahara.';
$string['indexstatusbad'] = 'Aktuální index "%s" má status "%s" a bude potřeba jej upravit.';
$string['indexstatusok'] = 'Aktuální index "%s" má status "zelená". Elasticsearch běží.';
$string['indexstatusunknown'] = 'Aktuální index "%s" má status neznámý dle HTTP odpovědi "%s".';
$string['license'] = 'Licence';
$string['newindextype'] = 'Nový druh indexu "%s" byl přidán do nastavení elasticsearch. Pro jeho palikaci je třeba reindexovat stránky.';
$string['newversion'] = 'Nová verze Elasticsearch PHP %s byla přidána do Mahary, která je kompatibilní s Elasticsearch serverem %s a vyššími. Aby se toto projevilo, bude potřeba  zvou naindexovat vaši síť.';
$string['none'] = 'nic';
$string['noticeenabled'] = 'Zásuvný modul Elasticsearch je v tuto chvíli povolen. Pro jeho vypnutí ho zrušte v Nastavení stránek: <a href="%s">Nastavení vyhledávání</a>.';
$string['noticenotactive'] = 'Server Elasticsearch na adrese %s a portu %s není dostupný. Zkontrolujte prosím jeho funkčnost.';
$string['noticenotenabled'] = 'Zásuvný modul Elasticsearch není v tuto chvíli povolen. Pro jeho zapnutí jej vyberte v Nastavení stránek: <a href="%s">Nastavení vyhledávání</a>.';
$string['noticepostgresrequired'] = 'Zásuvný modul Elasticsearch funguje v tomto okamžiku pouze s databází PostgreSQL.';
$string['noticepostgresrequiredtitle'] = 'Funkce není k dispozici';
$string['nrecords'] = array(

		0 => '%s záznam', 
		1 => '%s záznamy', 
		2 => '%s záznamů'
);
$string['owner'] = 'Vlastník';
$string['page'] = 'Stránka';
$string['pages'] = 'Stránky';
$string['pagetitle'] = 'Hledat';
$string['password'] = 'Heslo';
$string['passworddescription'] = '(Volitelné) Heslo, které bude předáno ElasticSearch přes HTTP Basic auth';
$string['passwordlength'] = '(délka hesla: %s)';
$string['port'] = 'Port Elasticsearch';
$string['portdescription'] = 'Port serveru Elasticsearch. Výchozí hodnota je 9200.';
$string['record'] = 'záznam';
$string['records'] = 'záznamy';
$string['relevance'] = 'Důležitost';
$string['replicashards'] = 'Kopie částí';
$string['replicashardsdescription'] = 'Počet kopií částí, které mají být vytvořeny.  Poznámka: Pokud máte pouze jeden uzel, nastavte počet kopií na 0.';
$string['reset'] = 'Obnovit';
$string['resetallindexes'] = 'Obnovit VŠECHNY indexy';
$string['resetdescription'] = 'Tato tabulka ukazuje počet záznamů všech typů, které v současné době čekají ve frontě na odeslání na server Elasticsearch. Položky jsou na server odesílány při každém běhu v cronu (v pětiminutových intervalech). Kliknutím na tlačítko v dolní části dojde k obnovení indexu vyhledávání, odstranění stávajících záznamů a jejich opětovnému zařazení do fronty.';
$string['resetlegend'] = 'Obnovit index';
$string['resume'] = 'Životopis';
$string['scheme'] = 'Schéma';
$string['schemedescription'] = 'Schéma Elasticsearch serveru. Výchozí je http';
$string['servererror'] = 'Problém s připojením k serveru: "%s"';
$string['shards'] = 'Části';
$string['shardsdescription'] = 'Počet kusů (částí) indexu, které mají být vytvořeny.';
$string['sortby'] = 'Seřadit podle';
$string['tags'] = 'Štítky';
$string['tagsonly'] = 'Pouze štítky';
$string['types'] = 'Druhy Elasticsearch';
$string['typesdescription'] = 'Čárkami oddělený seznam prvků do indexu. Výchozí hodnota jsou usr,interaction_instance,interaction_forum_post,group,view,artefact.';
$string['usedonpage'] = 'Používá se na stránce';
$string['usedonpages'] = 'Používá se na stránkách';
$string['username'] = 'Uživatelské jméno';
$string['usernamedescription'] = '(Volitelné) Uživatelské jméno, které bude předáno Elasticsearch přes HTTP Basic auth';
$string['wallpost'] = 'Příspěvek na zdi';
$string['xsearchresults'] = '%s výsledky vyhledávání';
$string['xsearchresultsfory'] = '%s výsledky vyhledávání pro %s';
$string['ztoa'] = 'Z až A';
