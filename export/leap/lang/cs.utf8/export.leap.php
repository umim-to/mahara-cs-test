<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['description'] = 'Předá vám export ve <a href="http://wiki.leapspecs.org/2A/specification">standardním formátu Leap2A</a>.

Později ho můžete použít pro import vašich dat do <a href="http://wiki.mahara.org/index.php?title=Developer_Area/Import%2F%2FExport/Interoperability/">jiných, Leap2A kompatibilních systémů</a>, i když je export obtížně čitelný lidem.';
$string['description1'] = 'Zde máte možnost exportu ve <a href="https://web.archive.org/web/20140912151415/http://wiki.leapspecs.org/2A/specification">standradním Leap2A formátu</a>. Tento formát můžete později použít k importu dat v <a href="https://wiki.mahara.org/wiki/Developer_Area/Import//Export/Interoperability">jiných Leap2A kompatibilních systémech</a>, ačkoli je formát pro člověka těžko čitelný.';
$string['title'] = 'Leap2A';
