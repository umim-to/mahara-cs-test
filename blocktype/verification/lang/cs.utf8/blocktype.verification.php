<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['addcomment'] = 'Přidat posudkový komentář';
$string['addcommentchecklist'] = 'Po zveřejnění vašeho vyjádření jej nebude možné vzít zpět.';
$string['addcommentchecklistlocking'] = 'Po zveřejnění vašeho vyjádření, - autor portfolia již nebude moci ve svém portfoliu udělat jakékoli změny. - autor portfolia nebude moci přidat dalšího posuzovatele. - nebudete moci vzít vaše vyjádření zpět.';
$string['addcommentchecklistlockingnames'] = 'Po zveřejnění vašeho vyjádření, - autor portfolia již nebude moci ve svém portfoliu udělat jakékoli změny. - autor portfolia nebude moci přidat dalšího posuzovatele. - nebudete moci vzít vaše vyjádření zpět. Pokud byste si to přál(a) udělat, kontaktujte jednoho z těchto lidí: %s s žádostí, aby vaše vyjádření vzali zpět přes tlačítko "Více možností".';
$string['addcommentchecklistnames'] = 'Po zveřejnění vašeho vyjádření ho již nebudete moci vzít zpět. Pokud byste si to přál(a) udělat, kontaktujte jednoho z těchto lidí: %s s žádostí, aby vaše vyjádření vzali zpět přes tlačítko "Více možností".';
$string['addcommentdescription'] = 'Když povolíte komentáře, posuzovatel má k dispozici textové pole pro přidání svého vlastního vyjádření. To může být použito i s výchozím vyjádřením. Vyjádření formou komentáře může být přidáno bez ohledu na to, zda je portfolio uzamčeno nebo ne.';
$string['addcommentdescriptionhtml'] = '<div><strong>Uložit koncept:</strong> Váš komentář vidíte pouze vy. Stále můžete provádět změny.<br> <strong>Zveřejnit:</strong> Všichni s přístupem k tomuto portfoliu vidí váš komentář. Zveřejněný komentář již není možné vrátit do stavu konceptu.</div>';
$string['addcommentsuccess'] = 'Přidání komentáře k "%s" proběhlo úspěšně.';
$string['addcommentsuccessdraft'] = 'Přidání rozpracovaného komentáře k "%s" proběhlo úspěšně.';
$string['availabilitydate'] = 'Datum dostuposti';
$string['availabilitydatedescription'] = 'Zvolte datum, od kterého má být dostupná možnost vyjádření. Před tímto datem uvidí posuzovatel upozornění, které informuje, že nemůže provést kontrolu před uvedeným datem. Toto datum je také zmíněno v upozornění na přístup. Pokud žádné datum nezvolíte, vyjádření bude možné přidat kdykoli.';
$string['availabilitydatemessage'] = 'Nemůžete zatím schválit vyjádření nebo přidávat komentáře (pokud jsou povoleny). Bude to možné po %s.';
$string['availableto'] = 'Dostupné do';
$string['availabletodescription'] = 'Zvolte roli nebo role, které musí posuzovatel mít, aby mohl schvalovat vyjádření. Pokud necháte toto pole prázdné, nebude potřeba žádná speciální role a každý, kdo má k portfoliu přístup, bude moci schvalovat vyjádření nebo poskytnout komentář. Role se nekumulují.';
$string['blockcontent'] = 'Text vyjádření';
$string['blockcontentdescription'] = 'Napište text vyjádření, které chcete schválit. Posuzovateli se zobrazí zaškrtávací políčko pro potvrzení tohoto vyjádření. Pokud necháte toto pole prázdné, povolte pole komentářů, aby posuzovatelé mohli napsat své vlastní vyjádření.';
$string['blockneedscollection'] = 'Tento typ bloku musí být umístěn na stánku dokončenosti portfolia ve sbírce, aby se zobrazoval správně.';
$string['cancel'] = 'Zrušit';
$string['commentformplaceholder'] = '<i>Zde se zobrazí formulář na posudkový komentář, jakmile si tuto stránku zobrazí někdo, kdo k ní může psát komentáře. </i>';
$string['description'] = 'Zobrazení podrobností o posudku';
$string['displayverifiername'] = 'Zobrazovat jméno posuzovatele';
$string['displayverifiernamedescription'] = 'Rozhodněte, zda chcete zobrazovat jméno posuzovatele, když potvrdí vyjádření. Vždy však bude zobrazena časová značka.';
$string['groupadmin'] = 'Správce skupiny';
$string['groupmember'] = 'Člen skupiny';
$string['grouptutor'] = 'Učitel skupiny';
$string['lockportfolio'] = 'Zamknout portfolio';
$string['lockportfoliodescription'] = 'Rozhodněte, zda má být portfolio uzamčeno po potvrzení vyjádření. Pokud je vyjádření více, portfolio se zamkne po potvrzení prvního, které povoluje zamčení. Vyjádření formou komentářů (pokud je takto zvoleno) mohou být nadále vkládána.';
$string['notification'] = 'Upozornění autorovi';
$string['notificationdescription'] = 'Zaslat autorovi upozornění, pokud bylo potvrzeno vyjádření a / nebo byl přidán komentář.';
$string['primarystatement'] = 'Hlavní vyjádření';
$string['primarystatementdescription'] = 'Pokud označíte své vyjádření jako vaše hlavní vyjádření, bude na stránce zobrazeno výrazněji. Pokud již na stránce hlavní vyjádření je, nemůžete vložit další.';
$string['publish'] = 'Zveřejnit';
$string['resetstatement'] = 'Odebrání vyjádření';
$string['resetstatementdescription'] = 'Zvolte, jakou roli či role musí člověk mít, aby mohl vyjádření odebrat. Pokud není vybrána žádná role, nikdo nemůže vyjádření odebírat.';
$string['savedraft'] = 'Uložit koncept';
$string['textdescription'] = 'Napište text vašeho vyjádření. Posuzovatel pak uvidí zaškrtávací pole k jeho schválení. Pokud necháte toto pole prázdné, povolte pole komentářů, aby posuzovatelé mohli napsat své vlastní vyjádření.';
$string['title'] = 'Posudek portfolia';
$string['toverifyspecific'] = 'Posudek "%s"';
$string['unverify'] = 'Pokud budete pokračovat, tento posudek bude odstraněn.';
$string['verificationchecklist'] = '<p>Po potvrzení tohoto vyjádření své rozhodnutí nebudete moci vzít zpět.</p>';
$string['verificationchecklistlocking'] = '<p>Po potvrzení tohoto vyjádření,</p> <ul> <li>autor portfolia již nebude moci ve svém portfoliu udělat jakékoli změny.</li> <li>autor portfolia nebude moci přidat dalšího posuzovatele.</li> <li>nebudete moci vzít své rozhodnutí zpět.</li> </ul>';
$string['verificationchecklistlockingnames'] = '<p>Po potvrzení tohoto vyjádření,</p> <ul> <li>autor portfolia již nebude moci ve svém portfoliu udělat jakékoli změny.</li> <li>autor portfolia nebude moci přidat dalšího posuzovatele.</li> <li>nebudete moci vzít své rozhodnutí zpět. Pokud byste si to přál(a) udělat, kontaktujte jednoho z těchto lidí: %s s žádostí, aby vaše vyjádření vzali zpět přes tlačítko "Více možností".</li> </ul>';
$string['verificationchecklistnames'] = '<p>Po potvrzení tohoto vyjádření nebudete moci vzít své rozhodnutí zpět. Pokud byste si to přál(a) udělat, kontaktujte jednoho z těchto lidí: %s s žádostí, aby vaše vyjádření vzali zpět přes tlačítko "Více možností".</p>';
$string['verificationmodaltitle'] = 'Posudek: %s';
$string['verifiedon'] = 'Posudek na %s';
$string['verifiedonby'] = '<a href="%s">%s</a> na %s';
$string['verifiedspecific'] = '"%s" je posuzováno';
$string['verifycomment'] = 'Přidat komentář';
$string['verifymessage'] = '%s schválil(a) následující vyjádření k %s: %s';
$string['verifymessagenoname'] = 'Následující vyjádření bylo schváleno k %s: %s';
$string['verifymessagesubject'] = '%s schválil(a) vyjádření k vašemu portfoliu';
$string['verifymessagesubjectnoname'] = 'Bylo schváleno vyjádření k vašemu portfoliu:';
