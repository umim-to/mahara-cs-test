<?php

defined('INTERNAL') || die();  

$string['description'] = 'Zobrazuje vaše přátele';
$string['numberoffriends'] = '(%s z %s)';
$string['otherusertitle'] = 'Přátelé uživatele %s';
$string['title'] = 'Moji přátelé';

