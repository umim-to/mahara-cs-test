<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['blockcontent'] = 'Obsah bloku';
$string['convertdescription'] = array(

		0 => 'K dispozici je %s poznámka, kterou je možné převést. Pokud se rozhodnete tuto poznámku převést, může to trvat delší dobu. Po dokončení se na této stránce zobrazí informace o úspěšné konverzi.', 
		1 => 'K dispozici jsou %s poznámky, které je možné převést. Pokud se rozhodnete tyto poznámky převést, může to trvat delší dobu. Po dokončení se na této stránce zobrazí informace o úspěšné konverzi.', 
		2 => 'K dispozici je %s poznámek, které je možné převést. Pokud se rozhodnete tyto poznámky převést, může to trvat delší dobu. Po dokončení se na této stránce zobrazí informace o úspěšné konverzi.'
);
$string['convertdescriptionfeatures'] = 'Můžete převést všechny znovupoužitelné bloky "poznámek", které nepoužívají žádné pokročilé funkce, do bloků prostého "textu". Tyto bloky zůstanou jen na stránce, kde byly vytvořeny, a nemůžou být použity na dalších stránkách.
Pokročilé funkce zahrnují:
<ul> <li>opakované použití v jiném bloku</li> <li>použití licence</li> <li>použití štítků</li> <li>přílohy</li> <li>zpětnou vazbu na zobrazené poznámkové položce portfolia</li> </ul>';
$string['convertibleokmessage'] = array(

		0 => 'Jeden blok "poznámka" byl úspěšně převeden na blok "text".', 
		1 => '%d bloky "poznámka" byly úspěšně převedeny na bloky "text".', 
		2 => '%d bloků "poznámka" bylo úspěšně převedeno na bloky "text".'
);
$string['description'] = 'Přidat text na stránku';
$string['optionlegend'] = 'Převést bloky "poznámek"';
$string['switchdescription'] = 'Převést všechny bloky "poznámek" nepoužívající pokročilé funkce na bloky prostého "textu"';
$string['title'] = 'Text';
