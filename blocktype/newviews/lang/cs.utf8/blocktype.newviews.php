<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Jestliže necháte pole pro titulek prázdné, bude vygenerován výchozí';
$string['description'] = 'Zobrazit naposledy aktualizované stránky';
$string['description1'] = 'Vypíše naposledy aktualizované stránky, ke kterým máte přístup na tomto webu';
$string['description2'] = 'Vypsat stránky a sbírky, ke kterým máte přístup a byly v poslední době upraveny.';
$string['title'] = 'Poslední stránky';
$string['title1'] = 'Poslední zobrazené změny';
$string['title2'] = 'Portfolia sdílená se mnou';
$string['viewstoshow'] = 'Maximální počet stránek ke zobrazení';
$string['viewstoshow1'] = 'Maximální množství výsledků k zobrazení';
$string['viewstoshowdescription'] = 'Mezi 1 a 100';
