<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['additionalfilters'] = 'Další filtry a nastavení';
$string['defaulttitledescription'] = 'Pokud necháte pole prázdné, bude vygenerován výchozí název.';
$string['description'] = 'Zobrazí stránky ve vašem seznamu sledovaných položek';
$string['filterby'] = 'Filtrovat podle času';
$string['filterby.2months'] = 'Poslední dva měsíce';
$string['filterby.half'] = 'Posledních šest měsíců';
$string['filterby.login'] = 'Od posledního přihlášení';
$string['filterby.month'] = 'Poslední měsíc';
$string['filterby.quarter'] = 'Poslední tři měsíce';
$string['filterby.week'] = 'Poslední týden';
$string['filterby.year'] = 'Poslední rok';
$string['filterbydesc'] = 'Vyberte z jakého časového období chcete zobrazit aktivity.';
$string['itemstoshow'] = 'Položky ke zobrazení';
$string['list.follower'] = 'Aktivity mých přátel';
$string['list.watchlist'] = 'Stránky, které sleduji';
$string['noactivities'] = 'Za toto časové období není žádný záznam.';
$string['nopages'] = 'V seznamu sledovaných položek nejsou žádné stránky';
$string['orderby'] = 'Seřadit podle';
$string['orderby.activity'] = 'Zpětné chronologické řazení';
$string['orderby.owner'] = 'Vlastník stránky';
$string['orderbydesc'] = 'Vyberte zobrazení stránek ve zpětném chronologické řazení nebo seřazené dle vlastníka stránek.';
$string['title'] = 'Moje sledované stránky';
$string['title1'] = 'Stránky, které sleduji';
$string['typetoshow'] = 'Zobrazit';
$string['typetoshowdesc'] = 'Vyberte, zda chcete na své hlavní stránce zobrazit vámi sledované stránky nebo portfolia vašich přátel.';
$string['watchlistdelaydescription'] = 'Zpoždění v minutách před rozesláním upozornění ohledně změn ve sledovaných položkách.';
$string['watchlistdelaytitle'] = 'Zpoždění zaslání upozornění na sledované položky';
$string['watchlistnotification'] = 'Upozornění na sledované položky';
