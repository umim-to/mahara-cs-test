<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['contenttypes'] = 'Druhy obsahu';
$string['defaulttitledescription'] = 'Pokud toto pole necháte prázdné, bude vygenerován výchozí nadpis.';
$string['description'] = 'Dočasný blok, který umožňuje vybrat si, jakým typem bloku se má stát';
$string['placeholderblocktypenotenabled'] = 'Typ bloku "Dočasný blok" musí být nainstalován a aktivní. Pokud je již nainstalován, prosím nastavte jeho stav na "active" (aktivní) v tabulce "blocktype_installed" v databázi.';
$string['placeholdertext'] = 'Pro zvolení typu bloku přejděte do jeho nastavení.';
$string['placeholdertext1'] = 'Prosím, pro výběr typu tohoto bloku přejděte do úpravy bloku.';
$string['title'] = 'Dočasný blok';
