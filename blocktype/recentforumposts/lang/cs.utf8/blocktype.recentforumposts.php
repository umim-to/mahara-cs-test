<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2015
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Není-li zde uveden titulek, bude použit název fóra.';
$string['description'] = 'Zobrazit poslední příspěvky fóra pro skupinu';
$string['group'] = 'Skupina';
$string['nogroupstochoosefrom'] = 'Je nám líto, nejsou k dispozici žádné skupiny k výběru';
$string['poststoshow'] = 'Maximální počet příspěvků ke zobrazení';
$string['poststoshowdescription'] = 'Mezi 1 a 100';
$string['recentforumpostsforgroup'] = 'Poslední příspěvky fóra pro %s';
$string['title'] = 'Poslední příspěvky fóra';
