<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['alphabetical'] = 'Od A do Z';
$string['description'] = 'Zobrazuje seznam skupin, do nichž patříte';
$string['earliest'] = 'První přidaní';
$string['latest'] = 'Poslední přidaní';
$string['limitto1'] = 'Maximální počet skupin k zobrazení';
$string['limittodesc'] = 'Maximální počet skupin k zobrazení v bloku. Nechte prázdné pro zobrazení všech vašich skupin.';
$string['limittodescsideblock1'] = 'Maximální počet skupin k zobrazení v seznamu "Mé skupiny" v postranní liště. Nechte prázdné pro zobrazení všech vašich skupin.';
$string['numberofmygroupsshowing'] = 'Zobrazeno %s z %s skupin';
$string['numberofmygroupsshowingearliest'] = 'Zobrazeno %s nejstarších z %s skupin';
$string['numberofmygroupsshowinglatest'] = 'Zobrazeno %s nejnovějších z %s skupin';
$string['otherusertitle'] = 'Skupiny uživatele %s';
$string['sortgroups'] = 'Seřadit skupiny';
$string['title'] = 'Moje skupiny';
