<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['description'] = 'Zobrazuje přehrávač videa';
$string['embedly'] = 'Embedly';
$string['enableservices'] = 'Žádný, %spovolte služby pro vkládání obsahu%s';
$string['glogster'] = 'Glogster';
$string['googlevideo'] = 'Google Video';
$string['height'] = 'Výška';
$string['invalidurl'] = 'Neplatná adresa URL';
$string['invalidurlorembed'] = 'Neplatné URL nebo kód vložení.';
$string['prezi'] = 'Prezi';
$string['scivee'] = 'SciVee';
$string['slideshare'] = 'SlideShare';
$string['teachertube'] = 'TeacherTube';
$string['title'] = 'Externí zdroj';
$string['urlorembedcode'] = 'URL nebo kód pro vložení (embed)';
$string['validembedservices'] = 'Jsou podporovány následující <strong>služby pro vkládání obsahu</strong>:';
$string['validiframesites'] = '<strong>Embed kód</strong> obsahující tagy &lt;iframe&gt;  je povolen z těchto stránek:';
$string['validurlsites'] = '<strong>URL</strong> jsou povoleny z těchto stránek:';
$string['videourl'] = 'URL adresa videa';
$string['videourldescription2'] = 'Překopírujte sem HTML kód pro vložení videa nebo adresu URL ze stránek, kde je video uloženo. Můžete vkládat videa z následujících stránek:';
$string['videourldescription3'] = 'Vložit <strong>embed kód</strong> nebo <strong>URL</strong> stránky, kde je obsah umístěn';
$string['vimeo'] = 'Vimeo';
$string['voicethread'] = 'VoiceThread';
$string['voki'] = 'Voki';
$string['width'] = 'Šířka';
$string['widthheightdescription'] = 'Pole výška a šířka jsou použita pouze u obsahů vkládaných přes URL. Pokud jste vložili embed kód nebo iframe, je nutné upravit výšku a šířku přímo v tomto kódu.';
$string['wikieducator'] = 'WikiEducator';
$string['youtube'] = 'YouTube';
