<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['badgedetails'] = 'Podrobné informace o odznaku';
$string['badgrpassword'] = 'Heslo Badgr';
$string['badgrsourcemissing1'] = 'Badgr chybí v konfiguraci zdrojů ve vašem souboru config.php.';
$string['badgrtoken'] = 'Token pro Badgr: %s';
$string['badgrtokenadded'] = 'Badgr token přidán k účtu';
$string['badgrtokendeleted'] = 'Badgr token smazán';
$string['badgrtokennotfound'] = 'Badgr token podle poskytnutých údajů nenalezen';
$string['badgrtokentitle'] = 'Badgr';
$string['badgrusername'] = 'Uživatelské jméno Badgr';
$string['confighelp'] = 'Zvolte sbírky odznaků k zobrazení v tomto bloku.<br/>Přejděte na následující služby pro správu sbírek a odznaků:<br/>%s';
$string['criteria'] = 'Kritéria';
$string['deprecatedhost'] = '<p class="alert alert-warning">Odznaky z následující služeb se nezobrazují, protože byly ukončeny: %s</p>';
$string['desc'] = 'Popis';
$string['description'] = 'Zobrazit otevřené odznaky';
$string['evidence'] = 'Evidence';
$string['expires'] = 'Vyprší';
$string['featuredisabled'] = 'Typ bloku openbadgedisplayer není aktivní';
$string['fetchingbadges'] = 'Vyhledávání záznamů. Tato akce může chvíli trvat.';
$string['issuancedetails'] = 'Podrobnosti o vydání';
$string['issuedon'] = 'Vydáno';
$string['issuerdetails'] = 'Podrobnosti o vydavateli';
$string['missingbadgesources'] = 'Chybějící nastavení zdrojů. Přidejte je do souboru config.php, např. <br><br>$cfg->openbadgedisplayer_source = \'{"backpack":"https://backpack.provider.org"}\'';
$string['name'] = 'Název';
$string['nbadges'] = array(

		0 => 'Jeden odznak', 
		1 => '%s odznaky', 
		2 => '%s odznaků'
);
$string['nobackpack'] = 'Nebyl nalezen žádný batoh.<br>Prosím, přidejte e-mailovou adresu svého <a href="%s">batohu</a> do svého <a href="%s">profilu</a>.';
$string['nobackpackidin'] = 'Váš email nebyl nalezen ve službě %s.';
$string['nobackpackidin1'] = 'Váš e-mail %s nebyl nalezen ve službě %s.';
$string['nobadgegroups'] = 'Nebyly nalezeny žádné veřejné odznaky / sbírky odznaků.';
$string['nobadgegroupsin'] = 'Ve službě %s nebyly nalezeny žádné odznaky / sbírky odznaků.';
$string['nobadgegroupsin1'] = 'Nebyly nalezeny žádné veřejné odznaky nebo sbírky odznaků ve službě %s pro e-mail %s.';
$string['nobadgesselectone'] = 'Nebyly vybrány žádné odznaky';
$string['nobadgruid1'] = 'Než budete používat Badgr, je potřeba nastavit pro něj token. Prosím, přejděte do "Uživatelské menu → Nastavení → Aplikace → Badgr" pro jeho nastavení.';
$string['nobadgruid2'] = 'Před použitím "Badgr" je nutné pro něj nastavit token. Prosím, nastavte jej přes " Uživatelské menu → Nastavení → Připojené aplikace → Badgr".';
$string['obppublicbadges'] = 'Všechny veřejné odznaky v Open Badge Passport';
$string['organization'] = 'Organizace';
$string['resetobsoletebadgrtokenmessage1'] = 'Dobrý den, %s,
váš aktuální Badgr token užívaný v %s je zastaralý. Tento token se používá k zobrazování vašich Badgr odznaků na stránkách portfolií. Prosím, běžte do "Uživatelské menu  → Nastavení→ Připojené aplikace → Badgr" a nastavte nový.';
$string['resetobsoletebadgrtokensubject'] = 'Musíte znovu nastavit váš Badgr token.';
$string['selectall'] = 'Označit vše';
$string['selectnone'] = 'Odznačit vše';
$string['title'] = 'Otevřené odznaky';
$string['title_backpack'] = 'Mozilla Backpack';
$string['title_badgr'] = 'Badgr Backpack';
$string['title_passport'] = 'Open Badge Passport';
$string['url'] = 'adresa';
