<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['blocktitleforowner'] = 'Dokončenost kurzu pro %s';
$string['completeconfiguration'] = 'Pro aktivaci tohoto bloku dokončete jeho nastavení.';
$string['completeconfiguration1'] = 'Nejsou k dispozici žádné informace o kurzu. Prosím, pro zobrazení dokončenosti kurzu upravte tento blok.';
$string['completeconfigurationnotpossible'] = 'Je třeba vytvořit připojení webové služby instituce pro tento blok. Prosím, požádejte správce těchto webových stránek, aby ho nastavil.';
$string['completedondate'] = 'Dokončeno';
$string['connectedwithexternalaccount'] = 'Nalezen externí účet';
$string['connectionresultsinvalid'] = 'Nelze načíst výsledky z externího zdroje';
$string['course'] = 'kurz';
$string['coursecompletion_function_title'] = 'Externí funkce pro dokončenost kurzu';
$string['courses'] = 'kurzy';
$string['coursesresultsfrom'] = 'Nalezeny kurzy od %s';
$string['coursesresultsfromto'] = 'Nalezeny kurzy mezi %s a %s';
$string['coursesresultsto'] = 'Nalezeny kurzy do %s';
$string['coursetype'] = 'Typ kurzu';
$string['dateoutofsync'] = 'Toto musí být starší než datum "Do".';
$string['defaulttitledescription'] = 'Pokud necháte prázdné, bude vygenerován výchozí název';
$string['description'] = 'Zobrazit informace o dokončenosti kurzu z externího zdroje';
$string['externaluserid'] = 'ID externího účtu';
$string['fromdate'] = 'Datum od';
$string['fromdatedescription'] = 'Zobrazuje pouze kurzy započaté po tomto datu. Použijte formát %s';
$string['hours'] = 'Hodiny';
$string['name'] = 'Informace o kurzu';
$string['ncourses'] = array(

		0 => '%s kurz', 
		1 => '%s kurzy', 
		2 => '%s kurzů'
);
$string['nocourses'] = 'Žádné informace o kurzu k zobrazení';
$string['nocourses1'] = 'Zatím nebyly nastaveny žádné informace o kurzu.';
$string['novalidconnectionauthtype'] = 'Neplatný typ webové služby. Musí užívat typ "REST".';
$string['novalidconnections'] = 'Žádné platné připojené objekty.';
$string['organisation'] = 'Organizace';
$string['placeholdermessage'] = 'Tento blok musí být plně nastaven než bude použit. Plně nastavit jej lze jen v případě, že je na stránce osobního portfolia.';
$string['plugininfo'] = '<p>Pro zobrazení informací o dokončenosti kurzů osoby z externích internetových stránek musíte nastavit následující:</p> <ol> <li>Mít aktivní rozšíření (zásuvný modul) \'blocktype/courseinfo\'.</li> <li>Mít zapnuto \'Přijímat odchozí požadavky webových služeb\' ve \'Administrátorské menu → Webové služby → Konfigurace\' a mít aktivní protokol \'REST\'.</li> <li>Jít do \'Administrátorské menu → Webové služby → Správce připojení\' a zvolit instituci, pro kterou chcete spojení vytvořit. Pak zvolit možnost \'PluginBlocktypeCourseinfo:courseinfo - Dokončenost kurzu\' z rozevírací nabídky a kliknout na \'Přidat\'.</li> <li>Vyplnit formulář následujícím způsobem: <ul> <li>Jméno: Uveďte jméno instance, např. \'Instituce A: Moodle\'.</li> <li>Připojení povoleno: Nastavte na \'Ano\'.</li> <li>Typ webové služby: Zvolte \'REST\'.</li> <li>Typ autentifikace: Vyberte \'Token\'.</li> <li>URL webové služby: Vložte URL adresu REST serveru externího zdroje , např. https://moodle/webservice/rest/server.php.</li> <li>Token: Nastavte na token vygenerovaný na straně externí služby, která má přístup k externím funkcím vyžadovaným zde.</li> <li>Stálé parametry k předání: Vložte jakékoli speciální parametry, potřebné pro to, aby URL adresa fungovala, například pro Moodle je potřeba přidat \'moodlewsrestformat=json\'.</li> <li>Kodování JSON: Nastavte na \'Ano\'.</li> <li>Externí funkce pro ID účtu: Toto nastavte na funkci externí služby, která umí vrátit ID na základě emailové adresy, např. pro Moodle \'core_user_get_users_by_field\'.</li> <li>Externí funkce pro dokončenost kurzu: Nastavte na funkci externí služby, která umí vrátit informace o dokončenosti kurzu na základě poskytnutého ID, např. pro Moodle vlastní funkce jako je \'local_client_get_course_completion_data\'.</li> </ul> <li>Uložte formulář.</li> </ol> <p>Nyní, když osoba vloží na svou stránku blok \'Dokončenost kurzu\', bude zjištěno externí ID jejího účtu, uloží se pro tento blok, a při zobrazení dané stránky se načtou dokončené kurzy pro toto ID účtu.</p>';
$string['title'] = 'Dokončenost kurzu';
$string['todate'] = 'Datum do';
$string['todatedescription'] = 'Zobrazí kurzy pouze započaté před tímto datem. Použijte formát %s';
$string['totalhours'] = 'Celkem hodin';
$string['unabletofetchdata'] = 'Nelze dodat data tohoto vlastníka stránky';
$string['userid_function_title'] = 'Externí funkce pro ID účtu';
