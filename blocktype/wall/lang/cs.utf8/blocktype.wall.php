<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2016
 *
 */

defined('INTERNAL') || die();

$string['Post'] = 'Přidat vzkaz';
$string['addpostsuccess'] = 'Příspěvek byl úspěšně přidán.';
$string['backtoprofile'] = 'Zpět k profilu';
$string['delete'] = 'odstranit vzkaz';
$string['deletepost'] = 'Odstranit vzkaz';
$string['deletepostsuccess'] = 'Odstranění vzkazu proběhlo úspěšně';
$string['deletepostsure'] = 'Opravdu chcete provést tuto akci? Akce je nenávratná.';
$string['description'] = 'Zobrazuje oblast, kde vám mohou uživatelé nechávat vzkazy';
$string['makeyourpostprivate'] = 'Jedná se o soukromý vzkaz?';
$string['maxcharacters'] = 'Maximálně %s znaků';
$string['newwallpostnotificationmessage'] = '%s';
$string['newwallpostnotificationsubject'] = 'Nový příspěvek na zdi';
$string['noposts'] = 'Žádné vzkazy k zobrazení';
$string['otherusertitle'] = 'Zeď uživatele %s';
$string['postsizelimit'] = 'Omezení počtu znaků vzkazu';
$string['postsizelimitdescription'] = 'Zde můžete omezit počet znaků nových vzkazů na zdi. Již vložené vzkazy nebudou zkráceny.';
$string['postsizelimitinvalid'] = 'Neplatné číslo';
$string['postsizelimitmaxcharacters'] = 'Maximální počet znaků';
$string['postsizelimittoosmall'] = 'Nemůže být menší než nula.';
$string['posttextrequired'] = 'Toto pole je povinné.';
$string['reply'] = 'odpovědět';
$string['sorrymaxcharacters'] = 'Bohužel, vzkazy nemohou být delší než %s znaků.';
$string['title'] = 'Zeď';
$string['typewallpost'] = 'Příspěvek na zdi';
$string['viewwall'] = 'Zobrazit zeď';
$string['wall'] = 'Zeď';
$string['wallpostprivate'] = 'Tento vzkaz na zdi je soukromý';
$string['wholewall'] = 'Zobrazit celou zeď';
