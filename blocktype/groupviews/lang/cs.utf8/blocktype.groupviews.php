<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2017
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Není-li zde uveden titulek, bude použit název skupiny.';
$string['description'] = 'Zobrazit stránky sdílené se skupinou';
$string['description1'] = 'Zobrazit stránky a sbírky související s touto skupinou';
$string['displaygroupviews'] = 'Zobrazit stránky skupiny?';
$string['displaygroupviews1'] = 'Řadit skupinové stránky a sbírky podle';
$string['displaygroupviewsdesc'] = 'Stránky skupiny - stránky vytvořené ve skupině';
$string['displaygroupviewsdesc1'] = 'Zobrazit seznam stránek a sbírek vytvořených v této skupině.';
$string['displaysharedcollections'] = 'Zobrazit sdílené sbírky';
$string['displaysharedcollectionsdesc'] = 'Zobrazit seznam sbírek sdílených s touto skupinou.';
$string['displaysharedviews'] = 'Zobrazit sdílené stránky?';
$string['displaysharedviewsdesc'] = 'Sdílené stránky - stránky sdílené členy skupiny z jejich individuálních portfolií';
$string['displaysharedviewsdesc1'] = 'Zobrazit seznam stránek sdílených s touto skupinou (vyjma stránek ve sbírkách).';
$string['displaysubmissions'] = 'Zobrazit odevzdané stránky a sbírky';
$string['displaysubmissionsdesc'] = 'Zobrazit seznam stránek a sbírek odevzdaných do této skupiny.';
$string['itemstoshow'] = 'Záznamů na stránku';
$string['itemstoshowdesc'] = 'Počet stránek nebo sbírek zobrazených v každé sekci. Maximum: 100';
$string['showbyanybody'] = 'Kýmkoli';
$string['showbygroupmembers'] = 'Členy skupiny';
$string['shownone'] = 'Nic';
$string['sortgroupviewstitle'] = 'Řadit skupinové stránky podle';
$string['sortgroupviewstitle1'] = 'Řadit skupinové stránky a sbírky podle';
$string['sortsharedviewstitle'] = 'Řadit sdílené stránky a sbírky podle';
$string['sortsubmittedtitle'] = 'Řadit odevzdané stránky a sbírky podle';
$string['sortviewsbyalphabetical'] = 'abecedy';
$string['sortviewsbylastupdate'] = 'poslední úpravy';
$string['sortviewsbytimesubmitted'] = 'naposledy odevzdaných';
$string['title'] = 'Skupinové stránky';
$string['title1'] = 'Skupinová portfolia';
