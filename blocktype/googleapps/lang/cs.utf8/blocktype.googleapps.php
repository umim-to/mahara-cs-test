<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2020
 *
 */

defined('INTERNAL') || die();

$string['appscodeorurl'] = 'Vložit kód nebo URL';
$string['appscodeorurldesc'] = 'Vložit kód nebo URL stránky, která je v Google Apps veřejně přístupná.';
$string['appscodeorurldesc1'] = 'Podívejte se do <a href="http://manual.mahara.org/en/%s/blocks/external.html#google-apps">uživatelské příručky (anglicky)</a> pro návod k zobrazení vloženého obsahu z Google.<br> Vložte zdrojový kód nebo odkaz na veřejně přístupný obsah z Google Apps.';
$string['appscodeorurldesc2'] = 'Podívejte se do <a href="https://manual.mahara.org/en/%s/blocks/external.html#google-apps">manuálu Mahary</a> na instrukce, jak vložit obsah z Google. <br> Vložte příslušný kód nebo URL s odkazem na stránku, kde jsou Google Apps veřejně dostupné.';
$string['badurlerror'] = 'Nelze analyzovat vložený kód nebo URL adresu: %s';
$string['description'] = 'Vložit Google kalendář nebo dokumenty';
$string['height'] = 'Výška';
$string['title'] = 'Google Apps';
