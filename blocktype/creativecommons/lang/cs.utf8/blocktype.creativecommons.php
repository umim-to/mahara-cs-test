<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2015
 *
 */

defined('INTERNAL') || die();

$string['alttext'] = 'Licence Creative Commons';
$string['blockcontent'] = 'Obsah bloku';
$string['by'] = 'Uvést autora';
$string['by-nc'] = 'Uvést autora - Nekomerční použití';
$string['by-nc-nd'] = 'Uvést autora - Nekomerční použití - Nezasahovat do díla';
$string['by-nc-sa'] = 'Uvést autora - Nekomerční použití - Zachovat licenci';
$string['by-nd'] = 'Uvést autora - Nezasahovat do díla';
$string['by-sa'] = 'Uvést autora - Zachovat licenci';
$string['cclicensename'] = 'Creative Commons %s %s Unported';
$string['cclicensestatement'] = '%s od %s je licencováno pod licencí %s';
$string['config:noderivatives'] = 'Povolit úpravy vaší práce?';
$string['config:noncommercial'] = 'Povolit komerční využití vaší práce?';
$string['config:sharealike'] = 'Ano, pokud upravené dílo bude k dispozici pod stejnou (nebo slučitelnou) licencí';
$string['config:version'] = 'Verze licence';
$string['description'] = 'Přidat licenci Creative Commons na vaši stránku';
$string['otherpermissions'] = 'Oprávnění nad rámec této licence může udělit %s.';
$string['sealalttext'] = 'Tato licence je přijatelná pro Svobodná kulturní díla.';
$string['title'] = 'Licence Creative Commons';
$string['version30'] = '3.0';
$string['version40'] = '4.0';
