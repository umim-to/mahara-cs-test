<?php

defined('INTERNAL') || die();  

$string['emailbodyending'] = 'Pro nastavení zasílání upozornění navštivte stránku %s';
$string['emailbodynoreply'] = 'Toto je automaticky zasílané upozornění ze stránek %s.
Následuje přehled všech dnešních upozornění
--------------------------------------------------

';
$string['emailsubject'] = 'Zpráva z %s: Denní přehled';
$string['name'] = 'Souhrnný přehled e-mailem';

