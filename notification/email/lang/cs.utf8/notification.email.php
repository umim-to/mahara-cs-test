<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, David Mudrák, Adam Pátek, Matouš Trča, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2018
 *
 */

defined('INTERNAL') || die();

$string['emailfooter'] = 'Toto je automaticky zasílané upozornění ze stránek %s. Pro nastavení zasílání upozornění navštivte stránku %s';
$string['emailheader'] = 'Máte nové upozornění ze stránek %s s následujícím zněním:';
$string['emailsubject'] = '%s';
$string['name'] = 'E-mailem';
$string['referurl'] = 'Vizte %s';
$string['unsubscribe'] = 'Pro zrušení odběru přejděte na %s';
$string['unsubscribe_watchlist'] = 'Pro odstranění této položky z vašeho seznamu sledovaných, přejděte na %s';
$string['unsubscribe_watchlist_heading'] = 'Zrušit upozorňování na "%s" z vašeho seznamu sledovaných položek.';
$string['unsubscribefailed1'] = 'Váš pokus o zrušení odběru selhal. Buď už jste odběr zrušil nebo bude potřeba toto nastavení provést manuálně. Prosím, přihlašte se a navštivte příslušnou sekci na webu.';
$string['unsubscribesuccess'] = 'Odběr úspěšně zrušen.';
$string['unsubscribetitle'] = 'Zrušit odběr';
