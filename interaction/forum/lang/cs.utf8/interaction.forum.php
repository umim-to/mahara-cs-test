<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['Body'] = 'Tělo zprávy';
$string['Close'] = 'Uzavřít';
$string['Closed'] = 'Uzavřená';
$string['Count'] = 'Počet';
$string['Key'] = 'Klíč';
$string['Moderators'] = 'Moderátoři';
$string['Moveto'] = 'Přesunout na';
$string['Open'] = 'Otevřít';
$string['Order'] = 'Pořadí';
$string['Post'] = 'Poslat';
$string['Poster'] = 'Přispěvatel';
$string['Posts'] = 'Příspěvky';
$string['Re:'] = 'Re:';
$string['Reply'] = 'Odpovědět';
$string['Sticky'] = 'Trvalý';
$string['Subject'] = 'Předmět';
$string['Subscribe'] = 'Odebírat';
$string['Subscribed'] = 'Odebíráno';
$string['Topic'] = 'Téma';
$string['Topics'] = 'Témata';
$string['Unsticky'] = 'Volný';
$string['Unsubscribe'] = 'Neodebírat';
$string['activetopicsdescription'] = 'Naposledy aktualizovaná témata ve vašich skupinách.';
$string['addpostsuccess'] = 'Příspěvek úspěšně přidán';
$string['addtitle'] = 'Přidat fórum';
$string['addtopic'] = 'Přidat téma';
$string['addtopicsuccess'] = 'Téma úspěšně přidáno';
$string['allowunsubscribe'] = 'Povolit uživatelům možnost neodebírat';
$string['allowunsubscribedescription'] = 'Zvolte, zda je uživatelům dovoleno neodebírat novinky z fóra a z témat';
$string['allposts'] = 'Všechny příspěvky';
$string['approvalofposts'] = 'Schválení příspěvků';
$string['autosubscribeusers'] = 'Automaticky nastavovat odebírání';
$string['autosubscribeusersdescription'] = 'Mají členové skupiny automaticky odebírat příspěvky e-mailem?';
$string['awaitingapproval'] = 'Čeká na schválení';
$string['cantaddposttoforum'] = 'Nemáte oprávnění přispívat do tohoto fóra';
$string['cantaddposttotopic'] = 'Nemáte oprávnění přispívat k tomuto tématu';
$string['cantaddtopic'] = 'Nemáte oprávnění přidávat témata do tohoto fóra';
$string['cantapproveposts'] = 'Nemáte oprávnění schvalovat příspěvky na tomto fóru';
$string['cantdeletepost'] = 'Nemáte oprávnění odstraňovat příspěvky z tohoto fóra';
$string['cantdeletethispost'] = 'Nemáte oprávnění odstranit tento příspěvek';
$string['cantdeletetopic'] = 'Nemáte oprávnění odstraňovat témata z tohoto fóra';
$string['canteditpost'] = 'Nemáte oprávnění upravovat tento příspěvek';
$string['cantedittopic'] = 'Nemáte oprávnění upravovat toto téma';
$string['cantfindforum'] = 'Nelze najít fórum id %s';
$string['cantfindpost'] = 'Nelze najít příspěvek id %s';
$string['cantfindtopic'] = 'Nelze najít téma id %s';
$string['cantmakenonobjectionable'] = 'Nemáte oprávnění označit tento příspěvek jako bezproblémový.';
$string['cantunsubscribe'] = 'Nemáte oprávnění neodebírat novinky z tohoto fóra';
$string['cantviewforums'] = 'Nemáte oprávnění nahlížet do diskusních fór v této skupině';
$string['cantviewtopic'] = 'Nemáte oprávnění prohlížet si témata v tomto fóru';
$string['chooseanaction'] = 'Zvolte akci';
$string['clicksetsubject'] = 'Nastavit předmět';
$string['closeddescription'] = 'Na uzavřená témata mohou odpovídat pouze moderátoři a správci skupiny';
$string['closetopics'] = 'Zavřít nová témata';
$string['closetopicsdescription'] = 'Pokud je zaškrtnuto, budou všechna nová témata v tomto fóru uzavřena dle výchozího nastavení. Jen moderátoři a skupina správců může odpovídat na uzavřená témata.';
$string['closetopicsdescription1'] = 'Ve výchozím stavu zavírat všechna nová témata. Jen moderátoři a správci skupiny mohou odpovídat na zavřená témata';
$string['complaint'] = 'Stížnost';
$string['createtopicusersdescription'] = 'Pokud je nastaveno na "Všichni členové skupiny" může kdokoliv zakládat nová témata a odpovídat do založených témat. Je-li nastaveno na "Pouze moderátoři a správci skupiny" mohou nové téma založit pouze moderátoři a správci, ale kdokoliv může přispívat do takto založené diskuse.';
$string['currentmoderators'] = 'Stávající moderátoři';
$string['defaultforumdescription'] = '%s fórum obecné diskuse';
$string['defaultforumtitle'] = 'Obecná diskuse';
$string['deleteforum'] = 'Odstranit fórum';
$string['deletepost'] = 'Odstranit příspěvek';
$string['deletepostsuccess'] = 'Příspěvek úspěšně odstraněn';
$string['deletepostsure'] = 'Jste si skutečně jisti, že to chcete udělat? Tuto akci nelze vrátit zpět.';
$string['deletetopic'] = 'Odstranit téma';
$string['deletetopicspecific'] = 'Odstranit téma "%s"';
$string['deletetopicsuccess'] = 'Téma úspěšně odstraněno';
$string['deletetopicsure'] = 'Jste si skutečně jisti, že to chcete udělat? Tuto akci nelze vrátit zpět.';
$string['deletetopicvariable'] = 'Odstranit téma \'%s\'';
$string['discussiontopics'] = 'Diskusní témata';
$string['editpost'] = 'Upravit příspěvek';
$string['editpostsuccess'] = 'Příspěvek úspěšně odstraněn';
$string['editstothispost'] = 'Úpravy tohoto příspěvku:';
$string['edittitle'] = 'Upravit fórum';
$string['edittopic'] = 'Upravit téma';
$string['edittopicspecific'] = 'Upravit téma "%s"';
$string['edittopicsuccess'] = 'Téma úspěšně upraveno';
$string['forumfailunsubscribe'] = 'Nemáte povolení zrušit odběr.';
$string['forumlower'] = 'fórum';
$string['forumname'] = 'Název fóra';
$string['forumpostattachmentinternal'] = '%s Přílohy: %s';
$string['forumpostattachmenttemplate'] = 'Fórum: %s (%s) ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Přílohy: %s ------------------------------------------------------------------------ Pokud chcete zobrazit tento příspěvek online nebo na něj odpovědět, použíjte tento odkaz: %s Pokud si přejete zrušit tento odběr %s, navštivte: %s';
$string['forumposthtmlattachmenttemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>Fórum: %s (%s)</strong></div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Přílohy:</p> <ul> %s </ul> </div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Odpovědět na tento příspěvek online</a></p> <p><a href="%s">Zrušit tento odběr %s</a></p> </div>';
$string['forumposthtmltemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">
<strong>Fórum: %s od %s</strong>
</div>
<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;>
<p><a href="%s">Odpovědět na tento příspěvek on-line</a></p>
<p><a href="%s">Odhlásit se z %s</a></p></div>';
$string['forumposttemplate'] = '%s od %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Pro zobrazení příspěvku a pro vložení odpovědi přejděte na:
%s

Ke zrušení odebírání emailů z %s navštivte:
%s';
$string['forumsettings'] = 'Nastavení fóra';
$string['forumsuccessfulsubscribe'] = 'Odebírání úspěšně nastaveno';
$string['forumsuccessfulunsubscribe'] = 'Odebírání úspěšně zrušeno';
$string['gotoforums'] = 'Jdi k fórům';
$string['groupadminlist'] = 'Správci skupiny:';
$string['groupadmins'] = 'Správci skupiny';
$string['indentflatindent'] = 'Žádné členění';
$string['indentfullindent'] = 'Plně rozšířit';
$string['indentmaxindent'] = 'Rozšířit na maximum';
$string['indentmode'] = 'Režim členění fóra';
$string['indentmodedescription'] = 'Určete, jak by měla být témata v tomto fóru členěna.';
$string['lastpost'] = 'Poslední příspěvek';
$string['latestforumposts'] = 'Poslední příspěvky ve fóru';
$string['maxindent'] = 'Maximální úroveň členění';
$string['maxindentdescription'] = 'Nastavit maximální úroveň členění pro téma. To platí pouze v případě, že režim členění byl nastaven na volbu Rozšířit na maximum.';
$string['moderatenewposts'] = 'Prověřit nové příspěvky';
$string['moderatenewpostsdescription1'] = 'Nové příspěvky musí být schváleny moderátorem fóra nebo správcem.';
$string['moderatorsandgroupadminsonly'] = 'Pouze moderátoři a správci skupiny';
$string['moderatorsdescription'] = 'Moderátoři mohou upravovat a odstraňovat témata a příspěvky. Mohou rovněž otevírat a uzavírat témata diskuse nebo je nastavovat jako trvale otevřená';
$string['moderatorslist'] = 'Moderátoři:';
$string['name'] = 'Fórum';
$string['nameplural'] = 'Fóra';
$string['newforum'] = 'Nové fórum';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newforumpostnotificationsubjectline'] = '%s';
$string['newpost'] = 'Nový příspěvek';
$string['newtopic'] = 'Nové téma';
$string['noforumpostsyet'] = 'V této skupině zatím nejsou žádné příspěvky';
$string['noforums'] = 'V této skupině nejsou žádná fóra';
$string['notifyadministrator'] = 'Upozornit správce';
$string['notifyauthor'] = 'Upozornit autora';
$string['notopics'] = 'V tomto fóru nejsou žádná témata diskuse';
$string['ntopicslower'] = array(

		0 => '%s téma', 
		1 => '%s témata', 
		2 => '%s témat'
);
$string['objectionablecontentpost'] = 'Nevhodný obsah v tématu fóra "%s", nahlásil "%s"';
$string['objectionablecontentposthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nevhodný obsah v tématu fóra "%s", nahlásil "%s" <br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Obsah nahlášeného příspěvku :<br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Stížnost se vztahuje k: <a href="%s">%s</a></p> <p>Nahlásil: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentposttext'] = 'Nevhodný obsah v tématu fóra"%s" nahlášen %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Obsah nahlášeného příspěvku : %s ------------------------------------------------------------------------ %s ----------------------------------------------------------------------- Pro zobrazení příspěvku klikněte na link: %s K zobrazení profilu uživatele, který příspěvek nahlásil: %s';
$string['objectionablepostdeletedbody'] = '%s zkontroloval příspěvek od %s, který byl nahlášen jako nevhodný, a odstranil jej. Obsah nahlášeného příspěvku byl: %s';
$string['objectionablepostdeletedsubject'] = 'Nevhodný příspěvek v tématu fóra "%s" byl odstraněn %s.';
$string['objectionabletopicdeletedbody'] = '%s zkontroloval příspěvek od %s, který byl nahlášen jako nevhodný, a odstranil jej. Obsah nahlášeného příspěvku byl: %s';
$string['objectionabletopicdeletedsubject'] = 'Nevhodné téma fóra "%s" bylo smazáno %s';
$string['orderdescription'] = 'Zvolte, kde se má toto fórum zobrazit vzhledem k ostatním fórům';
$string['pluginname'] = 'Forum';
$string['postaftertimeout'] = 'Odeslali jste změny po vypršení časového limitu %s minut. Vaše změny nebudou použity.';
$string['postapprovesuccessful'] = 'Příspěvek schválen';
$string['postbyuserwasdeleted'] = 'Příspěvek autora %s byl odstraněn';
$string['postdelay'] = 'Zpoždění příspěvku';
$string['postdelaydescription'] = 'Minimální doba (v minutách), která musí uběhnout, než je nový příspěvek rozeslán účastníkům fóra. Během této doby může autor příspěvku provádět úpravy.';
$string['postedin'] = '%s ve fóru %s';
$string['postneedapprovalbody'] = '%s přidal příspěvek do fóra "%s" a čeká na prověření. Obsah příspěvku je: %s';
$string['postneedapprovalhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nový příspěvek "%s" ve fóru "%s" vyžaduje prověření. <div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Obsah příspěvku je: <br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Autor příspěvku: <a href="%s">%s</a></p> </div>';
$string['postneedapprovalsubject'] = 'Nový příspěvek vyžaduje prověření ve fóru "%s"';
$string['postneedapprovaltext'] = 'Nový příspěvek "%s" ve fóru "%s" vyžaduje prověření. %s ------------------------------------------------------------------------ Obsah příspěvku je: %s ------------------------------------------------------------------------ Přejděte k tomuto příspěvku ve fóru pomocí tohoto odkazu: %s';
$string['postnotapprovederror'] = 'Během snahy označit tento příspěvek jako schválený došlo k chybě';
$string['postnotobjectionable'] = 'Tento příspěvek byl nahlášen jako obsahující nevhodný obsah. Pokud je nahlášení neopodstatněné, můžete kliknout na tlačítko pro odstraněné této zprávy a upozornění ostatních správců.';
$string['postnotobjectionablebody'] = '%s zkontroloval příspěvek od %s a zamítl nahlášení nevhodného obsahu';
$string['postnotobjectionablesubject'] = 'Příspěvek v tématu "%s" byl označen jako bezproblémový obsah %s';
$string['postnotobjectionablesubmit'] = 'Bezproblémový';
$string['postnotobjectionablesuccess'] = 'Příspěvek byl označen jako bezproblémový.';
$string['postobjectionable'] = 'Tento příspěvek byl nahlášen jako nevhodný.';
$string['postreply'] = 'Odpověď na příspěvek';
$string['postsandreplies'] = 'Příspěvky a odpovědi';
$string['postsbyuserweredeleted'] = '%s příspěvků od %s bylo smazáno';
$string['postsvariable'] = 'Příspěvky: %s';
$string['potentialmoderators'] = 'Moderátoři k dispozici';
$string['re'] = 'Re: %s';
$string['reasonempty'] = 'Pole pro zdůvodnění nesmí být prázdné';
$string['regulartopics'] = 'Pravidelná témata';
$string['rejectedpostbody'] = '%s prověřil příspěvek od %s čekající na schválení a smazal jej. Důvod zamítnutí: %s Obsah příspěvku byl: %s';
$string['rejectedposthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Příspěvek ve fóru "%s" zamítnut <br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Obsah zamítnutého příspěvku je: <br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Zamítnutí se vztahuje ke: <a href="%s">%s</a></p> <p>Zamítl: <a href="%s">%s</a></p> </div>';
$string['rejectedpostsubject'] = 'Příspěvek ve fóru "%s" zamítnut';
$string['rejectedposttext'] = 'Příspěvek ve fóru "%s" zamítnut oprávněnou osobou "%s" %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Obsah zamítnutého příspěvku je: %s ------------------------------------------------------------------------ %s ----------------------------------------------------------------------- Příspěvek ve fóru byl smazán, do fóra můžete přejít použitím tohoto odkazu: %s Pro zobrazení profilu pověřené osoby, použijte odkaz: %s';
$string['rejectpost'] = 'Zamítnout příspěvek';
$string['rejectpostsuccess'] = 'Příspěvek byl odstraněn';
$string['replies'] = 'Odpovědi';
$string['replyforumpostnotificationsubject'] = 'Re: %s: %s: %s';
$string['replyforumpostnotificationsubjectline'] = 'Re: %s';
$string['replyto'] = 'Odpověď na:';
$string['reporteddetails'] = 'Podrobnosti nahlášení';
$string['reportedpostdetails'] = '<b>Nahlášeno %s %s:</b><p>%s></p>';
$string['reportobjectionablematerial'] = 'Nahlášení';
$string['reportpost'] = 'Nahlášení příspěvku';
$string['reportpostsuccess'] = 'Příspěvek nahlášen';
$string['sendnow'] = 'Odeslat zprávu nyní';
$string['sendnowdescription'] = 'Odeslat zprávu ihned namísto čekání %s minut k jejímu odeslání';
$string['stickydescription'] = 'Trvalá témata diskuse jsou zobrazena vždy nahoře na stránce';
$string['stickytopics'] = 'Trvalá témata';
$string['strftimerecentfullrelative'] = '%%v, %%k:%%M';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = 'Odebírat příspěvky ve fóru e-mailem';
$string['subscribetotopic'] = 'Odebírat příspěvky v tomto tématu e-mailem';
$string['timeleftnotice'] = 'Zbývá vám %s minut pro dokončení úprav.';
$string['timeleftnotice1'] = 'Zbývá vám <span class="num">%s</span> minut na dokončení úprav.';
$string['timeleftnoticeexpired'] = 'Čas na úpravy vypršel. Vaše změny nebudou uloženy.';
$string['timeleftnoticeexpiredmoderator'] = 'Vaše změny budou nyní uloženy s poznámkou o editaci.';
$string['today'] = 'Dnes';
$string['topicclosedsuccess'] = 'Témata úspěšně uzavřena';
$string['topicisclosed'] = 'Toto téma je uzavřeno. Pouze moderátoři a správci skupiny mohou přidávat nové odpovědi.';
$string['topiclower'] = 'téma';
$string['topicmovedsuccess'] = array(

		0 => 'Téma bylo úspěšně přesunuto.', 
		1 => '%d témata byla úspěšně přesunuta.', 
		2 => '%d témat bylo úspěšně přesunuto.'
);
$string['topicopenedsuccess'] = 'Témata úspěšně otevřena';
$string['topicslower'] = 'témata';
$string['topicstickysuccess'] = 'Témata úspěšně nastavena jako trvalá';
$string['topicsubscribesuccess'] = 'Odebírání témat úspěšně nastaveno';
$string['topicsuccessfulunsubscribe'] = 'Odebírání témat zrušeno';
$string['topicunstickysuccess'] = 'Označení tématu jako trvalého úspěšně zrušeno';
$string['topicunsubscribesuccess'] = 'Odebírání témat úspěšně zrušeno';
$string['topicupdatefailed'] = 'Úprava témat selhala';
$string['typenewpost'] = 'Nový příspěvek ve fóru';
$string['typepostmoderation'] = 'Správa fóra';
$string['typereportpost'] = 'Nevhodný obsah ve fóru';
$string['unsubscribefromforum'] = 'Neodebírat příspěvky z tohoto fóra';
$string['unsubscribefromtopic'] = 'Neodebírat příspěvky z tohoto tématu';
$string['updateselectedtopics'] = 'Upravit vybraná témata';
$string['whocancreatetopics'] = 'Kdo může zakládat nová témata diskuse';
$string['yesterday'] = 'Včera';
$string['youarenotsubscribedtothisforum'] = 'Neodebíráte emaily z tohoto fóra';
$string['youarenotsubscribedtothistopic'] = 'Neodebíráte emaily z tohoto tématu diskuse';
$string['youcannotunsubscribeotherusers'] = 'Nemůžete rušit odebírání jiných uživatelů';
