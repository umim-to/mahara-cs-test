<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrák, Viktor Fuglík, Matouš Trča, Adam Pátek, Veronika Karičáková, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2017
 *
 */

defined('INTERNAL') || die();

$string['badassertion'] = 'Uvedený výraz BrowserID není platný: %s';
$string['badverification'] = 'Mahara neobdržela od ověřovatele BrowserID platný výstup JSON.';
$string['browserid'] = 'Persona';
$string['browseridnotenabled'] = 'Zásuvný modul pro ověřování BrowserID není povolen v žádné aktivní instituci.';
$string['deprecatedmsg1'] = 'K 30. listopadu 2016 přestává Mozilla provozovat ověřování prostřednictvím <a href="https://wiki.mozilla.org/Identity/Persona_Shutdown_Guidelines_for_Reliers">služby Persona</a>. Tento zásuvný modul pomáhá při migraci stávajících účtů Persony na interní autentizaci.';
$string['description'] = 'Ověření pomocí Persony';
$string['emailalreadyclaimed'] = 'E-mailovou adresu %s již využívá jiný uživatelský účet.';
$string['emailclaimedasusername'] = 'E-mailovou adresu %s již využívá jiný uživatelský účet jako své přihlašovací jméno.';
$string['emailnotfound'] = 'Uživatelský účet s e-mailovou adresou \'%s\' nebyl nalezen v žádné z institucí s povoleným BrowserID.';
$string['institutioncolumn'] = 'Instituce';
$string['login'] = 'Přihlášení BrowserID';
$string['migratedesc1'] = 'Automaticky přesune všechny uživatele služby Persona na interní autentizaci v rámci dané instituce a odstraní všechna ověřování pomocí Persony. Uživatelé neobdrží oznámení o svém novém účtu a bude tak potřeba je informovat.';
$string['migratetitle'] = 'Automatická migrace uživatelů Persona';
$string['missingassertion'] = 'BrowserID nevrátil alfanumerický výraz.';
$string['nobrowseridinstances'] = 'Tento web nepoužívá ověřování pomocí služby Persona, a proto nemusí být přijata žádná opatření.';
$string['notusable'] = 'Přerušeno';
$string['numuserscolumn'] = 'Počet aktivních uživatelů Persony';
$string['register'] = 'Registrovat s Persona';
$string['title'] = 'Persona';
