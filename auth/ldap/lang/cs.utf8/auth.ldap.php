<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['attributename'] = 'Název LDAP atributu je použit k synchronizaci skupin na základě jejich hodnot (vyžadováno, je třeba dodržovat velikost písmen)';
$string['cannotconnect'] = 'Nemohu se připojit k žádnému hostiteli LDAP';
$string['cannotdeleteandsuspend'] = 'Nemůžete zároveň použít parametry -d a -s.';
$string['cli_info_sync_groups'] = 'Tento PHP skript pro příkazovou řádku se pokusí synchronizovat seznam skupin instituci s adresářem LDAP. Chybějící skupiny budou vytvořeny a pojmenovány jako \'název instituce\'.';
$string['cli_info_sync_groups_attribute'] = 'Tento PHP skript pro příkazovou řádku se pokusí synchronizovat seznam skupin instituci s adresářem LDAP na základě různých hodnot atributů LDAP. Chybějící skupiny budou vytvořeny a pojmenovány jako \'název instituce : hodnota atributu LDAP\'.';
$string['cli_info_sync_users'] = 'Tento PHP skript pro příkazovou řádku se pokusí synchronizovat seznam institucí v účtech Mahary s adresářem LDAP.';
$string['contexts'] = 'Kontexty';
$string['description'] = 'Ověřovat oproti LDAP serveru';
$string['distinguishedname'] = 'Pod jakým DN se má provést bind';
$string['dodelete'] = 'Odstranit účty, které již nejsou v LDAP';
$string['dosuspend'] = 'Pozastavit účty, které již nejsou v LDAP';
$string['doupdate'] = 'Aktualizovat existující účty z LDAP (to může chvíli trvat)';
$string['dryrun'] = 'Falešné spuštění. Neprovádí žádné databázové operace.';
$string['emailmissing'] = 'Emailová adresa chybí.';
$string['excludelist'] = 'Vyloučit LDAP skupiny, odpovídající těmto regulárním výrazům v jejich jménech';
$string['extrafilterattribute'] = 'Další LDAP filtr pro omezení vyhledávání uživatelů';
$string['grouptype'] = 'Typ skupiny pro založení; výchozí nastavení je "standardní"';
$string['hosturl'] = 'URL hostitele';
$string['includelist'] = 'Zpracuj pouze LDAP skupiny, odpovídající těmto regulárním výrazům v jejich jménech';
$string['institutionname'] = 'Název instituce k vyřízení (vyžadováno)';
$string['ldapconfig'] = 'Konfigurace LDAP';
$string['ldapfieldforemail'] = 'LDAP pole s e-mailem';
$string['ldapfieldforfirstname'] = 'LDAP pole s křestním jménem';
$string['ldapfieldforpreferredname'] = 'LDAP pole se jménem';
$string['ldapfieldforstudentid'] = 'LDAP pole pro ID studenta';
$string['ldapfieldforsurname'] = 'LDAP pole s příjmením';
$string['ldapversion'] = 'Verze LDAP';
$string['loginlink'] = 'Dovolit uživatelům propojit jejich vlastní účet';
$string['nocreate'] = 'Nevytvářet nové účty';
$string['nocreatemissinggroups'] = 'Nevytvářet nové LDAP skupiny, pokud již nejsou nastaveny v instituci.';
$string['nomatchingauths'] = 'Pro tuto instituci nebyl nalezen žádný zásuvný modul LDAP';
$string['notusable'] = 'Nainstalujte PHP rozšíření LDAP';
$string['password'] = 'Heslo';
$string['searchcontexts'] = 'Omezit vyhledávání v těchto kontextech (přepíše hodnoty nastavené v zásuvném modulu pro autentizaci)';
$string['searchsubcontexts'] = 'Prohledávat i subkontexty';
$string['searchsubcontextscliparam'] = 'Hledání (1) nebo ne (0) v subkontextech (přepíše hodnoty nastavené v zásuvném modulu pro autentizaci)';
$string['starttls'] = 'Šifrování TLS';
$string['syncgroupsautocreate'] = 'Automaticky vytvořit chybějící skupiny';
$string['syncgroupsbyclass'] = 'Synchronizovat skupiny uložené jako objekty LDAP';
$string['syncgroupsbyuserfield'] = 'Synchronizovat skupiny uložené jako atributy uživatele';
$string['syncgroupscontexts'] = 'Synchronizovat skupiny pouze z těchto kontextů';
$string['syncgroupscontextsdesc'] = 'Pro výchozí uživatelskou autentizaci v kontextu ponechte prázdné';
$string['syncgroupscron'] = 'Synchronizovat skupiny automaticky pomocí cronu';
$string['syncgroupsexcludelist'] = 'Vyloučit LDAP skupiny s těmito názvy';
$string['syncgroupsgroupattribute'] = 'Atribut skupiny';
$string['syncgroupsgroupclass'] = 'Třída skupiny';
$string['syncgroupsgrouptype'] = 'Typy rolí v automaticky vytvořené skupině';
$string['syncgroupsincludelist'] = 'Zahrnout LDAP skupiny pouze s těmito názvy';
$string['syncgroupsmemberattribute'] = 'Atribut členství ve skupině';
$string['syncgroupsmemberattributeisdn'] = 'Je atribut člena dn?';
$string['syncgroupsnestedgroups'] = 'Synchronizovat vnořenou skupinu';
$string['syncgroupssettings'] = 'Synchronizace skupiny';
$string['syncgroupsuserattribute'] = 'Uživatelský atribut členství ve skupině je uložen v';
$string['syncgroupsusergroupnames'] = 'Pouze tyto názvy skupin';
$string['syncgroupsusergroupnamesdesc'] = 'Ponechte prázdné pro přijetí jakékoliv hodnoty. Jednotlivé názvy skupin oddělte čárkou.';
$string['syncuserscreate'] = 'Automaticky vytvářet uživatele pomocí cronu';
$string['syncuserscron'] = 'Synchronizovat uživatele automaticky přes cron';
$string['syncusersextrafilterattribute'] = 'Další LDAP filtr pro synchronizaci';
$string['syncusersgonefromldap'] = 'Pokud není uživatel dále uveden v LDAP';
$string['syncusersgonefromldapdelete'] = 'Smazat uživatelský účet a veškerý jeho obsah';
$string['syncusersgonefromldapdonothing'] = 'Nedělat nic';
$string['syncusersgonefromldapsuspend'] = 'Pozastavit platnost účtu';
$string['syncuserssettings'] = 'Synchronizace uživatelů';
$string['syncusersupdate'] = 'Aktualizovat informace o účtech pomocí cronu';
$string['title'] = 'LDAP';
$string['updateuserinfoonlogin'] = 'Aktualizovat uživatelské údaje při přihlášení';
$string['updateuserinfoonloginadnote'] = 'Poznámka: Povolením této volby můžete zábránit následnému přihlášování k Mahaře uživatelům a stránkám MS ActiveDirectory';
$string['userattribute'] = 'Uživatelské jméno hledat v atributu';
$string['usertype'] = 'Typ uživatelů';
$string['weautocreateusers'] = 'Automaticky zakládat profily v Mahaře';
