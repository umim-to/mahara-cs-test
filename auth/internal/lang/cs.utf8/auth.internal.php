<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['approvalemailmessagehtml'] = '<p>Dobrý den, %s,</p> <p>děkujeme za žádost o vytvoření účtu v rámci %s. Správce instituce o ní byl vyrozuměn. Jakmile bude vaše žádost zpracována, obdržíte další e-mail.</p> <pre>-- S pozdravem, Tým %s</pre>';
$string['approvalemailmessagetext'] = 'Dobrý den, %s, děkujeme za žádost o vytvoření účtu v rámci %s. Správce instituce o ní byl vyrozuměn. Jakmile bude vaše žádost zpracována, obdržíte další e-mail. -- S pozdravem, Tým %s';
$string['approvalemailsubject'] = 'Žádost o registraci na %s obdržena';
$string['completeregistration'] = 'Dokončit registraci';
$string['confirmcancelregistration'] = 'Jste si jisti, že chcete zrušit tuto registraci? Pokud tak učiníte, váš požadavek bude odstraněn ze systému.';
$string['confirmemailmessagehtml'] = '<p>Dobrý den %s,</p> <p>děkujeme za registraci účtu na %s. Pomocí následujícího odkazu potvrďte svou e-mailovou adresu. Správci instituce bude zasláno oznámení s žádostí o schválení a vy budete informování o výsledku.</p> <p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p> <p>Platnost odkazu vyprší za 24 hodin.</p> <pre> -- S pozdravem, Tým %s</pre>';
$string['confirmemailmessagetext'] = 'Dobrý den %s,</p> <p>děkujeme za registraci účtu na %s. Pomocí následujícího odkazu potvrďte svou e-mailovou adresu. Správci instituce bude zasláno oznámení s žádostí o schválení a vy budete informování o výsledku. %sregister.php?key=%s Platnost odkazu vyprší za 24 hodin. -- S pozdravem, Tým %s';
$string['confirmemailsubject'] = 'Potvrzovací e-mail pro registraci na %s';
$string['description'] = 'Autentizace vůči databázi Mahary';
$string['emailalreadytaken'] = 'Tuto e-mailovou adresu již nějaký uživatel používá';
$string['emailalreadytakenbyothers'] = 'Tuto e-mailovou adresu již používá jiný uživatel.';
$string['emailconfirmedok'] = '<p>Úspěšně jste potvrdili svou e-mailovou adresu. O dalším postupu budete brzy vyrozuměni.</p>';
$string['iagreetothetermsandconditions'] = 'Souhlasím s Podmínkami používání těchto stránek';
$string['internal'] = 'Interní';
$string['passwordformdescription'] = 'Vaše heslo musí být alespoň 6 znaků dlouhé a musí obsahovat alespoň jednu číslici a dvě písmena';
$string['passwordinvalidform'] = 'Vaše heslo musí být alespoň 6 znaků dlouhé a musí obsahovat alespoň jednu číslici a dvě písmena';
$string['passwordinvalidform1'] = 'Vaše heslo musí být minimálně %s znaků dlouhé a musí se lišit od vašeho uživatelského jména. U hesel se rozlišují malá a velká písmena. Vaše heslo musí obsahovat %s.';
$string['pendingregistrationadminemailhtml'] = '<p>Dobrý den %s,</p> <p> nový uživatel žádá o připojení do instituce \'%s\'. </p> <p>Protože jste její správce, je třeba tuto žádost schválit nebo zamítnout. Chcete-li to provést nyní, klikněte na následující odkaz: <a href=\'%s\'>%s</a></p> <p>Tuto žádost je třeba zpracovat do %s.</p> <p>Podrobnosti žádosti o registraci následují:</p> <p>Jméno: %s</p> <p>E-mail: %s</p> <p>Důvod registrace:</p> <p>%s</p> <pre>-- S pozdravem, Tým %s</pre>';
$string['pendingregistrationadminemailsubject'] = 'Nová registrace uživatele pro instituci \'%s\' na %s.';
$string['pendingregistrationadminemailtext'] = 'Dobrý den %s, nový uživatel žádá o připojení do instituce \'%s\'. Protože jste její správce, je třeba tuto žádost schválit nebo zamítnout. Chcete-li to provést nyní, klikněte na následující odkaz: %s Tuto žádost je třeba zpracovat do %s. Podrobnosti žádosti o registraci následují: Jméno: %s E-mail: %s Důvod registrace: %s -- S pozdravem, Tým %s';
$string['recaptcharegisterdesc'] = 'Napište prosím slova, která vidíte v bloku. Dodržujte přitom jejich pořadí a oddělujte je mezerami. Tímto nám pomáháte zamezit automatům zneužít naší službu.';
$string['recaptcharegisterdesc2'] = 'Prosím, pro pokračování zatrhněte pole "Nejsem robot"';
$string['recaptcharegistertitle'] = 'reCAPTCHA obrázek';
$string['registeredemailmessagehtml'] = '<p>Toto je automaticky generovaná zpráva pro: %s</p>

<p>Děkujeme vám za registraci uživatelského účtu na stránkách %s. Pro dokončení registrace navštivte následující stránku:</p>

<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>

<p>Platnost tohoto odkazu vyprší během 24 hodin.</p>

<p>S pozdravem, Tým %s</p>';
$string['registeredemailmessagetext'] = 'Toto je automaticky generovaná zpráva pro: %s

Děkujeme vám za registraci uživatelského účtu na stránkách %s. Pro dokončení registrace navštivte následující stránku:

%sregister.php?key=%s

Platnost tohoto odkazu vyprší během 24 hodin.

S pozdravem, Tým %s';
$string['registeredemailsubject'] = 'Byli jste zaregistrováni na %s';
$string['registeredok'] = '<p>Nyní jste úspěšně zaregistrováni. Další pokyny pro aktivaci vašeho účtu vám byly zaslány e-mailem.</p>';
$string['registeredokawaitingemail'] = 'Úspěšně jste podali přihlášku k registraci. Na vaši adresu byl zaslán e-mail a po jeho potvrzení lze pokračovat dále.';
$string['registeredokawaitingemail2'] = 'Úspěšně jste podali přihlášku k registraci. Správce instituce s tím byl obeznámen a vy obdržíte e-mail, jakmile vaše žádost zpracována.';
$string['registrationcancelledok'] = 'Úspěšně jste zrušili žádost o registraci.';
$string['registrationconfirm'] = 'Potvrdit registraci?';
$string['registrationconfirmdescription'] = 'Registrace musí být schválena správcem instituce.';
$string['registrationdeniedemailsubject'] = 'Pokus o registraci na %s byl odepřen.';
$string['registrationdeniedmessage'] = 'Dobrý den %s, obdrželi jsme vaši žádost pro vstup do naší instituce na %s a rozhodli jsme se vám neudělit přístup. Pokud si myslíte, že toto rozhodnutí není správné, spojte se se námi e-mailem. S pozdravem. Tým %s';
$string['registrationdeniedmessagereason'] = 'Dobrý den %s, obdrželi jsme vaši žádost pro vstup do naší instituce na %s a rozhodli jsme se vám neudělit přístup z následujícího důvodu: %s. Pokud si myslíte, že toto rozhodnutí není správné, spojte se se námi e-mailem. S pozdravem, Tým %s';
$string['registrationexpiredkey'] = 'Omlouváme se, ale platnost vašeho klíče již vypršela. Možný důvod je, že jste registraci nedokončili do 24 hodin. Pokud tomu tak není, chyba může být na naší straně.';
$string['registrationnosuchid'] = 'Omlouváme se, ale tento registrační klíč neexistuje. Možná jste si pomocí tohoto klíče svůj účet již aktivovali.';
$string['registrationnosuchkey'] = 'Je nám líto, ale neevidujeme žádnou žádost o registraci s tímto přístupovým klíčem. Možná že jste čekali s aktivací účtu déle než 24 hodin? Jinak by se mohlo jednat o naší chybu.';
$string['registrationnosuchkey1'] = 'Omlouváme se, ale v databázi nemáme registrační klíč, který je uvedený ve vašem odkazu. Možná ho zkomolil váš e-mailový klient.';
$string['registrationreason'] = 'Důvod registrace';
$string['registrationreasondesc'] = 'Zdůvodnění žádosti o registraci do vámi zvolené instituce spolu s poskytnutím dalších detailů může být užitečné pro její správce, kteří žádost budou zpracovávat. Registraci proto nelze dokončit bez uvedení těchto informací.';
$string['registrationreasondesc1'] = 'Důvod, proč žádáte o registraci ve vámi zvolené instituci a další detaily o tom, proč tak činíte mohou být správci instituce užitečné při posuzování vaší žádosti.';
$string['registrationunsuccessful'] = 'Omlouváme se, ale při pokusu o registraci došlo k chybě na straně našeho serveru. Prosíme, opakujte registraci později.';
$string['title'] = 'Interní';
$string['userdeletiondeniedemailsubject'] = 'Žádost o smazání uživatelského účtu z %s zamítnuta.';
$string['userdeletiondeniedmessage'] = 'Dobrý den %s, obdrželi jsme Vaši žádost o smazání Vašeho uživatelského účtu z %s a rozhodli se Vaše data nevymazat. Pokud si myslíte, že bylo toto rozhodnutí chybné, prosím, kontaktujte mě na e-mailu. S pozdravem, %s';
$string['userdeletiondeniedmessagereason'] = 'Dobrý den %s, obdrželi jsme Vaši žádost o smazání Vašeho uživatelského účtu z %s a rozhodli se Vaše data nevymazat z tohoto důvodu: %s Pokud si myslíte, že bylo toto rozhodnutí chybné, prosím, kontaktujte mě na e-mailu. S pozdravem, %s';
$string['userdeletionemailmessagehtml'] = '<p>Dobrý den %s,</p> <p>Váš účet byl úspěšně smazán z %s.</p> <pre>-- S pozdravem, %s tým</pre>';
$string['userdeletionemailmessagetext'] = 'Dobrý den %s, Váš účet byl úspěšně smazán z %s. -- S pozdravem, %s tým';
$string['userdeletionemailsubject'] = 'Váš uživatelský účet byl smazán z %s.';
$string['userdeletionnosuchid'] = 'Je nám líto, ale tato žádost o smazání neexistuje. Pravděpodobně už byla vyhodnocena?';
$string['usernamealreadytaken'] = 'Toto uživatelské jméno již používá jiný uživatel';
$string['usernamealreadytaken1'] = 'Je nám líto, ale toto uživatelské jméno nemůžete použít. Prosím, vyberte si nové.';
$string['usernameinvalidadminform'] = 'Uživatelská jména mohou obsahovat písmena, čísla a většinu běžných symbolů a jejich délka musí být 3 až 236 znaků. Mezery nejsou povoleny.';
$string['usernameinvalidform'] = 'Uživatelská jména mohou obsahovat pouze písmena, čísla a běžné symboly. Musejí být dlouhá 3 až 30 znaků. Mezery nejsou v uživatelských jménech povoleny.';
$string['youmaynotregisterwithouttandc'] = 'Pokud se chcete registrovat, musíte potvrdit, že se budete řídit <a href="terms.php">Podmínkami používání těchto stránek</a>.';
