<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Viktor Fuglík, Veronika Karičáková, David Mudrák, Adam Pátek, Matouš Trča, Marek Drahovzal, Lukáš Kotek, Jiřina Nováková, Tomáš Jeřábek
 * @copyright  (C) 2008-2021
 *
 */

defined('INTERNAL') || die();

$string['IdPSelection'] = 'Výběr Poskytovatele identit';
$string['attributemapfilenotamap'] = 'Soubor s mapou atributů "%s" neobsahuje mapu atributů.';
$string['attributemapfilenotfound'] = 'Nelze nalézt soubor s mapou atributů nebo do něj nelze zapisovat: %s';
$string['certificate1'] = 'Podepisování a šifrování certifikátu Poskytovatel služeb SAML';
$string['confirmdeleteidp'] = 'Určitě chcete smazat tohoto Poskytovatele identit?';
$string['createnewkeytext'] = 'Vytvořit nový klíč / certifikát';
$string['currentcertificate'] = 'Registrační a šifrovací certifikát poskytovatele služeb SAML';
$string['defaultinstitution'] = 'Výchozí instituce';
$string['deleteoldkeytext'] = 'Odstranit starý certifikát';
$string['description'] = 'Autentizace oproti službě Poskytování identit SAML 2.0';
$string['disco'] = 'Nalézt Poskytovatele identit';
$string['errnosamluser'] = 'Žádný uživatel nenalezen';
$string['errorbadcombo'] = 'Pokud jste si nevybrali vzdálené uživatele, můžete zvolit automatické zakládání nových uživatelů.';
$string['errorbadconfig'] = 'Adresář %s pro konfiguraci SimpleSAMLPHP není správný.';
$string['errorbadinstitution'] = 'Instituce pro připojení uživatele nelze kontaktovat.';
$string['errorbadinstitutioncombo'] = 'Ověřování v rámci instance v kombinaci s tímto atributem instituce a hodnotou instituce již existuje.';
$string['errorbadlib'] = 'Adresář %s pro knihovny SimpleSAMLPHP není správný. Ujistěte se, že instalujete SimpleSAMLphp prostřednictvím "make ssphp" a že je soubor čitelný.';
$string['errorbadmetadata'] = 'Špatně vytvořená metadata SAML. Ujistěte se, že XML obsahuje jednoho platného 
Poskytovatele identit.';
$string['errorbadmetadata1'] = 'Špatně vytvořená metadata SAML. Byly nalezeny následující problémy: %s';
$string['errorbadssphp'] = 'Neplatný popisovač relace SimpleSAMLphp - nesmí být phpsession';
$string['errorbadssphplib'] = 'Neplatná konfigurace knihovny SimpleSAMLphp';
$string['errorbadssphpmetadata'] = 'Neplatná konfigurace SimpleSAMLphp: nenakonfigurovaná metadata pro Poskytovatele identit';
$string['errorbadssphpspentityid'] = 'Neplatné entityId Poskytovatele služeb';
$string['errorduplicateidp1'] = 'Poskytovatel identit "%s" je již institucí "%s" používán. Ujistěte se, že XML obsahuje jednu platnou a jedinečnou identitu poskytovatele.';
$string['errorextrarequiredfield'] = 'Toto pole je nutné vyplnit, když je povolena možnost "Automaticky vytváříme uživatele".';
$string['errormissingmetadata'] = 'Rozhodli jste se přidat nová metadata Poskytovatele identit, ale žádná nebyla poskytnuta.';
$string['errormissinguserattributes'] = 'Zdá se, že jste byl autentizován, ale dosud nejsme neobdrželi požadované uživatelské atributy. Zkontrolujte si prosím, jestli váš poskytovatel identity uvolnil SSO pole pro křestní jméno, příjmení a e-mail a poskytovatele služeb Mahara je spušen nebo kontaktujte správce tohoto serveru.';
$string['errormissinguserattributes1'] = 'Zdáte se být autentizováni, ale dosud jsme neobdrželi potřebné uživatelské atributy. Zkontrolujte, zda váš Poskytovatel identit v rámci SSO pro %s poskytuje pole pro jméno, příjmení a e-mailovou adresu, nebo informujte správce.';
$string['errornomcrypt'] = 'PHP knihovna "mcrypt" musí být nainstalována pro auth/saml. Ujistěte se, že jste nainstaovali a aktivovali mcrypt, např.:<br>sudo apt-get install php5-mcrypt<br>sudo php5enmod mcrypt<br> a že jste restartovali webový server.';
$string['errornomcrypt7php'] = 'PHP knihovna "mcrypt" musí být nainstalována pro auth/saml. Ujistěte se, že jste nainstaovali a aktivovali mcrypt, např.:<br>sudo apt-get install php7.0-mcrypt<br>sudo phpenmod mcrypt<br> a že jste restartovali webový server.';
$string['errornomemcache'] = 'PHP knihovna "memcache" musí být nainstalována pro auth/saml.  Buď vyplňte cesty k memcache serverům s využitím proměnné $cfg>memcacheservers nebo nainstalujte memcache lokálně, např.:<br>sudo apt-get install php5-memcache<br>sudo php5enmod memcache<br> a následně restartujte webový server.';
$string['errornomemcache7php'] = 'PHP knihovna "memcache" musí být nainstalována pro auth/saml.  Buď vyplňte cesty k memcache serverům s využitím proměnné $cfg>memcacheservers nebo nainstalujte memcache lokálně, např.:<br>sudo apt-get install php-memcache<br>sudo phpenmod memcache<br> a následně restartujte webový server.';
$string['errornovalidsessionhandler'] = 'Manipulant SimpleSAMLphp sezení je špatně nakonfigurován nebo je dočasně nedostupný server.';
$string['errorregistrationenabledwithautocreate'] = 'Instituce má povolené registrace. Z důvodu zabezpečení je ale zakázáno automatické vytváření uživatelských účtů.';
$string['errorregistrationenabledwithautocreate1'] = 'Instituce umožňuje registraci. Z bezpečnostních důvodů to neplatí pro automatické zakládání nových uživatelů, pokud používáte vzdálená uživatelská jména.';
$string['errorremoteuser'] = 'Vazba na remoteuser je povinná, pokud je usersuniquebyusername vypnutý';
$string['errorremoteuser1'] = 'Shoda na "remoteuser" je povinná, pokud je "usersuniquebyusername" vypnutý.';
$string['errorretryexceeded'] = 'Maximální počet pokusů překročen (%s) - zřejmě je problém na straně služby poskytující identitu';
$string['errorssphpsetup'] = 'SAML není nastaven správně. Jako první je třeba spustit příkaz "make ssphp" z příkazové řádky.';
$string['errorupdatelib'] = 'Vaše aktuální verze knihovny SimpleSAMLPHP je zastaralá. Spusťte "make cleanssphp && make ssphp"';
$string['identityprovider'] = 'Poskytovatel identit';
$string['idpentityadded'] = 'Přidána metadata Poskytovatele identit pro tuto SAML instanci.';
$string['idpentityid'] = 'Entita Poskytovatele identit';
$string['idpentityupdated'] = 'Aktualizována metadata Poskytovatele identit pro tuto SAML instanci.';
$string['idpentityupdatedduplicates'] = array(

		0 => 'Aktualizována metadata Poskytovatele identit pro tuto a jednu další SAML instanci.', 
		1 => 'Aktualizována metadata Poskytovatele identit pro tuto a %s další SAML instance.', 
		2 => 'Aktualizována metadata Poskytovatele identit pro tuto a %s dalších SAML instancí.'
);
$string['idpprovider'] = 'Poskytovatel';
$string['idptable'] = 'Nainstalovaní Poskytovatelé identit';
$string['institutionattribute'] = 'Atribut instituce (obsahuje "%s")';
$string['institutionidp'] = 'SAML metadata Institucionálního Poskytovatel identit';
$string['institutionidpentity'] = 'Dostupní Poskytovatelé indentit';
$string['institutionregex'] = 'Ztotožněte dílčí řetězec se zkráceným jménem instituce';
$string['institutions'] = 'Instituce';
$string['institutionvalue'] = 'Hodnota instituce pro kontrolu vůči atributu';
$string['keypass'] = 'Heslo k privátnímu klíči';
$string['keypassdesc'] = 'Heslo k ochraně privátního klíče';
$string['keyrollfailed'] = 'Nepodařilo se odstranit starý klíč / certifikát';
$string['libchecks'] = 'Kontrola instalace potřebných knihoven: %s';
$string['link'] = 'Propojit účty';
$string['linkaccounts'] = 'Chcete propojit vzdálený účet %s s místním účtem %s?';
$string['login'] = 'Přihlášení SSO';
$string['loginlink'] = 'Povolit uživatelům odkazovat na vlastní účet';
$string['logintolink'] = 'Propojit místní účet %s se vzdáleným účtem.';
$string['logintolinkdesc'] = '<p><b>V tuto chvíli jste připojeni se vzdáleným uživatelem %s. Přihlašte se prosím se svým místním účtem pro jejich propojení nebo se zaregistrujte, pokud dosud na %s nemáte účet</b></p>';
$string['logo'] = 'Logo';
$string['manage_certificate1'] = 'Toto je certifkát vytvožený jako součást <a href="%s">metadat</a> Poskytovatele služeb SAML.';
$string['manage_certificate2'] = 'Toto je certifikát generovaný jako součást poskytovatele služeb SAML';
$string['manage_new_certificate'] = 'Toto je nový certifikát generovaný jako součást poskytovatele služeb SAML.<br>Nyní bude platný nový i starý certifikát. Až upozorníte všechny poskytovatele identity na váš nový certifikát, můžete staré certifikáty odstranit pomocí tlačítka "Odstranit starý certifikát".';
$string['metadatarefreshfailed_body'] = 'Automatická aktualizace SAML metadat nemohla být dokončena. Zkontrolujte log cronu pro více informací. Chybové hlášení se může objevit níže:';
$string['metadatarefreshfailed_subject'] = 'Automatická aktualizace metadat selhala';
$string['metadatavewlink'] = '<a href="%s">Zobrazit metadata</a>';
$string['metarefresh_metadata_signature'] = 'Metadata validačního podpisu';
$string['metarefresh_metadata_url'] = 'Metadata URL pro automatickou aktualizaci';
$string['missingnamespace'] = 'XML očekává, že bude jmenný prostor (namespace) “%s” definovaný ve značce EntityDescriptor';
$string['newcertificate'] = 'Nový registrační a šifrovací certifikát poskytovatele služeb SAML';
$string['newidpentity'] = 'Přidat nového Poskytovatele identit';
$string['newkeycreated'] = 'Nový klíč / certifikát vytvořen';
$string['newkeypass'] = 'Heslo k novému privátnímu klíči';
$string['newkeypassdesc'] = 'Heslo k ochraně nového privátního klíče, pokud chcete, aby bylo odlišné od toho stávajícího';
$string['newpublickey'] = 'Nový veřejný klíč';
$string['noentityidpfound'] = 'Nenalezeno žádné ID poskytovatele identit';
$string['noentityidpneednamespace'] = 'Vyžaduje vaše značka XML EntityDescriptor, aby bylo “xmlns=” definováno?';
$string['noidpsfound'] = 'Žádný poskytovatel identit nenalezen';
$string['notusable'] = 'Nainstalujte prosím knihovny Poskytovatele služeb SimpleSAMLPHP';
$string['novalidauthinstanceprovided'] = 'Váš výběr není možný. Prosím, vyberte jinou instituci.';
$string['nullprivatecert'] = 'Nelze vytvořit nebo uložit privátní klíč';
$string['nullpubliccert'] = 'Nelze vytvořit nebo uložit veřejný klíč';
$string['obsoletesamlinstance'] = 'Ověřování v rámci instance SAML <a href="%s">% s </a> pro instituci "%s" je třeba aktualizovat.';
$string['obsoletesamlplugin'] = 'Zásuvný modul auth/saml je třeba překonfigurovat. Aktualizujte prosím zásuvný modul prostřednictvím<a href="%s">příslušného formuláře</a>.';
$string['oldcertificate'] = 'Starý registrační a šifrovací certifikát poskytovatele služeb SAML';
$string['oldkeydeleted'] = 'Starý klíč / certifikát smazán';
$string['populaterolestoallsaml'] = 'Zkopírovat role do všech instancí SAML';
$string['populaterolestoallsamldescription'] = 'Pokud je tento přepínač povolen, hodnoty pro všechna pole "Role" se zkopírují do všech ostatních instancí v rámci ověřování SAML, které při odeslání tohoto formuláře používají stejného Poskytovatele identity. Toto pole se poté nastaví na hodnotu "Ne".';
$string['reallyreallysure1'] = 'Snažíte se uložit metadata Poskytovatele služeb pro Maharu. Tento krok nelze vrátit zpět. Stávající SAML přihlášení nebude fungovat, dokud nebudete znovu sdílet vaše nová metadata se všemi Poskytovateli identit.';
$string['remoteuser'] = 'Ztotožněte atribut uživatelského jména se vzdáleným uživatelským jménem';
$string['reset'] = 'Resetovat metadata';
$string['resetmetadata'] = 'Resetovat všechny certifikáty v rámci metadat Mahary. Tento krok nelze vrátit zpět a zároveň bude zapotřebí znovu sdílet vaše metadata s Poskytovatelem identit.';
$string['samlconfig'] = 'Konfigurace SAML';
$string['samlfieldauthloginmsg'] = 'Zpráva o chybě při přihlašování';
$string['samlfieldforautogroups'] = 'Mapování rolí pro \'Automatickou správu skupin\'';
$string['samlfieldforautogroupsall'] = 'Automatická správa skupin pro všechny skupiny na těchto webových stránkách';
$string['samlfieldforautogroupsalldescription'] = 'Pokud je povoleno, osoba s rolí \'Automatická správa skupin\' bude přidána jako správce skupiny ve všech skupinách na stránkách. Jinak bude přidána jako správce skupiny pouze do skupin v rámci své instituce.';
$string['samlfieldforavatar'] = 'SSO pole pro ikonu avataru';
$string['samlfieldforavatardescription'] = 'Poskytnutý avatar musí být obrázek kódovaný jako textový řetězec ve formátu base64';
$string['samlfieldforemail'] = 'SSO pole pro e-mail';
$string['samlfieldforemailaffiliations'] = 'SSO pole pro afiliační emaily';
$string['samlfieldforfirstname'] = 'SSO pole pro jméno';
$string['samlfieldforidaffiliations'] = 'SSO pole pro afiliační ID';
$string['samlfieldfororganisationname'] = 'SSO pole pro \'Organizaci\'';
$string['samlfieldforrole'] = 'SSO pole pro role';
$string['samlfieldforroleaffiliationdelimiter'] = 'Znak oddělovače rolí';
$string['samlfieldforroleaffiliations'] = 'SSO pole pro afiliační role';
$string['samlfieldforroleinstadmin'] = 'Mapování rolí pro \'Správce instituce\'';
$string['samlfieldforroleinststaff'] = 'Mapování rolí pro \'Zaměstnance instituce\'';
$string['samlfieldforroleinstsupportadmin'] = 'Mapování rolí pro \'Pomocného správce instituce\'';
$string['samlfieldforroleprefix'] = 'SSO pole pro předponu role';
$string['samlfieldforrolesiteadmin'] = 'Mapování rolí pro \'Správce stránek\'';
$string['samlfieldforrolesitestaff'] = 'Mapování rolí pro \'Zaměstnance stránek\'';
$string['samlfieldforstudentid'] = 'SSO pole pro studentské ID';
$string['samlfieldforsurname'] = 'SSO pole pro příjmení';
$string['samlneedtoremovephar'] = 'Nebylo možné odstranit soubor %s. Prosím, smažte daný soubor a pak spusťte "make ssphp" znovu.';
$string['selectidp'] = 'Vyberte prosím Poskytovatele identit, s jehož pomocí se chcete přihlásit.';
$string['selectmigrateto'] = 'Vyberte instituci k přesunu...';
$string['sha1'] = 'Starší SHA1 (nebezpečný)';
$string['sha256'] = 'SHA256 (výchozí)';
$string['sha384'] = 'SHA384';
$string['sha512'] = 'SHA512';
$string['sigalgo'] = 'Podpisový algoritmus';
$string['simplesamlphpconfig'] = 'Adresář pro konfiguraci SimpleSAMLPHP';
$string['simplesamlphplib'] = 'Adresář pro knihovny SimpleSAMLPHP';
$string['spentityid'] = 'entityId Poskytovatele služeb';
$string['spmetadata'] = 'Metadata poskytovatele služeb';
$string['ssolabelfor'] = '%s přihlášení';
$string['ssphpnotconfigured'] = 'SimpleSAMLPHP není nastaven';
$string['title'] = 'SAML';
$string['updateuserinfoonlogin'] = 'Aktualizovat uživatelské údaje při přihlášení';
$string['userattribute'] = 'Atribut uživatele';
$string['weautocreateusers'] = 'Automaticky zakládáme nové uživatele';
